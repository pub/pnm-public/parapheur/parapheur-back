const pastaConfig = require("@pasta/lint/back");

module.exports = {
  ...pastaConfig,
  overrides: [
    {
      files: ["**/*.spec.{j,t}s"],
      env: {
        jest: true,
      },
    },
  ],
};
