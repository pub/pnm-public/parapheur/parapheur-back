import { Injectable, PipeTransform } from "@nestjs/common";
import { LetterFile } from "../entities/letter-file.entity";
import { LetterFileService } from "../services/letter-file.service";

export type NewLetterFile = { chronoNumber: LetterFile["chronoNumber"] };

@Injectable()
export class SetChronoPipe implements PipeTransform<NewLetterFile> {
  constructor(private readonly letterFileService: LetterFileService) {}

  async transform(value: any): Promise<NewLetterFile> {
    return {
      ...value,
      chronoNumber: await this.letterFileService.getNextChronoNumber(),
    };
  }
}
