import { Injectable, PipeTransform } from "@nestjs/common";
import { isArray } from "lodash";
/**
 * Use this pipe to wrap a single id or an array of ids into an object with an id property.
 *
 * Useful when you want to use a DTO with a relation to another entity.
 *
 * @param fieldName The name of the field to wrap
 */
@Injectable()
export class WrapIdPipe implements PipeTransform {
  constructor(private readonly fieldName: string) {}

  /**
   * Shortcut to create a new instance of the pipe
   *
   * @param fieldName The name of the field to wrap
   * @returns A new instance of the pipe
   */
  static for(fieldName: string): WrapIdPipe {
    return new WrapIdPipe(fieldName);
  }

  async transform(value: any): Promise<any> {
    if (!value[this.fieldName]) {
      return value;
    }

    if (isArray(value[this.fieldName])) {
      return {
        ...value,
        [this.fieldName]: value[this.fieldName].map((id: number) => ({ id })),
      };
    }

    return {
      ...value,
      [this.fieldName]: { id: value[this.fieldName] },
    };
  }
}
