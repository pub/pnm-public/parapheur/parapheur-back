import { getRepositoryToken } from "@nestjs/typeorm";
import { AbstractNestCommand, Command } from "@pasta/cli";
import { Argv } from "yargs";
import { BannerType, InformationBanner } from "../entities/InformationBanner.entity";
import { Repository } from "typeorm";
import { InformationBannerService } from "../services/InformationBanner.service";

@Command
export class InformationBannerCli extends AbstractNestCommand {
  name = "information-banner";
  description = "add an active message or remove one";

  arguments(yargs: Argv<{}>) {
    return yargs
      .option("a", {
        alias: "add",
        boolean: true,
        describe: "add a new active message, removes any already active message",
      })
      .option("m", { alias: "message", string: true, describe: "the message" })
      .option("l", { alias: "link", string: true, demandOption: false, describe: "an url" })
      .option("t", { alias: "linkText", url: true, demandOption: false, describe: "the link text" })
      .option("T", {
        alias: "type",
        url: true,
        demandOption: false,
        default: BannerType.info,
        choices: Object.values(BannerType),
        describe: "the banner type",
      })
      .option("r", { alias: "remove", boolean: true, describe: "removes currently active message" })
      .check(argv => {
        if (!argv.a && !argv.r) {
          throw new Error("You must provide either -a or -r option");
        }
        if (argv.a && argv.r) {
          throw new Error("You must provide either -a OR -r option, not both");
        }
        if (argv.a && !argv.m) {
          throw new Error("You must provide a message");
        }
        if ((argv.l && !argv.t) || (!argv.l && argv.t)) {
          throw new Error("You must provide a link text with a link and vice versa");
        }
        return true;
      });
  }

  async main(argv: any): Promise<any> {
    // const repo: Repository<InformationBanner> = this.nest.get(getRepositoryToken(InformationBanner));
    const service = this.nest.get(InformationBannerService);

    await this.removeActive(service);

    if (argv.m) {
      const body: Partial<InformationBanner> = { message: argv.m, type: argv.T };
      if (argv.l && argv.t) {
        body.link = argv.l;
        body.linkText = argv.t;
      }
      const { id } = await service.createOne({}, body);
      this.print(`Added active information banner ${id}`);
    }

    this.print("done");
  }

  private async removeActive(service: InformationBannerService) {
    const active = await service.getActive();
    if (active) {
      await service.deleteOne({}, active.id);
      this.print(`deleted active information banner ${active.id}`);
    }
  }
}
