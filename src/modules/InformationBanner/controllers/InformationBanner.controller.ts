import { Controller, Get } from "@nestjs/common";
import { AbstractPastaCrudController, PastaCrud, PastaCrudOperationType } from "@pasta/back-core";
import { InformationBanner } from "../entities/InformationBanner.entity";
import { InformationBannerService } from "../services/InformationBanner.service";
import { CRUDAction, NeedAbility, Public } from "@pasta/back-auth";
import { CreateInformationBannerDto, UpdateInformationBannerDto } from "../dtos/InformationBanner.dto";

@PastaCrud({
  model: InformationBanner,
  dto: { create: CreateInformationBannerDto, update: UpdateInformationBannerDto },
  routes: {
    createOne: { decorators: [NeedAbility({ action: CRUDAction.CREATE, subject: InformationBanner })] },
    updateOne: { decorators: [NeedAbility({ action: CRUDAction.UPDATE, subject: InformationBanner })] },
    deleteOne: { decorators: [NeedAbility({ action: CRUDAction.DELETE, subject: InformationBanner })] },
    getMany: { decorators: [NeedAbility({ action: CRUDAction.READ, subject: InformationBanner })] },
    getOne: { decorators: [NeedAbility({ action: CRUDAction.READ, subject: InformationBanner })] },
  },
})
@Controller("/information-banner")
export class InformationBannerController implements AbstractPastaCrudController<InformationBanner> {
  constructor(public readonly service: InformationBannerService) {}

  @Get("/active")
  @Public()
  @PastaCrudOperationType("readOne")
  async getActive() {
    return this.service.getActive();
  }
}
