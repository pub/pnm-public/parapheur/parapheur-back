import { Module } from "@nestjs/common";
import { PastaAppModule, PastaCrudModule, PastaDbModule } from "@pasta/back-core";
import { InformationBannerController } from "./controllers/InformationBanner.controller";
import { InformationBannerService } from "./services/InformationBanner.service";
import { InformationBanner } from "./entities/InformationBanner.entity";

@Module({
  imports: [PastaDbModule.forFeature(InformationBanner), PastaCrudModule],
  controllers: [InformationBannerController],
  providers: [InformationBannerService],
})
export class InformationBannerModule extends PastaAppModule {}
