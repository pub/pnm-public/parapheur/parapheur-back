import { AbstractPastaCrudService, IPastaCrudQuery } from "@pasta/back-core";
import { InformationBanner } from "../entities/InformationBanner.entity";
import { IsNull, Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";

export class InformationBannerService extends AbstractPastaCrudService<InformationBanner> {
  constructor(@InjectRepository(InformationBanner) public readonly repo: Repository<InformationBanner>) {
    super();
  }

  async deleteOne(_query: IPastaCrudQuery, id: number): Promise<any> {
    return this.repo.softDelete(id);
  }

  async getActive() {
    return this.repo.findOne({ order: { id: "DESC" }, where: { deletedAt: IsNull() } });
  }
}
