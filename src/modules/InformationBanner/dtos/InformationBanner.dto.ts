import { WritableDto } from "@pasta/back-core";
import { IsEnum, IsOptional, IsString, IsUrl } from "class-validator";
import { BannerType, InformationBanner } from "../entities/InformationBanner.entity";

export class CreateInformationBannerDto implements WritableDto<InformationBanner, "type" | "link" | "linkText"> {
  @IsString()
  message: string;

  @IsEnum(BannerType)
  @IsOptional()
  type?: BannerType;

  @IsOptional()
  @IsUrl()
  link?: string;

  @IsOptional()
  @IsString()
  linkText?: string;
}

export class UpdateInformationBannerDto {
  @IsOptional()
  @IsString()
  message?: string;

  @IsEnum(BannerType)
  @IsOptional()
  type?: BannerType;

  @IsOptional()
  @IsUrl()
  link?: string;

  @IsOptional()
  @IsString()
  linkText?: string;
}
