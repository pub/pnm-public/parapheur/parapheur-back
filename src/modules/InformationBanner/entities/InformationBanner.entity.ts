import { PastaBaseEntity } from "@pasta/back-core";
import { Column, DeleteDateColumn, Entity } from "typeorm";

export enum BannerType {
  info = "info",
  error = "error",
  warning = "warning",
}

@Entity()
export class InformationBanner extends PastaBaseEntity {
  @DeleteDateColumn()
  deletedAt?: Date;

  @Column({ default: BannerType.info })
  type: BannerType;

  @Column({ type: "text" })
  message: string;

  @Column({ nullable: true })
  link: string;

  @Column({ nullable: true })
  linkText: string;
}
