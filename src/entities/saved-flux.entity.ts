import { User } from "@pasta/back-auth";
import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne } from "typeorm";

@Entity()
export class LetterFileSavedFlux extends PastaBaseEntity {
  @Column()
  name: string;

  @Column({ type: "simple-json" })
  flux: { name: string; mail: string }[];

  @ManyToOne(() => User, { onDelete: "CASCADE" })
  author: User;
}
