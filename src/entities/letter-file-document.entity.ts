import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne, OneToMany } from "typeorm";
import { LetterFileFolder } from "./letter-file-folder.entity";
import { DocumentVersion } from "./DocumentVersion.entity";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";
import { SignaturePosition } from "./SignaturePosition.entity";

@Entity()
export class LetterFileDocument extends PastaBaseEntity {
  @Column({ default: LetterFileDocumentType.DEFAULT })
  type: LetterFileDocumentType;

  @Column()
  name: string;

  @Column({ nullable: true })
  extension: string;

  @Column({ nullable: true })
  storageKey: string;

  @Column({ nullable: true })
  url: string;

  @Column()
  position: number;

  @Column({ nullable: true })
  mimeType: string;

  @Column({ nullable: true })
  nbPages: number;

  @Column({ nullable: true })
  signed: boolean;

  @ManyToOne(() => LetterFileFolder, letterFileFolder => letterFileFolder.documents, { onDelete: "CASCADE" })
  folder: LetterFileFolder;

  @OneToMany(() => DocumentVersion, signAction => signAction.document)
  documentVersions: DocumentVersion[];

  @OneToMany(() => SignaturePosition, signaturePosition => signaturePosition.document)
  signaturePositions: SignaturePosition[];

  @Column({ default: 0 })
  signaturePositionsCount: number;
}
