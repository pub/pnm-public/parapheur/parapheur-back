import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne } from "typeorm";
import { LetterFileDocument } from "./letter-file-document.entity";
import { plainToInstance } from "class-transformer";

@Entity()
export class DocumentVersion extends PastaBaseEntity {
  @Column()
  author: string;

  @ManyToOne(() => LetterFileDocument, letterFileDocument => letterFileDocument.documentVersions, {
    onDelete: "CASCADE",
  })
  document: LetterFileDocument;

  @Column()
  storageKey: string;

  @Column({ nullable: true })
  extension: string;

  static fromPlain(obj: Partial<DocumentVersion>): DocumentVersion {
    return plainToInstance(DocumentVersion, obj);
  }
}
