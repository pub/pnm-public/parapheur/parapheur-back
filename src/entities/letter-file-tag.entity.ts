import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToMany, Unique } from "typeorm";
import { LetterFile } from "./letter-file.entity";

@Entity()
@Unique(["name"])
export class LetterFileTag extends PastaBaseEntity {
  @Column()
  name: string;

  @ManyToMany(() => LetterFile, letterFile => letterFile.tags, { onDelete: "CASCADE" })
  letterFiles: LetterFile[];
}
