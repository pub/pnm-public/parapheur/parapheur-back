import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne } from "typeorm";
import { LetterFileSearchDto } from "../dtos/letter-file.dto";
import { User } from "@pasta/back-auth";

export enum ExportStatus {
  TODO = "TODO",
  IN_PROGRESS = "IN_PROGRESS",
  COMPLETED = "COMPLETED",
  ERROR = "ERROR",
}

@Entity()
export class LetterFileExport extends PastaBaseEntity {
  @Column({ type: "simple-json" })
  searchQuery: LetterFileSearchDto;

  @Column({ nullable: true })
  fileKey: string;

  @Column({ nullable: true })
  withDocuments: boolean;

  @Column({ nullable: true })
  hits: number;

  @Column({ default: ExportStatus.TODO })
  status: ExportStatus;

  @ManyToOne(() => User, { onDelete: "SET NULL" })
  author: User;
}
