import { User } from "@pasta/back-auth";
import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne, Unique } from "typeorm";

export enum LetterFileSuggestableField {
  senderOrganization = "senderOrganization",
  senderName = "senderName",
  senderRole = "senderRole",
  mailType = "mailType",
  recipientName = "recipientName",
  manager = "manager",
  externalMailNumber = "externalMailNumber",
}

@Entity()
@Unique(["user", "fieldType"])
export class Suggestion extends PastaBaseEntity {
  @ManyToOne(() => User, { onDelete: "CASCADE" })
  user: User;

  @Column()
  fieldType: LetterFileSuggestableField;

  @Column({ type: "simple-array" })
  values: string[];
}
