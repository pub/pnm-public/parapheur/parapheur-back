import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne } from "typeorm";
import { LetterFileDocument } from "./letter-file-document.entity";

@Entity()
export class SignaturePosition extends PastaBaseEntity {
  @Column("float")
  x: number;

  @Column("float")
  y: number;

  @Column("float")
  width: number;

  @Column("float")
  height: number;

  @Column()
  page: number;

  @ManyToOne(() => LetterFileDocument, letterFileDocument => letterFileDocument.signaturePositions, {
    onDelete: "CASCADE",
  })
  document: LetterFileDocument;
}
