import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne } from "typeorm";
import { LetterFile } from "./letter-file.entity";

export enum HistoryOperationType {
  "CREATE" = "CREATE",
  "UPDATE" = "UPDATE",
  "DELETE" = "DELETE",
  "DOWNLOAD" = "DOWNLOAD",
}

export enum HistoryTarget {
  "DOC" = "DOC",
  "FOLDER" = "FOLDER",
  "FLUX" = "FLUX",
  "BASE-INFO" = "BASE-INFO",
  "EXTERNAL-INFO" = "EXTERNAL-INFO",
  "DEFAULT" = "DEFAULT",
  "CCUSER" = "CCUSER",
  "COMMENT" = "COMMENT",
  "SIGNATURE_POSITION" = "SIGNATURE_POSITION",
}

@Entity()
export class LetterFileHistory extends PastaBaseEntity {
  @Column()
  label: string;

  @Column()
  type: HistoryOperationType;

  @Column()
  target: HistoryTarget;

  @Column()
  author: string;

  @ManyToOne(() => LetterFile, letterFile => letterFile.history, { onDelete: "CASCADE" })
  letterFile: LetterFile;
}
