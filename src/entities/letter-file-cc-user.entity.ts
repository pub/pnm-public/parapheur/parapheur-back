import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne, Unique } from "typeorm";
import { LetterFile } from "./letter-file.entity";
import { User } from "@pasta/back-auth";

@Entity()
@Unique(["mail", "letterFile"])
export class LetterFileCCUser extends PastaBaseEntity {
  @Column()
  mail: string;

  @Column()
  name: string;

  @Column({ default: false })
  frozen: boolean;

  @ManyToOne(() => User)
  author: User;

  @ManyToOne(() => LetterFile, letterFile => letterFile.ccUsers, { onDelete: "CASCADE" })
  letterFile: LetterFile;
}
