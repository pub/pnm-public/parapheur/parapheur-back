import { PastaBaseEntity } from "@pasta/back-core";
import { Column, DeleteDateColumn, Entity, JoinTable, ManyToMany, OneToMany } from "typeorm";
import { UpstreamFluxElement } from "./upstream-flux-element.entity";
import { LetterFileCCUser } from "./letter-file-cc-user.entity";
import { LetterFileComment } from "./letter-file-comment.entity";
import { LetterFileFolder } from "./letter-file-folder.entity";
import { LetterFileTag } from "./letter-file-tag.entity";
import { DownstreamFluxElement } from "./downstream-flux-element.entity";
import { LetterFileHistory } from "./letter-file-history.entity";

@Entity()
export class LetterFile extends PastaBaseEntity {
  @Column({ default: "" })
  name: string;

  @Column({ unique: true })
  chronoNumber: string;

  @Column({ nullable: true })
  deadline?: Date;

  @Column({ nullable: true })
  arrivalDate?: Date;

  @Column({ nullable: true })
  externalMailNumber?: string;

  @Column({ default: true })
  draft: boolean;

  @DeleteDateColumn()
  deletedAt?: Date;

  @Column({ default: false })
  archived: boolean;

  @Column({ nullable: true })
  unArchivedDate?: Date;

  @Column({ nullable: true })
  senderOrganization?: string;

  @Column({ nullable: true })
  senderName?: string;

  @Column({ nullable: true })
  senderRole?: string;

  @Column({ nullable: true })
  mailType?: string;

  @Column({ nullable: true })
  recipientName?: string;

  @Column({ nullable: true })
  manager?: string; // "Responsable du parapheur"

  @JoinTable()
  @ManyToMany(() => LetterFileTag, tag => tag.letterFiles)
  tags: LetterFileTag[];

  @OneToMany(() => UpstreamFluxElement, element => element.letterFile)
  upstream: UpstreamFluxElement[];

  @OneToMany(() => DownstreamFluxElement, element => element.letterFile)
  downstream: DownstreamFluxElement[];

  @OneToMany(() => LetterFileCCUser, ccUser => ccUser.letterFile)
  ccUsers: LetterFileCCUser[];

  @OneToMany(() => LetterFileComment, comment => comment.letterFile)
  comments: LetterFileComment[];

  @OneToMany(() => LetterFileFolder, folder => folder.letterFile)
  folders: LetterFileFolder[];

  @OneToMany(() => LetterFileHistory, history => history.letterFile)
  history: LetterFileHistory[];
}
