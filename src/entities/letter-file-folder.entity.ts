import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne, OneToMany, Unique } from "typeorm";
import { LetterFileDocument } from "./letter-file-document.entity";
import { LetterFile } from "./letter-file.entity";

@Entity()
@Unique(["letterFile", "name"])
export class LetterFileFolder extends PastaBaseEntity {
  @Column()
  name: string;

  // Position should be unique per letter file but as we need to update it when moving folders
  // we can't use a unique constraint
  @Column()
  position: number;

  @ManyToOne(() => LetterFile, letterFile => letterFile.folders, { onDelete: "CASCADE" })
  letterFile: LetterFile;

  @OneToMany(() => LetterFileDocument, letterFileDocument => letterFileDocument.folder)
  documents: LetterFileDocument[];
}
