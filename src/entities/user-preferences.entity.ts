import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, Unique } from "typeorm";
import { NotificationType } from "./notification.entity";
import { lastVersionNumber } from "../versions/versions";

export enum MailingFrequency {
  NONE,
  ONCE_A_DAY,
  TWICE_A_DAY,
  REAL_TIME,
}

export enum LetterFileColor {
  GREEN = "GREEN",
  BLUE = "BLUE",
  YELLOW = "YELLOW",
  RED = "RED",
}

export const DEFAULT_USER_PREF: Partial<UserPreferences> = {
  mailingFrequency: MailingFrequency.REAL_TIME,
  enabledMailTypes: Object.values(NotificationType),
  onboardingSeen: false,
  signatures: [],
  color: LetterFileColor.RED,
  job: "",
  city: "",
  immediateSignature: true,
  lastSeenVersion: lastVersionNumber,
};

@Entity()
@Unique(["login"])
export class UserPreferences extends PastaBaseEntity {
  @Column()
  login: string;

  @Column({ default: MailingFrequency.REAL_TIME })
  mailingFrequency: MailingFrequency;

  @Column({ type: "simple-array", default: Object.values(NotificationType).join(",") })
  enabledMailTypes: NotificationType[];

  @Column({ default: false })
  onboardingSeen: boolean;

  @Column({ nullable: true })
  hideLetterFilesBefore: Date;

  @Column({ default: "", type: "simple-array" })
  signatures: string[];

  @Column({ default: LetterFileColor.RED })
  color: LetterFileColor;

  @Column({ default: "" })
  job: string;

  @Column({ default: "" })
  city: string;

  @Column({ default: true })
  immediateSignature: boolean;

  @Column({ default: lastVersionNumber })
  lastSeenVersion: number;

  @Column({ default: null })
  initialOrgUnit: string;

  @Column({ default: null })
  currentOrgUnit: string;
}
