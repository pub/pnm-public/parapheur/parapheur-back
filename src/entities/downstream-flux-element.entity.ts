import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, Generated, Index, ManyToOne } from "typeorm";
import { LetterFile } from "./letter-file.entity";

@Entity()
export class DownstreamFluxElement extends PastaBaseEntity {
  @Column()
  mail: string;

  @Column()
  name: string;

  @Column()
  position: number;

  @Column({ default: "" })
  structure: string;

  @ManyToOne(() => LetterFile, letterFile => letterFile.downstream, { onDelete: "CASCADE" })
  letterFile: LetterFile;

  @Column({ nullable: true })
  recalled?: boolean;

  @Column({ nullable: false })
  @Generated("uuid")
  @Index()
  constantId: string;
}
