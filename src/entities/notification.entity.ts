import { Column, Entity, ManyToOne } from "typeorm";
import { PastaBaseEntity } from "@pasta/back-core";
import { LetterFile } from "./letter-file.entity";

export enum NotificationType {
  AtYourLevel = "AtYourLevel",
  CC = "CC",
  RECALL = "RECALL",
}

@Entity()
export class Notification extends PastaBaseEntity {
  @Column()
  to: string;

  @Column({default: null})
  from: string;

  @Column()
  sendDate: Date;

  // TODO: after this has been deployed to prod, remove the default value
  @Column({ default: NotificationType.AtYourLevel })
  type: NotificationType;

  @ManyToOne(() => LetterFile, { onDelete: "CASCADE" })
  letterFile: LetterFile;
}
