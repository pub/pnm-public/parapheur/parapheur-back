import { User } from "@pasta/back-auth";
import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne } from "typeorm";
import { LetterFile } from "./letter-file.entity";

@Entity()
export class LetterFileComment extends PastaBaseEntity {
  @Column()
  text: string;

  @ManyToOne(() => User)
  author: User;

  @Column({ default: false })
  frozen: boolean;

  @ManyToOne(() => LetterFile, letterFile => letterFile.comments, { onDelete: "CASCADE" })
  letterFile: LetterFile;
}
