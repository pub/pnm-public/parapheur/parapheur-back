import { IsBoolean, IsInt, IsOptional } from "class-validator";

export class EnhanceOptions {
  @IsOptional()
  @IsBoolean()
  name?: boolean;

  @IsOptional()
  @IsBoolean()
  date?: boolean;

  @IsOptional()
  @IsBoolean()
  job?: boolean;

  @IsOptional()
  @IsBoolean()
  city?: boolean;
}

export class EnhanceSignatureDto extends EnhanceOptions {
  @IsInt()
  signatureIndex: number;
}
