import { WritableDto } from "@pasta/back-core";
import { IsInt, IsOptional, IsString } from "class-validator";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import { ForbiddenValues } from "../validators/forbidden-values.validator";

export class CreateLetterFileFolderDto
  implements WritableDto<LetterFileFolder, "documents" | "position" | "letterFile">
{
  @IsString()
  @ForbiddenValues(["$$ROOT$$"])
  name: string;
}

export class UpdateLetterFileFolderDto
  implements WritableDto<LetterFileFolder, "documents" | "letterFile" | "position">
{
  @IsString()
  @IsOptional()
  @ForbiddenValues(["$$ROOT$$"])
  name: string;
}

export class ReorderLetterFileFoldersDto {
  @IsInt({ each: true })
  folders: number[];
}
