import { WritableDto } from "@pasta/back-core";
import { LetterFileColor, MailingFrequency, UserPreferences } from "../entities/user-preferences.entity";
import { ArrayMaxSize, IsBoolean, IsDateString, IsEnum, IsInt, IsOptional, IsString } from "class-validator";
import { NotificationType } from "../entities/notification.entity";

export class WriteUserPreferencesDto
  implements WritableDto<UserPreferences, "login" | "initialOrgUnit" | "currentOrgUnit">
{
  @IsOptional()
  @IsEnum(MailingFrequency)
  mailingFrequency: MailingFrequency;

  @IsOptional()
  @IsBoolean()
  onboardingSeen: boolean;

  @IsOptional()
  @IsDateString()
  hideLetterFilesBefore: Date;

  @IsOptional()
  @IsString({ each: true })
  @ArrayMaxSize(15)
  signatures: string[];

  @IsOptional()
  @IsEnum(LetterFileColor)
  color: LetterFileColor;

  @IsOptional()
  @IsString()
  job: string;

  @IsOptional()
  @IsString()
  city: string;

  @IsOptional()
  @IsEnum(NotificationType, { each: true })
  enabledMailTypes: NotificationType[];

  @IsOptional()
  @IsBoolean()
  immediateSignature: boolean;

  @IsOptional()
  @IsInt()
  lastSeenVersion: number;
}
