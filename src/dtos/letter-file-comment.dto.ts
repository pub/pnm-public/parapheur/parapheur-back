import { WritableDto } from "@pasta/back-core";
import { IsString } from "class-validator";
import { LetterFileComment } from "../entities/letter-file-comment.entity";

export class UpdateLetterFileCommentDto implements WritableDto<LetterFileComment, "author" | "letterFile" | "frozen"> {
  @IsString()
  text: string;
}

export class CreateLetterFileCommentDto implements WritableDto<LetterFileComment, "author" | "frozen" | "letterFile"> {
  @IsString()
  text: string;
}
