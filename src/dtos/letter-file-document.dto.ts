import { WritableDto } from "@pasta/back-core";
import { IsEnum, IsInt, IsOptional, IsString, IsUrl, ValidateIf } from "class-validator";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";

export class CreateLetterFileDocumentDto
  implements
    WritableDto<
      LetterFileDocument,
      | "position"
      | "mimeType"
      | "nbPages"
      | "signed"
      | "documentVersions"
      | "extension"
      | "signaturePositions"
      | "signaturePositionsCount"
    >
{
  @IsString()
  name: string;

  @IsString()
  @ValidateIf(o => (o.type || LetterFileDocumentType.DEFAULT) === LetterFileDocumentType.DEFAULT)
  storageKey: string;

  @IsString()
  @IsUrl({ require_tld: false })
  @ValidateIf(o => o.type === LetterFileDocumentType.URL)
  url: string;

  @IsInt()
  folder: number;

  @IsOptional()
  @IsEnum(LetterFileDocumentType)
  type: LetterFileDocumentType;
}

export class UpdateLetterFileDocumentDto
  implements
    WritableDto<
      LetterFileDocument,
      | "storageKey"
      | "position"
      | "mimeType"
      | "nbPages"
      | "signed"
      | "documentVersions"
      | "type"
      | "url"
      | "extension"
      | "signaturePositions"
      | "signaturePositionsCount"
    >
{
  @IsString()
  @IsOptional()
  name: string;

  @IsInt()
  @IsOptional()
  folder: number;
}

export class ReorderLetterFileDocumentsDto {
  @IsInt({ each: true })
  documents: number[];
}
