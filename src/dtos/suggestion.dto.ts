import { WritableDto } from "@pasta/back-core";
import { LetterFileSuggestableField, Suggestion } from "../entities/Suggestion.entity";
import { IsArray, IsEnum, IsString } from "class-validator";

export type WriteSuggestionDto = WritableDto<Suggestion, "user", "fieldType">;

export class CreateSuggestionDto implements WriteSuggestionDto {
  @IsEnum(LetterFileSuggestableField)
  fieldType: LetterFileSuggestableField;

  @IsArray()
  @IsString({ each: true })
  values: string[];
}

export class UpdateSuggestionDto implements WriteSuggestionDto {
  @IsArray()
  @IsString({ each: true })
  values: string[];
}
