import { WritableDto } from "@pasta/back-core";
import { IsString } from "class-validator";
import { LetterFileTag } from "../entities/letter-file-tag.entity";

export class CreateLetterFileTagDto implements WritableDto<LetterFileTag, "letterFiles"> {
  @IsString()
  name: string;
}
