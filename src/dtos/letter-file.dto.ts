import { WritableDto } from "@pasta/back-core";
import { IsArray, IsBoolean, IsDateString, IsInt, IsOptional, IsString, ValidateNested } from "class-validator";
import { LetterFile } from "../entities/letter-file.entity";
import { Type } from "class-transformer";

export type WritableLetterFileDto = WritableDto<
  LetterFile,
  | "chronoNumber"
  | "draft"
  | "currentPosition"
  | "ccUsers"
  | "comments"
  | "folders"
  | "author"
  | "name"
  | "tags"
  | "upstream"
  | "downstream"
  | "deletedAt"
  | "archived"
  | "history"
>;

export class UpdateLetterFileDto implements WritableLetterFileDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  deadline: Date;

  @IsString()
  @IsOptional()
  arrivalDate: Date;

  @IsString()
  @IsOptional()
  externalMailNumber: string;

  @IsInt({ each: true })
  @IsOptional()
  tags: number[];

  @IsString()
  @IsOptional()
  senderOrganization: string;

  @IsString()
  @IsOptional()
  senderName: string;

  @IsString()
  @IsOptional()
  senderRole: string;

  @IsString()
  @IsOptional()
  mailType: string;

  @IsString()
  @IsOptional()
  recipientName: string;

  @IsString()
  @IsOptional()
  manager: string;
}

export class LetterFileSearchDto {
  @IsOptional()
  @IsString()
  searchPattern?: string;

  @IsOptional()
  @IsBoolean()
  archived?: boolean;

  @IsOptional()
  @IsString({ each: true })
  tags?: string[];

  @IsOptional()
  @IsBoolean()
  alreadySeen?: boolean;

  @IsOptional()
  @IsBoolean()
  createdByMe?: boolean;

  @IsOptional()
  @IsBoolean()
  inTodo?: boolean;

  @IsOptional()
  @IsBoolean()
  inCurrent?: boolean;

  @IsOptional()
  @IsBoolean()
  inDone?: boolean;

  @IsOptional()
  @IsDateString()
  startDate?: Date;

  @IsOptional()
  @IsDateString()
  endDate?: Date;

  @IsOptional()
  @IsDateString()
  createdStartDate?: Date;

  @IsOptional()
  @IsDateString()
  createdEndDate?: Date;

  @IsOptional()
  @IsString({ each: true })
  @IsArray()
  fileTypes?: string[];
}

export class ExportDto extends LetterFileSearchDto {
  @IsOptional()
  @IsBoolean()
  withDocuments: boolean;
}

export class CreateLetterFileSavedFluxDto {
  @IsString()
  name: string;

  @ValidateNested({ each: true })
  @Type(() => SavedFluxItem)
  flux: SavedFluxItem[];
}

export class SavedFluxItem {
  @IsString()
  name: string;

  @IsString()
  mail: string;
}
