import { WritableDto } from "@pasta/back-core";
import { SignaturePosition } from "../entities/SignaturePosition.entity";
import { ArrayNotEmpty, IsArray, IsInt, IsNumber, IsOptional, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

export class CreateSignaturePositionDto implements WritableDto<SignaturePosition> {
  @IsNumber()
  x: number;

  @IsNumber()
  y: number;

  @IsNumber()
  width: number;

  @IsNumber()
  height: number;

  @IsInt()
  page: number;

  @IsInt()
  document: number;
}

export class UpdateSignaturePositionDto implements WritableDto<SignaturePosition, "document"> {
  @IsNumber()
  @IsOptional()
  x: number;

  @IsNumber()
  @IsOptional()
  y: number;

  @IsNumber()
  @IsOptional()
  width: number;

  @IsNumber()
  @IsOptional()
  height: number;

  @IsInt()
  @IsOptional()
  page: number;
}

export class PositionPatch {
  @IsOptional()
  @IsNumber()
  id?: number;

  @IsOptional()
  @IsNumber()
  x?: number;

  @IsOptional()
  @IsNumber()
  y?: number;

  @IsOptional()
  @IsNumber()
  width?: number;

  @IsOptional()
  @IsNumber()
  height?: number;

  @IsOptional()
  @IsNumber()
  page?: number;
}

export class PatchPositionsForDocDto {
  @IsArray()
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => PositionPatch)
  positions: PositionPatch[];
}
