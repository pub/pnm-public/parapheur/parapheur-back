import { WritableDto } from "@pasta/back-core";
import { IsString } from "class-validator";
import { LetterFileCCUser } from "../entities/letter-file-cc-user.entity";

export class CreateLetterFileCCUserDto
  implements WritableDto<LetterFileCCUser, "frozen" | "author" | "name" | "letterFile">
{
  @IsString()
  mail: string;
}
