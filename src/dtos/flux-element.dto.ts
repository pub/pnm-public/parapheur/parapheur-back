import { IsInt, IsString } from "class-validator";

export class AppendDownstreamDto {
  @IsString()
  mail: string;
}

export class ReorderDownstreamDto {
  @IsInt({ each: true })
  downstream: number[];
}
