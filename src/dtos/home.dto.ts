import { plainToInstance } from "class-transformer";
import { LetterFile } from "../entities/letter-file.entity";
import { DownstreamFluxElement } from "../entities/downstream-flux-element.entity";
import { UpstreamFluxElement } from "../entities/upstream-flux-element.entity";
import { LetterFileStatus } from "../types/letter-file-status.enum";

export type HomeDtoLetterFile = Omit<LetterFile, "upstream" | "downstream"> & {
  current?: DownstreamFluxElement;
  from?: UpstreamFluxElement;
  status?: LetterFileStatus;
  recalled?: boolean;
};

export class HomeDto {
  todo: { data: HomeDtoLetterFile[]; total: number };
  current: { data: HomeDtoLetterFile[]; total: number; draft: number };
  done: { data: HomeDtoLetterFile[]; total: number; archived: number };

  static fromPlain(plain: HomeDto): HomeDto {
    return plainToInstance(HomeDto, plain);
  }
}
