import { Type } from "class-transformer";
import { ArrayNotEmpty, IsArray, IsNumber, IsOptional, ValidateNested } from "class-validator";
import { EnhanceOptions } from "./enhance-signature.dto";

export class SignatureDto {
  @IsNumber()
  x: number;

  @IsNumber()
  y: number;

  @IsNumber()
  page: number;

  @IsNumber()
  width: number;

  @IsNumber()
  height: number;

  @IsNumber()
  key: number;

  @IsOptional()
  @ValidateNested()
  @Type(() => EnhanceOptions)
  enhanceOptions?: EnhanceOptions;
}

export class SignDto {
  @IsArray()
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => SignatureDto)
  signatures: SignatureDto[];
}
