export function getAbstractClass(instance: Object): Function {
  return Object.getPrototypeOf(Object.getPrototypeOf(instance));
}
