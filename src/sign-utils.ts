import Path from "path";

export function getDocVersionBaseKey(letterFileId: number, documentId: number) {
  return `versions/${letterFileId}/${documentId}`;
}
export function getDocVersionKey(letterFileId: number, documentId: number, version: number, extension: string) {
  return `${getDocVersionBaseKey(letterFileId, documentId)}/v${version}${extension}`;
}

export function getConvertedKey(key: string): string {
  const parsedDoc = Path.parse(key);
  return Path.join("convert", parsedDoc.dir, `${parsedDoc.name}.pdf`);
}

export function getPagePrefix(lfId: number, folderId: number, docId: number) {
  return `pages/${lfId}/${folderId}/${docId}/`;
}
