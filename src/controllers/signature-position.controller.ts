import { AbstractPastaCrudController, PastaCrud, ValidationBody } from "@pasta/back-core";
import { SignaturePosition } from "../entities/SignaturePosition.entity";
import { Body, Controller, Get, Param, Patch, UseInterceptors } from "@nestjs/common";
import { SignaturePositionService } from "../services/signature-position.service";
import {
  CreateSignaturePositionDto,
  PatchPositionsForDocDto,
  UpdateSignaturePositionDto,
} from "../dtos/signature-position.dto";
import { IsCurrentInterceptor } from "../interceptors/is-current.interceptor";
import { WrapIdPipe } from "../pipe/wrap-id.pipe";
import { IsRelatedInterceptor } from "../interceptors/is-related.interceptor";

@PastaCrud({
  model: SignaturePosition,
  dto: { create: CreateSignaturePositionDto, update: UpdateSignaturePositionDto },
  routes: {
    exclude: ["getMany"],
    createOne: { interceptors: [IsCurrentInterceptor], bodyPipes: [WrapIdPipe.for("document")] },
    updateOne: { interceptors: [IsCurrentInterceptor] },
    deleteOne: { interceptors: [IsCurrentInterceptor] },
  },
  joins: {
    document: {},
  },
})
@Controller("letter-file/:letterFileId/document/:documentId/signature-position")
export class SignaturePositionController implements AbstractPastaCrudController<SignaturePosition> {
  constructor(public readonly service: SignaturePositionService) {}

  @Get("")
  @UseInterceptors(IsRelatedInterceptor)
  async getMany(@Param("documentId") docId: number) {
    return this.service.getManySignPos(docId);
  }

  @Patch("")
  @ValidationBody(PatchPositionsForDocDto)
  @UseInterceptors(IsRelatedInterceptor)
  async patchSignatures(@Param("documentId") docId: number, @Body() body: PatchPositionsForDocDto) {
    return this.service.patchForDoc(docId, body.positions);
  }
}
