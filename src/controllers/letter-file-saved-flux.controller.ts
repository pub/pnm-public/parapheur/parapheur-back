import { AbstractPastaCrudController, PastaCrud } from "@pasta/back-core";
import { LetterFileSavedFlux } from "../entities/saved-flux.entity";
import { LetterFileSavedFuxService } from "../services/saved-flux.service";
import { CreateLetterFileSavedFluxDto } from "../dtos/letter-file.dto";
import { SetAuthorInterceptor } from "../interceptors/set-author.interceptor";
import { Controller } from "@nestjs/common";

@PastaCrud({
  model: LetterFileSavedFlux,
  dto: {
    create: CreateLetterFileSavedFluxDto,
  },
  routes: {
    exclude: ["getOne", "updateOne"],
    createOne: {
      interceptors: [SetAuthorInterceptor],
    },
    deleteOne: {
      interceptors: [SetAuthorInterceptor],
    },
  },
  joins: {
    author: {},
  },
})
@Controller("saved-flux")
export class LetterFileSavedFluxController implements AbstractPastaCrudController<LetterFileSavedFlux> {
  constructor(public readonly service: LetterFileSavedFuxService) {}
}
