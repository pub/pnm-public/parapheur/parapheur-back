import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Query,
  UseInterceptors,
} from "@nestjs/common";
import { ReqAbility, ReqUser, User } from "@pasta/back-auth";
import {
  AbstractPastaCrudController,
  IPastaCrudQuery,
  PastaCrud,
  PastaCrudInterceptor,
  PastaCrudOperationType,
  PastaCrudQuery,
  ValidationBody,
} from "@pasta/back-core";
import { AppendDownstreamDto, ReorderDownstreamDto } from "../dtos/flux-element.dto";
import { ReorderLetterFileFoldersDto } from "../dtos/letter-file-folder.dto";
import { LetterFileSearchDto, UpdateLetterFileDto } from "../dtos/letter-file.dto";
import { LetterFile } from "../entities/letter-file.entity";
import { LetterFileSortInterceptor } from "../interceptors/letter-file-sort.interceptor";
import { SetFirstFluxElementInterceptor } from "../interceptors/set-first-flux-element.interceptor";
import { SetLFStatusInterceptor } from "../interceptors/set-letter-file-status.interceptor";
import { SetChronoPipe } from "../pipe/set-chrono.pipe";
import { WrapIdPipe } from "../pipe/wrap-id.pipe";
import { LetterFileService } from "../services/letter-file.service";
import { Id, IntParam } from "../utils";
import { SearchInterceptor } from "../interceptors/search.interceptor";
import { IsCurrentInterceptor } from "../interceptors/is-current.interceptor";
import { LetterFileIndexationService } from "../services/letter-file-indexation.service";
import { IsRelatedInterceptor } from "../interceptors/is-related.interceptor";
import { Ability } from "@casl/ability";
import { UpdateSuggestionsInterceptor } from "../interceptors/update-suggestions.interceptor";
import { MutexInterceptor } from "../interceptors/letter-file-mutex.interceptor";
import { IsLetterFileUnarchivedInterceptor } from "../interceptors/is-letterFile-unarchived.interceptor";
import { UnarchivedDisallowed } from "../decorators/unarchived-disallowed.decorator";

@PastaCrud({
  model: LetterFile,
  dto: {
    create: class {}, // No body allowed for create
    update: UpdateLetterFileDto,
  },
  routes: {
    createOne: {
      interceptors: [SetFirstFluxElementInterceptor, LetterFileSortInterceptor, SetLFStatusInterceptor],
      bodyPipes: [SetChronoPipe, WrapIdPipe.for("tags")],
    },
    updateOne: {
      interceptors: [
        IsCurrentInterceptor,
        LetterFileSortInterceptor,
        SetLFStatusInterceptor,
        UpdateSuggestionsInterceptor,
        MutexInterceptor,
        IsLetterFileUnarchivedInterceptor,
      ],
      bodyPipes: [WrapIdPipe.for("tags")],
    },
    getOne: {
      interceptors: [IsRelatedInterceptor, LetterFileSortInterceptor, SetLFStatusInterceptor],
    },
    exclude: ["getMany", "deleteOne"],
  },
  joins: {
    "folders": {},
    "folders.documents": {},
    "upstream": {},
    "downstream": {},
    "ccUsers": {},
    "ccUsers.author": {},
    "tags": {},
    "comments": {},
    "comments.author": {},
    "history": {},
  },
})
@Controller("/letter-file")
export class LetterFileController implements AbstractPastaCrudController<LetterFile> {
  constructor(
    public readonly service: LetterFileService,
    public readonly indexationService: LetterFileIndexationService
  ) {}

  @Delete("/:id")
  @PastaCrudOperationType("delete")
  @UseInterceptors(PastaCrudInterceptor, IsCurrentInterceptor)
  async deleteOne(@Id() id: number, @PastaCrudQuery() _query: IPastaCrudQuery, @ReqAbility() ability: Ability) {
    const lf = await this.service.getOne({}, id);
    if (!lf) {
      throw new NotFoundException();
    }

    if (lf.draft || ability.can("manage", "all")) {
      await this.service.deleteOne({}, id);
    } else {
      throw new ForbiddenException("Cannot delete non draft letter file");
    }

    return;
  }

  @Get("/home")
  async getHome(@ReqUser() user: User) {
    return this.service.getHome(user.login);
  }

  @Get("/chrono/:chronoNumber")
  @UseInterceptors(IsRelatedInterceptor, LetterFileSortInterceptor, SetLFStatusInterceptor, PastaCrudInterceptor)
  @PastaCrudOperationType("readOne")
  async getByChronoNumber(@Param("chronoNumber") chronoNumber: string, @PastaCrudQuery() query: IPastaCrudQuery) {
    query.filter = { chronoNumber: { $eq: chronoNumber } };
    const letterFile = (await this.service.getMany(query)).data.at(0);
    if (!letterFile) throw new NotFoundException();
    return letterFile;
  }

  @UseInterceptors(
    IsCurrentInterceptor,
    LetterFileSortInterceptor,
    SetLFStatusInterceptor,
    PastaCrudInterceptor,
    MutexInterceptor
  )
  @PastaCrudOperationType("update")
  @Post("/:id/undraft")
  async undraft(@Id() id: number, @PastaCrudQuery() query: IPastaCrudQuery, @ReqUser() user: User) {
    return this.service.undraft(query, id, user);
  }

  @Post("/:id/next")
  @UseInterceptors(
    IsCurrentInterceptor,
    LetterFileSortInterceptor,
    SetLFStatusInterceptor,
    PastaCrudInterceptor,
    MutexInterceptor
  )
  @PastaCrudOperationType("update")
  async next(@Id() id: number, @PastaCrudQuery() query: IPastaCrudQuery, @ReqUser() user: User) {
    return this.service.next(query, id, user);
  }

  @Post("/:id/take")
  @UseInterceptors(
    IsRelatedInterceptor,
    LetterFileSortInterceptor,
    SetLFStatusInterceptor,
    PastaCrudInterceptor,
    MutexInterceptor
  )
  @PastaCrudOperationType("update")
  async take(
    @Id() id: number,
    @ReqUser() user: User,
    @PastaCrudQuery() query: IPastaCrudQuery,
    @ReqAbility() ability: Ability
  ) {
    return this.service.take(query, id, user, ability.can("manage", "all"));
  }

  @Post("/:id/recall")
  @UseInterceptors(IsRelatedInterceptor, LetterFileSortInterceptor, SetLFStatusInterceptor, PastaCrudInterceptor)
  @PastaCrudOperationType("update")
  async recall(@Id() id: number, @PastaCrudQuery() query: IPastaCrudQuery) {
    return this.service.recall(query, id);
  }

  @Post("/:id/downstream")
  @ValidationBody(AppendDownstreamDto)
  @UseInterceptors(
    IsCurrentInterceptor,
    LetterFileSortInterceptor,
    SetLFStatusInterceptor,
    PastaCrudInterceptor,
    MutexInterceptor
  )
  @PastaCrudOperationType("update")
  async appendDownstream(@Id() id: number, @Body() dto: AppendDownstreamDto, @PastaCrudQuery() query: IPastaCrudQuery) {
    return this.service.appendDownstream(query, id, dto);
  }

  @Delete("/:id/downstream")
  @UseInterceptors(IsCurrentInterceptor, SetLFStatusInterceptor, PastaCrudInterceptor, MutexInterceptor)
  @PastaCrudOperationType("update")
  async emptyDownstream(@Id() id: number, @PastaCrudQuery() query: IPastaCrudQuery) {
    return this.service.emptyDownstream(query, id);
  }

  @Post("/:id/downstream/reorder")
  @ValidationBody(ReorderDownstreamDto)
  @UseInterceptors(
    IsCurrentInterceptor,
    LetterFileSortInterceptor,
    SetLFStatusInterceptor,
    PastaCrudInterceptor,
    MutexInterceptor
  )
  @PastaCrudOperationType("update")
  async reorderDownstream(
    @Id() id: number,
    @Body() dto: ReorderDownstreamDto,
    @PastaCrudQuery() query: IPastaCrudQuery
  ) {
    return this.service.reorderDownstream(query, id, dto);
  }

  @Delete("/:id/downstream/:downstreamId")
  @UseInterceptors(
    IsCurrentInterceptor,
    LetterFileSortInterceptor,
    SetLFStatusInterceptor,
    PastaCrudInterceptor,
    MutexInterceptor
  )
  @PastaCrudOperationType("update")
  async removeDownstream(
    @Id() id: number,
    @IntParam("downstreamId") downstreamId: number,
    @PastaCrudQuery() query: IPastaCrudQuery
  ) {
    return this.service.removeDownstream(query, id, downstreamId);
  }

  @Post("/:id/folders/reorder")
  @ValidationBody(ReorderLetterFileFoldersDto)
  @UseInterceptors(IsCurrentInterceptor, LetterFileSortInterceptor, SetLFStatusInterceptor, PastaCrudInterceptor)
  @PastaCrudOperationType("update")
  async reorderFolders(
    @Id() id: number,
    @Body() dto: ReorderLetterFileFoldersDto,
    @PastaCrudQuery() query: IPastaCrudQuery
  ) {
    return this.service.reorderFolders(query, id, dto);
  }

  @Post("/:id/archive")
  @UseInterceptors(IsRelatedInterceptor)
  generateArchive(@Id() id: number) {
    return this.service.generateArchive(id);
  }

  @Post("/search")
  @ValidationBody(LetterFileSearchDto)
  @UseInterceptors(SearchInterceptor, LetterFileSortInterceptor, SetLFStatusInterceptor)
  search(
    @Body() dto: LetterFileSearchDto,
    @Query("offset", ParseIntPipe) offset: number,
    @Query("limit", ParseIntPipe) limit: number,
    @ReqUser() user: User
  ) {
    return this.service.search(dto, { offset, limit }, user.login);
  }

  @Delete("/:id/documents")
  @UseInterceptors(IsCurrentInterceptor, IsLetterFileUnarchivedInterceptor)
  @UnarchivedDisallowed()
  async removeAllDocs(@Id() id: number) {
    return this.service.removeAllDocs(id);
  }

  @Post("/:id/apply-flux/:fluxId")
  @UseInterceptors(IsCurrentInterceptor)
  async applyFlux(@Id() id: number, @IntParam("fluxId") fluxId: number) {
    return this.service.applyFlux(id, fluxId);
  }

  @Post("/:id/clone")
  @UseInterceptors(IsRelatedInterceptor, LetterFileSortInterceptor, SetLFStatusInterceptor, PastaCrudInterceptor)
  @PastaCrudOperationType("create")
  async clone(@Id() id: number, @ReqUser() user: User) {
    return this.service.clone(id, user);
  }

  @Post("/:id/unarchive")
  @UseInterceptors(IsRelatedInterceptor)
  async unarchive(@Id() id: number, @ReqUser() user: User) {
    return this.service.unarchive(id, user.login);
  }
}
