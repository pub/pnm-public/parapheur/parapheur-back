import { Body, ConflictException, Controller, ForbiddenException, Get, Post } from "@nestjs/common";
import { LetterFileExportService } from "../services/export.service";
import { ReqUser, ReqUserId, User } from "@pasta/back-auth";
import { ValidationBody } from "@pasta/back-core";
import { ExportDto } from "../dtos/letter-file.dto";
import { Id } from "../utils";
import { omit } from "lodash";
import { StorageService } from "@pasta/back-files";

@Controller("/export")
export class LetterFileExportController {
  constructor(private readonly service: LetterFileExportService, private readonly storageService: StorageService) {}

  @Post()
  @ValidationBody(ExportDto)
  async startExport(@Body() body: ExportDto, @ReqUser() user: User) {
    if (!(await this.isReady(user.id))) {
      throw new ConflictException("Un export est déjà en cours");
    }
    return this.service.startExport(omit(body, "withDocuments"), body.withDocuments, user);
  }

  // Warning: order of endpoints matters, do not place below get /:id
  @Get("/is-ready")
  async isReady(@ReqUserId() userId: number) {
    return this.service.isReady(userId);
  }

  @Get("/:id")
  async getOne(@Id() id: number, @ReqUserId() userId: number) {
    const result = await this.service.getOne({ join: ["author"] }, id);
    if (result.author.id !== userId) {
      throw new ForbiddenException("Not yours");
    }
    return omit(result, "author");
  }

  @Get("/:id/link")
  async getLink(@Id() id: number, @ReqUserId() userId: number) {
    const { fileKey } = await this.getOne(id, userId);
    return this.storageService.getLink(fileKey, true);
  }
}
