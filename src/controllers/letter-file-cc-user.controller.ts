import { Controller, Post, UseInterceptors } from "@nestjs/common";
import { AbstractPastaCrudController, PastaCrud } from "@pasta/back-core";
import { CreateLetterFileCCUserDto } from "../dtos/letter-file-cc-user.dto";
import { LetterFileCCUser } from "../entities/letter-file-cc-user.entity";
import { LetterFileCCUserService } from "../services/letter-file-cc-user.service";
import { SetAuthorInterceptor } from "../interceptors/set-author.interceptor";
import { SetLetterFileIdInterceptor } from "../interceptors/set-letter-file-id.interceptor";
import { IsCCUserFrozenInterceptor } from "../interceptors/is-ccUser-frozen.interceptor";
import { IsCurrentInterceptor } from "../interceptors/is-current.interceptor";
import { IntParam } from "../utils";
import { ReqUserId } from "@pasta/back-auth";

@PastaCrud({
  model: LetterFileCCUser,
  dto: {
    create: CreateLetterFileCCUserDto,
  },
  routes: {
    exclude: ["updateOne", "getMany", "getOne"],
    createOne: {
      interceptors: [SetAuthorInterceptor, SetLetterFileIdInterceptor],
    },
    deleteOne: {
      interceptors: [IsCCUserFrozenInterceptor],
    },
  },
  joins: { author: {} },
})
@Controller("letter-file/:letterFileId/cc-user/")
export class LetterFileCCUserController implements AbstractPastaCrudController<LetterFileCCUser> {
  constructor(public readonly service: LetterFileCCUserService) {}

  @Post("/apply-flux/:fluxId")
  @UseInterceptors(IsCurrentInterceptor)
  async applyFlux(
    @IntParam("letterFileId") lfId: number,
    @IntParam("fluxId") fluxId: number,
    @ReqUserId() userId: number
  ) {
    return this.service.applyFlux(lfId, fluxId, userId);
  }
}
