import path from "path";
import {
  BadRequestException,
  Controller,
  ForbiddenException,
  Get,
  Headers,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Query,
  RawBodyRequest,
  Req,
  Res,
  UseInterceptors,
} from "@nestjs/common";
import { WopiService, WopiTokenData } from "../services/wopi.service";
import { Public, ReqUserId, UserService } from "@pasta/back-auth";
import { Request, Response } from "express";
import { IsRelatedInterceptor } from "../interceptors/is-related.interceptor";
import { IsCurrentInterceptor } from "../interceptors/is-current.interceptor";
import { get } from "lodash";
import { Histogram, UseMetric } from "@pasta/prometheus";
import { LetterFileDocumentService } from "../services/letter-file-document.service";
import { IsWopiEnabledInterceptor } from "../interceptors/is-wopi-enabled.interceptor";

@Controller("/wopi")
export class WopiController {
  constructor(
    private readonly service: WopiService,
    private readonly userService: UserService,
    private readonly documentService: LetterFileDocumentService
  ) {}

  @UseMetric("wopi_time_to_save")
  timeToSave: Histogram;

  @Get("/:documentId/auth")
  @UseInterceptors(IsWopiEnabledInterceptor, IsRelatedInterceptor)
  async getAuth(@Param("documentId", ParseIntPipe) id: number, @ReqUserId() userId: number) {
    return this.service.generateToken(id, userId);
  }

  @Get("/files/:documentId")
  @Public() // disables standard auth as we use a custom one here
  async checkFileInfo(
    @Param("documentId", ParseIntPipe) documentId: number,
    @Query("access_token") accessToken: string
  ) {
    const tokenData = await this.validateToken(accessToken);
    const user = await this.userService.getOne({}, tokenData.userId);
    const doc = await this.getDoc(documentId);
    return {
      UserCanNotWriteRelative: true,
      UserId: user.id + "",
      UserFriendlyName: `${user.firstName} ${user.lastName}`,
      UserCanWrite: true,
      ...(await this.service.getInfos(doc)),
    };
  }

  @Get("/files/:documentId/contents")
  @Public() // disables standard auth as we use a custom one here
  @UseInterceptors(IsWopiEnabledInterceptor)
  async getFile(
    @Param("documentId", ParseIntPipe) documentId: number,
    @Query("access_token") accessToken: string,
    @Res() res: Response
  ) {
    await this.validateToken(accessToken);
    const doc = await this.getDoc(documentId);
    const readable = await this.service.getFile(doc);
    readable.pipe(res);
  }

  @Post("/files/:documentId/contents")
  @Public() // disables standard auth as we use a custom one here
  @UseInterceptors(IsWopiEnabledInterceptor, IsCurrentInterceptor)
  async putFile(
    @Param("documentId", ParseIntPipe) documentId: number,
    @Req() req: RawBodyRequest<Request>,
    @Res() res: Response
  ) {
    // no need to validate token as it is done in IsCurrentInterceptor
    const now = Date.now();
    this.timeToSave.observe({ user: get(req, "user") }, now - get(req, "tokenData.timestamp", now));
    const doc = await this.getDoc(documentId);
    await this.service.putFile(doc, req.rawBody);
    const infos = await this.service.getInfos(doc);
    res.header("X-COOL-WOPI-Timestamp", infos.LastModifiedTime.toString());
    res.status(HttpStatus.OK).end();
  }

  @Post("/files/:documentId")
  @Public() // disables standard auth as we use a custom one here
  @UseInterceptors(IsWopiEnabledInterceptor, IsCurrentInterceptor)
  async renameFile(
    @Param("documentId", ParseIntPipe) documentId: number,
    @Headers("x-wopi-suggestedtarget") name: string,
    @Res() res: Response
  ) {
    const newName = path.parse(name).name;
    if (/\+.*/g.test(newName)) {
      throw new BadRequestException("Specified name contains special characters");
    }
    await this.documentService.updateOne({}, documentId, { name: newName });
    res.status(HttpStatus.OK).end();
  }

  @Get("/editables")
  @Public()
  @UseInterceptors(IsWopiEnabledInterceptor)
  async getEditables() {
    return this.service.getEditables();
  }

  private getDoc(id: number) {
    return this.service.getDoc(id);
  }

  private async validateToken(token: string): Promise<WopiTokenData> {
    const tokenData = await this.service.getTokenData(token);
    if (!tokenData) {
      throw new ForbiddenException("Provided token is invalid or expired");
    }
    return tokenData;
  }
}
