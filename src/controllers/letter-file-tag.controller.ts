import { Controller } from "@nestjs/common";
import { AbstractPastaCrudController, PastaCrud } from "@pasta/back-core";
import { CreateLetterFileTagDto } from "../dtos/letter-file-tag.dto";
import { LetterFileTag } from "../entities/letter-file-tag.entity";
import { LetterFileTagService } from "../services/letter-file-tag.service";

@PastaCrud({
  model: LetterFileTag,
  dto: {
    create: CreateLetterFileTagDto,
    update: CreateLetterFileTagDto,
  },
  sort: {
    default: ["id"],
  },
})
@Controller("letter-file-tag")
export class LetterFileTagController implements AbstractPastaCrudController<LetterFileTag> {
  constructor(public readonly service: LetterFileTagService) {}
}
