import { Controller, Delete, Get, Injectable, Query } from "@nestjs/common";
import { LdapService } from "../services/ldap.service";
import { NeedAbility } from "@pasta/back-auth";

@Controller("ldap")
@Injectable()
export class LdapController {
  constructor(private readonly ldapService: LdapService) {}

  @Get("/")
  autocomplete(@Query("pattern") pattern: string) {
    return this.ldapService.autocomplete(pattern);
  }

  @Delete("/")
  @NeedAbility({ action: "manage", subject: "all" })
  async emptyCache() {
    return this.ldapService.emptyCache();
  }
}
