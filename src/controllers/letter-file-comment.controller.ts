import { Controller } from "@nestjs/common";
import { AbstractPastaCrudController, PastaCrud } from "@pasta/back-core";
import { CreateLetterFileCommentDto, UpdateLetterFileCommentDto } from "../dtos/letter-file-comment.dto";
import { LetterFileComment } from "../entities/letter-file-comment.entity";
import { SetAuthorInterceptor } from "../interceptors/set-author.interceptor";
import { LetterFileCommentService } from "../services/letter-file-comment.service";
import { IsCommentFrozenInterceptor } from "../interceptors/is-comment-frozen.interceptor";
import { SetLetterFileIdInterceptor } from "../interceptors/set-letter-file-id.interceptor";
import { IsCurrentInterceptor } from "../interceptors/is-current.interceptor";

@PastaCrud({
  model: LetterFileComment,
  dto: {
    create: CreateLetterFileCommentDto,
    update: UpdateLetterFileCommentDto,
  },
  routes: {
    exclude: ["getMany", "getOne"],
    createOne: {
      interceptors: [SetAuthorInterceptor, SetLetterFileIdInterceptor, IsCurrentInterceptor],
    },
    updateOne: {
      interceptors: [IsCommentFrozenInterceptor, IsCurrentInterceptor],
    },
    deleteOne: {
      interceptors: [IsCommentFrozenInterceptor, IsCurrentInterceptor],
    },
  },
  joins: {
    author: {},
  },
})
@Controller("letter-file/:letterFileId/comments")
export class LetterFileCommentController implements AbstractPastaCrudController<LetterFileComment> {
  constructor(public readonly service: LetterFileCommentService) {}
}
