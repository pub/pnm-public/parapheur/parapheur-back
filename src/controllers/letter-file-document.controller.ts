import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  ParseBoolPipe,
  Post,
  Query,
  UseInterceptors,
} from "@nestjs/common";
import { AbstractPastaCrudController, PastaCrud, ValidationBody } from "@pasta/back-core";
import { CreateLetterFileDocumentDto, UpdateLetterFileDocumentDto } from "../dtos/letter-file-document.dto";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { WrapIdPipe } from "../pipe/wrap-id.pipe";
import { LetterFileDocumentService } from "../services/letter-file-document.service";
import { Id, IntParam } from "../utils";
import { IsCurrentInterceptor } from "../interceptors/is-current.interceptor";
import { IsRelatedInterceptor } from "../interceptors/is-related.interceptor";
import { SignService } from "../services/sign.service";
import { HttpStatusCode } from "axios";
import { SignDto } from "../dtos/sign.dto";
import { ReqUser, User } from "@pasta/back-auth";
import { DocumentVersionService } from "../services/document-version.service";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";
import { SetDocumentExtensionInterceptor } from "../interceptors/set-document-extension.interceptor";
import { IsLetterFileUnarchivedInterceptor } from "../interceptors/is-letterFile-unarchived.interceptor";

@PastaCrud({
  model: LetterFileDocument,
  dto: {
    create: CreateLetterFileDocumentDto,
    update: UpdateLetterFileDocumentDto,
  },
  routes: {
    exclude: ["getMany", "getOne"],
    createOne: {
      interceptors: [IsCurrentInterceptor, SetDocumentExtensionInterceptor],
      bodyPipes: [WrapIdPipe.for("folder")],
    },
    updateOne: {
      interceptors: [IsCurrentInterceptor, IsLetterFileUnarchivedInterceptor],
      bodyPipes: [WrapIdPipe.for("folder")],
    },
    deleteOne: { interceptors: [IsCurrentInterceptor, IsLetterFileUnarchivedInterceptor] },
  },
  joins: {
    "folder": {},
    "folder.letterFile": {},
    "documentVersions": {},
    "signaturePositions": {},
  },
})
@Controller("letter-file/:letterFileId/document")
export class LetterFileDocumentController implements AbstractPastaCrudController<LetterFileDocument> {
  constructor(
    public readonly service: LetterFileDocumentService,
    private readonly signService: SignService,
    private readonly docVersionService: DocumentVersionService
  ) {}

  @Get("/:id/link")
  @UseInterceptors(IsRelatedInterceptor)
  async getLink(@Id() id: number, @Query("asAttachment", ParseBoolPipe) asAttachment: boolean = false) {
    await this.checkType(id);
    return this.service.getLink(id, asAttachment);
  }

  @Post("/:id/sign")
  @HttpCode(HttpStatusCode.Created)
  @ValidationBody(SignDto)
  @UseInterceptors(IsCurrentInterceptor)
  async sign(@Id() id: number, @Body() dto: SignDto, @ReqUser() user: User) {
    await this.checkType(id);
    await this.signService.sign(id, dto.signatures, user);
  }

  @Get("/:id/page/:pageNumber")
  @UseInterceptors(IsCurrentInterceptor)
  async getPage(@Id() id: number, @IntParam("pageNumber") pageNumber: number) {
    await this.checkType(id);
    return this.signService.getPage(id, pageNumber);
  }

  @Get("/:id/versions")
  @UseInterceptors(IsRelatedInterceptor)
  async getVersions(@Id() id: number) {
    await this.checkType(id);
    return this.docVersionService.getVersions(id);
  }

  @Get("/:id/versions/:versionId/link")
  @UseInterceptors(IsRelatedInterceptor)
  async getVersionLink(@IntParam("versionId") docVersionId: number) {
    const { document } = await this.docVersionService.findOne({ where: { id: docVersionId }, relations: ["document"] });
    await this.checkType(document);

    return this.docVersionService.getLink(docVersionId);
  }

  @Post("/:id/computeNbPages")
  @UseInterceptors(IsCurrentInterceptor)
  async computeNbPages(@Id() id: number) {
    await this.checkType(id);
    return this.service.computeNbPages(id);
  }

  private async checkType(arg: number | LetterFileDocument) {
    let type: LetterFileDocumentType;
    if (typeof arg === "number") {
      type = (await this.service.getOne({}, arg)).type;
    } else {
      type = arg.type;
    }

    if (type !== LetterFileDocumentType.DEFAULT) {
      throw new BadRequestException(`Operation not possible for document type "${type}"`);
    }
  }
}
