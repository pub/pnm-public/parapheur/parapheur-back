import { Body, Controller, Post, UseInterceptors } from "@nestjs/common";
import {
  AbstractPastaCrudController,
  IPastaCrudQuery,
  PastaCrud,
  PastaCrudInterceptor,
  PastaCrudOperationType,
  PastaCrudQuery,
  ValidationBody,
} from "@pasta/back-core";
import { ReorderLetterFileDocumentsDto } from "../dtos/letter-file-document.dto";
import { CreateLetterFileFolderDto, UpdateLetterFileFolderDto } from "../dtos/letter-file-folder.dto";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import { WrapIdPipe } from "../pipe/wrap-id.pipe";
import { LetterFileFolderService } from "../services/letter-file-folder.service";
import { Id } from "../utils";
import { SetLetterFileIdInterceptor } from "../interceptors/set-letter-file-id.interceptor";
import { IsCurrentInterceptor } from "../interceptors/is-current.interceptor";
import { IsRelatedInterceptor } from "../interceptors/is-related.interceptor";
import { IsLetterFileUnarchivedInterceptor } from "../interceptors/is-letterFile-unarchived.interceptor";

@PastaCrud({
  model: LetterFileFolder,
  dto: {
    create: CreateLetterFileFolderDto,
    update: UpdateLetterFileFolderDto,
  },
  routes: {
    exclude: ["getMany", "getOne"],
    createOne: {
      interceptors: [SetLetterFileIdInterceptor, IsCurrentInterceptor],
      bodyPipes: [WrapIdPipe.for("letterFile")],
    },
    updateOne: {
      interceptors: [IsCurrentInterceptor, IsLetterFileUnarchivedInterceptor],
      bodyPipes: [WrapIdPipe.for("letterFile")],
    },
    deleteOne: { interceptors: [IsCurrentInterceptor, IsLetterFileUnarchivedInterceptor] },
  },
  joins: {
    documents: { eager: true },
    letterFile: {},
  },
})
@Controller("letter-file/:letterFileId/folder")
export class LetterFileFolderController implements AbstractPastaCrudController<LetterFileFolder> {
  constructor(public readonly service: LetterFileFolderService) {}

  @Post(":id/documents/reorder")
  @ValidationBody(ReorderLetterFileDocumentsDto)
  @UseInterceptors(IsCurrentInterceptor, PastaCrudInterceptor)
  @PastaCrudOperationType("update")
  async reorder(
    @Id() id: number,
    @Body() dto: ReorderLetterFileDocumentsDto,
    @PastaCrudQuery() query: IPastaCrudQuery
  ) {
    return this.service.reorderDocuments(query, id, dto);
  }

  @Post(":id/archive")
  @UseInterceptors(IsRelatedInterceptor)
  async generateArchive(@Id() id: number) {
    return this.service.generateArchive(id);
  }
}
