import { DEFAULT_USER_PREF, UserPreferences } from "../entities/user-preferences.entity";
import { UserPreferencesService } from "../services/user-preferences.service";
import { BadRequestException, Body, Controller, Get, Patch, Post } from "@nestjs/common";
import { ReqUser, User } from "@pasta/back-auth";
import { WriteUserPreferencesDto } from "../dtos/user-preferences.dto";
import { ValidationBody } from "@pasta/back-core";
import dayjs from "dayjs";
import { HideBeforeError } from "../errors/user-preferences.error";
import { SignService } from "../services/sign.service";
import { EnhanceSignatureDto } from "../dtos/enhance-signature.dto";
import { omit } from "lodash";

@Controller("/user-preferences")
export class UserPreferencesController {
  private get selects(): (keyof UserPreferences)[] {
    return [
      "login",
      "mailingFrequency",
      "onboardingSeen",
      "hideLetterFilesBefore",
      "signatures",
      "color",
      "job",
      "city",
      "enabledMailTypes",
      "immediateSignature",
      "lastSeenVersion",
    ];
  }
  constructor(
    public readonly service: UserPreferencesService,
    private readonly signService: SignService
  ) {}

  @Get("/my")
  async my(@ReqUser() user: User) {
    const found = await this.service.findOne({
      where: { login: user.login },
      select: this.selects,
    });
    if (found) return found;
    return {
      ...DEFAULT_USER_PREF,
      login: user.login,
    };
  }

  @Patch("/my")
  @ValidationBody(WriteUserPreferencesDto)
  async updateMy(@ReqUser() user: User, @Body() body: WriteUserPreferencesDto) {
    const found = await this.service.findOneBy({ login: user.login });
    if (!found) {
      return this.service.createOne({ select: this.selects }, { login: user.login, ...body });
    }

    if (body.hideLetterFilesBefore === null) {
      throw new HideBeforeError("Cannot unset hideLetterFilesBefore");
    }
    if (body.hideLetterFilesBefore && dayjs(body.hideLetterFilesBefore).isBefore(found.hideLetterFilesBefore)) {
      throw new HideBeforeError("Cannot set hideLetterFilesBefore earlier than current value");
    }

    return this.service.updateOne({ select: this.selects }, found.id, body);
  }

  @Post("/enhance-signature")
  @ValidationBody(EnhanceSignatureDto)
  async generateEnhancedSignature(@ReqUser() user: User, @Body() dto: EnhanceSignatureDto) {
    if (!Object.values(omit(dto, "signatureIndex")).some(val => val)) {
      throw new BadRequestException("Options must contain at least one true value");
    }
    return this.signService.generateEnhancedSignature(dto, user);
  }
}
