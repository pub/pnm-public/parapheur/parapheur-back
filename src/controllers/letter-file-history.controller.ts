import {
  AbstractPastaCrudController,
  IPastaCrudQuery,
  PastaCrud,
  PastaCrudInterceptor,
  PastaCrudOperationType,
  PastaCrudQuery,
} from "@pasta/back-core";
import { LetterFileHistory } from "../entities/letter-file-history.entity";
import { IsRelatedInterceptor } from "../interceptors/is-related.interceptor";
import { Controller, Get, UseInterceptors } from "@nestjs/common";
import { LetterFileHistoryService } from "../services/letter-file-history.service";
import { IntParam } from "../utils";

@PastaCrud({
  model: LetterFileHistory,
  routes: {
    exclude: ["createOne", "updateOne", "deleteOne", "getOne", "getMany"],
  },
})
@Controller("letter-file/:letterFileId/history")
export class LetterFileHistoryController {
  constructor(public readonly service: LetterFileHistoryService) {}

  @Get("")
  @UseInterceptors(IsRelatedInterceptor, PastaCrudInterceptor)
  @PastaCrudOperationType("readMany")
  async getMany(@IntParam("letterFileId") lfId: number, @PastaCrudQuery() query: IPastaCrudQuery) {
    query.filter = { $and: [{ letterFileId: { $eq: lfId } }] };
    return this.service.getMany(query);
  }
}
