import { AbstractPastaCrudController, PastaCrud } from "@pasta/back-core";
import { Suggestion } from "../entities/Suggestion.entity";
import { SuggestionService } from "../services/suggestion.service";
import { Controller, Get } from "@nestjs/common";
import { CreateSuggestionDto, UpdateSuggestionDto } from "../dtos/suggestion.dto";
import { IsMySuggestionInterceptor } from "../interceptors/is-my-suggestion.interceptor";
import { ReqUserId } from "@pasta/back-auth";
import { SetUserInterceptor } from "../interceptors/set-user.interceptor";

@PastaCrud({
  model: Suggestion,
  dto: {
    create: CreateSuggestionDto,
    update: UpdateSuggestionDto,
  },
  routes: {
    exclude: ["getMany", "getOne", "deleteOne"],
    createOne: { interceptors: [SetUserInterceptor] },
    updateOne: { interceptors: [IsMySuggestionInterceptor] },
  },
})
@Controller("/suggestion")
export class SuggestionController implements AbstractPastaCrudController<Suggestion> {
  constructor(public readonly service: SuggestionService) {}

  @Get("/my")
  public async getMine(@ReqUserId() userId: number) {
    const suggestions = await this.service.getMany({ filter: { userId: { $eq: userId } } });
    return suggestions.data;
  }
}
