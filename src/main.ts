import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { useContainer } from "class-validator";
import { LoggerService } from "@pasta/back-logger";
import { NestExpressApplication } from "@nestjs/platform-express";

async function bootstrap() {
  const app: NestExpressApplication = await NestFactory.create(AppModule, {
    logger: LoggerService.getInstance(),
    rawBody: true,
  });
  app.useBodyParser("raw");
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
