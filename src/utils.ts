import { createHash } from "crypto";
import { Param, ParseIntPipe, PipeTransform } from "@nestjs/common";

export function IntParam(paramName: string, ...pipes: PipeTransform[]) {
  return Param(paramName, new ParseIntPipe(), ...pipes);
}

export function Id(...pipes: PipeTransform[]) {
  return IntParam("id", ...pipes);
}

export const searchAliasName = "letter-file";
export const searchIndexName = "letter-file-v1";

export const md5 = data => createHash("md5").update(data).digest("hex");
