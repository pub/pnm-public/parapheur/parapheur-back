import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint()
export class ForbiddenValuesConstraint implements ValidatorConstraintInterface {
  validate(target: object, args: ValidationArguments) {
    return !args.constraints.includes(args.value);
  }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Value "${validationArguments.value.name}" is forbidden for field ${validationArguments.property}`;
  }
}

export function ForbiddenValues(forbiddenValues: any[], options: ValidationOptions = {}): PropertyDecorator {
  return (object: Object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options,
      constraints: forbiddenValues,
      validator: ForbiddenValuesConstraint,
    });
  };
}
