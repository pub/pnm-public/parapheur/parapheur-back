import { Module } from "@nestjs/common";
import { PastaConfigModule } from "@pasta/back-core";
import { ElasticSearchConfig } from "./elasticsearch.config";
import { ElasticsearchService } from "./elasticsearch.service";

@Module({
  providers: [ElasticsearchService],
  exports: [ElasticsearchService],
  imports: [PastaConfigModule.register(ElasticSearchConfig)],
})
export class ElasticsearchModule {}
