import { FromEnv } from "@pasta/back-core";
import { ISearchModuleOptions } from "@pasta/back-search";
import { IsString, IsUrl } from "class-validator";

export class ElasticSearchConfig implements ISearchModuleOptions {
  @FromEnv("ELASTICSEARCH_URL")
  @IsUrl({ require_tld: false })
  @IsString()
  host: string = "http://localhost:9200";
}
