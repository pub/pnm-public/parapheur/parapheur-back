export interface IndexParams<T = Record<string, any>> {
  id?: string;
  index: string;
  body: T;
}

export interface UpdateIndexParams<T = Record<string, any>> {
  id: string;
  index: string;
  body: T;
}
