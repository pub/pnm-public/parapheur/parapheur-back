export interface Index {
  "health": string;
  "status": string;
  "index": string;
  "uuid": string;
  "pri": string;
  "rep": string;
  "docs.count": string;
  "docs.deleted": string;
  "store.size": string;
  "pri.store.size": string;
}
