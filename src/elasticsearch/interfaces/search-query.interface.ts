export type SortCriteria = Record<string, { order: "asc" | "desc"; nested?: { path: string } }> | string;

export interface SearchQueryParams {
  query?: object;
  suggest?: object;
  sort?: SortCriteria[];
  from?: number;
  size?: number;
  _source?: string[];
}
