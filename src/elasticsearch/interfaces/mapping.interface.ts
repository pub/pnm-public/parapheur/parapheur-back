export type FieldType =
  | "binary"
  | "boolean"
  | "byte"
  | "completion"
  | "date_nanos"
  | "date_range"
  | "date"
  | "double_range"
  | "double"
  | "float_range"
  | "float"
  | "geo_point"
  | "geo_shape"
  | "half_float"
  | "integer_range"
  | "integer"
  | "ip_range"
  | "ip"
  | "keyword"
  | "long_range"
  | "long"
  | "nested"
  | "scaled_float"
  | "search_as_you_type"
  | "short"
  | "text";

export interface Properties {
  [key: string]: FieldMapping;
}

export interface IndexSettings {
  default_pipeline?: string;
  index?: {
    max_ngram_diff?: number;
  };
  analysis?: {
    analyzer?: {
      [key: string]: {
        type: "custom";
        tokenizer: string;
        char_filter: string[];
        filter: string[];
      };
    };
    filter?: {
      [key: string]: {
        [key: string]: any;
        type: string;
      };
    };
    char_filter?: {
      [key: string]: {
        type: string;
        pattern?: string;
        replacement?: string;
        mappings?: string[];
      };
    };
    normalizer: {
      [key: string]: {
        type: string;
        char_filter: string[];
        filter: string[];
      };
    };
    tokenizer?: {
      [key: string]: {
        type: string;
        min_gram?: number;
        max_gram?: number;
        token_chars?: string[];
      };
    };
  };
}
export interface IndexMapping {
  dynamic?: boolean;
  properties?: Properties;
}

export type FieldMapping = BaseMapping | ObjectMapping | CompletionMapping;

export interface BaseMapping {
  type: FieldType;
  analyzer?: string;
  coerce?: boolean;
  copy_to?: string;
  doc_values?: boolean;
  dynamic?: boolean;
  eager_global_ordinals?: boolean;
  enabled?: boolean;
  fielddata?: string;
  format?: string;
  ignore_above?: number;
  ignore_malformed?: boolean;
  index?: boolean;
  index_options?: "docs" | "freqs" | "positions" | "offsets";
  index_phrases?: boolean;
  index_prefixes?: { min_chars: number; max_chars: number };
  meta?: object;
  fields?: { [key: string]: FieldMapping };
  normalizer?: string;
  norms?: boolean;
  null_value?: any;
  position_increment_gap?: number;
  properties?: Properties;
  search_analyzer?: string;
  similarity?: string;
  store?: boolean;
  term_vector?: string;
}

interface ObjectMapping extends Omit<BaseMapping, "type" | "properties"> {
  type?: FieldType;
  properties: Properties;
}

interface CompletionMapping extends BaseMapping {
  preserve_separators?: boolean;
  preserve_position_increments?: boolean;
  max_input_length: number;
}
