export type EsPipelineProcessorType =
  | "append"
  | "attachment"
  | "bytes"
  | "circle"
  | "convert"
  | "csv"
  | "date"
  | "date_index_name"
  | "dissect"
  | "dot_expander"
  | "drop"
  | "enrich"
  | "fail"
  | "foreach"
  | "geo_ip"
  | "grok"
  | "gsub"
  | "html_strip"
  | "inference"
  | "join"
  | "json"
  | "kv"
  | "lowercase"
  | "pipeline"
  | "remove"
  | "rename"
  | "script"
  | "set"
  | "set_security_user"
  | "sort"
  | "split"
  | "trim"
  | "uppercase"
  | "url_decode"
  | "user_agent";

export type EsPipelineProcessor = {
  [key in EsPipelineProcessorType]?: {
    field: string;
    if?: string;
    on_failure?: EsPipelineProcessor[];
    ignore_failure?: boolean;
    [key: string]: any;
  };
};

export interface EsPipeline {
  description: string;
  processors: EsPipelineProcessor[];
  on_failure?: EsPipelineProcessor[];
}
