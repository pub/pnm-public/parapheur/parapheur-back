import { Injectable } from "@nestjs/common";
import { Client } from "@elastic/elasticsearch";
import { IndexParams, UpdateIndexParams } from "./interfaces/index-params.interface";
import { SearchQueryParams, SortCriteria } from "./interfaces/search-query.interface";
import { IndexMapping, IndexSettings, Properties } from "./interfaces/mapping.interface";
import { EsPipeline } from "./interfaces/pipeline.interface";
import { ElasticSearchConfig } from "./elasticsearch.config";
import { Index } from "./interfaces/index.interface";

@Injectable()
export class ElasticsearchService {
  client: Client;

  constructor(esConfig: ElasticSearchConfig) {
    this.client = new Client({
      node: esConfig.host,
    });
  }

  getHealth() {
    return this.client.cat.health({ format: "json" });
  }

  async listIndices() {
    return (
      await this.client.cat.indices({
        format: "JSON",
      })
    ).body as Index[];
  }

  async count(index: string, query?: object): Promise<number> {
    const result = await this.client.count({ index, body: query ? { query } : undefined });
    return result.body.count;
  }

  putPipeline(id: string, pipelineDef: EsPipeline) {
    return this.client.ingest.putPipeline({
      id,
      body: pipelineDef,
    });
  }

  deletePipeline(id: string) {
    return this.client.ingest.deletePipeline({
      id,
    });
  }

  async getPipeline(id: string): Promise<EsPipeline> {
    try {
      const response = await this.client.ingest.getPipeline({
        id,
      });

      return response.body as EsPipeline;
    } catch (e) {
      return null;
    }
  }

  getDocument(id: string, index: string) {
    return this.client.get({ id, index });
  }

  deleteDocument(id: string, index: string) {
    return this.client.delete({ id, index, refresh: true });
  }

  search<T = SearchQueryParams>(query: T, index: string, pagination: { from: number; size: number }) {
    return this.client.search({ index, body: query, ...pagination });
  }

  scroll(scrollId: string) {
    return this.client.scroll({
      scroll: "1h",
      scroll_id: scrollId,
    });
  }

  getAll(index: string, offset: number, limit: number, sort: SortCriteria[] = [{ id: { order: "desc" } }]) {
    return this.client.search({
      index,
      body: {
        query: {
          match_all: {},
        },
        from: offset,
        size: limit,
        sort,
      },
    });
  }

  index(params: IndexParams) {
    return this.client.index({
      ...params,
      refresh: "wait_for",
    });
  }

  /**
   * Partial indexation of document (PATCH)
   */
  updateDocument(params: UpdateIndexParams) {
    return this.client.update({ ...params, body: { doc: params.body } });
  }

  createIndex(indexName: string, mappings: IndexMapping, settings?: IndexSettings) {
    return this.client.indices.create({
      index: indexName,
      body: {
        mappings,
        settings,
      },
    });
  }

  updateSettings(indexName: string, settings: IndexSettings) {
    return this.client.indices.putSettings({
      index: indexName,
      body: { settings },
    });
  }

  updateMapping(indexName: string, properties: Properties) {
    return this.client.indices.putMapping({
      index: indexName,
      body: { properties },
    });
  }

  setAlias(index: string, alias: string) {
    return this.client.indices.putAlias({
      index,
      name: alias,
    });
  }

  async deleteAlias(alias: string, index?: string) {
    return this.client.indices.deleteAlias({
      index: index || (await this.getAliasTarget(alias)).target,
      name: alias,
    });
  }

  async deleteIndex(index: string) {
    return this.client.indices.delete({
      index,
    });
  }

  async startReindex(oldIndex: string, newIndex: string) {
    return this.client.reindex({
      body: {
        source: { index: oldIndex },
        dest: { index: newIndex },
      },
      wait_for_completion: false,
    });
  }

  async getTaskStatus(taskId: string) {
    return this.client.tasks.get({
      task_id: taskId,
    });
  }

  async getAliasTarget(alias: string) {
    try {
      const result = await this.client.indices.getAlias({
        name: alias,
      });
      return {
        name: alias,
        target: Object.keys(result.body)[0],
      };
    } catch (e) {
      if (((e?.meta?.body?.error as string) || "").match(/alias \[.*\] missing/)) {
        return {
          name: alias,
          target: null,
        };
      }

      throw e;
    }
  }

  getMapping(index: string) {
    return this.client.indices.getMapping({ index });
  }
}
