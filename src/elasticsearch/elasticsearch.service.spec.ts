import { Test, TestingModule } from "@nestjs/testing";
import { ElasticSearchConfig } from "./elasticsearch.config";
import { ElasticsearchService } from "./elasticsearch.service";

describe("ElasticsearchService", () => {
  let service: ElasticsearchService;
  const esConfig: ElasticSearchConfig = { host: "http://localhost:9200" };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ElasticsearchService, { provide: ElasticSearchConfig, useValue: esConfig }],
    }).compile();

    service = module.get<ElasticsearchService>(ElasticsearchService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
