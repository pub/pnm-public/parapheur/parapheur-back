import { getRepositoryToken } from "@nestjs/typeorm";
import { AbstractNestCommand, Command } from "@pasta/cli";
import { Repository } from "typeorm";
import { Argv } from "yargs";
import { LetterFile } from "../entities/letter-file.entity";
import { StorageService } from "@pasta/back-files";
import { ElasticsearchService } from "../elasticsearch/elasticsearch.service";
import { timeout } from "rxjs";
import { searchSchema, indexSettings } from "../search-schema";
import { searchIndexName, searchAliasName } from "../utils";
import { UserPreferences } from "../entities/user-preferences.entity";
import { LetterFileExport } from "../entities/export.entity";

@Command
export class ResetParapheur extends AbstractNestCommand {
  name = "reset-parapheur";
  description = "Removes all letter files, keeps users";

  arguments(yargs: Argv<{}>) {
    return yargs;
  }

  async main(_argv: any): Promise<any> {
    if(!["test", "dev"].includes(process.env.NODE_ENV)) {
      throw new Error("This command can only be run in test / dev mode, please set NODE_ENV to test or dev");
    }

    const letterFileRepo: Repository<LetterFile> = this.nest.get(getRepositoryToken(LetterFile));
    const storageService = this.nest.get(StorageService);
    const esService = this.nest.get(ElasticsearchService);
    const preferencesRepo: Repository<UserPreferences> = this.nest.get(getRepositoryToken(UserPreferences));
    const exportsRepo: Repository<LetterFileExport> = this.nest.get(getRepositoryToken(LetterFileExport));

    await letterFileRepo.createQueryBuilder("remove-all").delete().execute();
    this.print("Removed all letter files");
    await exportsRepo.createQueryBuilder("remove-all").delete().execute();
    this.print("Removed all exports");
    await storageService.removeObjectsInDir("", true);
    this.print("Removed all storage objects");
    await preferencesRepo.update({}, { signatures: [] });
    this.print("Removed all signatures in user preferences");

    // es
    await esService.deleteIndex(searchIndexName);
    this.print(`Deleted index ${searchIndexName}`);
    await timeout(1000);
    await esService.createIndex(searchIndexName, searchSchema, indexSettings);
    await esService.setAlias(searchIndexName, searchAliasName);
    this.print(`Recreated index ${searchIndexName}`);
  }
}
