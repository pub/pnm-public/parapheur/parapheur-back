import { getRepositoryToken } from "@nestjs/typeorm";
import { AbstractNestCommand, Command } from "@pasta/cli";
import { Argv } from "yargs";
import { LetterFileExport } from "../entities/export.entity";
import { StorageService } from "@pasta/back-files";
import { LessThan, Repository } from "typeorm";
import { asDuration } from "@pasta/back-core";
import dayjs, { Dayjs } from "dayjs";
import { LetterFile } from "../entities/letter-file.entity";
import { archiveBasePath } from "../services/archive.service";

@Command
export class RemoveOldStorageObjects extends AbstractNestCommand {
  name = "remove-old-storage-objects";
  description = "remove old archives or exports that use up needed space";

  arguments(yargs: Argv<{}>) {
    return yargs
      .option("A", {
        alias: "archives",
        boolean: true,
        default: false,
      })
      .option("E", {
        alias: "exports",
        boolean: true,
        default: false,
      })
      .option("d", {
        alias: "durationBack",
        describe:
          "the duration to substract from now, expressed in the same way we do env vars (asDuration ..), example: 30d",
        demandOption: true,
        coerce: (durationBack: string) => {
          return dayjs().subtract(asDuration(durationBack, "minute"), "minute");
        },
      })
      .check(argv => {
        if (!argv.A && !argv.E) {
          throw new Error("please provide either -E or -A option");
        }
        return true;
      });
  }

  async main(argv: any): Promise<any> {
    const lfRepo: Repository<LetterFile> = this.nest.get(getRepositoryToken(LetterFile));
    const exportRepo: Repository<LetterFileExport> = this.nest.get(getRepositoryToken(LetterFileExport));
    const storageService = this.nest.get(StorageService);

    const dateBefore = (argv.d as Dayjs).toDate();
    const humanTime = (argv.d as Dayjs).format("DD/MM/YYYY - HH:mm:ss");
    if (argv.E) {
      this.print(`Removing exports created before ${humanTime}`);
      const exports = await exportRepo.find({ where: { creationDate: LessThan(dateBefore) } });
      this.print(`Found ${exports.length} exports to remove`);
      for (const theExport of exports) {
        this.print(`removing ${theExport.fileKey}`);
        await storageService.removeObject(theExport.fileKey);
      }
      this.print("Cleaning db");
      if (exports.length > 0) {
        await exportRepo.delete(exports.map(exp => exp.id));
      }
      this.print("finished removing exports");
    }

    if (argv.A) {
      const letterFiles = await lfRepo.find({
        where: { archived: true, modificationDate: LessThan(dateBefore) },
      });
      for (const lf of letterFiles) {
        const objectPath = `${archiveBasePath}/${lf.id}/`;
        this.print(`Removing ${objectPath}`);
        await storageService.removeObjectsInDir(objectPath, true);
      }
      this.print("finished removing archives");
    }

    this.print("done");
  }
}
