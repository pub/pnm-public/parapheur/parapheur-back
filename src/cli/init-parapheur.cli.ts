import { getRepositoryToken } from "@nestjs/typeorm";
import { AbstractNestCommand, Command } from "@pasta/cli";
import { Repository } from "typeorm";
import { Argv } from "yargs";
import { LetterFileTag } from "../entities/letter-file-tag.entity";
import { difference } from "lodash";

@Command
export class InitParapheur extends AbstractNestCommand {
  name = "init";
  description = "Initializes parapheur on a fresh new install";

  arguments(yargs: Argv<{}>) {
    return yargs;
  }

  async main(_argv: any): Promise<any> {
    const tagsRepo: Repository<LetterFileTag> = this.nest.get(getRepositoryToken(LetterFileTag));

    const tags = ["Urgent", "Confidentiel", "Ministre", "Parlement", "Collectivité", "Préfet", "Signé"];
    const dbTags = await tagsRepo.find();
    const toCreate = difference(
      tags,
      dbTags.map(tag => tag.name)
    );
    await tagsRepo.save(toCreate.map(tag => ({ name: tag })));
  }
}
