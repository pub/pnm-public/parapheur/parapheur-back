import * as path from "path";
import { getRepositoryToken } from "@nestjs/typeorm";
import { AbstractNestCommand, Command } from "@pasta/cli";
import { IsNull, Repository } from "typeorm";
import { Argv } from "yargs";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";

@Command
export class SetDocExtensions extends AbstractNestCommand {
  name = "set-doc-extensions";
  description = "Sets 'extension' column for all documents";

  arguments(yargs: Argv<{}>) {
    return yargs;
  }

  async main(_argv: any): Promise<any> {
    const documentsRepo: Repository<LetterFileDocument> = this.nest.get(getRepositoryToken(LetterFileDocument));

    const docs = await documentsRepo.findBy({ type: LetterFileDocumentType.DEFAULT, extension: IsNull() });
    this.print(`Nb to migrate: ${docs.length}`);

    let remaining = docs.length;
    for (const doc of docs) {
      const parsed = path.parse(doc.name);
      await documentsRepo.update(doc.id, { name: parsed.name, extension: parsed.ext.toLowerCase() });
      remaining = remaining - 1;
      this.print(`Treated ${doc.id} (${remaining} remaining)`);
    }
  }
}
