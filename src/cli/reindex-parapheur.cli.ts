import { getRepositoryToken } from "@nestjs/typeorm";
import { AbstractNestCommand, Command } from "@pasta/cli";
import { FindOptionsWhere, Repository } from "typeorm";
import { Argv } from "yargs";
import { LetterFile } from "../entities/letter-file.entity";
import { LetterFileIndexationService } from "../services/letter-file-indexation.service";

@Command
export class InitParapheur extends AbstractNestCommand {
  name = "reindex-parapheur";
  description = "Réindexation complète d'un parapheur";
  relations = ["tags", "comments", "folders", "folders.documents", "upstream", "downstream", "ccUsers"];

  arguments(yargs: Argv<{}>) {
    return yargs
      .option("c", {
        alias: "chrono",
        demandOption: false,
        string: true,
        describe: "the target's chronoNumber",
        array: false,
      })
      .option("i", {
        alias: "id",
        demandOption: false,
        number: true,
        describe: "the target's id",
        array: false,
      })
      .option("A", {
        alias: "all",
        demandOption: false,
        boolean: true,
        describe: "reindex all letterfiles",
        array: false,
      })
      .check(argv => {
        if (!argv.id && !argv.chrono && !argv.A) {
          throw new Error("You must pass either id, chrono number or -A option");
        }
        return true;
      });
  }

  async main(argv: any): Promise<any> {
    const repo: Repository<LetterFile> = this.nest.get(getRepositoryToken(LetterFile));
    const indexationService = this.nest.get(LetterFileIndexationService);

    if (argv.A) {
      await this.reindexAll(repo, indexationService);
    } else if (argv.c || argv.i) {
      await this.reindexOne(argv, repo, indexationService);
    }
  }

  async reindexOne(argv: any, repo: Repository<LetterFile>, indexationService: LetterFileIndexationService) {
    let where: FindOptionsWhere<LetterFile>;
    if (argv.i) {
      where = { id: argv.i };
    }
    if (argv.c) {
      where = { chronoNumber: argv.c };
    }

    const letterFile = await repo.findOne({
      where,
      relations: this.relations,
    });
    if (!letterFile) {
      throw new Error("Letter file not found");
    }
    this.print(`Full reindexation for letter file ${letterFile.chronoNumber} (id: ${letterFile.id})`);
    await indexationService.indexFull(letterFile);

    this.print("done");
  }

  async reindexAll(repo: Repository<LetterFile>, indexationService: LetterFileIndexationService) {
    let start = 0;
    const limit = 50;
    const total = await repo.count();
    this.print(`total: ${total}`);

    do {
      const letterFiles = await repo.find({
        skip: start,
        take: limit,
        relations: this.relations,
        order: { id: "ASC" },
      });
      this.print(`Treating batch ${start / limit + 1} / ${Math.ceil(total / limit)}`);
      for (const lf of letterFiles) {
        this.print(`Reindexing ${lf.id}`);
        await indexationService.indexFull(lf);
      }
      start += letterFiles.length;
    } while (total - start > 0);
  }
}
