import { getRepositoryToken } from "@nestjs/typeorm";
import { AbstractNestCommand, Command } from "@pasta/cli";
import { Argv } from "yargs";
import { LetterFile } from "../entities/letter-file.entity";
import { In, Repository } from "typeorm";

@Command
export class RemoveLetterFiles extends AbstractNestCommand {
  name = "remove-letter-files";
  description = "safely delete letter files by chrono or id";

  arguments(yargs: Argv<{}>) {
    return yargs
      .option("c", {
        alias: "chrono",
        demandOption: false,
        string: true,
        describe: "a list of chrono numbers, separated by whitespace",
        array: true,
      })
      .option("i", {
        alias: "id",
        demandOption: false,
        number: true,
        describe: "a list of ids, separated by whitespace",
        array: true,
      })
      .check(argv => {
        if (!argv.id && !argv.chrono) {
          throw new Error("You must pass either ids or chrono numbers using appropriate options");
        }
        return true;
      })
      .coerce("i", ids => {
        return ids.filter(id => !Number.isNaN(id));
      });
  }

  async main(argv: any): Promise<any> {
    const repo: Repository<LetterFile> = this.nest.get(getRepositoryToken(LetterFile));

    if (argv.i) {
      this.print(`Removing ${argv.i.length} letter files by id...`);
      await repo.softDelete(argv.i);
    }

    if (argv.c) {
      this.print(`Removing ${argv.c.length} letter files by chrono`);
      const results = await repo.find({ where: { chronoNumber: In(argv.c) }, select: ["id"] });
      this.print(`Found ${results.length} matching letter files, removing them safely...`);
      await repo.softDelete(results.map(r => r.id));
    }

    this.print("done");
  }
}
