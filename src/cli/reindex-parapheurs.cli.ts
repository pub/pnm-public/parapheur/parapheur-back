import { AbstractNestCommand, Command } from "@pasta/cli";
import { Argv } from "yargs";
import { ElasticsearchService } from "../elasticsearch/elasticsearch.service";
import { searchAliasName } from "../utils";
import { indexSettings, searchSchema } from "../search-schema";
import { last } from "lodash";
import { sleep } from "@pasta/back-files";

@Command
export class InitParapheur extends AbstractNestCommand {
  name = "reindex-parapheurs";
  description = "Réindexation de tous les parapheurs";

  arguments(yargs: Argv<{}>) {
    return yargs;
  }

  async main(_argv: any): Promise<any> {
    const esService = this.nest.get(ElasticsearchService);

    const oldIndex = (await esService.getAliasTarget(searchAliasName)).target;
    const oldVersion = parseInt(last(oldIndex.split("-v")));
    const newVersion = oldVersion + 1;
    const newIndex = `${searchAliasName}-v${newVersion}`;
    await esService.createIndex(newIndex, searchSchema, indexSettings);
    this.print(`Created index ${newIndex}`);
    await sleep(1000);
    this.print(`Reindexing ${oldIndex} -> ${newIndex}`);
    const taskId = (await esService.startReindex(oldIndex, newIndex)).body.task;

    await this.completeReindexation(taskId, newVersion, oldIndex, newIndex, esService);
    this.print(`Finished reindex ${oldIndex} -> ${newIndex}`);
  }

  public async completeReindexation(
    taskId: string,
    newVersion: number,
    oldIndex: string,
    newIndex: string,
    esService: ElasticsearchService
  ) {
    const result = await esService.getTaskStatus(taskId);

    if (!result.body.completed) {
      this.print(`Reindexation in progress`);
      await sleep(15_000);
      return this.completeReindexation(taskId, newVersion, oldIndex, newIndex, esService);
    }
    this.print(`Reindexation completed. Alias migration`);
    await Promise.all([esService.setAlias(newIndex, searchAliasName), esService.deleteIndex(oldIndex)]);
  }
}
