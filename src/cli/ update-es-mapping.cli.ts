import { AbstractNestCommand, Command } from "@pasta/cli";
import { Argv } from "yargs";
import { ElasticsearchService } from "../elasticsearch/elasticsearch.service";
import { searchSchema } from "../search-schema";
import { searchAliasName } from "../utils";

@Command
export class InitParapheur extends AbstractNestCommand {
  name = "update-es-mapping";
  description = "Updates ES mapping";

  arguments(yargs: Argv<{}>) {
    return yargs;
  }

  async main(argv: any): Promise<any> {
    const esService = this.nest.get(ElasticsearchService);

    try {
      await esService.updateMapping(searchAliasName, searchSchema.properties);
      this.print("done");
    } catch (e) {
      this.printError("Failed to update mapping", true);
      this.printError(e, true);
    }
  }

  async migrate() {
    const esService = this.nest.get(ElasticsearchService);
    const indices = await esService.listIndices();
    // TODO utiliser le noticeReindexationService
  }
}
