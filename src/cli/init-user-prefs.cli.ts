import { getRepositoryToken } from "@nestjs/typeorm";
import { AbstractNestCommand, Command } from "@pasta/cli";
import { Repository } from "typeorm";
import { Argv } from "yargs";
import { LetterFileColor, MailingFrequency, UserPreferences } from "../entities/user-preferences.entity";
import { User } from "@pasta/back-auth";
import { LdapService } from "../services/ldap.service";

@Command
export class InitParapheur extends AbstractNestCommand {
  name = "init-user-prefs";
  description = "Initializes user prefs (one time script to be removed)";

  arguments(yargs: Argv<{}>) {
    return yargs;
  }

  async main(_argv: any): Promise<any> {
    const userRepo: Repository<User> = this.nest.get(getRepositoryToken(User));
    const userPrefRepo: Repository<UserPreferences> = this.nest.get(getRepositoryToken(UserPreferences));
    const ldapService = this.nest.get(LdapService);

    const results = await userRepo
      .createQueryBuilder("user")
      .select()
      .leftJoinAndSelect(UserPreferences, "userpref", "userpref.login = user.login")
      .where("userpref.job is null")
      .orWhere("userpref.city is null")
      .orWhere("userpref.job = ''")
      .orWhere("userpref.city = ''")
      .getRawMany();

    this.print(`Total to do: ${results.length}`);
    for (const res of results) {
      this.print(`Treating ${res.user_login}`);
      const ldapEntry = await ldapService.lookup(res.user_login);
      if (res.userpref_id) {
        // found user pref but no job/city, update it
        this.print("update");
        await userPrefRepo.update(res.userpref_id, {
          job: ldapEntry.description || "",
          city: ldapEntry.city || "",
        });
      } else {
        // no user prefs found, create one
        this.print("create");
        await userPrefRepo.insert({
          login: res.user_login,
          mailingFrequency: MailingFrequency.REAL_TIME,
          onboardingSeen: false,
          signatures: [],
          color: LetterFileColor.RED,
          job: ldapEntry.description || "",
          city: ldapEntry.city || "",
        });
      }
    }

    this.print("done");
  }
}
