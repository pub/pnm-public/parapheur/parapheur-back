import { IndexMapping, IndexSettings } from "./elasticsearch/interfaces/mapping.interface";

export const searchSchema: IndexMapping = {
  dynamic: false,
  properties: {
    id: { type: "integer" },
    creationDate: { type: "date" },
    lastIndexedAt: { type: "date" },
    name: { type: "text", analyzer: "lowercase_ascii" },
    chronoNumber: { type: "keyword" },
    deadline: { type: "date" },
    arrivalDate: { type: "date" },
    externalMailNumber: { type: "keyword" },
    draft: { type: "boolean" },
    archived: { type: "boolean" },
    archivedDate: { type: "date" },
    tags: { type: "keyword" },
    creator: {
      properties: {
        mail: { type: "keyword", normalizer: "lowercase_ascii_normalizer" },
        name: { type: "text", analyzer: "lowercase_ascii" },
      },
    },
    current: {
      properties: {
        mail: { type: "keyword", normalizer: "lowercase_ascii_normalizer" },
        name: { type: "text", analyzer: "lowercase_ascii" },
      },
    },
    // Unlike data stored in SQL, downstream in ES only contains REAL downstream elements (not including CURRENT)
    downstream: {
      properties: {
        mail: { type: "keyword", normalizer: "lowercase_ascii_normalizer" },
        name: { type: "text", analyzer: "lowercase_ascii" },
      },
    },
    upstream: {
      properties: {
        mail: { type: "keyword", normalizer: "lowercase_ascii_normalizer" },
        name: { type: "text", analyzer: "lowercase_ascii" },
      },
    },
    ccUsers: {
      properties: {
        mail: { type: "keyword", normalizer: "lowercase_ascii_normalizer" },
        name: { type: "text", analyzer: "lowercase_ascii" },
      },
    },
    relatedUsers: {
      properties: {
        mail: { type: "keyword", normalizer: "lowercase_ascii_normalizer" },
      },
    },
    touchedDates: {
      type: "date",
    },
    comments: { type: "text", analyzer: "lowercase_ascii" },
    folders: { type: "text", analyzer: "lowercase_ascii" },
    documents: {
      properties: {
        name: {
          type: "text",
          analyzer: "lowercase_ascii",
          fields: {
            keyword: {
              type: "keyword",
            },
          },
        },
      },
    },
    senderOrganization: { type: "text", analyzer: "lowercase_ascii" },
    senderName: { type: "text", analyzer: "lowercase_ascii" },
    senderRole: { type: "text", analyzer: "lowercase_ascii" },
    mailType: { type: "text", analyzer: "lowercase_ascii" },
    recipientName: { type: "text", analyzer: "lowercase_ascii" },
    manager: { type: "text", analyzer: "lowercase_ascii" },
  },
};

/**
 * A letterFile indexed in ES
 */
export interface IndexedLetterFile {
  id: number;
  creationDate: Date;
  lastIndexedAt: Date;
  touchedDates: string[];
  name: string;
  chronoNumber: string;
  deadline: Date;
  arrivalDate: Date;
  externalMailNumber: string;
  draft: boolean;
  archived: boolean;
  archivedDate?: Date;
  tags: string[];
  creator: { mail: string; name: string };
  current: { mail: string; name: string };
  upstream: { mail: string; name: string }[];
  downstream: { mail: string; name: string }[];
  relatedUsers: { mail: string }[];
  ccUsers: { mail: string; name: string }[];
  comments: string[];
  folders: string[];
  documents: { name: string }[];
  senderOrganization: string;
  senderName: string;
  senderRole: string;
  mailType: string;
  recipientName: string;
  manager: string;
}

export const indexSettings: IndexSettings = {
  analysis: {
    char_filter: {
      quote: {
        type: "mapping",
        mappings: ['« => "', '» => "'],
      },
    },
    normalizer: {
      lowercase_ascii_normalizer: {
        type: "custom",
        char_filter: ["quote"],
        filter: ["lowercase", "asciifolding"],
      },
    },
    analyzer: {
      lowercase_ascii: {
        type: "custom",
        tokenizer: "custom_tokenizer",
        char_filter: [],
        filter: ["lowercase", "asciifolding"],
      },
    },
    tokenizer: {
      custom_tokenizer: {
        type: "whitespace",
      },
    },
  },
};
