export class LdapError extends Error {}

export class LdapUnknownMailError extends LdapError {}
export class LdapInvalidMailError extends LdapError {}
