export class LetterFileError extends Error {}

export class UndraftError extends LetterFileError {}

export class FluxError extends LetterFileError {}

export class RootFolderError extends LetterFileError {}

export class TakeError extends LetterFileError {}
export class TakeDraftError extends TakeError {}
export class TakeArchivedError extends TakeError {}

export class NextError extends LetterFileError {}

export class RecallError extends LetterFileError {}
export class RecallArchivedError extends RecallError {}
export class RecallDraftError extends RecallError {}
