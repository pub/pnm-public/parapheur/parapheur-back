export class UserPreferencesError extends Error {}
export class HideBeforeError extends UserPreferencesError {}
