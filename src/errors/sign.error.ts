export class SignError extends Error {}
export class InvalidPageError extends SignError {}
