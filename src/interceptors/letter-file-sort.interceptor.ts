import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { GetManyResult } from "@pasta/back-core";
import { orderBy, sortBy } from "lodash";
import { map, Observable } from "rxjs";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import { LetterFile } from "../entities/letter-file.entity";
import { LetterFileHistory } from "../entities/letter-file-history.entity";

@Injectable()
export class LetterFileSortInterceptor implements NestInterceptor {
  interceptOne(data: LetterFile): LetterFile {
    return {
      ...data,
      upstream: this.sortArray(data.upstream, "position"),
      downstream: this.sortArray(data.downstream, "position"),
      comments: this.sortArray(data.comments, "creationDate"),
      tags: this.sortArray(data.tags, "id"),
      folders: this.sortFolders(data.folders),
      history: this.sortHistory(data.history),
    };
  }

  intercept(_context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
    return next.handle().pipe(
      map((data: LetterFile | GetManyResult<LetterFile>) => {
        if (!data) {
          return data;
        }

        if (this.isMany(data)) {
          return {
            ...data,
            data: data.data.map((letterFile: LetterFile) => this.interceptOne(letterFile)),
          };
        }

        if (this.isSingle(data)) {
          return this.interceptOne(data);
        }

        return data;
      })
    );
  }

  isMany(data: LetterFile | GetManyResult<LetterFile>): data is GetManyResult<LetterFile> {
    return !!(data as GetManyResult<LetterFile>).data;
  }

  isSingle(data: LetterFile | GetManyResult<LetterFile>): data is LetterFile {
    return !!(data as LetterFile).id;
  }

  sortArray<T>(items: T[] | undefined, sortingKey: keyof T) {
    return items ? sortBy(items, sortingKey) : items;
  }

  sortFolders(folders?: LetterFileFolder[]) {
    if (!folders) {
      return folders;
    }

    return sortBy(
      folders.map(folder => {
        return {
          ...folder,
          documents: folder.documents ? sortBy(folder.documents, "position") : folder.documents,
        };
      }),
      "position"
    );
  }

  sortHistory(history: LetterFileHistory[]) {
    if (!history) {
      return history;
    }

    return orderBy(history, "creationDate", "desc");
  }
}
