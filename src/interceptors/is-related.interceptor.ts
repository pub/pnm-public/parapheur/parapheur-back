import {
  BadRequestException,
  CallHandler,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  NestInterceptor,
} from "@nestjs/common";
import { Observable } from "rxjs";
import { UserService } from "@pasta/back-auth";
import { LetterFileIndexationService } from "../services/letter-file-indexation.service";
import { LetterFileService } from "../services/letter-file.service";
import { PureAbility } from "@casl/ability";
import { LdapService } from "../services/ldap.service";
import { intersection } from "lodash";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { LetterFileDocumentService } from "../services/letter-file-document.service";

@Injectable()
export class IsRelatedInterceptor implements NestInterceptor {
  @InjectLogger("default", IsRelatedInterceptor.name)
  logger: PastaLogger;

  constructor(
    private readonly indexationService: LetterFileIndexationService,
    private readonly letterFileService: LetterFileService,
    private readonly userService: UserService,
    private readonly ldapService: LdapService,
    private readonly documentService: LetterFileDocumentService
  ) {}

  async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const ability: PureAbility = request.ability;

    let lfId = request.params.letterFileId || request.params.id || null;
    const chrono = request.params.chronoNumber;
    if (chrono) {
      lfId = (
        await this.letterFileService.getMany({ select: ["id"], filter: { chronoNumber: { $eq: chrono } } })
      ).data.at(0).id;
    }
    if (request.params.documentId) {
      const document = await this.documentService.getOne(
        { join: ["folder", "folder.letterFile"] },
        parseInt(request.params.documentId)
      );
      lfId = document.folder.letterFile.id;
    }
    if (!lfId) throw new BadRequestException("Letter file id is invalid");

    const { login } = await this.userService.getOne({ select: ["login"] }, request.user);
    this.logger.debug(`User ${login} is requesting letterFile ${lfId}`);
    const { relatedMails: requesterRelatedMails } = await this.ldapService.lookup(login);
    this.logger.debug(`User ${login} is related to ${requesterRelatedMails.join(", ")}`);

    const { relatedUsers } = await this.indexationService.getOne(lfId);
    this.logger.debug(`LetterFile ${lfId} is related to ${relatedUsers.map(el => el.mail).join(", ")}`);
    if (
      intersection(
        relatedUsers.map(el => el.mail),
        [login, ...requesterRelatedMails]
      ).length <= 0 &&
      ability.cannot("manage", "all")
    ) {
      throw new ForbiddenException(`Operation not permitted, user is not related to letterFile ${lfId}`);
    }
    return next.handle();
  }
}
