import {
  BadRequestException,
  CallHandler,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  NestInterceptor,
} from "@nestjs/common";
import { Observable } from "rxjs";
import { FluxElementService } from "../services/flux-element.service";
import { UserService } from "@pasta/back-auth";
import { LdapService } from "../services/ldap.service";
import { WopiService } from "../services/wopi.service";
import { LetterFileDocumentService } from "../services/letter-file-document.service";

@Injectable()
export class IsCurrentInterceptor implements NestInterceptor {
  constructor(
    private readonly fluxElementService: FluxElementService,
    private readonly userService: UserService,
    private readonly ldapService: LdapService,
    private readonly wopiService: WopiService,
    private readonly documentService: LetterFileDocumentService
  ) {}

  async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    let lfId = request.params.letterFileId || request.params.id;
    if (request.params.documentId) {
      const document = await this.documentService.getOne(
        { join: ["folder", "folder.letterFile"] },
        parseInt(request.params.documentId)
      );
      lfId = document.folder.letterFile.id;
    }
    if (!lfId) throw new BadRequestException("Letter file id is invalid");

    const current = await this.fluxElementService.getCurrent(lfId);
    if (!current) throw new BadRequestException("Changes not permitted on archived letter file");

    let userId: number = request.user;
    if (!userId && request.query.access_token) {
      const tokenData = await this.wopiService.getTokenData(request.query.access_token);
      if (!tokenData) throw new ForbiddenException("Provided token is invalid or expired");
      userId = tokenData.userId;
      request.user = userId;
      request.tokenData = tokenData;
    }
    const user = await this.userService.getOne({}, userId);
    const { relatedMails: requesterRelatedMails } = await this.ldapService.lookup(user.login);
    if (![user.login, ...requesterRelatedMails].some(el => el === current.mail)) {
      throw new ForbiddenException(`Operation not permitted, user is not current for letterFile ${lfId}`);
    }
    return next.handle();
  }
}
