import { NestInterceptor, ExecutionContext, CallHandler, ForbiddenException, Injectable } from "@nestjs/common";
import { Observable } from "rxjs";
import { LetterFileCCUserService } from "../services/letter-file-cc-user.service";

@Injectable()
export class IsCCUserFrozenInterceptor implements NestInterceptor {
  constructor(private readonly ccUserService: LetterFileCCUserService) {}

  async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const { params } = request;

    const ccUser = await this.ccUserService.getOne({}, params.id);

    if (ccUser?.frozen) {
      throw new ForbiddenException("This comment is frozen");
    }

    return next.handle();
  }
}
