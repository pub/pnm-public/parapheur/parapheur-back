import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { Observable, mergeMap } from "rxjs";
import { LetterFileSuggestableField } from "../entities/Suggestion.entity";
import { intersection, isEqual, uniq } from "lodash";
import { SuggestionService } from "../services/suggestion.service";

const supportedFields = Object.keys(LetterFileSuggestableField);

@Injectable()
export class UpdateSuggestionsInterceptor implements NestInterceptor {
  constructor(private readonly suggestionService: SuggestionService) {}

  intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
    return next.handle().pipe(
      mergeMap(async data => {
        const request = context.switchToHttp().getRequest();
        const modifiedFields = Object.keys(request.body);
        const fields = intersection(supportedFields, modifiedFields) as LetterFileSuggestableField[];
        if (fields.length) {
          const userId: number = request.user;
          for (const field of fields) {
            const suggestion = await this.suggestionService.findOne({
              where: { user: { id: userId }, fieldType: field as any },
              relations: ["user"],
            });
            if (suggestion) {
              const newVal = request.body[field];
              if (!suggestion.values.includes(newVal) && newVal) {
                const values: string[] = uniq([newVal, ...suggestion.values]).slice(0, 50);
                if (!isEqual(values, suggestion.values)) {
                  await this.suggestionService.updateOne({}, suggestion.id, { values });
                }
              }
            } else {
              await this.suggestionService.createOne(
                {},
                { user: { id: userId }, fieldType: field, values: [request.body[field]] }
              );
            }
          }
        }
        return data;
      })
    );
  }
}
