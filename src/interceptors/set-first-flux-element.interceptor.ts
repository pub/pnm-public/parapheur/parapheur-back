import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { UserService } from "@pasta/back-auth";
import { Observable } from "rxjs";

@Injectable()
export class SetFirstFluxElementInterceptor implements NestInterceptor {
  // Request isn't available in Pipes, so we use an interceptor instead

  constructor(private readonly userService: UserService) {}

  async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const userId = request.user;
    const body = request.body;

    const user = await this.userService.findOneBy({ id: userId });

    request.body = { ...body, downstream: [user.login] };
    return next.handle();
  }
}
