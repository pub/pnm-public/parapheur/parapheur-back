import * as path from "path";
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { Observable } from "rxjs";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";

@Injectable()
export class SetDocumentExtensionInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const body = request.body;
    if (body.type === LetterFileDocumentType.DEFAULT || body.storageKey) {
      const parsed = path.parse(body.name);
      request.body = { ...body, name: parsed.name, extension: parsed.ext.toLowerCase() };
    }
    return next.handle();
  }
}
