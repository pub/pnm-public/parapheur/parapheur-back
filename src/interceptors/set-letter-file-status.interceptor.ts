import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { User, UserService } from "@pasta/back-auth";
import { GetManyResult, Nullable } from "@pasta/back-core";
import { mergeMap, Observable } from "rxjs";
import { DownstreamFluxElement } from "../entities/downstream-flux-element.entity";
import { LetterFile } from "../entities/letter-file.entity";
import { LetterFileService } from "../services/letter-file.service";
import { LetterFileStatus } from "../types/letter-file-status.enum";
import { LdapService } from "../services/ldap.service";
import { FluxElementService } from "../services/flux-element.service";

@Injectable()
export class SetLFStatusInterceptor implements NestInterceptor {
  constructor(
    private readonly userService: UserService,
    private readonly letterFileService: LetterFileService,
    private readonly ldapService: LdapService,
    private readonly fluxService: FluxElementService
  ) {}

  async interceptOne(
    data: LetterFile,
    user: User
  ): Promise<LetterFile & { status: LetterFileStatus; current: Nullable<DownstreamFluxElement> }> {
    const letterFile =
      data.upstream && data.downstream && data.ccUsers
        ? data
        : await this.letterFileService.getOne({ join: ["upstream", "downstream", "ccUsers"] }, data.id);

    const { relatedMails } = await this.ldapService.lookup(user.login);
    const current = letterFile.downstream.find(el => el.position === 0);

    const response = {
      ...data,
      status: this.letterFileService.getStatus(letterFile, relatedMails),
      current,
    };

    if (response.downstream?.length) {
      response.downstream = data.downstream.filter(el => el.position !== 0);
    }

    return response;
  }

  async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<any>> {
    const userId = context.switchToHttp().getRequest().user;
    const user = await this.userService.getOne({}, userId);
    return next.handle().pipe(
      mergeMap(async (data: LetterFile | GetManyResult<LetterFile>) => {
        if (!data) {
          return data;
        }

        if (this.isMany(data)) {
          return {
            ...data,
            data: await Promise.all(
              data.data.map((letterFile: LetterFile) => {
                return this.interceptOne(letterFile, user);
              })
            ),
          };
        }

        if (this.isSingle(data)) {
          return this.interceptOne(data, user);
        }

        return data;
      })
    );
  }

  isMany(data: LetterFile | GetManyResult<LetterFile>): data is GetManyResult<LetterFile> {
    return !!(data as GetManyResult<LetterFile>).data;
  }

  isSingle(data: LetterFile | GetManyResult<LetterFile>): data is LetterFile {
    return !!(data as LetterFile).id;
  }
}
