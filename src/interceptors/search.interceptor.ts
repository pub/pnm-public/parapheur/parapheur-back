import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { GetManyResult } from "@pasta/back-core";
import { Observable, map } from "rxjs";
import { LetterFile } from "../entities/letter-file.entity";
import { omit } from "lodash";

@Injectable()
export class SearchInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
    return next.handle().pipe(
      map((result: GetManyResult<LetterFile>) => {
        return { ...result, data: result.data.map(lf => omit(lf, "upstream", "downstream", "ccUsers", "folders")) };
      })
    );
  }
}
