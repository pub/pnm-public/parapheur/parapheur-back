import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { Observable } from "rxjs";

@Injectable()
export class SetUserInterceptor implements NestInterceptor {
  // Request isn't available in Pipes, so we use an interceptor instead
  intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    const body = request.body;
    request.body = { ...body, user: user };
    return next.handle();
  }
}
