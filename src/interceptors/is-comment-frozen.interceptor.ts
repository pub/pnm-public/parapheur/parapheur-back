import { NestInterceptor, ExecutionContext, CallHandler, ForbiddenException, Injectable } from "@nestjs/common";
import { Observable } from "rxjs";
import { LetterFileCommentService } from "../services/letter-file-comment.service";

@Injectable()
export class IsCommentFrozenInterceptor implements NestInterceptor {
  constructor(private readonly commentService: LetterFileCommentService) {}

  async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const { params } = request;

    const comment = await this.commentService.getOne({}, params.id);

    if (comment?.frozen) {
      throw new ForbiddenException("This comment is frozen");
    }

    return next.handle();
  }
}
