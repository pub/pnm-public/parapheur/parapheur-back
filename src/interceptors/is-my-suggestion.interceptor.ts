import { CallHandler, ExecutionContext, ForbiddenException, Injectable, NestInterceptor } from "@nestjs/common";
import { Observable } from "rxjs";
import { SuggestionService } from "../services/suggestion.service";

@Injectable()
export class IsMySuggestionInterceptor implements NestInterceptor {
  constructor(private readonly suggestionService: SuggestionService) {}

  async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();

    const userId = request.user;
    const suggestionId = request.params.id;
    const suggestion = await this.suggestionService.findOne({
      where: { id: suggestionId, user: { id: userId } },
      relations: ["user"],
    });

    if (!suggestion) {
      throw new ForbiddenException("Suggestion does not belong to current user");
    }

    return next.handle();
  }
}
