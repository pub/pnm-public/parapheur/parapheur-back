import { CallHandler, ExecutionContext, Injectable, NestInterceptor, NotImplementedException } from "@nestjs/common";
import { Observable } from "rxjs";
import { WopiConfig } from "../config/wopi.config";

@Injectable()
export class IsWopiEnabledInterceptor implements NestInterceptor {
  constructor(private readonly conf: WopiConfig) {}

  intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
    if (!this.conf.enabled) throw new NotImplementedException("Wopi protocol is not enabled on this instance");
    return next.handle();
  }
}
