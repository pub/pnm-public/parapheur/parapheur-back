import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { Deferred } from "@pasta/deferred";
import { Observable, catchError, mergeMap, tap } from "rxjs";
import { FluxElementService } from "../services/flux-element.service";
import { LetterFile } from "../entities/letter-file.entity";
import { sortBy, uniq } from "lodash";

@Injectable()
export class MutexInterceptor implements NestInterceptor {
  @InjectLogger("default", MutexInterceptor.name)
  logger: PastaLogger;

  constructor(private readonly fluxService: FluxElementService) {}

  stacks = new Map<number, { wait: Deferred[]; ongoingRequest: boolean }>();

  releaseMutex(id: number, idStack: { wait: Deferred[]; ongoingRequest: boolean }, val: any) {
    idStack.ongoingRequest = false;
    this.stacks.set(id, idStack);
    if (idStack.wait.length > 0) {
      const deferred = idStack.wait.shift();
      deferred.resolve(val);
    } else {
      this.stacks.delete(id); // To prevent memory leak
    }
  }

  async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<LetterFile>> {
    const request = context.switchToHttp().getRequest();
    const id = request.params.id;

    const idStack = this.stacks.get(id) || { wait: [], ongoingRequest: false };

    if (idStack.ongoingRequest) {
      this.logger.debug(`Request is ongoing on letter-file ${id}, waiting...`);
      const deferred = new Deferred();
      idStack.wait.push(deferred);
      this.stacks.set(id, idStack);
      await deferred;
      this.logger.debug(`Request is completed on letter-file ${id}, resuming...`);
    }

    idStack.ongoingRequest = true;
    this.stacks.set(id, idStack);

    return next.handle().pipe(
      catchError(async err => {
        this.releaseMutex(id, idStack, err);
        throw err;
      }),
      tap((value: LetterFile) => {
        this.releaseMutex(id, idStack, value);
      }),
      mergeMap(async value => {
        const downstream = await this.fluxService.getDownstream(value.id);
        const positions = downstream.map(el => el.position);
        const uniquePositions = uniq(positions);
        if (positions.length !== uniquePositions.length || uniquePositions.some(pos => pos < 0)) {
          // All positions aren't unique. So try to autofix it.
          this.logger.warn(`Letter-file ${id} has duplicate positions, trying to fix it...`);
          await this.fluxService.reorder(
            downstream,
            sortBy(downstream, "position").map(el => el.id)
          );
        }

        return value;
      })
    );
  }
}
