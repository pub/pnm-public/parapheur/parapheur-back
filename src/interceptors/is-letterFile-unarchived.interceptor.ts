import { BadRequestException, CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { MutexInterceptor } from "./letter-file-mutex.interceptor";
import { LetterFileService } from "../services/letter-file.service";
import { Observable } from "rxjs";
import { ModuleRef, Reflector } from "@nestjs/core";
import { HttpMethod, PASTA_CRUD_METADATA, PastaBaseEntity, PastaCrudConfig } from "@pasta/back-core";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { getRepositoryToken } from "@nestjs/typeorm";
import { LetterFile } from "../entities/letter-file.entity";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import dayjs from "dayjs";
import { isNil } from "lodash";
import { Repository } from "typeorm";
import { getDelayedType } from "@pasta/back-core/dist/core/crud/utils";
import { UNARCHIVED_DISALLOWED_KEY } from "../decorators/unarchived-disallowed.decorator";

@Injectable()
export class IsLetterFileUnarchivedInterceptor implements NestInterceptor {
  @InjectLogger("default", MutexInterceptor.name)
  logger: PastaLogger;

  constructor(
    private readonly letterFileService: LetterFileService,
    private readonly reflector: Reflector,
    private readonly moduleRef: ModuleRef
  ) {}

  async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const controller = context.getClass();
    const route = context.getHandler();
    const pastaCrudMetadata: PastaCrudConfig<any> = this.reflector.get(PASTA_CRUD_METADATA, controller);
    const EntityCls = getDelayedType(pastaCrudMetadata.model);

    const lfId = request.params.letterFileId || request.params.id;
    const lf = await this.letterFileService.getOne({ select: ["unArchivedDate"] }, lfId);

    if (!lf.unArchivedDate) return next.handle();

    const unarchivedDisallowed = this.reflector.get(UNARCHIVED_DISALLOWED_KEY, route);
    if (unarchivedDisallowed) {
      throw new BadRequestException("Cannot modify unarchived letter file.");
    }

    const unArchivedDate = dayjs(lf.unArchivedDate);
    if (EntityCls === LetterFile) {
      if (!isNil(request.body.name)) {
        throw new BadRequestException("Cannot modify name of unarchived letter file.");
      }
    } else if ([LetterFileFolder, LetterFileDocument].includes(EntityCls as any)) {
      const entityId = request.params.id;
      const repo: Repository<PastaBaseEntity> = this.moduleRef.get(getRepositoryToken(EntityCls), { strict: false });
      const entity = await repo.findOne({ where: { id: entityId }, select: ["creationDate"] });
      const creationDate = dayjs(entity.creationDate);
      if (creationDate.isBefore(unArchivedDate)) {
        throw new BadRequestException(`Cannot modify ${EntityCls.name} of unarchived letter file.`);
      }
    }

    return next.handle();
  }
}
