import { EnvPrefix, FromEnv } from "@pasta/back-core";
import { IsString, IsUrl } from "class-validator";

@EnvPrefix("SIGN_API_")
export class SignApiConf {
  @FromEnv("SECRET_KEY")
  @IsString()
  secretKey: string = "secret_key";

  @FromEnv("BASEURL")
  @IsString()
  @IsUrl({ require_tld: false })
  baseURL: string = "http://localhost:3001";
}
