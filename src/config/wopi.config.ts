import { ParseBoolPipe } from "@nestjs/common";
import { ConfDoc, EnvPrefix, FromEnv, ParseDurationPipe, UsePipeConf, asDuration } from "@pasta/back-core";
import { IsBoolean, IsNumber, IsString, IsUrl } from "class-validator";

@EnvPrefix("WOPI_")
export class WopiConfig {
  @FromEnv("ENABLED")
  @UsePipeConf(new ParseBoolPipe())
  @ConfDoc({ required: false, description: "enable wopi routes", examples: ["true", "false"] })
  @IsBoolean()
  enabled: boolean = false;

  @FromEnv("TOKEN_TTL")
  @UsePipeConf(new ParseDurationPipe("millisecond"))
  @ConfDoc({ required: false, description: "The ttl for wopi (collabora) tokens", examples: ["1h"] })
  @IsNumber()
  tokenTTL: number = asDuration("6h", "millisecond");

  @FromEnv("CLIENT_URL")
  @ConfDoc({ required: false, description: "The collabora container url" })
  @IsString()
  @IsUrl({ require_tld: false })
  clientUrl: string = "http://localhost:9980";
}
