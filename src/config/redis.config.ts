import { ParseIntPipe } from "@nestjs/common";
import { EnvPrefix, FromEnv, UsePipeConf } from "@pasta/back-core";
import { IsInt, IsString } from "class-validator";

@EnvPrefix("REDIS_")
export class RedisConfig {
  @FromEnv("HOST")
  @IsString()
  host: string = "localhost";

  @FromEnv("PORT")
  @UsePipeConf(new ParseIntPipe())
  @IsInt()
  port: number = 6379;
}
