import * as CacheManager from "cache-manager";
import * as redisStore from "cache-manager-redis-yet";
import { RedisConfig } from "./redis.config";

export const createCacheConfig = (redisConf: RedisConfig) => {
  return CacheManager.caching(redisStore.redisStore, {
    url: `redis://${redisConf.host}:${redisConf.port}`,
    database: 0,
    ttl: 0, // forever
  });
};
