import { EnvPrefix, FromEnv } from "@pasta/back-core";
import { IsOptional, IsString } from "class-validator";

@EnvPrefix("HEDWIGE_")
export class HedwigeConfig {
  @FromEnv("SOURCE_ADDRESS")
  @IsString()
  sourceAddress: string;

  @FromEnv("VERSION")
  @IsString()
  version: string;

  @FromEnv("URL_PREFIX")
  @IsOptional()
  @IsString()
  urlPrefix = "/api/hedwige/";
}
