import { ParseBoolPipe, ParseIntPipe } from "@nestjs/common";
import { EnvPrefix, FromEnv, ParseDurationPipe, UsePipeConf, asDuration } from "@pasta/back-core";
import { IsBoolean, IsNumber, IsString, IsUrl } from "class-validator";

@EnvPrefix("PARAPHEUR_")
export class ParapheurConfig {
  @FromEnv("MAIL_WORKER_INTERVAL")
  @IsNumber()
  @UsePipeConf(new ParseDurationPipe("ms"))
  mailWorkerInterval: number = asDuration("1m", "ms");

  @FromEnv("MAIL_WORKER_START_OF_DAY_HOUR")
  @UsePipeConf(new ParseIntPipe())
  @IsNumber()
  startOfDayHour: number = 8;

  @FromEnv("MAIL_WORKER_START_OF_DAY_MINUTE")
  @UsePipeConf(new ParseIntPipe())
  @IsNumber()
  startOfDayMinute: number = 0;

  @FromEnv("MAIL_WORKER_MORNING_HOUR")
  @UsePipeConf(new ParseIntPipe())
  @IsNumber()
  morningEmailHour: number = 10;

  @FromEnv("MAIL_WORKER_MORNING_MINUTE")
  @UsePipeConf(new ParseIntPipe())
  @IsNumber()
  morningEmailMinute: number = 0;

  @FromEnv("MAIL_WORKER_AFTERNOON_HOUR")
  @UsePipeConf(new ParseIntPipe())
  @IsNumber()
  afternoonEmailHour: number = 15;

  @FromEnv("MAIL_WORKER_AFTERNOON_MINUTE")
  @UsePipeConf(new ParseIntPipe())
  @IsNumber()
  afternoonEmailMinute: number = 0;

  @FromEnv("MAIL_WORKER_END_OF_DAY_HOUR")
  @UsePipeConf(new ParseIntPipe())
  @IsNumber()
  endOfDayHour: number = 19;

  @FromEnv("MAIL_WORKER_END_OF_DAY_MINUTE")
  @UsePipeConf(new ParseIntPipe())
  @IsNumber()
  endOfDayMinute: number = 55;

  @FromEnv("MAIL_WORKER_BYPASS_RUNNING_RULES")
  @UsePipeConf(new ParseBoolPipe())
  @IsBoolean()
  bypassRunningRules: boolean = false;

  @FromEnv("FRONT_BASEURL")
  @IsUrl({ require_tld: false })
  frontBaseUrl: string;

  @FromEnv("NOTIFICATION_TIMEZONE")
  @IsString()
  timezone: string = "Europe/Paris";

  @FromEnv("NOTIFICATIONS_DRY_RUN")
  @UsePipeConf(new ParseBoolPipe())
  @IsBoolean()
  notificationsDryRun: boolean = false;

  @FromEnv("ALLOWED_CONVERSION_FORMATS")
  @IsString({ each: true })
  allowedConversionFormats: string[] = [
    "doc",
    "docm",
    "docx",
    "ppt",
    "pptm",
    "pptx",
    "xls",
    "xlsm",
    "xlsx",
    "odp",
    "ods",
    "odt",
  ];
}
