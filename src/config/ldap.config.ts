import {
  ConfDoc,
  EnvPrefix,
  FromEnv,
  ParseDurationPipe,
  SplitStringPipe,
  UsePipeConf,
  asDuration,
} from "@pasta/back-core";
import { IsOptional, IsString } from "class-validator";

@EnvPrefix("LDAP_API_")
export class LdapConf {
  @FromEnv("VERSION")
  @IsString()
  @ConfDoc({
    required: true,
    description: "The api version",
    examples: ["1.0"],
  })
  apiVersion: string = "1.0";

  @FromEnv("LOOKUP_CACHE_DURATION")
  @UsePipeConf(new ParseDurationPipe("s"))
  lookupCacheDuration: number = asDuration("1d", "s");

  @FromEnv("AUTOCOMPLETE_CACHE_DURATION")
  @UsePipeConf(new ParseDurationPipe("s"))
  autocompleteCacheDuration: number = asDuration("1h", "s");

  @FromEnv("REJECT_UNAUTHORIZED")
  rejectUnauthorized: boolean = false;

  @FromEnv("BALA_WHITELIST")
  @UsePipeConf(new SplitStringPipe(","))
  @IsOptional()
  @IsString({ each: true })
  balaWhiteList: string[] = [];

  @FromEnv("URL_PREFIX")
  @IsOptional()
  @IsString()
  urlPrefix = "/api/ldap";
}
