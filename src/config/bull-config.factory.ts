import { RedisConfig } from "./redis.config";

export const createBullConfig = async (redisConf: RedisConfig) => {
  return { redis: redisConf };
};
