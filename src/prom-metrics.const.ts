import { TypedMetricConfig } from "@pasta/prometheus";
import { NotificationService } from "./services/notification.service";

export const promMetrics: TypedMetricConfig[] = [
  {
    type: "counter",
    name: "ldap_cache_hits",
    help: "Number of ldap cache hits",
    labelNames: ["origin"] as const,
  },
  {
    type: "counter",
    name: "ldap_cache_miss",
    help: "Number of ldap cache miss",
    labelNames: ["origin"] as const,
  },
  {
    type: "counter",
    name: "email_notifications_sent",
    help: "Number of sent emails",
  },
  {
    type: "gauge",
    name: "email_notifications_buffered",
    help: "Number of notifications to be sent sometime in the future",
    async collect() {
      const notificationService = this.moduleRef.get(NotificationService, { strict: false });
      const nb = await notificationService.countAll();
      this.set(nb);
    },
  },
  {
    type: "histogram",
    name: "wopi_time_to_save",
    help: "time between token request and call to save doc",
    labelNames: ["user"] as const,
  },
];
