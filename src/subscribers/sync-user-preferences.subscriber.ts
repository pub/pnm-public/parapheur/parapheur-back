import { InjectDataSource, InjectRepository } from "@nestjs/typeorm";
import { DataSource, EntitySubscriberInterface, InsertEvent, RemoveEvent, Repository } from "typeorm";
import { User } from "@pasta/back-auth";
import { DEFAULT_USER_PREF, UserPreferences } from "../entities/user-preferences.entity";
import { LdapService } from "../services/ldap.service";
import { LdapError } from "../errors/ldap.error";

export class SyncUserPreferencesSubscriber implements EntitySubscriberInterface<User> {
  constructor(
    @InjectRepository(UserPreferences) private readonly userPreferencesRepo: Repository<UserPreferences>,
    @InjectDataSource() dataSource: DataSource,
    private readonly ldapService: LdapService
  ) {
    dataSource.subscribers.push(this);
  }

  listenTo(): typeof User {
    return User;
  }

  async afterRemove(event: RemoveEvent<User>) {
    await this.userPreferencesRepo.delete({ login: event.databaseEntity.login });
  }

  async afterInsert(event: InsertEvent<User>) {
    try {
      const ldapEntry = await this.ldapService.lookup(event.entity.login);

      await this.userPreferencesRepo.insert({
        ...DEFAULT_USER_PREF,
        login: event.entity.login,
        job: ldapEntry.description || "",
        city: ldapEntry.city || "",
      });
    } catch (e) {
      if (e instanceof LdapError) {
        // accept ldap errors, so that we can create accounts that do not match ldap (bridge ..)
      } else {
        throw e;
      }
    }
  }
}
