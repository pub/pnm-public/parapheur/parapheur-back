import { InjectDataSource } from "@nestjs/typeorm";
import { DataSource, EntitySubscriberInterface, InsertEvent } from "typeorm";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import { LetterFile } from "../entities/letter-file.entity";

export class CreateRootFolderSubscriber implements EntitySubscriberInterface<LetterFile> {
  constructor(@InjectDataSource() dataSource: DataSource) {
    dataSource.subscribers.push(this);
  }

  listenTo(): typeof LetterFile {
    return LetterFile;
  }

  async afterInsert(event: InsertEvent<LetterFile>) {
    const rootFolder: Partial<LetterFileFolder> = {
      letterFile: event.entity,
      name: "$$ROOT$$",
      position: 0,
    };

    await event.manager.getRepository(LetterFileFolder).insert(rootFolder);
  }
}
