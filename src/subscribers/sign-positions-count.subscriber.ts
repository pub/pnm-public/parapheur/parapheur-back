import { DataSource, EntitySubscriberInterface, InsertEvent, RemoveEvent } from "typeorm";
import { SignaturePosition } from "../entities/SignaturePosition.entity";
import { InjectDataSource } from "@nestjs/typeorm";
import { LetterFileDocumentService } from "../services/letter-file-document.service";
import { Injectable } from "@nestjs/common";

@Injectable()
export class UpdateSignPositionsCount implements EntitySubscriberInterface<SignaturePosition> {
  constructor(
    @InjectDataSource() dataSource: DataSource,
    private readonly letterFileDocumentService: LetterFileDocumentService
  ) {
    dataSource.subscribers.push(this);
  }

  listenTo(): string | Function {
    return SignaturePosition;
  }

  async afterInsert(event: InsertEvent<SignaturePosition>): Promise<any> {
    await this.letterFileDocumentService.repo.increment({ id: event.entity.document.id }, "signaturePositionsCount", 1);
    return;
  }

  async afterRemove(event: RemoveEvent<SignaturePosition>): Promise<any> {
    const repo = event.connection.getRepository(SignaturePosition);
    if (!event.entityId) return;
    const { document: documentId } = await repo.findOne({
      where: { id: event.entityId },
      loadRelationIds: true,
    });
    await this.letterFileDocumentService.repo.decrement(
      { id: documentId as any as number },
      "signaturePositionsCount",
      1
    );
    return;
  }
}
