import { InjectDataSource } from "@nestjs/typeorm";
import { Group, GroupType, User } from "@pasta/back-auth";
import { DataSource, EntitySubscriberInterface, InsertEvent } from "typeorm";
import { UserPreferences } from "../entities/user-preferences.entity";

export class UpdateOrgUnitSubscriber implements EntitySubscriberInterface<any> {
  constructor(@InjectDataSource() dataSource: DataSource) {
    dataSource.subscribers.push(this);
  }

  listenTo(): string | Function {
    return "auth_group_users_auth_user";
  }

  async afterInsert(event: InsertEvent<any>): Promise<any> {
    const userRepo = event.connection.getRepository(User);
    const userPrefRepo = event.connection.getRepository(UserPreferences);
    const groupRepo = event.connection.getTreeRepository(Group);
    const unit = await groupRepo.findOneBy({
      id: event.entityId.authGroupId,
    });
    if (unit.type !== GroupType.UNIT) return;

    const tree = await groupRepo.findAncestorsTree(unit);
    const orgUnit = this.buildOrgUnitFromTree(tree);

    const user = await userRepo.findOneBy({ id: event.entityId.authUserId });
    const userPref = await userPrefRepo.findOneBy({ login: user.login });

    if (userPref.currentOrgUnit !== orgUnit) {
      const body: Partial<Pick<UserPreferences, "initialOrgUnit" | "currentOrgUnit">> = !userPref.initialOrgUnit
        ? { initialOrgUnit: orgUnit, currentOrgUnit: orgUnit }
        : { currentOrgUnit: orgUnit };
      await userPrefRepo.update(userPref.id, body);
    }
  }

  private buildOrgUnitFromTree(tree: Group): string {
    if (tree.parent) {
      return `${this.buildOrgUnitFromTree(tree.parent)}/${tree.name}`;
    }
    return tree.name;
  }
}
