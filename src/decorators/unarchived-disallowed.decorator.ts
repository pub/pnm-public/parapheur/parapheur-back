import { SetMetadata } from "@nestjs/common";

export const UNARCHIVED_DISALLOWED_KEY = "unarchivedDisallowed";
export const UnarchivedDisallowed = () => SetMetadata(UNARCHIVED_DISALLOWED_KEY, true);
