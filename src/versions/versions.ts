import { sortBy } from "lodash";

export interface Version {
  id: number;
  number: string;
  releaseDate: string;
  body: string[];
}

export const versions: Version[] = [
  { id: 1, number: "V1", releaseDate: "13 novembre 2023", body: ["Version Initiale"] },
  {
    id: 2,
    number: "V2",
    releaseDate: "30 novembre 2023",
    body: [
      "Mise en œuvre de la signature augmentée",
      "Ajout de la structure d'appartenance des parapheurs",
      "Possibilité d'ajouter des flux enregistrés en copie",
    ],
  },
  {
    id: 3,
    number: "V3",
    releaseDate: "12 décembre 2023",
    body: ['Bascule du circuit de visas vers "Copie à"', " Corrections d'anomalies"],
  },
  {
    id: 4,
    number: "V4",
    releaseDate: "18 décembre 2024",
    body: ["Ajout de badges dans la recherche", "Notifications"],
  },
  {
    id: 5,
    number: "V5",
    releaseDate: "30 janvier 2024",
    body: ["Signature des documents autres que pdf (texte, tableur, présentation)"],
  },
  {
    id: 6,
    number: "V6",
    releaseDate: "06 février 2024",
    body: [
      "Amélioration de la passerelle AIRS",
      "Désarchivage de parapheurs",
      "Nouveau critère dans la recherche avancée",
    ],
  },
  {
    id: 7,
    number: "V7",
    releaseDate: "15 février 2024",
    body: [
      "Signalement en cas de prise de main sur un parapheur",
      "Amélioration de la lisibilité des messages d'alerte et d'erreur",
    ],
  },
  {
    id: 8,
    number: "V8",
    releaseDate: "09 avril 2024",
    body: ["Positionnement de la signature", "Focus sur la position de la signature"],
  },
  {
    id: 9,
    number: "V9",
    releaseDate: "09 avril 2024",
    body: ["Actualisation du tutoriel", "Ajout de la liste des versions dans l'accueil"],
  },
  {
    id: 10,
    number: "V10",
    releaseDate: "23 avril 2024",
    body: [
      "Amélioration de la recherche",
      "signature des formulaires pdf",
      "Ajout de la structure d'appartenance des agents",
    ],
  },
];

export const lastVersionNumber = sortBy(versions, "id").at(-1).id;
