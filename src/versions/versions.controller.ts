import { Controller, Get } from "@nestjs/common";
import { VersionsService } from "./versions.service";
import { Public } from "@pasta/back-auth";

@Controller("versions")
export class VersionsController {
  constructor(public readonly service: VersionsService) {}

  @Get()
  @Public()
  getMany() {
    return this.service.getMany();
  }
}
