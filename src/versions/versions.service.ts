import { Injectable } from "@nestjs/common";
import { versions } from "./versions";

@Injectable()
export class VersionsService {
  async getMany() {
    return versions;
  }
}
