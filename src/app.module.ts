import { BadRequestException, Module, NotFoundException, OnModuleInit } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import {
  PastaAppModule,
  PastaClsModule,
  PastaConfigModule,
  PastaCrudModule,
  PastaDbModule,
  PastaErrorModule,
  SimpleErrorManager,
  asGlobalInterceptor,
} from "@pasta/back-core";
import { LoggerModule } from "@pasta/back-logger";
import { AuthModule } from "@pasta/back-auth";
import { SearchModule } from "@pasta/back-search";
import { LetterFile } from "./entities/letter-file.entity";
import { UpstreamFluxElement } from "./entities/upstream-flux-element.entity";
import { LetterFileCCUser } from "./entities/letter-file-cc-user.entity";
import { LetterFileComment } from "./entities/letter-file-comment.entity";
import { LetterFileDocument } from "./entities/letter-file-document.entity";
import { LetterFileFolder } from "./entities/letter-file-folder.entity";
import { LetterFileTag } from "./entities/letter-file-tag.entity";
import { FluxElementService } from "./services/flux-element.service";
import { LetterFileCCUserService } from "./services/letter-file-cc-user.service";
import { LetterFileCommentService } from "./services/letter-file-comment.service";
import { LetterFileDocumentService } from "./services/letter-file-document.service";
import { LetterFileFolderService } from "./services/letter-file-folder.service";
import { LetterFileTagService } from "./services/letter-file-tag.service";
import { LetterFileService } from "./services/letter-file.service";
import { LetterFileCCUserController } from "./controllers/letter-file-cc-user.controller";
import { LetterFileCommentController } from "./controllers/letter-file-comment.controller";
import { LetterFileDocumentController } from "./controllers/letter-file-document.controller";
import { LetterFileTagController } from "./controllers/letter-file-tag.controller";
import { LetterFileController } from "./controllers/letter-file.controller";
import { LetterFileFolderController } from "./controllers/letter-file-folder.controller";
import { SetChronoPipe } from "./pipe/set-chrono.pipe";
import { NotFoundInDbError } from "./errors/not-found.error";
import { CreateRootFolderSubscriber } from "./subscribers/create-root-folder.subscriber";
import { LetterFileError, NextError, RecallError, TakeError } from "./errors/letter-file.error";
import { PastaFilesModule, StorageConfig } from "@pasta/back-files";
import { SetFirstFluxElementInterceptor } from "./interceptors/set-first-flux-element.interceptor";
import { DownstreamFluxElement } from "./entities/downstream-flux-element.entity";
import { SetLFStatusInterceptor } from "./interceptors/set-letter-file-status.interceptor";
import { SentryModule } from "@pasta/back-sentry";
import { IsCommentFrozenInterceptor } from "./interceptors/is-comment-frozen.interceptor";
import { ArchiveService } from "./services/archive.service";
import { ElasticsearchModule } from "./elasticsearch/elasticsearch.module";
import { ElasticsearchService } from "./elasticsearch/elasticsearch.service";
import { indexSettings, searchSchema } from "./search-schema";
import { searchAliasName, searchIndexName } from "./utils";
import { SetLetterFileIdInterceptor } from "./interceptors/set-letter-file-id.interceptor";
import { LetterFileIndexationService } from "./services/letter-file-indexation.service";
import { IsCCUserFrozenInterceptor } from "./interceptors/is-ccUser-frozen.interceptor";
import { LdapService } from "./services/ldap.service";
import { LetterFileSavedFluxController } from "./controllers/letter-file-saved-flux.controller";
import { LetterFileSavedFuxService } from "./services/saved-flux.service";
import { LetterFileSavedFlux } from "./entities/saved-flux.entity";
import { IsCurrentInterceptor } from "./interceptors/is-current.interceptor";
import { IsRelatedInterceptor } from "./interceptors/is-related.interceptor";
import { LdapController } from "./controllers/ldap.controller";
import { LdapConf } from "./config/ldap.config";
import { LdapInvalidMailError, LdapUnknownMailError } from "./errors/ldap.error";
import { UserPreferences } from "./entities/user-preferences.entity";
import { UserPreferencesController } from "./controllers/user-preferences.controller";
import { UserPreferencesService } from "./services/user-preferences.service";
import { Notification } from "./entities/notification.entity";
import { NotificationService } from "./services/notification.service";
import { HedwigeService } from "./services/hedwige.service";
import { HedwigeConfig } from "./config/hedwige.config";
import { ParapheurConfig } from "./config/parapheur.config";
import { SyncUserPreferencesSubscriber } from "./subscribers/sync-user-preferences.subscriber";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
import { TemplatingService } from "./services/templating.service";
import { CsvExporter } from "./services/csv.exporter";
import { LetterFileExportController } from "./controllers/export.controller";
import { LetterFileExportService } from "./services/export.service";
import { LetterFileExport } from "./entities/export.entity";
import { LetterFileExportProcessor } from "./services/export.processor";
import { BullModule } from "@nestjs/bull";
import { RedisConfig } from "./config/redis.config";
import { createBullConfig } from "./config/bull-config.factory";
import { LetterFileHistory } from "./entities/letter-file-history.entity";
import { LetterFileHistoryService } from "./services/letter-file-history.service";
import { SetUserIdClsInterceptor } from "./interceptors/set-userid-cls.interceptor";
import { SignApi } from "./services/sign-api.service";
import { SignService } from "./services/sign.service";
import { SignApiConf } from "./config/sign-api.config";
import { DocumentError } from "./errors/document.error";
import { SignError } from "./errors/sign.error";
import { DocumentVersion } from "./entities/DocumentVersion.entity";
import { DocumentVersionService } from "./services/document-version.service";
import { LetterFileHistoryController } from "./controllers/letter-file-history.controller";
import { SetDocumentExtensionInterceptor } from "./interceptors/set-document-extension.interceptor";
import { HideBeforeError } from "./errors/user-preferences.error";
import { Suggestion } from "./entities/Suggestion.entity";
import { SuggestionController } from "./controllers/suggestion.controller";
import { SuggestionService } from "./services/suggestion.service";
import { IsMySuggestionInterceptor } from "./interceptors/is-my-suggestion.interceptor";
import { UpdateSuggestionsInterceptor } from "./interceptors/update-suggestions.interceptor";
import { MutexInterceptor } from "./interceptors/letter-file-mutex.interceptor";
import { InformationBannerModule } from "./modules/InformationBanner/InformationBanner.module";
import { CacheModule } from "@nestjs/cache-manager";
import { IsLetterFileUnarchivedInterceptor } from "./interceptors/is-letterFile-unarchived.interceptor";
import { PastaSocketModule } from "./PastaSocket/pasta-socket.module";
import { LetterFileEventService } from "./services/LetterFileEvent.service";
import { SignaturePositionController } from "./controllers/signature-position.controller";
import { SignaturePositionService } from "./services/signature-position.service";
import { SignaturePosition } from "./entities/SignaturePosition.entity";
import { VersionsModule } from "./versions/version.module";
import { PrometheusModule } from "@pasta/prometheus";
import { UpdateSignPositionsCount } from "./subscribers/sign-positions-count.subscriber";
import { UpdateOrgUnitSubscriber } from "./subscribers/update-orgunit.subscriber";
import { ApiManagerClientModule } from "@pasta/api-manager-client";
import { WopiController } from "./controllers/wopi.controller";
import { WopiService } from "./services/wopi.service";
import { WopiConfig } from "./config/wopi.config";
import { createCacheConfig } from "./config/cache-config.factory";
import { promMetrics } from "./prom-metrics.const";

dayjs.extend(utc);
dayjs.extend(timezone);

const confModule = PastaConfigModule.register(
  StorageConfig,
  LdapConf,
  HedwigeConfig,
  ParapheurConfig,
  RedisConfig,
  SignApiConf,
  WopiConfig
);
@Module({
  imports: [
    PastaFilesModule.register({
      routes: {
        uploadFile: {
          enabled: true,
        },
        getLink: {
          enabled: true,
        },
      },
    }),
    PastaCrudModule,
    PastaDbModule.forRoot(),
    PastaDbModule.forFeature(
      LetterFile,
      UpstreamFluxElement,
      DownstreamFluxElement,
      LetterFileCCUser,
      LetterFileComment,
      LetterFileDocument,
      LetterFileFolder,
      LetterFileTag,
      LetterFileSavedFlux,
      UserPreferences,
      Notification,
      LetterFileExport,
      LetterFileHistory,
      DocumentVersion,
      Suggestion,
      SignaturePosition
    ),
    PastaClsModule.forRoot(),
    PastaErrorModule.forRoot(
      SimpleErrorManager(NotFoundInDbError, NotFoundException),
      SimpleErrorManager(LetterFileError, BadRequestException),
      SimpleErrorManager(TakeError, BadRequestException),
      SimpleErrorManager(LdapUnknownMailError, BadRequestException),
      SimpleErrorManager(LdapInvalidMailError, BadRequestException),
      SimpleErrorManager(DocumentError, BadRequestException),
      SimpleErrorManager(SignError, BadRequestException),
      SimpleErrorManager(NextError, BadRequestException),
      SimpleErrorManager(HideBeforeError, BadRequestException),
      SimpleErrorManager(RecallError, BadRequestException)
    ),
    LoggerModule.register({ accessLogging: true }),
    AuthModule.register(),
    SearchModule.forRoot({
      backend: "elasticsearch",
      options: {
        host: process.env.ELASTICSEARCH_URL || "http://127.0.0.1:9200",
      },
    }),
    confModule,
    SentryModule.forRoot(),
    PastaClsModule.forRoot(),
    ElasticsearchModule,
    CacheModule.registerAsync({
      imports: [confModule],
      inject: [RedisConfig],
      useFactory: createCacheConfig,
    }),
    BullModule.forRootAsync({ imports: [confModule], inject: [RedisConfig], useFactory: createBullConfig }),
    BullModule.registerQueue({ name: "exports", defaultJobOptions: { attempts: 1 } }),
    InformationBannerModule,
    PastaSocketModule,
    VersionsModule,
    PrometheusModule.forRoot({
      prefix: "parapheur_",
      metrics: promMetrics,
    }),
    ApiManagerClientModule,
  ],
  controllers: [
    AppController,
    LetterFileController,
    LetterFileCCUserController,
    LetterFileCommentController,
    LetterFileDocumentController,
    LetterFileFolderController,
    LetterFileTagController,
    LetterFileSavedFluxController,
    LdapController,
    UserPreferencesController,
    LetterFileExportController,
    LetterFileHistoryController,
    SuggestionController,
    SignaturePositionController,
    WopiController,
  ],
  providers: [
    AppService,
    LetterFileService,
    FluxElementService,
    LetterFileCCUserService,
    LetterFileCommentService,
    LetterFileDocumentService,
    LetterFileFolderService,
    LetterFileTagService,
    LetterFileIndexationService,
    SetChronoPipe,
    CreateRootFolderSubscriber,
    SyncUserPreferencesSubscriber,
    UpdateSignPositionsCount,
    UpdateOrgUnitSubscriber,
    SetFirstFluxElementInterceptor,
    SetLFStatusInterceptor,
    IsCommentFrozenInterceptor,
    IsCCUserFrozenInterceptor,
    SetLetterFileIdInterceptor,
    IsCurrentInterceptor,
    IsRelatedInterceptor,
    ArchiveService,
    LdapService,
    LetterFileSavedFuxService,
    UserPreferencesService,
    NotificationService,
    HedwigeService,
    TemplatingService,
    CsvExporter,
    LetterFileExportService,
    LetterFileExportProcessor,
    LetterFileHistoryService,
    asGlobalInterceptor(SetUserIdClsInterceptor),
    SignApi,
    SignService,
    DocumentVersionService,
    SetDocumentExtensionInterceptor,
    SuggestionService,
    IsMySuggestionInterceptor,
    UpdateSuggestionsInterceptor,
    MutexInterceptor,
    IsLetterFileUnarchivedInterceptor,
    LetterFileEventService,
    SignaturePositionService,
    WopiService,
  ],
})
export class AppModule extends PastaAppModule implements OnModuleInit {
  constructor(
    private readonly esService: ElasticsearchService,
    private readonly ldapService: LdapService
  ) {
    super();
  }

  async onModuleInit() {
    const response = await this.esService.getAliasTarget(searchAliasName);
    if (!response.target) {
      await this.esService.createIndex(searchIndexName, searchSchema, indexSettings);
      await this.esService.setAlias(searchIndexName, searchAliasName);
    }
    await this.ldapService.emptyCache();
  }
}
