import { Server, Socket } from "socket.io";
import { PastaSocketEvent } from "../gateways/pasta.gateway";
import { PastaLogger, InjectLogger } from "@pasta/back-logger";

export type PastaWebsocketEventInfos = { userId: number; login: string };
export type PastaWebsocketEventHandler = (
  data: any,
  client?: Socket,
  server?: Server,
  infos?: PastaWebsocketEventInfos
) => void;

export class PastaWebsocketService {
  @InjectLogger("default", PastaWebsocketService.name)
  logger: PastaLogger;

  private server: Server;
  private handlers: Array<{
    type: string;
    handler: PastaWebsocketEventHandler;
  }> = [];

  public on(type: string, handler: PastaWebsocketEventHandler) {
    const exists = this.handlers.some(h => h.type === type && h.handler === handler);
    if (exists) return;
    this.handlers.push({ type, handler });
  }

  public async handleEvent(event: PastaSocketEvent, client: Socket, infos?: PastaWebsocketEventInfos) {
    const handlers = this.handlers.filter(h => h.type === event.type).map(h => h.handler);
    this.logInfo("handleEvent", { event, handlers: handlers.length }, client.id);
    for (const handler of handlers) {
      try {
        await handler(event.data, client, this.server, infos);
      } catch (e) {
        this.logger.error("Error handling event", e.message, "handleEvent", { event, handler: handler.name, error: e });
      }
    }
  }

  public setServer(server: Server) {
    this.server = server;
    this.server.of("/").adapter.on("join-room", (room, id) => {
      this.logInfo("joined room", { room }, id);
    });
    this.server.of("/").adapter.on("leave-room", (room, id) => {
      this.logInfo("left room", { room }, id);
    });
  }

  public to(room: string, event: PastaSocketEvent) {
    this.server.to(room).emit("event", event);
  }

  private logInfo(message: string, metadata: Object, id: string) {
    const client = this.server.sockets.sockets.get(id).client;
    this.logger.info(message, PastaWebsocketService.name, {
      ...metadata,
      transport: client.conn.transport.name,
    });
  }
}
