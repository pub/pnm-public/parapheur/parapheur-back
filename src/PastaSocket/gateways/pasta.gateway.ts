import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from "@nestjs/websockets";
import { JwtService } from "@nestjs/jwt";
import { Server, Socket } from "socket.io";
import { PastaWebsocketService } from "../services/pasta.websocket.service";
import { OnModuleInit } from "@nestjs/common";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { Gauge, UseMetric } from "@pasta/prometheus";

export interface PastaSocketEvent {
  type: string;
  data: any;
}

@WebSocketGateway({ cors: { origin: "*", namespace: "parapheur" } })
export class PastaGateway implements OnGatewayConnection, OnGatewayDisconnect, OnModuleInit {
  @InjectLogger("default", PastaGateway.name)
  logger: PastaLogger;

  constructor(
    private readonly jwtService: JwtService,
    private readonly pastaWsService: PastaWebsocketService
  ) {}

  onModuleInit() {
    if (!global.PASTA_IS_CLI) {
      this.pastaWsService.setServer(this.server);
    }
  }

  @UseMetric("socket_clients")
  socketClientsCount: Gauge;

  @WebSocketServer()
  server: Server;

  async handleConnection(client: Socket) {
    this.logger.info("connection");
    this.socketClientsCount.inc();
    try {
      await this.jwtService.verifyAsync(client.handshake.auth?.token || "");
      const { id, login, exp } = this.jwtService.decode(client.handshake.auth?.token || "");
      client.data.infos = { id, login, exp };
      this.logger.info("connection auth success");
    } catch (e) {
      this.logger.warn("connection auth failed");
      client.disconnect();
      return;
    }

    client.emit("authenticated");
    client.join("app");
  }

  handleDisconnect(_client: Socket) {
    this.logger.info("disconnect");
    this.socketClientsCount.dec();
  }

  @SubscribeMessage("event")
  async handleEvent(@MessageBody() event: PastaSocketEvent, @ConnectedSocket() client: Socket) {
    const { infos } = client.data;
    this.logger.info(`received event ${event.type}`);
    await this.pastaWsService.handleEvent(event, client, { userId: infos.id, login: infos.login });
    return;
  }
}
