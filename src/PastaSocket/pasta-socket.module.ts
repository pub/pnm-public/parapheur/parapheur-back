import { Module } from "@nestjs/common";
import { PastaGateway } from "./gateways/pasta.gateway";
import { AuthModule } from "@pasta/back-auth";
import { PastaWebsocketService } from "./services/pasta.websocket.service";
import { LoggerModule } from "@pasta/back-logger";
import { PrometheusModule } from "@pasta/prometheus";

@Module({
  providers: [PastaGateway, PastaWebsocketService],
  imports: [
    AuthModule.register(),
    LoggerModule.register(),
    PrometheusModule.forFeature([
      {
        type: "gauge",
        name: "socket_clients",
        help: "Gauge of currently connected sockets",
      },
    ]),
  ],
  exports: [PastaWebsocketService],
})
export class PastaSocketModule {}
