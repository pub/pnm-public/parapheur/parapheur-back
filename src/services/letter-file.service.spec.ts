import { LetterFileService } from "./letter-file.service";
import { LetterFile } from "../entities/letter-file.entity";
import { MockRepository, PastaCrudHookService, PastaCrudOperatorService, mockObject } from "@pasta/back-core";
import { Test, TestingModule } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { FluxElementService } from "./flux-element.service";
import { LetterFileFolderService } from "./letter-file-folder.service";
import { LetterFileDocumentService } from "./letter-file-document.service";
import { StorageService } from "@pasta/back-files";
import { LetterFileCommentService } from "./letter-file-comment.service";
import { LetterFileCCUserService } from "./letter-file-cc-user.service";
import { ArchiveService } from "./archive.service";
import { LetterFileTagService } from "./letter-file-tag.service";
import { LetterFileIndexationService } from "./letter-file-indexation.service";
import { LetterFileSavedFuxService } from "./saved-flux.service";
import { LdapService } from "./ldap.service";
import { NotificationService } from "./notification.service";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { UserPreferencesService } from "./user-preferences.service";
import { LetterFileStatus } from "../types/letter-file-status.enum";
import { set } from "lodash";
import { getAbstractClass } from "../tests/utils";
import { HistoryOperationType, HistoryTarget } from "../entities/letter-file-history.entity";
import { InternalServerErrorException } from "@nestjs/common";
import { NotFoundInDbError } from "../errors/not-found.error";
import { User } from "@pasta/back-auth";
import { NextError, TakeArchivedError, TakeDraftError, UndraftError } from "../errors/letter-file.error";
import { LetterFileSearchDto } from "../dtos/letter-file.dto";
import dayjs from "dayjs";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";
import { NotificationType } from "../entities/notification.entity";
import { LetterFileEventService } from "./LetterFileEvent.service";

describe("LetterFileService", () => {
  let service: LetterFileService;
  let repo: MockRepository<LetterFile>;
  let fluxService: FluxElementService;
  let folderService: LetterFileFolderService;
  let documentService: LetterFileDocumentService;
  let storageService: StorageService;
  let commentService: LetterFileCommentService;
  let ccUserService: LetterFileCCUserService;
  let archiveService: ArchiveService;
  let tagsService: LetterFileTagService;
  let indexationService: LetterFileIndexationService;
  let savedFluxService: LetterFileSavedFuxService;
  let ldapService: LdapService;
  let notificationService: NotificationService;
  let historyService: LetterFileHistoryService;
  let userPreferencesService: UserPreferencesService;
  let letterFileEventService: LetterFileEventService;

  beforeEach(async () => {
    repo = new MockRepository<LetterFile>();
    fluxService = mockObject(FluxElementService);
    folderService = mockObject(LetterFileFolderService);
    documentService = mockObject(LetterFileDocumentService);
    storageService = mockObject(StorageService);
    commentService = mockObject(LetterFileCommentService);
    ccUserService = mockObject(LetterFileCCUserService);
    archiveService = mockObject(ArchiveService);
    tagsService = mockObject(LetterFileTagService);
    indexationService = mockObject(LetterFileIndexationService);
    savedFluxService = mockObject(LetterFileSavedFuxService);
    ldapService = mockObject(LdapService);
    notificationService = mockObject(NotificationService);
    historyService = mockObject(LetterFileHistoryService);
    userPreferencesService = mockObject(UserPreferencesService);
    letterFileEventService = mockObject(LetterFileEventService);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: LetterFileService, useClass: LetterFileService },
        { provide: getRepositoryToken(LetterFile), useValue: repo },
        { provide: FluxElementService, useValue: fluxService },
        { provide: LetterFileFolderService, useValue: folderService },
        { provide: LetterFileDocumentService, useValue: documentService },
        { provide: StorageService, useValue: storageService },
        { provide: LetterFileCommentService, useValue: commentService },
        { provide: LetterFileCCUserService, useValue: ccUserService },
        { provide: ArchiveService, useValue: archiveService },
        { provide: LetterFileTagService, useValue: tagsService },
        { provide: LetterFileIndexationService, useValue: indexationService },
        { provide: LetterFileSavedFuxService, useValue: savedFluxService },
        { provide: LdapService, useValue: ldapService },
        { provide: NotificationService, useValue: notificationService },
        { provide: LetterFileHistoryService, useValue: historyService },
        { provide: UserPreferencesService, useValue: userPreferencesService },
        { provide: PastaCrudOperatorService, useValue: jest.fn() },
        { provide: PastaCrudHookService, useValue: jest.fn() },
        { provide: LetterFileEventService, useValue: letterFileEventService },
      ],
    }).compile();

    service = module.get(LetterFileService);
  });

  it("should be defined", async () => {
    expect(service).toBeDefined();
  });

  describe("the getHome method", () => {
    it("gets home", async () => {
      // given
      const mail = "jane.doe@gouv.fr";
      const relatedMails = ["bob.lazar@gouv.fr"];
      (ldapService.lookup as jest.Mock).mockResolvedValue({ relatedMails: relatedMails });
      (userPreferencesService.findOne as jest.Mock).mockResolvedValue({});
      const dbLetterFiles = [
        // done
        {
          id: 1,
          upstream: [{ mail: "bob.lazar@gouv.fr" }],
          downstream: [{ mail: "john.doe@gouv.fr", position: 0 }],
          tags: [
            { id: 5, name: "toto" },
            { id: 2, name: "tutu" },
          ],
        },
        {
          id: 5,
          archived: true,
          upstream: [{ mail: "bob.lazar@gouv.fr" }],
          downstream: [{ mail: "john.doe@gouv.fr", position: 0 }],
        },
        // current
        {
          id: 2,
          upstream: [
            { mail: "marie.dupont@gouv.fr", position: 0 },
            { mail: "charles.dupré@gouv.fr", position: 1 },
          ],
          downstream: [{ mail: "bob.lazar@gouv.fr", position: 0 }],
          tags: [
            { id: 5, name: "toto" },
            { id: 2, name: "tutu" },
          ],
        },
        {
          id: 3,
          draft: true,
          upstream: [],
          downstream: [{ mail: "bob.lazar@gouv.fr", position: 0 }],
        },
        // todo
        {
          id: 4,
          upstream: [{ mail: "bob.lazar@gouv.fr" }],
          downstream: [{ mail: "bob.lazar@gouv.fr", position: 2 }],
          tags: [
            { id: 5, name: "toto" },
            { id: 2, name: "tutu" },
          ],
        },
      ];
      (repo.queryBuilder.getMany as jest.Mock).mockResolvedValue(dbLetterFiles);

      // when
      const result = await service.getHome(mail);

      // then
      expect(result).toEqual({
        todo: {
          data: [
            {
              id: 4,
              tags: [
                { id: 2, name: "tutu" },
                { id: 5, name: "toto" },
              ],
            },
          ],
          total: 1,
        },
        current: {
          data: [
            {
              id: 2,
              tags: [
                { id: 2, name: "tutu" },
                { id: 5, name: "toto" },
              ],
              from: { mail: "charles.dupré@gouv.fr", position: 1 },
            },
            { id: 3, draft: true, tags: [] },
          ],
          total: 1,
          draft: 1,
        },
        done: {
          data: [
            {
              id: 1,
              tags: [
                { id: 2, name: "tutu" },
                { id: 5, name: "toto" },
              ],
              current: { mail: "john.doe@gouv.fr", position: 0 },
              status: "UPSTREAM",
            },
            { id: 5, archived: true, current: { mail: "john.doe@gouv.fr", position: 0 }, tags: [], status: "ARCHIVED" },
          ],
          total: 1,
          archived: 1,
        },
      });
      expect(ldapService.lookup).toHaveBeenCalledWith(mail);
      expect(repo.createQueryBuilder).toHaveBeenCalled();
      expect(userPreferencesService.findOne).toHaveBeenCalledWith({
        select: ["hideLetterFilesBefore"],
        where: { login: mail },
      });
      expect(repo.queryBuilder.where).not.toHaveBeenCalled();
    });

    it("handles 'hideLetterFilesBefore' user pref", async () => {
      // given
      const mail = "jane.doe@gouv.fr";
      const relatedMails = ["bob.lazar@gouv.fr"];
      (ldapService.lookup as jest.Mock).mockResolvedValue({ relatedMails: relatedMails });
      (userPreferencesService.findOne as jest.Mock).mockResolvedValue({
        hideLetterFilesBefore: "2023-09-04 14:46:53.791862",
      });
      const dbLetterFiles = [];
      (repo.queryBuilder.getMany as jest.Mock).mockResolvedValue(dbLetterFiles);

      // when
      const result = await service.getHome(mail);

      // then
      expect(result).toEqual({
        todo: {
          data: [],
          total: 0,
        },
        current: {
          data: [],
          total: 0,
          draft: 0,
        },
        done: {
          data: [],
          total: 0,
          archived: 0,
        },
      });
      expect(ldapService.lookup).toHaveBeenCalledWith(mail);
      expect(repo.createQueryBuilder).toHaveBeenCalled();
      expect(userPreferencesService.findOne).toHaveBeenCalledWith({
        select: ["hideLetterFilesBefore"],
        where: { login: mail },
      });
      expect(repo.queryBuilder.where).toHaveBeenCalledWith("letterFile.creationDate >= :hideLetterFilesBefore", {
        hideLetterFilesBefore: "2023-09-04 14:46:53.791862",
      });
    });
  });

  describe("the getStatus method", () => {
    it("returns ARCHIVED status", async () => {
      // given
      const lf = { archived: true } as LetterFile;
      const relatedMails = [];

      // when
      const result = service.getStatus(lf, relatedMails);

      // then
      expect(result).toBe(LetterFileStatus.ARCHIVED);
    });
    it("returns CURRENT status", async () => {
      // given
      const lf = { downstream: [{ position: 0, mail: "toto" }] } as LetterFile;
      const relatedMails = ["toto"];

      // when
      const result = service.getStatus(lf, relatedMails);

      // then
      expect(result).toBe(LetterFileStatus.CURRENT);
    });
    it("returns DOWNSTREAM status", async () => {
      // given
      const lf = { downstream: [{ position: 1, mail: "toto" }] } as LetterFile;
      const relatedMails = ["toto"];

      // when
      const result = service.getStatus(lf, relatedMails);

      // then
      expect(result).toBe(LetterFileStatus.DOWNSTREAM);
    });
    it("returns UPSTREAM status", async () => {
      // given
      const lf = {
        downstream: [{ position: 0, mail: "nop" }],
        upstream: [{ position: 0, mail: "toto" }],
      } as LetterFile;
      const relatedMails = ["toto"];

      // when
      const result = service.getStatus(lf, relatedMails);

      // then
      expect(result).toBe(LetterFileStatus.UPSTREAM);
    });
    it("returns CC status", async () => {
      // given
      const lf = {
        downstream: [{ position: 0, mail: "nop" }],
        upstream: [{ position: 0, mail: "nop" }],
        ccUsers: [{ mail: "toto" }],
      } as LetterFile;
      const relatedMails = ["toto"];

      // when
      const result = service.getStatus(lf, relatedMails);

      // then
      expect(result).toBe(LetterFileStatus.CC);
    });
    it("returns UNKNOWN status", async () => {
      // given
      const lf = { upstream: [], downstream: [], ccUsers: [] } as LetterFile;
      const relatedMails = [];

      // when
      const result = service.getStatus(lf, relatedMails);

      // then
      expect(result).toBe(LetterFileStatus.UNKNOWN);
    });
  });

  describe("the getNextChronoNumber method", () => {
    it("returns next chrono number", async () => {
      // given
      const getPrefixMock = jest.fn().mockReturnValue("202309");
      set(service, "getChronoNumberPrefix", getPrefixMock);
      (repo.queryBuilder.getCount as jest.Mock).mockResolvedValue(10);

      // when
      const result = await service.getNextChronoNumber();

      // then
      expect(result).toBe("2023090000011");
    });
  });

  describe("the createOne method", () => {
    it("creates one", async () => {
      // given
      const query = {};
      const body = { downstream: [{ mail: "robert.biglow@gouv.fr" }] };
      const getNextChronoMock = jest.fn().mockResolvedValue("2023090000011");
      set(service, "getNextChronoNumber", getNextChronoMock);
      const expected = { id: 27 };
      const abstractPastaCrudServiceCreateOne = jest.fn().mockResolvedValue(expected);
      set(getAbstractClass(service), "createOne", abstractPastaCrudServiceCreateOne);
      (fluxService.addOneDownstream as jest.Mock).mockResolvedValue([
        {
          id: 1,
          position: 0,
          name: "toto",
          mail: "toto@gouv.fr",
          letterFile: {},
        },
      ]);

      // when
      const result = await service.createOne(query, body);

      // then
      expect(result).toBe(expected);
      expect(service.getNextChronoNumber).not.toHaveBeenCalled();
      expect(abstractPastaCrudServiceCreateOne).toHaveBeenCalledWith(query, body);
      expect(fluxService.addOneDownstream).toHaveBeenCalledWith("robert.biglow@gouv.fr", 27);
      expect(indexationService.indexNewLetterFile).toHaveBeenCalledWith(
        { id: 27, downstream: [{ position: 0, name: "toto", mail: "toto@gouv.fr" }] },
        { name: "toto", mail: "toto@gouv.fr" }
      );
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: 27, downstream: [{ position: 0, name: "toto", mail: "toto@gouv.fr" }] },
        label: "Création",
        target: HistoryTarget["BASE-INFO"],
        type: HistoryOperationType.CREATE,
      });
    });

    it("retries upon db conflict", async () => {
      // given
      const query = {};
      const body = { downstream: [{ mail: "robert.biglow@gouv.fr" }] };
      const getNextChronoMock = jest.fn().mockResolvedValue("2023090000011");
      set(service, "getNextChronoNumber", getNextChronoMock);
      const expected = { id: 27 };
      const abstractPastaCrudServiceCreateOne = jest
        .fn()
        .mockRejectedValueOnce({ code: "23505" })
        .mockResolvedValue(expected);
      set(getAbstractClass(service), "createOne", abstractPastaCrudServiceCreateOne);
      (fluxService.addOneDownstream as jest.Mock).mockResolvedValue([
        {
          id: 1,
          position: 0,
          name: "toto",
          mail: "toto@gouv.fr",
          letterFile: {},
        },
      ]);

      // when
      const result = await service.createOne(query, body);

      // then
      expect(result).toBe(expected);
      expect(service.getNextChronoNumber).toHaveBeenCalled();
      expect(abstractPastaCrudServiceCreateOne).toHaveBeenCalledTimes(2);
      expect(abstractPastaCrudServiceCreateOne).toHaveBeenCalledWith(query, body);
      expect(fluxService.addOneDownstream).toHaveBeenCalledWith("robert.biglow@gouv.fr", 27);
      expect(indexationService.indexNewLetterFile).toHaveBeenCalledWith(
        { id: 27, downstream: [{ position: 0, name: "toto", mail: "toto@gouv.fr" }] },
        { name: "toto", mail: "toto@gouv.fr" }
      );
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: 27, downstream: [{ position: 0, name: "toto", mail: "toto@gouv.fr" }] },
        label: "Création",
        target: HistoryTarget["BASE-INFO"],
        type: HistoryOperationType.CREATE,
      });
    });

    it("throws when mail is not found", async () => {
      // given
      const query = {};
      const body = {};

      // when
      const promise = service.createOne(query, body);

      // then
      await expect(promise).rejects.toEqual(new InternalServerErrorException("No mail found in downstream"));
    });
  });

  describe("the updateOne method", () => {
    it("updates tags", async () => {
      // given
      const query = {};
      const id = 1;
      const body = { tags: [{ id: 2 }, { id: 3 }] };
      const expected = { id, modificationDate: "2023-09-04 14:46:53.791862" };
      const abstractPastaCrudUpdateOne = jest.fn().mockResolvedValue(expected);
      set(getAbstractClass(service), "updateOne", abstractPastaCrudUpdateOne);
      (tagsService.getMany as jest.Mock).mockResolvedValue({
        data: [
          { id: 2, name: "a" },
          { id: 3, name: "b" },
        ],
      });

      // when
      const result = await service.updateOne(query, id, body);

      // then
      expect(result).toBe(expected);
      expect(abstractPastaCrudUpdateOne).toHaveBeenCalledWith(query, id, body);
      expect(tagsService.getMany).toHaveBeenCalledWith({ filter: { id: { $in: [2, 3] } } });
      expect(indexationService.putTags).toHaveBeenCalledWith(id, ["a", "b"], "2023-09-04 14:46:53.791862");
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: expected,
        label: 'Modification des badges: "a, b"',
        target: HistoryTarget["BASE-INFO"],
        type: HistoryOperationType.UPDATE,
      });
      expect(historyService.registerEvent).toHaveBeenCalledTimes(1);
      expect(indexationService.patchBase).not.toHaveBeenCalled();
    });

    it("updates base fields", async () => {
      // given
      const query = {};
      const id = 1;
      const body = {
        name: "toto",
        deadline: "2023-09-04 17:21:39.845201",
        manager: "Abdel",
        extra: "fool",
      };
      const expected = { id, modificationDate: "2023-09-04 14:46:53.791862" };
      const abstractPastaCrudUpdateOne = jest.fn().mockResolvedValue(expected);
      set(getAbstractClass(service), "updateOne", abstractPastaCrudUpdateOne);

      // when
      const result = await service.updateOne(query, id, body);

      // then
      expect(result).toBe(expected);
      expect(abstractPastaCrudUpdateOne).toHaveBeenCalledWith(query, id, body);
      expect(tagsService.getMany).not.toHaveBeenCalled();
      expect(indexationService.putTags).not.toHaveBeenCalled();
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: expected,
        label: 'Modification des champs "nom du parapheur, date limite de traitement, responsable du parapheur"',
        target: HistoryTarget["BASE-INFO"],
        type: HistoryOperationType.UPDATE,
      });
      expect(historyService.registerEvent).toHaveBeenCalledTimes(1);
      expect(indexationService.patchBase).toHaveBeenCalledWith(id, {
        name: "toto",
        deadline: "2023-09-04 17:21:39.845201",
        manager: "Abdel",
        modificationDate: "2023-09-04 14:46:53.791862",
      });
    });

    it("updates external fields", async () => {
      // given
      const query = {};
      const id = 1;
      const body = {
        senderOrganization: "Orga",
        senderName: "Hubert",
        senderRole: "role",
        mailType: "type de mail",
        externalMailNumber: "1234ABC",
        arrivalDate: "2023-08-02 13:49:51.953323",
        recipientName: "recipient name",
        extra: "fool",
      };
      const expected = { id, modificationDate: "2023-09-04 14:46:53.791862" };
      const abstractPastaCrudUpdateOne = jest.fn().mockResolvedValue(expected);
      set(getAbstractClass(service), "updateOne", abstractPastaCrudUpdateOne);

      // when
      const result = await service.updateOne(query, id, body);

      // then
      expect(result).toBe(expected);
      expect(abstractPastaCrudUpdateOne).toHaveBeenCalledWith(query, id, body);
      expect(tagsService.getMany).not.toHaveBeenCalled();
      expect(indexationService.putTags).not.toHaveBeenCalled();
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: expected,
        label:
          'Modification des champs "organisme, nom, prénom, fonction, type de courrier, numéro de courrier extérieur, date du courrier extérieur, destinataire du courrier reçu"',
        target: HistoryTarget["EXTERNAL-INFO"],
        type: HistoryOperationType.UPDATE,
      });
      expect(historyService.registerEvent).toHaveBeenCalledTimes(1);
      expect(indexationService.patchBase).toHaveBeenCalledWith(id, {
        senderOrganization: "Orga",
        senderName: "Hubert",
        senderRole: "role",
        mailType: "type de mail",
        externalMailNumber: "1234ABC",
        arrivalDate: "2023-08-02 13:49:51.953323",
        recipientName: "recipient name",
        modificationDate: "2023-09-04 14:46:53.791862",
      });
    });

    it("updates one field", async () => {
      // given
      const query = {};
      const id = 1;
      const body = {
        senderOrganization: "Orga",
      };
      const expected = { id, modificationDate: "2023-09-04 14:46:53.791862" };
      const abstractPastaCrudUpdateOne = jest.fn().mockResolvedValue(expected);
      set(getAbstractClass(service), "updateOne", abstractPastaCrudUpdateOne);

      // when
      const result = await service.updateOne(query, id, body);

      // then
      expect(result).toBe(expected);
      expect(abstractPastaCrudUpdateOne).toHaveBeenCalledWith(query, id, body);
      expect(tagsService.getMany).not.toHaveBeenCalled();
      expect(indexationService.putTags).not.toHaveBeenCalled();
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: expected,
        label: 'Modification du champ "organisme" -> Orga',
        target: HistoryTarget["EXTERNAL-INFO"],
        type: HistoryOperationType.UPDATE,
      });
      expect(historyService.registerEvent).toHaveBeenCalledTimes(1);
      expect(indexationService.patchBase).toHaveBeenCalledWith(id, {
        senderOrganization: "Orga",
        modificationDate: "2023-09-04 14:46:53.791862",
      });
    });

    it("empties one field", async () => {
      // given
      const query = {};
      const id = 1;
      const body = {
        senderOrganization: null,
      };
      const expected = { id, modificationDate: "2023-09-04 14:46:53.791862" };
      const abstractPastaCrudUpdateOne = jest.fn().mockResolvedValue(expected);
      set(getAbstractClass(service), "updateOne", abstractPastaCrudUpdateOne);

      // when
      const result = await service.updateOne(query, id, body);

      // then
      expect(result).toBe(expected);
      expect(abstractPastaCrudUpdateOne).toHaveBeenCalledWith(query, id, body);
      expect(tagsService.getMany).not.toHaveBeenCalled();
      expect(indexationService.putTags).not.toHaveBeenCalled();
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: expected,
        label: 'Modification du champ "organisme" -> champ supprimé',
        target: HistoryTarget["EXTERNAL-INFO"],
        type: HistoryOperationType.UPDATE,
      });
      expect(historyService.registerEvent).toHaveBeenCalledTimes(1);
      expect(indexationService.patchBase).toHaveBeenCalledWith(id, {
        senderOrganization: null,
        modificationDate: "2023-09-04 14:46:53.791862",
      });
    });
  });

  describe("the next method", () => {
    it("works", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const firstResponse = {
        id,
        downstream: [
          { position: 0, id: 123, name: "john-connor@gouv.fr" },
          { position: 1, id: 456 },
        ],
      };
      const secondResponse = { id, downstream: [{ position: 0, id: 456, mail: "bob@gouv.fr" }] };
      const getOneMock = jest.fn().mockResolvedValueOnce(firstResponse).mockResolvedValueOnce(secondResponse);
      set(service, "getOne", getOneMock);
      const updateOneMock = jest.fn();
      set(service, "updateOne", updateOneMock);
      (ccUserService.findBy as jest.Mock).mockResolvedValue([{ id: 98, mail: "elise@gouv.fr" }]);

      // when
      const result = await service.next(query, id, user);

      // then
      expect(result).toBe(secondResponse);
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenNthCalledWith(1, { join: ["downstream", "upstream"] }, id);
      expect(fluxService.moveToUpstream).toHaveBeenCalledWith(123);
      expect(updateOneMock).not.toHaveBeenCalled();
      expect(commentService.freeze).toHaveBeenCalledWith(id);
      expect(ccUserService.findBy).toHaveBeenCalledWith({ frozen: false, letterFile: { id } });
      expect(notificationService.add).toHaveBeenNthCalledWith(
        1,
        "elise@gouv.fr",
        "john-connor@gouv.fr",
        firstResponse,
        NotificationType.CC
      );
      expect(ccUserService.freeze).toHaveBeenCalledWith(id);
      expect(getOneMock).toHaveBeenNthCalledWith(2, { join: ["downstream", "upstream"] }, id);
      expect(notificationService.add).toHaveBeenNthCalledWith(
        2,
        "bob@gouv.fr",
        "john-connor@gouv.fr",
        secondResponse,
        NotificationType.AtYourLevel
      );
      expect(indexationService.putFlux).toHaveBeenCalledWith(id, secondResponse);
    });

    it("archives", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const firstResponse = {
        id,
        downstream: [{ position: 0, id: 123 }],
      };
      const secondResponse = { id, downstream: [] };
      const getOneMock = jest.fn().mockResolvedValueOnce(firstResponse).mockResolvedValueOnce(secondResponse);
      set(service, "getOne", getOneMock);
      const updateOneMock = jest.fn();
      set(service, "updateOne", updateOneMock);
      (ccUserService.findBy as jest.Mock).mockResolvedValue([]);

      // when
      const result = await service.next(query, id, user);

      // then
      expect(result).toBe(secondResponse);
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenNthCalledWith(1, { join: ["downstream", "upstream"] }, id);
      expect(fluxService.moveToUpstream).toHaveBeenCalledWith(123);
      expect(updateOneMock).toHaveBeenCalledWith({}, id, { archived: true, unArchivedDate: null });
      expect(commentService.freeze).toHaveBeenCalledWith(id);
      expect(ccUserService.freeze).toHaveBeenCalledWith(id);
      expect(getOneMock).toHaveBeenNthCalledWith(2, { join: ["downstream", "upstream"] }, id);
      expect(notificationService.add).not.toHaveBeenCalled();
      expect(indexationService.putFlux).toHaveBeenCalledWith(id, secondResponse);
    });

    it("throws when not found", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const getOneMock = jest.fn().mockResolvedValue(undefined);
      set(service, "getOne", getOneMock);
      const updateOneMock = jest.fn();
      set(service, "updateOne", updateOneMock);

      // when
      const promise = service.next(query, id, user);

      // then
      expect(promise).rejects.toEqual(new NotFoundInDbError("No letter file found"));
      expect(getOneMock).toHaveBeenCalledTimes(1);
      expect(getOneMock).toHaveBeenNthCalledWith(1, { join: ["downstream", "upstream"] }, id);
      expect(fluxService.moveToUpstream).not.toHaveBeenCalled();
      expect(updateOneMock).not.toHaveBeenCalled();
      expect(commentService.freeze).not.toHaveBeenCalled();
      expect(ccUserService.freeze).not.toHaveBeenCalled();
      expect(notificationService.add).not.toHaveBeenCalled();
      expect(indexationService.putFlux).not.toHaveBeenCalled();
    });

    it("throws when draft", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const getOneMock = jest.fn().mockResolvedValue({ id, draft: true });
      set(service, "getOne", getOneMock);
      const updateOneMock = jest.fn();
      set(service, "updateOne", updateOneMock);

      // when
      const promise = service.next(query, id, user);

      // then
      expect(promise).rejects.toEqual(new NextError("Not permitted on a draft letter file"));
      expect(getOneMock).toHaveBeenCalledTimes(1);
      expect(getOneMock).toHaveBeenNthCalledWith(1, { join: ["downstream", "upstream"] }, id);
      expect(fluxService.moveToUpstream).not.toHaveBeenCalled();
      expect(updateOneMock).not.toHaveBeenCalled();
      expect(commentService.freeze).not.toHaveBeenCalled();
      expect(ccUserService.freeze).not.toHaveBeenCalled();
      expect(notificationService.add).not.toHaveBeenCalled();
      expect(indexationService.putFlux).not.toHaveBeenCalled();
    });
  });

  describe("the take method", () => {
    it("works", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const expected = {
        id,
        downstream: [
          { id: 20, position: 0, mail: "robert@gouv.fr" },
          { id: 21, position: 1, mail: "other@gouv.fr" },
        ],
      };
      const getOneMock = jest.fn().mockResolvedValueOnce({ id }).mockResolvedValue(expected);
      set(service, "getOne", getOneMock);

      // when
      const result = await service.take(query, id, user, false);

      // then
      expect(result).toBe(expected);
      expect(commentService.freeze).toHaveBeenCalledWith(id);
      expect(ccUserService.freeze).toHaveBeenCalledWith(id);
      expect(fluxService.take).toHaveBeenCalledWith("robert@gouv.fr", id);
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenNthCalledWith(1, {}, id);
      expect(getOneMock).toHaveBeenNthCalledWith(2, { join: ["downstream"] }, id);
      expect(indexationService.putDownstream).toHaveBeenCalledWith(id, expected.downstream);
      expect(indexationService.putRelatedUsers).not.toHaveBeenCalled();
    });

    it("works as superadmin", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const expected = {
        id,
        downstream: [
          { id: 20, position: 0, mail: "robert@gouv.fr" },
          { id: 21, position: 1, mail: "other@gouv.fr" },
        ],
      };
      const getOneMock = jest.fn().mockResolvedValueOnce({ id }).mockResolvedValue(expected);
      set(service, "getOne", getOneMock);
      (indexationService.getOne as jest.Mock).mockResolvedValue({ relatedUsers: [{ mail: "bob@gouv.fr" }] });

      // when
      const result = await service.take(query, id, user, true);

      // then
      expect(result).toBe(expected);
      expect(commentService.freeze).toHaveBeenCalledWith(id);
      expect(ccUserService.freeze).toHaveBeenCalledWith(id);
      expect(fluxService.take).toHaveBeenCalledWith("robert@gouv.fr", id);
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenNthCalledWith(1, {}, id);
      expect(getOneMock).toHaveBeenNthCalledWith(2, { join: ["downstream"] }, id);
      expect(indexationService.putDownstream).toHaveBeenCalledWith(id, expected.downstream);
      expect(indexationService.getOne).toHaveBeenCalledWith(id);
      expect(indexationService.putRelatedUsers).toHaveBeenCalledWith(id, [
        { mail: "bob@gouv.fr" },
        { mail: "robert@gouv.fr" },
      ]);
    });

    it("throws for draft", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const getOneMock = jest.fn().mockResolvedValueOnce({ id, draft: true });
      set(service, "getOne", getOneMock);

      // when
      const promise = service.take(query, id, user, true);

      // then
      await expect(promise).rejects.toEqual(new TakeDraftError("Should not take a draft letter file"));
      expect(commentService.freeze).not.toHaveBeenCalled();
      expect(ccUserService.freeze).not.toHaveBeenCalled();
      expect(fluxService.take).not.toHaveBeenCalled();
      expect(getOneMock).toHaveBeenCalledTimes(1);
      expect(getOneMock).toHaveBeenNthCalledWith(1, {}, id);
      expect(indexationService.putDownstream).not.toHaveBeenCalled();
      expect(indexationService.getOne).not.toHaveBeenCalled();
      expect(indexationService.putRelatedUsers).not.toHaveBeenCalled();
    });

    it("throws for archives", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const getOneMock = jest.fn().mockResolvedValueOnce({ id, archived: true });
      set(service, "getOne", getOneMock);

      // when
      const promise = service.take(query, id, user, true);

      // then
      await expect(promise).rejects.toEqual(new TakeArchivedError("Should not take an archived letter file"));
      expect(commentService.freeze).not.toHaveBeenCalled();
      expect(ccUserService.freeze).not.toHaveBeenCalled();
      expect(fluxService.take).not.toHaveBeenCalled();
      expect(getOneMock).toHaveBeenCalledTimes(1);
      expect(getOneMock).toHaveBeenNthCalledWith(1, {}, id);
      expect(indexationService.putDownstream).not.toHaveBeenCalled();
      expect(indexationService.getOne).not.toHaveBeenCalled();
      expect(indexationService.putRelatedUsers).not.toHaveBeenCalled();
    });
  });

  describe("the undraft method", () => {
    it("undrafts", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const getOneMock = jest.fn().mockResolvedValueOnce({ id, draft: true });
      set(service, "getOne", getOneMock);
      const expected = { id, toto: "tutu" };
      const nextMock = jest.fn().mockResolvedValue(expected);
      set(service, "next", nextMock);

      // when
      const result = await service.undraft(query, id, user);

      // then
      expect(result).toBe(expected);
      expect(getOneMock).toHaveBeenCalledWith({ join: ["downstream", "upstream", "folders", "tags"] }, id);
      expect(repo.save).toHaveBeenCalledWith({ id, draft: false });
      expect(nextMock).toHaveBeenCalledWith(query, id, user);
      expect(indexationService.patchBase).toHaveBeenCalledWith(id, { draft: false });
    });

    it("throws when not found", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const getOneMock = jest.fn().mockResolvedValueOnce(undefined);
      set(service, "getOne", getOneMock);
      const nextMock = jest.fn();
      set(service, "next", nextMock);

      // when
      const promise = service.undraft(query, id, user);

      // then
      await expect(promise).rejects.toEqual(new NotFoundInDbError("No letter file found"));
      expect(getOneMock).toHaveBeenCalledWith({ join: ["downstream", "upstream", "folders", "tags"] }, id);
      expect(repo.save).not.toHaveBeenCalled();
      expect(nextMock).not.toHaveBeenCalled();
      expect(indexationService.patchBase).not.toHaveBeenCalled();
    });

    it("throws when not draft", async () => {
      // given
      const query = {};
      const id = 1;
      const user = { id: 20, login: "robert@gouv.fr", firstName: "Robert", lastName: "Dupont" } as User;
      const getOneMock = jest.fn().mockResolvedValueOnce({ id, draft: false });
      set(service, "getOne", getOneMock);
      const nextMock = jest.fn();
      set(service, "next", nextMock);

      // when
      const promise = service.undraft(query, id, user);

      // then
      await expect(promise).rejects.toEqual(new UndraftError("Cannot undraft a letterfile that is not draft"));
      expect(getOneMock).toHaveBeenCalledWith({ join: ["downstream", "upstream", "folders", "tags"] }, id);
      expect(repo.save).not.toHaveBeenCalled();
      expect(nextMock).not.toHaveBeenCalled();
      expect(indexationService.patchBase).not.toHaveBeenCalled();
    });
  });

  describe("the deleteOne method", () => {
    it("deletes one", async () => {
      // given
      const query = {};
      const id = 1;
      const expected = {};
      repo.softDelete.mockResolvedValue(expected);

      // when
      const result = await service.deleteOne(query, id);

      // then
      expect(result).toBe(expected);
      expect(storageService.removeObjectsInDir).toHaveBeenCalledWith("1/", true);
      expect(repo.softDelete).toHaveBeenCalledWith(id);
      expect(indexationService.remove).toHaveBeenCalledWith(id);
    });
  });

  describe("the appendDownstream method", () => {
    it("works", async () => {
      // given
      const query = {};
      const id = 1;
      const body = { mail: "bob@gouv.fr" };
      const expected = { id, toto: "tutu" };
      const getOneMock = jest
        .fn()
        .mockResolvedValueOnce({ id, downstream: [{ id: 20, mail: "kevin@gouv.fr" }] })
        .mockResolvedValueOnce(expected);
      set(service, "getOne", getOneMock);
      const addedElement = [{ id: 20 }];
      (fluxService.addOneDownstream as jest.Mock).mockResolvedValue([addedElement]);
      (indexationService.getOne as jest.Mock).mockResolvedValue({ relatedUsers: [{ mail: "lisa@gouv.fr" }] });

      // when
      const result = await service.appendDownstream(query, id, body);

      // then
      expect(result).toBe(expected);
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenCalledWith({ join: ["downstream"] }, id);
      expect(fluxService.addOneDownstream).toHaveBeenCalledWith("bob@gouv.fr", id);
      expect(indexationService.putDownstream).toHaveBeenCalledWith(id, [
        { id: 20, mail: "kevin@gouv.fr" },
        addedElement,
      ]);
      expect(indexationService.getOne).toHaveBeenCalledWith(id);
      expect(indexationService.putRelatedUsers).toHaveBeenCalledWith(id, [{ mail: "lisa@gouv.fr" }, addedElement]);
      expect(getOneMock).toHaveBeenCalledWith(query, id);
    });
  });

  describe("the emptyDownstream method", () => {
    it("works", async () => {
      // given
      const query = {};
      const id = 1;
      const expected = {
        id,
        downstream: [{ mail: "toto@gouv.fr", position: 0 }],
        upstream: [{ mail: "toto2@gouv.fr", position: 0 }],
        ccUsers: [{ mail: "toto3@gouv.fr" }],
      };
      const getOneMock = jest.fn().mockResolvedValue(expected);
      set(service, "getOne", getOneMock);

      // when
      const result = await service.emptyDownstream(query, id);

      // then
      expect(result).toBe(expected);
      expect(fluxService.emptyDownstream).toHaveBeenCalledWith(id);
      expect(indexationService.putDownstream).toHaveBeenCalledWith(id, [{ mail: "toto@gouv.fr", position: 0 }]);
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenNthCalledWith(1, { join: ["upstream", "ccUsers", "downstream"] }, id);
      expect(getOneMock).toHaveBeenNthCalledWith(2, query, id);
    });
  });

  describe("the reorderDownstream method", () => {
    it("works", async () => {
      // given
      const query = {};
      const id = 1;
      const body = { downstream: [20, 21, 22] };
      const getOneMock = jest.fn().mockResolvedValue({ id, downstream: [{ id: 20 }, { id: 22 }, { id: 21 }] });
      set(service, "getOne", getOneMock);

      // when
      const result = await service.reorderDownstream(query, id, body);

      // then
      expect(result).toEqual({ id, downstream: [{ id: 20 }, { id: 22 }, { id: 21 }] });
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenNthCalledWith(1, { join: ["downstream", "upstream"] }, id);
      expect(fluxService.reorder).toHaveBeenCalledWith([{ id: 20 }, { id: 22 }, { id: 21 }], [20, 21, 22]);
      expect(getOneMock).toHaveBeenNthCalledWith(2, query, id);
    });

    it("throws not found", async () => {
      // given
      const query = {};
      const id = 1;
      const body = { downstream: [20, 21, 22] };
      const getOneMock = jest.fn().mockResolvedValue(undefined);
      set(service, "getOne", getOneMock);

      // when
      const promise = service.reorderDownstream(query, id, body);

      // then
      await expect(promise).rejects.toEqual(new NotFoundInDbError("No letter file found"));
      expect(getOneMock).toHaveBeenCalledTimes(1);
      expect(getOneMock).toHaveBeenCalledWith({ join: ["downstream", "upstream"] }, id);
      expect(fluxService.reorder).not.toHaveBeenCalled();
    });
  });

  describe("the reorderFolders method", () => {
    it("works", async () => {
      // given
      const query = {};
      const id = 1;
      const body = { folders: [10, 11, 12] };
      const expected = { id, folders: [{ id: 10 }, { id: 11 }, { id: 12 }] };
      const getOneMock = jest
        .fn()
        .mockResolvedValueOnce({ id, folders: [{ id: 12 }, { id: 10 }, { id: 11 }] })
        .mockResolvedValueOnce(expected);
      set(service, "getOne", getOneMock);

      // when
      const result = await service.reorderFolders(query, id, body);

      // then
      expect(result).toBe(expected);
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenNthCalledWith(1, { join: ["folders"] }, id);
      expect(folderService.reorder).toHaveBeenCalledWith([{ id: 12 }, { id: 10 }, { id: 11 }], [10, 11, 12]);
      expect(getOneMock).toHaveBeenCalledWith(query, id);
    });

    it("throws when not found", async () => {
      // given
      const query = {};
      const id = 1;
      const body = { folders: [10, 11, 12] };
      const getOneMock = jest.fn().mockResolvedValueOnce(undefined);
      set(service, "getOne", getOneMock);

      // when
      const promise = service.reorderFolders(query, id, body);

      // then
      await expect(promise).rejects.toEqual(new NotFoundInDbError("No letter file found"));
      expect(getOneMock).toHaveBeenCalledTimes(1);
      expect(getOneMock).toHaveBeenCalledWith({ join: ["folders"] }, id);
      expect(folderService.reorder).not.toHaveBeenCalled();
    });
  });

  describe("the removeDownstream method", () => {
    it("works", async () => {
      // given
      const query = {};
      const id = 1;
      const downstreamId = 100;
      const expected = { id };
      const getOneMock = jest
        .fn()
        .mockResolvedValueOnce({ id, downstream: [{ id: 200 }], upstream: [{ id: 300 }], ccUsers: [{ id: 400 }] })
        .mockResolvedValueOnce(expected);
      set(service, "getOne", getOneMock);

      // when
      const result = await service.removeDownstream(query, id, downstreamId);

      // then
      expect(result).toBe(expected);
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenCalledWith({ join: ["downstream", "upstream", "ccUsers"] }, id);
      expect(indexationService.putDownstream).toHaveBeenCalledWith(id, [{ id: 200 }]);
      expect(indexationService.putRelatedUsers).toHaveBeenCalledWith(id, [{ id: 300 }, { id: 200 }, { id: 400 }]);
      expect(getOneMock).toHaveBeenCalledWith(query, id);
    });
  });

  describe("the generateArchive method", () => {
    it("handles letter file without name", async () => {
      // given
      const id = 1;
      const letterFile = {
        id,
        name: "toto",
        chronoNumber: "123",
        folders: [
          {
            id: 10,
            name: "my folder",
            documents: [{ id: 20, storageKey: "key20", name: "bibi.pdf", type: LetterFileDocumentType.DEFAULT }],
          },
        ],
      };
      const getOneMock = jest.fn().mockResolvedValueOnce(letterFile);
      set(service, "getOne", getOneMock);
      const theKey = "the key";
      (archiveService.generateArchiveForLf as jest.Mock).mockResolvedValue(theKey);

      // when
      const result = await service.generateArchive(id);

      // then
      expect(result).toBe(theKey);
      expect(getOneMock).toHaveBeenCalledWith({ join: ["folders", "folders.documents"] }, id);
      expect(archiveService.generateArchiveForLf).toHaveBeenCalledWith(letterFile);
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile,
        label: "Téléchargement de tous les dossiers et documents",
        target: HistoryTarget.DEFAULT,
        type: HistoryOperationType.DOWNLOAD,
      });
    });

    it("handles letter file with name", async () => {
      // given
      const id = 1;
      const letterFile = {
        id,
        chronoNumber: "123",
        folders: [
          {
            id: 10,
            name: "my folder",
            documents: [{ id: 20, storageKey: "key20", name: "bibi.pdf", type: LetterFileDocumentType.DEFAULT }],
          },
        ],
      };
      const getOneMock = jest.fn().mockResolvedValueOnce(letterFile);
      set(service, "getOne", getOneMock);
      const theKey = "the key";
      (archiveService.generateArchiveForLf as jest.Mock).mockResolvedValue(theKey);

      // when
      const result = await service.generateArchive(id);

      // then
      expect(result).toBe(theKey);
      expect(getOneMock).toHaveBeenCalledWith({ join: ["folders", "folders.documents"] }, id);
      expect(archiveService.generateArchiveForLf).toHaveBeenCalledWith(letterFile);
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile,
        label: "Téléchargement de tous les dossiers et documents",
        target: HistoryTarget.DEFAULT,
        type: HistoryOperationType.DOWNLOAD,
      });
    });
  });

  describe("the search method", () => {
    it("works", async () => {
      // given
      const startDate = dayjs("2023-08-02T14:39:57").toDate();
      const endDate = dayjs("2023-09-04T14:46:53").toDate();
      const dto: LetterFileSearchDto = {
        searchPattern: "abc",
        archived: true,
        tags: ["xyz"],
        fileTypes: ["jpg"],
        startDate,
        endDate,
        createdByMe: true,
        alreadySeen: true,
        inTodo: true,
        inCurrent: true,
        inDone: true,
      };
      const pagination = { offset: 50, limit: 25 };
      const mail = "bob@gouv.fr";
      (userPreferencesService.findOne as jest.Mock).mockResolvedValue({
        hideLetterFilesBefore: "2023-08-02 13:49:51.953323",
      });
      (ldapService.lookup as jest.Mock).mockResolvedValue({ relatedMails: ["bob@gouv.fr", "robert@gouv.fr"] });
      (indexationService.search as jest.Mock).mockResolvedValue({
        total: 2,
        items: [
          { id: 1, highlight: { a: ["a", "b"] } },
          { id: 2, highlight: { c: ["c", "d"] } },
        ],
      });
      const getManyMock = jest.fn().mockResolvedValue({
        data: [
          { id: 1, name: "toto" },
          { id: 2, name: "tutu" },
        ],
      });
      set(service, "getMany", getManyMock);

      // when
      const result = await service.search(dto, pagination, mail);

      // then
      expect(result).toEqual({
        data: [
          {
            highlight: {
              a: ["a", "b"],
            },
            id: 1,
            name: "toto",
          },
          {
            highlight: {
              c: ["c", "d"],
            },
            id: 2,
            name: "tutu",
          },
        ],
        isLastPage: true,
        limit: 25,
        offset: 50,
        page: 3,
        total: 2,
      });
      expect(ldapService.lookup).toHaveBeenCalledWith(mail);
      expect(indexationService.search).toHaveBeenCalledWith(
        {
          andConditions: {
            filters: [
              {
                range: {
                  creationDate: {
                    gte: "2023-08-02 13:49:51.953323",
                  },
                },
              },
              {
                term: {
                  archived: {
                    value: true,
                  },
                },
              },
              {
                terms_set: {
                  tags: {
                    minimum_should_match_script: {
                      source: "1",
                    },
                    terms: ["xyz"],
                  },
                },
              },
              {
                term: {
                  "creator.mail": {
                    value: "bob@gouv.fr",
                  },
                },
              },
              {
                term: {
                  "upstream.mail": {
                    value: "bob@gouv.fr",
                  },
                },
              },
              {
                term: {
                  "downstream.mail": {
                    value: "bob@gouv.fr",
                  },
                },
              },
            ],
            mustNot: [
              {
                range: {
                  archivedDate: {
                    lt: startDate,
                  },
                },
              },
              {
                range: {
                  creationDate: {
                    gt: endDate,
                  },
                },
              },
            ],
            searchPattern: "abc",
            should: [
              {
                wildcard: {
                  "documents.name.keyword": {
                    value: "*jpg",
                  },
                },
              },
            ],
          },
          highlight: [
            "name",
            "chronoNumber",
            "current.name",
            "comments",
            "documents.name",
            "documents.name.keyword",
            "externalMailNumber",
            "ccUsers.name",
            "downstream.name",
            "upstream.name",
            "senderOrganization",
            "senderName",
            "senderRole",
            "mailType",
            "recipientName",
            "manager",
            "folders",
          ],
          orConditions: [
            {
              term: {
                "downstream.mail": {
                  value: "bob@gouv.fr",
                },
              },
            },
            {
              term: {
                "current.mail": {
                  value: "bob@gouv.fr",
                },
              },
            },
            {
              bool: {
                filter: [
                  {
                    term: {
                      "upstream.mail": {
                        value: "bob@gouv.fr",
                      },
                    },
                  },
                ],
                mustNot: [
                  {
                    term: {
                      "current.mail": {
                        value: "bob@gouv.fr",
                      },
                    },
                  },
                  {
                    term: {
                      "downstream.mail": {
                        value: "bob@gouv.fr",
                      },
                    },
                  },
                ],
              },
            },
          ],
          userEmails: ["bob@gouv.fr", "robert@gouv.fr"],
        },
        { from: 50, size: 25 }
      );
      expect(getManyMock).toHaveBeenCalledWith({
        join: ["tags", "upstream", "downstream", "ccUsers", "folders", "folders.documents"],
        filter: { $and: [{ id: { $in: [1, 2] } }] },
      });
    });
  });

  describe("the removeAllDocs method", () => {
    it("works", async () => {
      // given
      const id = 1;
      const letterFile = {
        id,
        folders: [
          { id: 11, name: "toto" },
          { id: 10, name: "$$ROOT$$" },
        ],
      };
      const getOneMock = jest.fn().mockResolvedValue(letterFile);
      set(service, "getOne", getOneMock);

      // when
      const result = await service.removeAllDocs(id);

      // then
      expect(result).toBeUndefined();
      expect(documentService.forgetForFolder).toHaveBeenCalledWith(10);
      expect(folderService.forget).toHaveBeenCalledWith([11]);
      expect(storageService.removeObjectsInDir).toHaveBeenCalledWith("1/", true);
      expect(indexationService.putDocuments).toHaveBeenCalledWith(1, []);
      expect(indexationService.putFolders).toHaveBeenCalledWith(1, []);
    });
  });

  describe("the applyFlux method", () => {
    it("works", async () => {
      // given
      const lfId = 1;
      const fluxId = 10;
      const getOneMock = jest.fn().mockResolvedValue({ id: lfId, downstream: [{ id: 20 }] });
      set(service, "getOne", getOneMock);
      (savedFluxService.getOne as jest.Mock).mockResolvedValue({
        id: 30,
        name: "toto",
        flux: [{ name: "bob", mail: "bob@gouv.fr" }],
      });
      (indexationService.getOne as jest.Mock).mockResolvedValue({ relatedUsers: [{ mail: "bob@gouv.fr" }] });

      // when
      const result = await service.applyFlux(lfId, fluxId);

      // then
      expect(result).toBeUndefined();
      expect(getOneMock).toHaveBeenNthCalledWith(1, {}, lfId);
      expect(savedFluxService.getOne).toHaveBeenCalledWith({}, fluxId);
      expect(fluxService.addManyDownstream).toHaveBeenCalledWith([{ name: "bob", mail: "bob@gouv.fr" }], lfId, "toto");
      expect(getOneMock).toHaveBeenNthCalledWith(2, { join: ["downstream"] }, lfId);
      expect(indexationService.putDownstream).toHaveBeenCalledWith(lfId, [{ id: 20 }]);
    });
  });

  describe("the clone method", () => {
    it("works", async () => {
      // given
      const id = 1;
      const user = { id: 10, login: "bob@gouv.fr" } as User;
      const source = {
        id,
        name: "toto",
        tags: [{ id: 3, name: "bidule" }],
        folders: [
          {
            id: 10,
            name: "$$ROOT$$",
            documents: [
              { type: LetterFileDocumentType.DEFAULT, id: 20, name: "letter.pdf", storageKey: "key1", position: 0 },
            ],
          },
          {
            id: 11,
            name: "en cours",
            documents: [
              { type: LetterFileDocumentType.DEFAULT, id: 21, name: "image.jpg", storageKey: "key2", position: 0 },
              {
                type: LetterFileDocumentType.URL,
                id: 22,
                name: "mdn",
                url: "https://developer.mozilla.org/",
                position: 1,
              },
            ],
          },
        ],
        downstream: [
          { id: 30, mail: "sarah@gouv.fr", position: 0 },
          { id: 31, mail: "julie@gouv.fr", name: "Julie", position: 1 },
        ],
        deadline: "2023-09-04 14:46:53.791862",
        arrivalDate: "2023-08-02 13:49:51.953323",
        externalMailNumber: "123ABC",
        archived: true,
      };
      const expected = { id: 2, name: "[Dupliqué] toto" };
      const getOneMock = jest.fn().mockResolvedValueOnce(source).mockResolvedValueOnce(expected);
      set(service, "getOne", getOneMock);
      const createOneMock = jest
        .fn()
        .mockResolvedValue({ id: 2, folders: [{ id: 12, name: "$$ROOT$$", position: 0 }] });
      set(service, "createOne", createOneMock);
      const getNextChronoMock = jest.fn().mockReturnValue("2023090000001");
      set(service, "getNextChronoNumber", getNextChronoMock);
      (folderService.createOne as jest.Mock).mockImplementation(async (_query, obj: any) => {
        return { name: obj.name, id: 100 };
      });
      const getKeyMock = jest.fn().mockReturnValueOnce("newkey1").mockReturnValueOnce("newkey2");
      set(service, "getKey", getKeyMock);

      // when
      const result = await service.clone(id, user);

      // then
      expect(result).toBe(expected);
      expect(getOneMock).toHaveBeenCalledTimes(2);
      expect(getOneMock).toHaveBeenNthCalledWith(
        1,
        { join: ["tags", "folders", "folders.documents", "downstream"] },
        id
      );
      expect(createOneMock).toHaveBeenCalledWith(
        { join: ["folders"] },
        {
          name: "[Dupliqué] toto",
          chronoNumber: "2023090000001",
          draft: true,
          archived: false,
          arrivalDate: "2023-08-02 13:49:51.953323",
          deadline: "2023-09-04 14:46:53.791862",
          externalMailNumber: "123ABC",
          tags: [
            {
              id: 3,
              name: "bidule",
            },
          ],
          downstream: [
            {
              mail: "bob@gouv.fr",
            },
          ],
          unArchivedDate: null,
        }
      );
      expect(fluxService.addManyDownstream).not.toHaveBeenCalled();
      expect(folderService.createOne).toHaveBeenCalledWith({}, { name: "en cours", letterFile: { id: 2 } }, true);
      expect(storageService.copyObject).toHaveBeenCalledTimes(2);
      expect(storageService.copyObject).toHaveBeenNthCalledWith(1, "newkey1", "key1");
      expect(storageService.copyObject).toHaveBeenNthCalledWith(2, "newkey2", "key2");
      expect(documentService.createOne).toHaveBeenCalledTimes(3);
      expect(documentService.createOne).toHaveBeenNthCalledWith(
        1,
        {},
        {
          type: LetterFileDocumentType.DEFAULT,
          name: "letter.pdf",
          position: 0,
          storageKey: "newkey1",
          url: undefined,
          folder: { id: 12 },
        },
        true
      );
      expect(documentService.createOne).toHaveBeenNthCalledWith(
        2,
        {},
        {
          type: LetterFileDocumentType.DEFAULT,
          name: "image.jpg",
          position: 0,
          storageKey: "newkey2",
          url: undefined,
          folder: { id: 100 },
        },
        true
      );
      expect(documentService.createOne).toHaveBeenNthCalledWith(
        3,
        {},
        {
          type: LetterFileDocumentType.URL,
          name: "mdn",
          position: 1,
          storageKey: undefined,
          url: "https://developer.mozilla.org/",
          folder: { id: 100 },
        },
        true
      );
      expect(getOneMock).toHaveBeenNthCalledWith(
        2,
        { join: ["tags", "folders", "folders.documents", "downstream"] },
        2
      );
      expect(indexationService.indexFull).toHaveBeenCalledWith(expected);
    });
  });
});
