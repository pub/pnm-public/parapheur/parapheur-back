import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AbstractPastaCrudService, IPastaCrudQuery } from "@pasta/back-core";
import { DeepPartial, Repository } from "typeorm";
import { LetterFileCCUser } from "../entities/letter-file-cc-user.entity";
import { LetterFileIndexationService } from "./letter-file-indexation.service";
import { LdapService, LookupResponse } from "./ldap.service";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { HistoryTarget, HistoryOperationType } from "../entities/letter-file-history.entity";
import { LetterFile } from "../entities/letter-file.entity";
import { LetterFileSavedFuxService } from "./saved-flux.service";
import { uniqBy } from "lodash";
import { LdapInvalidMailError } from "../errors/ldap.error";

@Injectable()
export class LetterFileCCUserService extends AbstractPastaCrudService<LetterFileCCUser> {
  constructor(
    @InjectRepository(LetterFileCCUser) public readonly repo: Repository<LetterFileCCUser>,
    @InjectRepository(LetterFile) private readonly letterFileRepo: Repository<LetterFile>,
    private readonly indexationService: LetterFileIndexationService,
    private readonly ldapService: LdapService,
    private readonly historyService: LetterFileHistoryService,
    private readonly savedFluxService: LetterFileSavedFuxService
  ) {
    super();
  }

  async createOne(query: IPastaCrudQuery, body: DeepPartial<LetterFileCCUser>): Promise<LetterFileCCUser> {
    let lookupResult: LookupResponse;
    try {
      lookupResult = await this.ldapService.lookup(body.mail);
    } catch (e) {
      if (e instanceof LdapInvalidMailError) {
        await this.historyService.registerEvent({
          letterFile: { id: body.letterFile as number },
          label: `${body.mail} non ajouté en copie car invalide`,
          target: HistoryTarget.CCUSER,
          type: HistoryOperationType.CREATE,
        });
      }
      throw e;
    }

    const result = await super.createOne(query, {
      ...lookupResult,
      author: body.author,
      letterFile: body.letterFile,
    });
    await this.updateCCUsersIndex(body.letterFile as number);
    const { relatedUsers } = await this.indexationService.getOne(body.letterFile as number);
    await this.indexationService.putRelatedUsers(body.letterFile as number, [...relatedUsers, result]);
    await this.historyService.registerEvent({
      letterFile: { id: body.letterFile as number },
      label: `Ajout en copie "${result.name}"`,
      target: HistoryTarget.CCUSER,
      type: HistoryOperationType.CREATE,
    });
    return result;
  }

  async deleteOne(query: IPastaCrudQuery, id: number): Promise<any> {
    const ccUser = await this.getOne(
      { join: ["letterFile", "letterFile.upstream", "letterFile.downstream", "letterFile.ccUsers"] },
      id
    );
    const { letterFile } = ccUser;
    const result = await super.deleteOne(query, id);
    await this.updateCCUsersIndex(letterFile.id);
    await this.indexationService.putRelatedUsers(letterFile.id, [
      ...letterFile.upstream,
      ...letterFile.downstream,
      ...letterFile.ccUsers.filter(cc => cc.mail !== ccUser.mail),
    ]);
    await this.historyService.registerEvent({
      letterFile: { id: ccUser.letterFile.id },
      label: `Suppression de la copie à "${ccUser.name}"`,
      target: HistoryTarget.CCUSER,
      type: HistoryOperationType.DELETE,
    });
    return result;
  }

  async freeze(lfId: number) {
    return this.repo.update({ letterFile: { id: lfId } }, { frozen: true });
  }

  async applyFlux(lfId: number, fluxId: number, userId: number) {
    // automatically produces 404 errors if not found
    await this.letterFileRepo.findOneByOrFail({ id: lfId });
    const savedFlux = await this.savedFluxService.getOne({}, fluxId);

    const currentCcUsers = (
      await this.repo.find({ where: { letterFile: { id: lfId } }, relations: ["letterFile"] })
    ).map(item => item.mail);
    const reducedFlux = uniqBy(savedFlux.flux, "mail");
    const toAdd = reducedFlux.filter(item => {
      return !currentCcUsers.includes(item.mail);
    });

    for (const todo of toAdd) {
      await super.createOne(
        {},
        {
          ...(await this.ldapService.lookup(todo.mail)),
          author: { id: userId },
          letterFile: { id: lfId },
        }
      );
    }

    if (toAdd.length) {
      await this.updateCCUsersIndex(lfId);
      const { relatedUsers } = await this.indexationService.getOne(lfId);
      await this.indexationService.putRelatedUsers(lfId, [...relatedUsers, ...toAdd]);
      await this.historyService.registerEvent({
        letterFile: { id: lfId },
        label: `Ajout en copie: ${toAdd.map(el => el.mail).join(", ")} (total: ${toAdd.length})`,
        target: HistoryTarget.CCUSER,
        type: HistoryOperationType.CREATE,
      });
    }

    return { added: toAdd.length };
  }

  private async updateCCUsersIndex(lfId: number) {
    const ccUsers = await this.getMany({ join: ["letterFile"], filter: { "letterFile.id": { $eq: lfId } } });
    await this.indexationService.putCCUsers(lfId, ccUsers.data);
  }
}
