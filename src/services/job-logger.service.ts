import { PastaLogger } from "@pasta/back-logger";
import { Job } from "bull";

export class JobLogger {
  constructor(private readonly job: Job, private readonly pastaLogger: PastaLogger) {}

  private logJob(level: string, message, context?: string, extraMetadata?: object) {
    this.job
      .log(`[${level}] ${message}`)
      .catch(e =>
        this.pastaLogger.warn(
          `Could not log to job ${this.job.id} (queue: ${this.job.queue.name}) - Reason: ${e}`,
          context,
          extraMetadata
        )
      );
  }

  error(message: string, trace?: string, context?: string, extraMetadata?: object): void {
    this.pastaLogger.error(message, trace, context, extraMetadata);
    this.logJob("error", trace ? `${message} - ${trace}` : message, context, extraMetadata);
  }

  info(message: string, context?: string, extraMetadata?: object): void {
    this.pastaLogger.info(message, context, extraMetadata);
    this.logJob("info", message, context, extraMetadata);
  }

  log(message: string, context?: string, extraMetadata?: object): void {
    this.pastaLogger.log(message, context, extraMetadata);
    this.logJob("log", message, context, extraMetadata);
  }

  warn(message: string, context?: string, extraMetadata?: object): void {
    this.pastaLogger.warn(message, context, extraMetadata);
    this.logJob("warn", message, context, extraMetadata);
  }

  debug(message: string, context?: string, extraMetadata?: object): void {
    this.pastaLogger.debug(message, context, extraMetadata);
    this.logJob("debug", message, context, extraMetadata);
  }

  verbose(message: string, context?: string, extraMetadata?: object): void {
    this.pastaLogger.verbose(message, context, extraMetadata);
    this.logJob("verbose", message, context, extraMetadata);
  }
}
