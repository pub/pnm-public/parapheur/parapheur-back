import * as path from "path";
import { Inject, OnModuleDestroy } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Deferred } from "@pasta/deferred";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import "dayjs/locale/fr";
import { ExportStatus, LetterFileExport } from "../entities/export.entity";
import { CsvExporter } from "./csv.exporter";
import { LetterFileService } from "./letter-file.service";
import { LdapService } from "./ldap.service";
import { InjectQueue, OnQueueCompleted, OnQueueFailed, OnQueueStalled, Process, Processor } from "@nestjs/bull";
import { Job, Queue } from "bull";
import { JobLogger } from "./job-logger.service";
import { flatten } from "lodash";
import { ArchiveService } from "./archive.service";
import { HedwigeClient } from "@pnm3/hedwige-ts-sdk";
import { HedwigeConfig } from "../config/hedwige.config";
import { HEDWIGE_SYMBOL } from "./hedwige.service";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";

@Processor("exports")
export class LetterFileExportProcessor implements OnModuleDestroy {
  @InjectLogger("default", LetterFileExportProcessor.name)
  logger: PastaLogger;

  private lock: Deferred;

  constructor(
    @InjectQueue("exports") private readonly queue: Queue<{ exportId: number }>,
    @InjectRepository(LetterFileExport) private readonly repo: Repository<LetterFileExport>,
    private readonly csvExporter: CsvExporter,
    private readonly letterFileService: LetterFileService,
    private readonly ldapService: LdapService,
    private readonly archiveService: ArchiveService,
    private readonly hedwigeConf: HedwigeConfig,
    @Inject(HEDWIGE_SYMBOL) private readonly hedwigeService: HedwigeClient
  ) {}

  @Process()
  async process(job: Job<{ exportId: number }>) {
    const { exportId } = job.data;
    const jobLogger = new JobLogger(job, this.logger);
    jobLogger.info(`processing job ${job.id}`, LetterFileExportProcessor.name, { exportId });
    this.lock = new Deferred();
    const theExport = await this.repo.findOne({ where: { id: exportId }, relations: ["author"] });
    if (theExport.withDocuments) {
      jobLogger.info("export as archive with all documents");
    } else {
      jobLogger.info("export as csv without documents");
    }

    jobLogger.info(`update status to ${ExportStatus.IN_PROGRESS}`, LetterFileExportProcessor.name, { exportId });
    await this.repo.update(theExport.id, { status: ExportStatus.IN_PROGRESS });

    jobLogger.info("search letter-files using provided query", LetterFileExportProcessor.name, {
      searchQuery: theExport.searchQuery,
      exportId,
    });
    const searchResult = await this.letterFileService.search(
      theExport.searchQuery,
      { limit: 10000, offset: 0 },
      theExport.author.login
    );

    jobLogger.info("lookup ldap", LetterFileExportProcessor.name, { exportId });
    const { relatedMails } = await this.ldapService.lookup(theExport.author.login);

    const docPathMap = searchResult.data.reduce((acc, curr) => {
      const documents = flatten(
        curr.folders.map(folder => {
          return folder.documents.map(doc => {
            return {
              key: doc.storageKey || doc.url,
              path: path.join(
                this.sanitize(curr.name),
                this.sanitize(folder.name),
                this.sanitize(doc.type === LetterFileDocumentType.URL ? `${doc.name}.url` : doc.name)
              ),
            };
          });
        })
      );
      return { ...acc, [curr.id]: documents };
    }, {} as Record<number, { key: string; path: string }[]>);

    jobLogger.info(`processing results`, LetterFileExportProcessor.name, { hits: searchResult.total, exportId });
    const csvKey = await this.csvExporter.process(
      searchResult.data.map(lf => ({ ...lf, status: this.letterFileService.getStatus(lf, relatedMails) })),
      docPathMap
    );

    let fileKey: string;
    if (theExport.withDocuments) {
      const archive = await this.archiveService.generateArchiveForMany(csvKey, searchResult.data, theExport.author);
      fileKey = archive.storageKey;

      await this.hedwigeService.sendMail({
        to: theExport.author.login,
        from: this.hedwigeConf.sourceAddress,
        subject: "Parapheur - Votre archive est prête",
        html: `Votre archive est prête, <a href="${archive.link}">cliquez ici pour la télécharger</a>`,
      });
    } else {
      fileKey = csvKey;
    }

    jobLogger.info(`update hits and filekey`, LetterFileExportProcessor.name, { exportId, hits: searchResult.total });
    await this.repo.update(theExport.id, { fileKey, hits: searchResult.total });
  }

  private sanitize(name: string): string {
    return name === "$$ROOT$$" ? "racine" : name.replaceAll(" ", "_").replaceAll("/", "_");
  }

  async onModuleDestroy() {
    this.logger.info("received termination signal");
    await this.queue.close();
    this.logger.info(`closed ${this.queue.name} queue`);
    if (!this.lock || (this.lock && this.lock.isComplete)) return;
    this.logger.info("waiting for current job to finish before exiting");
    await this.lock;
  }

  @OnQueueStalled()
  onQueueStalled(job: Job<{ exportId: number }>) {
    this.logger.warn(`Detected stalled job ${job.id}`);
  }

  @OnQueueFailed()
  onQueueFailed(job: Job<{ exportId: number }>, err: Error) {
    const jobLogger = new JobLogger(job, this.logger);
    jobLogger.error(`job ${job.id} failed - attempts made: ${job.attemptsMade + 1}`, err.message, null, {
      stack: err.stack,
    });
    this.updateStatus(ExportStatus.ERROR, job);
    this.lock.resolve(err);
  }

  @OnQueueCompleted()
  async onQueueCompleted(job: Job<{ exportId: number }>) {
    this.logger.info(`completed job ${job.id}`, null, { exportId: job.data.exportId });
    this.updateStatus(ExportStatus.COMPLETED, job);
    this.lock.resolve(true);
  }

  private async updateStatus(status: ExportStatus, job: Job<{ exportId: number }>) {
    try {
      this.logger.info(`updating status to ${status}`, LetterFileExportProcessor.name, { exportId: job.data.exportId });
      await this.repo.update(job.data.exportId, { status });
    } catch (e) {
      this.logger.warn(`failed to update status to ${status}`, LetterFileExportProcessor.name, { e });
    }
  }
}
