import { forwardRef, Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AbstractPastaCrudService, IPastaCrudQuery } from "@pasta/back-core";
import { StorageService } from "@pasta/back-files";
import { DeepPartial, Repository } from "typeorm";
import { ReorderLetterFileDocumentsDto } from "../dtos/letter-file-document.dto";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import { LetterFileError, RootFolderError } from "../errors/letter-file.error";
import { NotFoundInDbError } from "../errors/not-found.error";
import { LetterFileDocumentService } from "./letter-file-document.service";
import { ArchiveService } from "./archive.service";
import { LetterFileIndexationService } from "./letter-file-indexation.service";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { HistoryTarget, HistoryOperationType } from "../entities/letter-file-history.entity";
import { UpdateLetterFileFolderDto } from "../dtos/letter-file-folder.dto";

@Injectable()
export class LetterFileFolderService extends AbstractPastaCrudService<LetterFileFolder> {
  constructor(
    @InjectRepository(LetterFileFolder) public readonly repo: Repository<LetterFileFolder>,
    private readonly storageService: StorageService,
    @Inject(forwardRef(() => LetterFileDocumentService)) private readonly documentsService: LetterFileDocumentService,
    private readonly archiveService: ArchiveService,
    private readonly indexationService: LetterFileIndexationService,
    private readonly historyService: LetterFileHistoryService
  ) {
    super();
  }

  async getNextPosition(letterFile: number): Promise<number> {
    const maxPosition = await this.repo
      .createQueryBuilder("letterFileFolder")
      .select("MAX(letterFileFolder.position)", "maxPosition")
      .where("letterFileFolder.letterFileId = :letterFile", { letterFile })
      .getRawOne();

    return maxPosition.maxPosition + 1;
  }

  async createOne(
    query: IPastaCrudQuery,
    body: DeepPartial<LetterFileFolder>,
    skipIndexation?: boolean
  ): Promise<LetterFileFolder> {
    const result = await super.createOne(query, { ...body, position: await this.getNextPosition(body.letterFile.id) });
    if (!skipIndexation) await this.updateIndex(body.letterFile.id);
    await this.historyService.registerEvent({
      letterFile: { id: body.letterFile.id },
      label: "Ajout de dossier",
      target: HistoryTarget.FOLDER,
      type: HistoryOperationType.CREATE,
    });
    return result;
  }

  /**
   * Only removes folders from DB, does not reindex or remove from storage
   */
  async forget(ids: number[]) {
    await this.repo.createQueryBuilder().delete().whereInIds(ids).execute();
  }

  async deleteOne(_query: IPastaCrudQuery, id: number): Promise<any> {
    const folder = await this.repo.findOne({ where: { id }, relations: ["letterFile"] });
    if (folder.name === "$$ROOT$$") {
      throw new RootFolderError("Cannot delete root folder");
    }

    const oldPosition = folder.position;

    const result = await super.deleteOne(_query, id);

    await this.repo
      .createQueryBuilder()
      .update()
      .set({ position: () => "position - 1" })
      .where("position >= :position", { position: oldPosition })
      .andWhere("letterFileId = :letterFile", { letterFile: folder.letterFile.id })
      .execute();

    await this.storageService.removeObjectsInDir(`${folder.letterFile.id}/${folder.id}/`);
    await this.updateIndex(folder.letterFile.id);

    await this.historyService.registerEvent({
      letterFile: { id: folder.letterFile.id },
      label: `Suppression du dossier "${this.getFolderNamePretty(folder)}"`,
      target: HistoryTarget.FOLDER,
      type: HistoryOperationType.DELETE,
    });

    return result;
  }

  async updateOne(query: IPastaCrudQuery, id: number, body: UpdateLetterFileFolderDto): Promise<LetterFileFolder> {
    const previous = await this.getOne({}, id);
    const result = await super.updateOne({ ...query, join: ["letterFile"] }, id, body);
    await this.updateIndex(result.letterFile.id);
    await this.historyService.registerEvent({
      letterFile: { id: result.letterFile.id },
      label: `Renommage du dossier de "${this.getFolderNamePretty(previous)}" vers "${this.getFolderNamePretty(
        result
      )}"`,
      target: HistoryTarget.FOLDER,
      type: HistoryOperationType.UPDATE,
    });
    return result;
  }

  async reorder(folders: LetterFileFolder[], sortedIds: number[]): Promise<LetterFileFolder[]> {
    if (folders.length !== sortedIds.length) {
      throw new LetterFileError("Folders and sorted ids length mismatch");
    }
    return this.repo.save(
      sortedIds.map((el, index) => {
        const folder = folders.find(fl => fl.id === el);

        if (!folder) {
          throw new LetterFileError(`Folder with id ${el} not found`);
        }

        return { ...folder, position: index };
      })
    );
  }

  async reorderDocuments(query: IPastaCrudQuery, id: number, body: ReorderLetterFileDocumentsDto) {
    const folder = await this.getOne(
      {
        join: ["documents"],
      },
      id
    );

    if (!folder) {
      throw new NotFoundInDbError("Folder not found");
    }

    await this.documentsService.reorder(folder.documents, body.documents);
    return this.getOne(query, id);
  }

  async generateArchive(id: number) {
    const folder = await this.getOne({ join: ["letterFile", "documents"] }, id);

    const key = await this.archiveService.generateArchiveForFolder(folder);

    await this.historyService.registerEvent({
      letterFile: { id: folder.letterFile.id },
      label: `Téléchargement du dossier "${this.getFolderNamePretty(folder)}"`,
      target: HistoryTarget.FOLDER,
      type: HistoryOperationType.DOWNLOAD,
    });
    return key;
  }

  private async updateIndex(lfId: number) {
    const siblings = await this.repo.find({ relations: ["letterFile"], where: { letterFile: { id: lfId } } });
    await this.indexationService.putFolders(lfId, siblings);
  }

  private getFolderNamePretty(folder: LetterFileFolder): string {
    return folder.name === "$$ROOT$$" ? "racine" : folder.name;
  }
}
