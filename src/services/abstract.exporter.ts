import { v4 } from "uuid";
import { LetterFile } from "../entities/letter-file.entity";
import { StorageConfig, StorageService, base64Encode } from "@pasta/back-files";
import { LetterFileStatus } from "../types/letter-file-status.enum";
import { Injectable } from "@nestjs/common";

export const exportsBasePath = "exports";
@Injectable()
export abstract class AbstractExporter {
  constructor(protected readonly storageService: StorageService, protected readonly config: StorageConfig) {}

  abstract name: string;
  abstract process(letterFiles: (LetterFile & { status: LetterFileStatus })[]): Promise<string>;

  getKey() {
    return `${exportsBasePath}/${v4()}.${this.name}`;
  }

  async uploadFile(key: string, body: any) {
    return this.storageService.uploadFromStream(key, body, {
      originalname: base64Encode(key.split("/").at(-1)),
      ["Content-Type"]: "text/csv",
    });
  }
}
