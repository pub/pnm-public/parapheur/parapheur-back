import Path from "path";
import { Injectable } from "@nestjs/common";
import { SignApi } from "./sign-api.service";
import { SignatureDto } from "../dtos/sign.dto";
import { LetterFileDocumentService } from "./letter-file-document.service";
import { StorageService } from "@pasta/back-files";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { HistoryOperationType, HistoryTarget } from "../entities/letter-file-history.entity";
import { InvalidPageError } from "../errors/sign.error";
import { User } from "@pasta/back-auth";
import { UserPreferencesService } from "./user-preferences.service";
import { DocumentVersionService } from "./document-version.service";
import dayjs from "dayjs";
import { getConvertedKey, getDocVersionKey, getPagePrefix } from "../sign-utils";
import { UserPreferences } from "../entities/user-preferences.entity";
import { EnhanceSignatureDto } from "../dtos/enhance-signature.dto";
import { omit } from "lodash";
import { ParapheurConfig } from "../config/parapheur.config";
import { SignaturePositionService } from "./signature-position.service";

@Injectable()
export class SignService {
  constructor(
    private readonly signApi: SignApi,
    private readonly documentService: LetterFileDocumentService,
    private readonly storageService: StorageService,
    private readonly historyService: LetterFileHistoryService,
    private readonly userPrefService: UserPreferencesService,
    private readonly documentVersionService: DocumentVersionService,
    private readonly parapheurConf: ParapheurConfig,
    private readonly signaturePositionService: SignaturePositionService
  ) {}

  async sign(documentId: number, signatures: SignatureDto[], user: User) {
    const document = await this.documentService.getOne(
      { join: ["folder", "folder.letterFile", "signaturePositions"] },
      documentId
    );
    const { letterFile } = document.folder;

    const userPreferences = await this.userPrefService.findOne({ where: { login: user.login } });
    const mappedSignatures = signatures.map(signature => {
      let key: string;
      if (signature.enhanceOptions) {
        key = this.userPrefService.getEnhancedSignKey(userPreferences, {
          signatureIndex: signature.key,
          ...signature.enhanceOptions,
        });
      } else {
        key = userPreferences.signatures[signature.key];
      }

      return {
        ...omit(signature, "enhanceOptions"),
        key,
      };
    });

    // save old version
    const nbVersions = await this.documentVersionService.repo.count({
      where: { document: { id: document.id } },
      relations: ["document"],
    });
    const ext = document.extension;
    const docVersionKey = getDocVersionKey(letterFile.id, document.id, nbVersions + 1, ext);
    await this.storageService.copyObject(docVersionKey, document.storageKey);
    await this.documentVersionService.createOne(
      {},
      {
        document: { id: document.id },
        author: `${user.firstName} ${user.lastName}`,
        storageKey: docVersionKey,
        extension: document.extension,
      }
    );

    const isConvertedDoc = this.parapheurConf.allowedConversionFormats.includes(document.extension.split(".").pop());
    let targetStorageKey = document.storageKey;
    let extension = document.extension;
    let mimeType = document.mimeType;
    if (isConvertedDoc) {
      const parsedKey = Path.parse(document.storageKey);
      targetStorageKey = Path.join(parsedKey.dir, `${parsedKey.name}.pdf`);
      extension = ".pdf";
      const infos = await this.documentService.getInfos(getConvertedKey(document.storageKey));
      mimeType = infos.mimeType;
    }
    await this.signApi.sign(
      isConvertedDoc ? getConvertedKey(document.storageKey) : document.storageKey,
      mappedSignatures,
      targetStorageKey
    );

    await this.documentService.repo.save({
      id: document.id,
      signed: true,
      storageKey: targetStorageKey,
      extension,
      mimeType,
      modificationDate: this.getNow(),
      signaturePositionsCount: 0,
    });

    if (document.signaturePositions.length) {
      await this.signaturePositionService.repo.delete(document.signaturePositions.map(item => item.id));
    }

    if (isConvertedDoc) {
      await this.storageService.removeObject(document.storageKey);
      await this.storageService.removeObject(getConvertedKey(document.storageKey));
    }

    // remove pages cache
    await this.storageService.removeObjectsInDir(getPagePrefix(letterFile.id, document.folder.id, document.id));

    await this.historyService.registerEvent({
      letterFile,
      type: HistoryOperationType.UPDATE,
      target: HistoryTarget.DOC,
      label: `signé: ${document.name + document.extension}`,
    });
  }

  async getPage(docId: number, page: number): Promise<string> {
    const document = await this.documentService.getOne({ join: ["folder", "folder.letterFile"] }, docId);
    const { folder } = document;
    const { letterFile } = folder;
    const prefix = getPagePrefix(letterFile.id, folder.id, document.id);
    const pageKey = `${prefix}${page}.png`;

    try {
      // throws if object does not exist
      await this.storageService.statObject(pageKey);
      return this.storageService.getLink(pageKey);
    } catch (e) {
      //
    }

    let docStorageKey = document.storageKey;

    // if doc is not already a pdf, convert it
    if (
      document.mimeType !== "application/pdf" &&
      this.parapheurConf.allowedConversionFormats.includes(document.extension.split(".").pop())
    ) {
      docStorageKey = getConvertedKey(document.storageKey);
      try {
        // throws if object does not exist
        await this.storageService.statObject(docStorageKey);
      } catch (e) {
        await this.signApi.convertToPdf(document.storageKey, docStorageKey);
      }
    }

    const { urls } = await this.signApi.generatePreview(docStorageKey, prefix, page);
    const link = urls[0];
    if (!link) throw new InvalidPageError(`Page ${page} does not exist in document ${document.id}`);
    return link;
  }

  async generateEnhancedSignature(options: EnhanceSignatureDto, user: User): Promise<{ url: string }> {
    const userPreferences = await this.userPrefService.findOne({ where: { login: user.login } });

    const signatureKey = userPreferences.signatures.at(options.signatureIndex);
    const text = this.buildSignatureText(user, userPreferences, options);
    const destination = this.userPrefService.getEnhancedSignKey(userPreferences, options);

    return this.signApi.generateEnhancedSignature(signatureKey, text, destination);
  }

  private getNow() {
    return dayjs().toISOString();
  }

  private buildSignatureText(
    user: User,
    userPreferences: UserPreferences,
    options: { name?: boolean; date?: boolean; job?: boolean; city?: boolean }
  ): string {
    let result = "Signé";

    if (options.name) {
      result = `${result} par ${user.firstName} ${user.lastName.toUpperCase()}`;
    }

    if (options.job) {
      result = `${result}, ${userPreferences.job}`;
      if (options.date || options.city) result = `${result},`;
    }

    if (options.date) {
      result = `${result} le ${dayjs().format("DD/MM/YYYY")}`;
    }

    if (options.city) {
      result = `${result} à ${userPreferences.city}`;
    }

    return result;
  }
}
