import { Inject, OnModuleDestroy, OnModuleInit } from "@nestjs/common";
import { HEDWIGE_SYMBOL } from "./hedwige.service";
import { HedwigeClient } from "@pnm3/hedwige-ts-sdk";
import { InjectRepository } from "@nestjs/typeorm";
import { Notification, NotificationType } from "../entities/notification.entity";
import { LessThan, Repository } from "typeorm";
import { UserPreferencesService } from "./user-preferences.service";
import { DEFAULT_USER_PREF, MailingFrequency } from "../entities/user-preferences.entity";
import { Dictionary, flatten, get } from "lodash";
import { Deferred } from "@pasta/deferred";
import { ParapheurConfig } from "../config/parapheur.config";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { HedwigeConfig } from "../config/hedwige.config";
import dayjs from "dayjs";
import "dayjs/locale/fr";
import { LetterFileTemplateData, TemplatingService } from "./templating.service";
import { Counter, UseMetric } from "@pasta/prometheus";

export class NotificationService implements OnModuleInit, OnModuleDestroy {
  @InjectLogger("default", NotificationService.name)
  logger: PastaLogger;

  private timer: NodeJS.Timer;
  private lock: Deferred;

  constructor(
    @InjectRepository(Notification) private readonly repo: Repository<Notification>,
    private readonly parapheurConf: ParapheurConfig,
    private readonly hedwigeConf: HedwigeConfig,
    @Inject(HEDWIGE_SYMBOL) private readonly hedwigeService: HedwigeClient,
    private readonly userPreferencesService: UserPreferencesService,
    private readonly templatingService: TemplatingService
  ) {}

  @UseMetric("email_notifications_sent")
  emailNotificationsCount: Counter;

  async onModuleInit() {
    if (!global.PASTA_IS_CLI) {
      await this.run();
      this.timer = setInterval(this.run.bind(this), this.parapheurConf.mailWorkerInterval);
    }
  }

  async onModuleDestroy() {
    this.logger.info("Received termination signal");
    clearInterval(this.timer);
    if (this.lock && !this.lock.isComplete) {
      this.logger.info("Work in progress, waiting for lock to be released before exiting");
      await this.lock;
    }
  }

  async add(to: string, from: string, letterFile: { id: number }, type: NotificationType) {
    const userPref = (await this.userPreferencesService.findOneBy({ login: to })) || DEFAULT_USER_PREF;
    const mailingFrequency = get(userPref, "mailingFrequency", MailingFrequency.REAL_TIME);
    if (mailingFrequency !== MailingFrequency.NONE && userPref.enabledMailTypes.includes(type)) {
      const sendDate = this.getSendDate(mailingFrequency);
      this.logger.info(`Register new notification`, NotificationService.name, {
        type,
        to,
        from,
        mailingFrequency,
        sendDate: sendDate.toISOString(),
      });
      await this.repo.save([{ type, to, from, sendDate, letterFile }]);
    }
  }

  async run() {
    this.logger.info("Wake up");
    const now = this.getNow();
    if (this.lock && !this.lock.isComplete) return;

    // do nothing when not in working hours / day
    if (!this.parapheurConf.bypassRunningRules) {
      if (now.day() === 0 || now.day() === 6) {
        this.logger.info("Not running because weekend");
        return;
      }

      const { startOfDay, endOfDay } = this.getDates();
      if (now.isAfter(endOfDay)) {
        this.logger.info("Not running because too late");
        return;
      }

      if (now.isBefore(startOfDay)) {
        this.logger.info("Not running because too early");
        return;
      }
    }

    // start run
    this.logger.info("Scan db");
    const notifications = await this.scanDb();

    if (notifications.length) {
      await this.sendMail(
        notifications.reduce(
          this.reducer.bind(this),
          {} as Dictionary<Partial<Record<NotificationType, Notification[]>>>
        )
      );
    }
  }

  public countAll() {
    return this.repo.count();
  }

  private scanDb() {
    return this.repo.find({
      where: { sendDate: LessThan(new Date()) },
      relations: ["letterFile", "letterFile.upstream"],
      order: { to: "ASC" },
    });
  }

  private async sendMail(toSend: Dictionary<Partial<Record<NotificationType, Notification[]>>>) {
    for (const [to, notifs] of Object.entries(toSend)) {
      try {
        const atYourLevelData =
          notifs[NotificationType.AtYourLevel]?.map((...args) => this.mapToTemplate(...args)) || [];
        const addedToCcData = notifs[NotificationType.CC]?.map((...args) => this.mapToTemplate(...args)) || [];
        const recalledData = notifs[NotificationType.RECALL]?.map((...args) => this.mapToTemplate(...args)) || [];

        const html = await this.templatingService.render({
          atYourLevel: atYourLevelData || [],
          addedToCc: addedToCcData || [],
          recalled: recalledData || [],
        });
        const nbNotifs =
          (notifs[NotificationType.AtYourLevel]?.length || 0) +
          (notifs[NotificationType.CC]?.length || 0) +
          (notifs[NotificationType.RECALL]?.length || 0);
        this.logger.info(`Sending mail to ${to}`, NotificationService.name, {
          nbNotifs,
        });

        await this.hedwigeService.sendMail(
          {
            to,
            from: this.hedwigeConf.sourceAddress,
            subject: `Vous avez ${nbNotifs} notification(s) sur le parapheur`,
            html,
          },
          this.parapheurConf.notificationsDryRun
        );
        this.emailNotificationsCount.inc();

        this.logger.info(`Deleting sent notifications for ${to}`, NotificationService.name, {
          nbNotifs,
        });
        await this.repo.delete(flatten(Object.values(notifs)).map(notif => notif.id));
      } catch (e) {
        this.logger.error(`Failed to treat notifications`, e, NotificationService.name, { to });
      }
    }
  }

  private reducer(acc: Dictionary<Partial<Record<NotificationType, Notification[]>>>, curr: Notification) {
    if (acc[curr.to]) {
      if (acc[curr.to][curr.type]) {
        acc[curr.to][curr.type] = [...acc[curr.to][curr.type], curr];
      } else {
        acc[curr.to][curr.type] = [curr];
      }
    } else {
      acc[curr.to] = { [curr.type]: [curr] };
    }
    return acc;
  }

  private mapToTemplate(notif: Notification, index: number, array: Notification[]): LetterFileTemplateData {
    const link = new URL(this.parapheurConf.frontBaseUrl);
    link.pathname = `letter-file/${notif.letterFile.chronoNumber}`;
    return {
      // TODO: remove 2nd OR condition after deployement to prod
      from: notif.from || notif.letterFile.upstream.at(-1).name,
      title: notif.letterFile.name,
      date: dayjs(notif.creationDate).tz(this.parapheurConf.timezone).locale("fr").format("DD MMMM HH:mm"),
      link: link.toString(),
      isLast: index >= array.length - 1,
    };
  }

  private getNow() {
    return dayjs().tz(this.parapheurConf.timezone);
  }

  private getSendDate(mailingFrequency: MailingFrequency): Date {
    const now = this.getNow();
    const baseSendDate = this.getNow().second(0).millisecond(0);

    const { startOfDay, endOfDay, morningDate, afternoonDate } = this.getDates();

    if (now.day() === 0 || now.day() === 6) {
      // weekend
      const nbDaysToAdd = now.day() === 0 ? 1 : 2;
      if (mailingFrequency === MailingFrequency.REAL_TIME) {
        return startOfDay.add(nbDaysToAdd, "days").toDate();
      }
      return morningDate.add(nbDaysToAdd, "days").toDate();
    }
    // working day - non working hours
    if (now.isAfter(endOfDay)) {
      // after end of day & before midnight
      let nbDaysToAdd = 1;
      if (now.day() === 5) nbDaysToAdd = 3; // friday -> next monday
      if (mailingFrequency === MailingFrequency.REAL_TIME) {
        return startOfDay.add(nbDaysToAdd, "day").toDate();
      }
      return morningDate.add(nbDaysToAdd, "day").toDate();
    }
    if (now.isBefore(startOfDay)) {
      // between midnight and start of day
      if (mailingFrequency === MailingFrequency.REAL_TIME) {
        return startOfDay.toDate();
      }
      return morningDate.toDate();
    }
    // during working hours
    if (mailingFrequency === MailingFrequency.REAL_TIME) {
      return baseSendDate.toDate();
    }
    if (now.isBefore(morningDate)) {
      // before morning send time
      return morningDate.toDate();
    }
    if (mailingFrequency === MailingFrequency.ONCE_A_DAY && now.isAfter(morningDate)) {
      return morningDate.add(1, "day").toDate();
    }
    if (mailingFrequency === MailingFrequency.TWICE_A_DAY) {
      if (now.isAfter(morningDate) && now.isBefore(afternoonDate)) {
        return afternoonDate.toDate();
      }
      return morningDate.add(1, "day").toDate();
    }
  }

  private getDates() {
    const baseDate = this.getNow().second(0).millisecond(0);
    const startOfDay = baseDate.hour(this.parapheurConf.startOfDayHour).minute(this.parapheurConf.startOfDayMinute);
    const endOfDay = baseDate.hour(this.parapheurConf.endOfDayHour).minute(this.parapheurConf.endOfDayMinute);
    const morningDate = baseDate
      .hour(this.parapheurConf.morningEmailHour)
      .minute(this.parapheurConf.morningEmailMinute);
    const afternoonDate = baseDate
      .hour(this.parapheurConf.afternoonEmailHour)
      .minute(this.parapheurConf.afternoonEmailMinute);

    return { startOfDay, endOfDay, morningDate, afternoonDate };
  }
}
