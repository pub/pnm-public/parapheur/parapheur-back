import { FactoryProvider } from "@nestjs/common";
import { HedwigeClient } from "@pnm3/hedwige-ts-sdk";
import { HedwigeConfig } from "../config/hedwige.config";
import { ApiManagerConfig, PastaApiManagerTokenService } from "@pasta/api-manager-client";
import { getTunnelingAgents } from "@pasta/proxy";

export const HEDWIGE_SYMBOL = Symbol("HedwigeClient");

export const HedwigeService: FactoryProvider<HedwigeClient> = {
  provide: HEDWIGE_SYMBOL,
  useFactory: async (
    config: HedwigeConfig,
    tokenProvider: PastaApiManagerTokenService,
    apiManagerConfig: ApiManagerConfig
  ) => {
    return new HedwigeClient(
      {
        version: config.version,
        prefix: config.urlPrefix,
        baseURL: apiManagerConfig.baseUrl,
      },
      tokenProvider,
      {
        ...getTunnelingAgents(),
      }
    );
  },
  inject: [HedwigeConfig, PastaApiManagerTokenService, ApiManagerConfig],
};
