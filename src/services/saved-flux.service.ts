import { AbstractPastaCrudService } from "@pasta/back-core";
import { LetterFileSavedFlux } from "../entities/saved-flux.entity";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";

export class LetterFileSavedFuxService extends AbstractPastaCrudService<LetterFileSavedFlux> {
  constructor(@InjectRepository(LetterFileSavedFlux) public readonly repo: Repository<LetterFileSavedFlux>) {
    super();
  }
}
