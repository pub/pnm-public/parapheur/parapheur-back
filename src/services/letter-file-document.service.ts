import { forwardRef, Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AbstractPastaCrudService, IPastaCrudQuery, Nullable } from "@pasta/back-core";
import { StorageService } from "@pasta/back-files";
import { isNil } from "lodash";
import { DeepPartial, Repository, SelectQueryBuilder } from "typeorm";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { LetterFileError } from "../errors/letter-file.error";
import { LetterFileFolderService } from "./letter-file-folder.service";
import { LetterFileIndexationService } from "./letter-file-indexation.service";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { HistoryOperationType, HistoryTarget } from "../entities/letter-file-history.entity";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import { SignApi } from "./sign-api.service";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";
import { getConvertedKey, getDocVersionBaseKey } from "../sign-utils";
import { ParapheurConfig } from "../config/parapheur.config";
import { DocumentError } from "../errors/document.error";

@Injectable()
export class LetterFileDocumentService extends AbstractPastaCrudService<LetterFileDocument> {
  constructor(
    @InjectRepository(LetterFileDocument) public readonly repo: Repository<LetterFileDocument>,
    private readonly storageService: StorageService,
    @Inject(forwardRef(() => LetterFileFolderService)) private readonly folderService: LetterFileFolderService,
    private readonly indexationService: LetterFileIndexationService,
    private readonly historyService: LetterFileHistoryService,
    private readonly signApi: SignApi,
    private readonly parapheurConf: ParapheurConfig
  ) {
    super();
  }

  async getNextPosition(folder: number): Promise<number> {
    const { maxPosition } = await this.repo
      .createQueryBuilder("letterFileDocument")
      .select("MAX(letterFileDocument.position)", "maxPosition")
      .where("letterFileDocument.folder = :folder", { folder })
      .getRawOne();

    return isNil(maxPosition) ? 0 : maxPosition + 1;
  }

  async checkFileExist(storageKey: string): Promise<boolean> {
    try {
      return !!(await this.storageService.statObject(storageKey));
    } catch (e) {
      return false;
    }
  }

  async createOne(
    query: IPastaCrudQuery,
    body: DeepPartial<LetterFileDocument>,
    skipIndexation?: boolean
  ): Promise<LetterFileDocument> {
    if (body.storageKey && !(await this.checkFileExist(body.storageKey))) {
      throw new Error("File does not exist");
    }

    const folder = await this.folderService.getOne({ join: ["letterFile"] }, body.folder.id);

    let result: LetterFileDocument;
    if (body.storageKey) {
      const newStorageKey = `${folder.letterFile.id}/${folder.id}/${body.storageKey}`;

      await this.storageService.moveObject(newStorageKey, body.storageKey);

      const { mimeType } = await this.getInfos(newStorageKey);

      result = await super.createOne(query, {
        ...body,
        name: body.name,
        extension: body.extension,
        storageKey: newStorageKey,
        position: await this.getNextPosition(body.folder.id),
        mimeType,
      });
    } else {
      result = await super.createOne(query, {
        ...body,
        position: await this.getNextPosition(body.folder.id),
      });
    }

    if (!skipIndexation) await this.updateIndex(folder.letterFile.id);

    const noun = result.type === LetterFileDocumentType.DEFAULT ? "document" : "lien";
    const name = result.type === LetterFileDocumentType.DEFAULT ? result.name + result.extension : result.name;
    await this.historyService.registerEvent({
      letterFile: folder.letterFile,
      label: `ajout du ${noun} ${name} dans le répertoire "${this.getFolderNamePretty(folder)}"`,
      target: HistoryTarget.DOC,
      type: HistoryOperationType.CREATE,
    });

    return result;
  }

  async deleteOne(_query: IPastaCrudQuery, id: number): Promise<any> {
    const document = await this.repo.findOne({ where: { id }, relations: ["folder", "folder.letterFile"] });
    const oldPosition = document.position;
    const result = await super.deleteOne(_query, id);

    await this.repo
      .createQueryBuilder()
      .update()
      .set({ position: () => "position - 1" })
      .where("position >= :position", { position: oldPosition })
      .andWhere("folderId = :folder", { folder: document.folder.id })
      .execute();

    if (document.type === LetterFileDocumentType.DEFAULT) {
      await this.storageService.removeObject(document.storageKey);
      if (document.mimeType === "application/pdf") {
        const baseKey = `${getDocVersionBaseKey(document.folder.letterFile.id, document.id)}/`;
        await this.storageService.removeObjectsInDir(baseKey);
      }
    }

    await this.updateIndex(document.folder.letterFile.id);

    const noun = document.type === LetterFileDocumentType.DEFAULT ? "document" : "lien";
    const name = document.type === LetterFileDocumentType.DEFAULT ? document.name + document.extension : document.name;
    await this.historyService.registerEvent({
      letterFile: document.folder.letterFile,
      label: `suppression du ${noun} ${name}`,
      target: HistoryTarget.DOC,
      type: HistoryOperationType.DELETE,
    });

    return result;
  }

  /**
   * Only removes docs from DB, does not reindex or remove from storage
   */
  async forgetForFolder(id: number) {
    await this.repo.createQueryBuilder().delete().where(`"folderId" = :folderId`, { folderId: id }).execute();
  }

  async updateOne(
    query: IPastaCrudQuery,
    id: number,
    body: DeepPartial<LetterFileDocument>
  ): Promise<LetterFileDocument> {
    const document = await this.repo.findOne({ where: { id }, relations: ["folder", "folder.letterFile"] });

    // sync with minio
    if (body.folder) {
      const targetFolder = await this.folderService.findOneBy({ id: body.folder.id });
      if (document.type === LetterFileDocumentType.DEFAULT) {
        body.storageKey = this.getKeyForFolder(document.storageKey, body.folder.id);
        await this.storageService.moveObject(body.storageKey, document.storageKey);
      }

      const noun = document.type === LetterFileDocumentType.DEFAULT ? "document" : "lien";
      const name =
        document.type === LetterFileDocumentType.DEFAULT ? document.name + document.extension : document.name;
      await this.historyService.registerEvent({
        letterFile: document.folder.letterFile,
        label: `déplacement du ${noun} ${name} vers le dossier ${this.getFolderNamePretty(targetFolder)}`,
        target: HistoryTarget.DOC,
        type: HistoryOperationType.UPDATE,
      });
    }

    const result = await super.updateOne(query, id, body);

    if (Object.keys(body).includes("name")) {
      await this.updateIndex(document.folder.letterFile.id);
    }

    if (body.name) {
      const origName =
        document.type === LetterFileDocumentType.DEFAULT ? document.name + document.extension : document.name;
      const resultName = result.type === LetterFileDocumentType.DEFAULT ? result.name + result.extension : result.name;
      await this.historyService.registerEvent({
        letterFile: document.folder.letterFile,
        label: `renommage "${origName}" -> "${resultName}"`,
        target: HistoryTarget.DOC,
        type: HistoryOperationType.UPDATE,
      });
    }

    return result;
  }

  async reorder(documents: LetterFileDocument[], sortedIds: number[]): Promise<LetterFileDocument[]> {
    if (documents.length !== sortedIds.length) {
      throw new LetterFileError("Documents and sorted ids length mismatch");
    }
    return this.repo.save(
      sortedIds.map((el, index) => {
        const document = documents.find(fl => fl.id === el);

        if (!document) {
          throw new LetterFileError(`Document with id ${el} not found`);
        }

        return { ...document, position: index };
      })
    );
  }

  async getLink(id: number, asAttachment: boolean = false) {
    const document = await this.getOne({ join: ["folder", "folder.letterFile"] }, id);
    return this.storageService.getLink(document.storageKey, asAttachment, document.name + document.extension);
  }

  async computeNbPages(id: number) {
    const document = await this.getOne({}, id);
    const extension = document.extension;
    let nbPages: Nullable<number> = null;
    if (document.mimeType === "application/pdf") {
      nbPages = await this.signApi.getNbPages(document.storageKey);
    } else if (this.parapheurConf.allowedConversionFormats.includes(extension.replace(".", ""))) {
      const convertedStorageKey = getConvertedKey(document.storageKey);
      await this.signApi.convertToPdf(document.storageKey, convertedStorageKey);
      nbPages = await this.signApi.getNbPages(convertedStorageKey);
    } else {
      throw new DocumentError("Document format not supported");
    }

    await this.repo.update(id, { nbPages });
    return { nbPages };
  }

  async getInfos(storageKey: string): Promise<{ mimeType: string }> {
    const stats = await this.storageService.statObject(storageKey);
    const mimeType: string = stats.metaData["content-type"] || "";
    return { mimeType };
  }

  private async updateIndex(lfId: number) {
    const siblings = await this.repo.find({
      relations: ["folder", "folder.letterFile"],
      where: { folder: { letterFile: { id: lfId } } },
    });
    await this.indexationService.putDocuments(lfId, siblings);
  }

  private getFolderNamePretty(folder: LetterFileFolder): string {
    return folder.name === "$$ROOT$$" ? "racine" : folder.name;
  }

  private getKeyForFolder(currentKey: string, folderId: number): string {
    const splitKey = currentKey.split("/");
    splitKey[1] = `${folderId}`;
    return splitKey.join("/");
  }
}
