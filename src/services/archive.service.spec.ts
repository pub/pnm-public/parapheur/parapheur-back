import { Test, TestingModule } from "@nestjs/testing";
import { ArchiveService } from "./archive.service";
import { StorageService, base64Encode } from "@pasta/back-files";
import { mockObject } from "@pasta/back-core";
import * as fs from "fs/promises";
import * as os from "os";
import { get, set } from "lodash";
import { User } from "@pasta/back-auth";
import { LetterFile } from "../entities/letter-file.entity";
import * as uuid from "uuid";
import { exportsBasePath } from "./abstract.exporter";
import { createWriteStream } from "fs";
import archiver from "archiver";
import { EventEmitter } from "typeorm/platform/PlatformTools";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";
import dedent from "dedent";

jest.mock("fs/promises");
jest.mock("fs");
jest.mock("os");
jest.mock("uuid");
jest.mock("archiver");

describe("ArchiveService", () => {
  let service: ArchiveService;
  let storageService: StorageService;

  beforeEach(async () => {
    jest.resetAllMocks();

    storageService = mockObject(StorageService);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: ArchiveService, useClass: ArchiveService },
        { provide: StorageService, useValue: storageService },
      ],
    }).compile();

    service = module.get(ArchiveService);
  });

  it("should be defined", async () => {
    expect(service).toBeDefined();
  });

  describe("the generateArchiveForLf method", () => {
    it("works", async () => {
      // given
      const folder1Docs = [
        { id: 20, name: "doc.pdf", storageKey: "1/10/thekey", type: LetterFileDocumentType.DEFAULT },
      ];
      const folder2Docs = [
        { id: 21, name: "presentation.ppt", storageKey: "1/11/otherkey", type: LetterFileDocumentType.DEFAULT },
        { id: 22, name: "mon lien", url: "http://fract.org", type: LetterFileDocumentType.URL },
      ];
      const lf = {
        id: 1,
        name: "Voici le projet avec/des caractères spéciaux et surtout à rallonge car trop n'est jamais assez pour décrire correctement ce qui doit l'être",
        folders: [
          { id: 10, name: "$$ROOT$$", documents: folder1Docs },
          { id: 11, name: "dossier1", documents: folder2Docs },
        ],
      };
      (os.tmpdir as jest.Mock).mockReturnValue("abc123");
      (fs.mkdtemp as jest.Mock).mockResolvedValue("abc123/parapheur-05328");
      const prepareFolderMock = jest.fn();
      set(service, "prepareFolder", prepareFolderMock);
      const finalizeArchiveMock = jest.fn().mockResolvedValue("thelink");
      set(service, "finalizeArchive", finalizeArchiveMock);

      // when
      const result = await service.generateArchiveForLf(lf as any);

      // then
      expect(result).toEqual("thelink");
      expect(fs.mkdtemp).toHaveBeenCalledTimes(1);
      expect(prepareFolderMock).toHaveBeenCalledTimes(2);
      expect(prepareFolderMock).toHaveBeenNthCalledWith(1, "abc123/parapheur-05328", "dossier racine", folder1Docs);
      expect(prepareFolderMock).toHaveBeenNthCalledWith(2, "abc123/parapheur-05328", "dossier1", folder2Docs);
      expect(finalizeArchiveMock).toHaveBeenCalledWith(
        "abc123/parapheur-05328",
        "abc123/Voici_le_projet_avec_des_caractères_spéciaux_et_surtout_à_rallonge_car_trop_n'est_jamais_assez_pour_décrire_correctement_ce_qui_.zip",
        "1"
      );
    });
  });

  describe("the generateArchiveForFolder method", () => {
    it("works", async () => {
      // given
      const docs = [
        { id: 21, name: "presentation.ppt", storageKey: "1/11/otherkey", type: LetterFileDocumentType.DEFAULT },
        { id: 22, name: "mon lien", url: "http://fract.org", type: LetterFileDocumentType.URL },
      ];
      const folder = { id: 10, name: "$$ROOT$$", documents: docs, letterFile: { id: 1 } };
      (os.tmpdir as jest.Mock).mockReturnValue("abc123");
      (fs.mkdtemp as jest.Mock).mockResolvedValue("abc123/parapheur-05328");
      const prepareFolderMock = jest.fn();
      set(service, "prepareFolder", prepareFolderMock);
      const finalizeArchiveMock = jest.fn().mockResolvedValue("thelink");
      set(service, "finalizeArchive", finalizeArchiveMock);

      // when
      const result = await service.generateArchiveForFolder(folder as any);

      // then
      expect(result).toEqual("thelink");
      expect(fs.mkdtemp).toHaveBeenCalledTimes(1);
      expect(prepareFolderMock).toHaveBeenCalledWith("abc123/parapheur-05328", "dossier racine", docs);
      expect(finalizeArchiveMock).toHaveBeenCalledWith("abc123/parapheur-05328", "abc123/dossier_racine.zip", "1/10");
    });
  });

  describe("the generateArchiveForMany method", () => {
    it("works", async () => {
      // given
      const csvKey = "the/csv/key.csv";
      const lf1Folder1Docs = [
        { id: 20, name: "doc.pdf", storageKey: "1/10/thekey", type: LetterFileDocumentType.DEFAULT },
      ];
      const lf1Folder2Docs = [
        { id: 21, name: "presentation.ppt", storageKey: "1/11/otherkey", type: LetterFileDocumentType.DEFAULT },
        { id: 22, name: "mon lien", url: "http://fract.org", type: LetterFileDocumentType.URL },
      ];
      const lf2Docs = [
        { id: 30, name: "tableur.xls", storageKey: "1/20/blakey", type: LetterFileDocumentType.DEFAULT },
      ];
      const letterFiles = [
        {
          id: 1,
          name: "toto",
          folders: [
            { id: 10, name: "$$ROOT$$", documents: lf1Folder1Docs },
            { id: 11, name: "dossier1", documents: lf1Folder2Docs },
          ],
        },
        { id: 2, chronoNumber: "chrono123", folders: [{ id: 20, name: "$$ROOT$$", documents: lf2Docs }] },
      ] as LetterFile[];
      const author = { id: 50 } as User;
      (fs.mkdtemp as jest.Mock)
        .mockResolvedValueOnce("abc123/tmp-path-05328")
        .mockResolvedValueOnce("abc123/tmp-path-59583");
      (os.tmpdir as jest.Mock).mockReturnValueOnce("abc123").mockReturnValueOnce("def456");
      (uuid.v4 as jest.Mock).mockReturnValue("239G128QZ49BCX52310");
      const prepareFolderMock = jest.fn();
      set(service, "prepareFolder", prepareFolderMock);
      const archiveDirMock = jest.fn();
      set(service, "archiveDir", archiveDirMock);
      (storageService.getLink as jest.Mock).mockResolvedValue("thelink");

      // when
      const result = await service.generateArchiveForMany(csvKey, letterFiles, author);

      // then
      expect(result).toEqual({ storageKey: `${exportsBasePath}/50/239G128QZ49BCX52310.zip`, link: "thelink" });
      expect(os.tmpdir).toHaveBeenCalledTimes(2);
      expect(fs.mkdtemp).toHaveBeenNthCalledWith(1, "abc123/export-");
      expect(fs.mkdtemp).toHaveBeenNthCalledWith(2, "def456/zip-");
      expect(prepareFolderMock).toHaveBeenNthCalledWith(
        1,
        "abc123/tmp-path-05328/toto",
        "dossier racine",
        lf1Folder1Docs
      );
      expect(prepareFolderMock).toHaveBeenNthCalledWith(2, "abc123/tmp-path-05328/toto", "dossier1", lf1Folder2Docs);
      expect(prepareFolderMock).toHaveBeenNthCalledWith(
        3,
        "abc123/tmp-path-05328/Parapheur-chrono123",
        "dossier racine",
        lf2Docs
      );
      expect(archiveDirMock).toHaveBeenCalledWith(
        "abc123/tmp-path-05328",
        "abc123/tmp-path-59583/239G128QZ49BCX52310.zip"
      );
      expect(storageService.uploadFile).toHaveBeenCalledWith(
        `${exportsBasePath}/50/239G128QZ49BCX52310.zip`,
        "abc123/tmp-path-59583/239G128QZ49BCX52310.zip",
        { originalname: base64Encode("239G128QZ49BCX52310.zip") }
      );
      expect(storageService.removeObject).toHaveBeenCalledWith(csvKey);
      expect(storageService.getLink).toHaveBeenCalledWith(`${exportsBasePath}/50/239G128QZ49BCX52310.zip`, true);
    });
  });

  describe("the archiveDir private method", () => {
    it("works", async () => {
      // given
      const dir = "dir";
      const to = "to";
      const writeStream = new EventEmitter();
      (createWriteStream as jest.Mock).mockReturnValue(writeStream);
      const archiveMock = { on: jest.fn(), finalize: jest.fn(), directory: jest.fn(), pipe: jest.fn() };
      (archiver as unknown as jest.Mock).mockReturnValue(archiveMock);

      // when
      const archiveDir = (get(service, "archiveDir") as Function).bind(service);
      const promise = archiveDir(dir, to);
      writeStream.emit("close");
      const result = await promise;

      // then
      expect(result).toEqual(to);
      expect(createWriteStream).toHaveBeenCalledWith(to);
      expect(archiver).toHaveBeenCalledWith("zip", { zlib: { level: 9 } });
      expect(archiveMock.directory).toHaveBeenCalledWith(dir, false);
      expect(archiveMock.pipe).toHaveBeenCalledWith(writeStream);
    });
  });

  describe("the prepareFolder private method", () => {
    it("works", async () => {
      // given
      const prepareFolder = (get(service, "prepareFolder") as Function).bind(service);
      const tmpFolder = "/tmp/abc";
      const folderName = "tralala";
      const documents = [
        { id: 1, type: LetterFileDocumentType.DEFAULT, name: "toto", storageKey: "thekey", extension: ".pdf" },
        { id: 2, type: LetterFileDocumentType.URL, name: "youpi", url: "http://the.url" },
      ];
      const getUrlFileContentMock = jest.fn().mockReturnValue("the content");
      set(service, "getUrlFileContent", getUrlFileContentMock);

      // when
      const result = await prepareFolder(tmpFolder, folderName, documents);

      // then
      expect(result).toBeUndefined();
      expect(fs.mkdir).toHaveBeenCalledWith("/tmp/abc/tralala", { recursive: true });
      expect(storageService.downloadFile).toHaveBeenCalledWith("thekey", "/tmp/abc/tralala/toto.pdf");
      expect(fs.writeFile).toHaveBeenCalledWith("/tmp/abc/tralala/youpi.url", "the content");
    });
  });

  describe("the getUrlFileContent private method", () => {
    it("works", async () => {
      // given
      const getUrlFileContent = (get(service, "getUrlFileContent") as Function).bind(service);

      // when
      const result = getUrlFileContent("http://fract.org");

      // then
      expect(result).toEqual(dedent`
        [InternetShortcut]
        URL=http://fract.org
      `);
    });
  });

  describe("the finalizeArchive private method", () => {
    it("works", async () => {
      // given
      const finalizeArchive = (get(service, "finalizeArchive") as Function).bind(service);
      const tmpFolder = "/tmp/abc";
      const archivePath = "tmp/abc/toto.zip";
      const bucketPath = "1";
      const archiveDirMock = jest.fn();
      set(service, "archiveDir", archiveDirMock);
      (storageService.getLink as jest.Mock).mockResolvedValue("thekey");

      // when
      const result = await finalizeArchive(tmpFolder, archivePath, bucketPath);

      // then
      expect(result).toEqual("thekey");
      expect(archiveDirMock).toHaveBeenCalledWith(tmpFolder, archivePath);
      expect(storageService.uploadFile).toHaveBeenCalledWith("archives/1/toto.zip", archivePath);
      expect(storageService.getLink).toHaveBeenCalledWith("archives/1/toto.zip");
    });
  });
});
