import { Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import { HistoryOperationType, HistoryTarget, LetterFileHistory } from "../entities/letter-file-history.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { ClsService } from "nestjs-cls";
import { UserService } from "@pasta/back-auth";
import { AbstractPastaCrudService } from "@pasta/back-core";

@Injectable()
export class LetterFileHistoryService extends AbstractPastaCrudService<LetterFileHistory> {
  constructor(
    @InjectRepository(LetterFileHistory) public readonly repo: Repository<LetterFileHistory>,
    private readonly clsService: ClsService,
    private readonly userService: UserService
  ) {
    super();
  }

  async registerEvent(event: {
    letterFile: { id: number };
    type: HistoryOperationType;
    label: string;
    target: HistoryTarget;
  }) {
    const { firstName, lastName } = await this.userService.findOne({
      where: { id: this.clsService.get("user") },
      select: ["firstName", "lastName"],
    });
    return this.repo.save({ ...event, author: `${firstName} ${lastName}` });
  }
}
