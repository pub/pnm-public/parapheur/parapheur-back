import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AbstractPastaCrudService, IPastaCrudQuery } from "@pasta/back-core";
import { DeepPartial, Repository } from "typeorm";
import { LetterFileComment } from "../entities/letter-file-comment.entity";
import { LetterFileIndexationService } from "./letter-file-indexation.service";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { HistoryTarget, HistoryOperationType } from "../entities/letter-file-history.entity";

@Injectable()
export class LetterFileCommentService extends AbstractPastaCrudService<LetterFileComment> {
  constructor(
    @InjectRepository(LetterFileComment) public readonly repo: Repository<LetterFileComment>,
    private readonly indexationService: LetterFileIndexationService,
    private readonly historyService: LetterFileHistoryService
  ) {
    super();
  }

  freeze(letterFileId: number) {
    return this.repo.update({ letterFile: { id: letterFileId } }, { frozen: true });
  }

  async createOne(
    query: IPastaCrudQuery,
    body: DeepPartial<LetterFileComment>,
    skipHistory = false
  ): Promise<LetterFileComment> {
    const result = await super.createOne(query, body);
    await this.updateIndex(body.letterFile as number);
    if (!skipHistory) {
      await this.historyService.registerEvent({
        letterFile: { id: body.letterFile as number },
        label: "Ajout de commentaire",
        target: HistoryTarget.COMMENT,
        type: HistoryOperationType.CREATE,
      });
    }
    return result;
  }

  async updateOne(
    query: IPastaCrudQuery,
    id: number,
    body: DeepPartial<LetterFileComment>
  ): Promise<LetterFileComment> {
    const result = await super.updateOne({ ...query, join:  ["letterFile"] }, id, body);
    await this.updateIndex(result.letterFile.id);
    await this.historyService.registerEvent({
      letterFile: { id: result.letterFile.id },
      label: "Mise à jour de commentaire",
      target: HistoryTarget.COMMENT,
      type: HistoryOperationType.UPDATE,
    });
    return result;
  }

  async deleteOne(query: IPastaCrudQuery, id: number): Promise<any> {
    const { letterFile } = await this.getOne({ join: ["letterFile"] }, id);
    const result = await super.deleteOne(query, id);
    await this.updateIndex(letterFile.id);
    await this.historyService.registerEvent({
      letterFile,
      label: "Suppression de commentaire",
      target: HistoryTarget.COMMENT,
      type: HistoryOperationType.DELETE,
    });
    return result;
  }

  private async updateIndex(lfId: number) {
    const comments = await this.getMany({ join: ["letterFile"], filter: { "letterFile.id": { $eq: lfId } } });
    await this.indexationService.putComments(lfId, comments.data);
  }
}
