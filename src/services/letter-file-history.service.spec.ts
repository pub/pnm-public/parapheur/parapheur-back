import { LetterFileHistoryService } from "./letter-file-history.service";
import { MockRepository, PastaCrudHookService, PastaCrudOperatorService, mockObject } from "@pasta/back-core";
import { Test, TestingModule } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { HistoryOperationType, HistoryTarget, LetterFileHistory } from "../entities/letter-file-history.entity";
import { UserService } from "@pasta/back-auth";
import { ClsService } from "nestjs-cls";

describe("LetterFileHistoryService", () => {
  let service: LetterFileHistoryService;
  let repo: MockRepository<LetterFileHistory>;
  let clsService: ClsService;
  let userService: UserService;

  beforeEach(async () => {
    repo = new MockRepository<LetterFileHistory>();
    clsService = mockObject(ClsService);
    userService = mockObject(UserService);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: LetterFileHistoryService, useClass: LetterFileHistoryService },
        { provide: getRepositoryToken(LetterFileHistory), useValue: repo },
        { provide: ClsService, useValue: clsService },
        { provide: UserService, useValue: userService },
        { provide: PastaCrudOperatorService, useValue: jest.fn() },
        { provide: PastaCrudHookService, useValue: jest.fn() },
      ],
    }).compile();

    service = module.get(LetterFileHistoryService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("the registerEvent method", () => {
    it("registers the event", async () => {
      // given
      const userId = 1;
      const letterFile = { id: 2 };
      (clsService.get as jest.Mock).mockReturnValue(userId);
      const user = { id: userId, firstName: "John", lastName: "Doe" };
      (userService.findOne as jest.Mock).mockResolvedValue(user);
      const returnedEntity = { the: "returned entity" };
      repo.save.mockResolvedValue(returnedEntity);
      const event = {
        letterFile,
        type: HistoryOperationType.CREATE,
        label: "toto",
        target: HistoryTarget.COMMENT,
      };

      // when
      const result = await service.registerEvent(event);

      // then
      expect(result).toEqual(returnedEntity);
      expect(clsService.get).toHaveBeenCalledWith("user");
      expect(userService.findOne).toHaveBeenCalledWith({ where: { id: userId }, select: ["firstName", "lastName"] });
      expect(repo.save).toHaveBeenCalledWith({ ...event, author: "John Doe" });
    });
  });
});
