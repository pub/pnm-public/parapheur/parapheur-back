import { ConflictException, Injectable, InternalServerErrorException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AbstractPastaCrudService, IPastaCrudQuery, PastaCrudGetManyResult } from "@pasta/back-core";
import { StorageService, keyFn } from "@pasta/back-files";
import { get, intersection, isNil, maxBy, omit, padStart, pick, sortBy } from "lodash";
import { Brackets, DeepPartial, Repository, SelectQueryBuilder } from "typeorm";
import { AppendDownstreamDto, ReorderDownstreamDto } from "../dtos/flux-element.dto";
import { LetterFile } from "../entities/letter-file.entity";
import {
  NextError,
  RecallArchivedError,
  RecallDraftError,
  TakeArchivedError,
  TakeDraftError,
  UndraftError,
} from "../errors/letter-file.error";
import { NotFoundInDbError } from "../errors/not-found.error";
import { FluxElementService } from "./flux-element.service";
import { DownstreamFluxElement } from "../entities/downstream-flux-element.entity";
import dayjs from "dayjs";
import { User } from "@pasta/back-auth";
import { ReorderLetterFileFoldersDto } from "../dtos/letter-file-folder.dto";
import { LetterFileFolderService } from "./letter-file-folder.service";
import { HomeDto } from "../dtos/home.dto";
import { LetterFileCommentService } from "./letter-file-comment.service";
import { LetterFileStatus } from "../types/letter-file-status.enum";
import { ArchiveService } from "./archive.service";
import { LetterFileIndexationService } from "./letter-file-indexation.service";
import { LetterFileTagService } from "./letter-file-tag.service";
import { LetterFileSearchDto } from "../dtos/letter-file.dto";
import { LetterFileCCUserService } from "./letter-file-cc-user.service";
import { LetterFileSavedFuxService } from "./saved-flux.service";
import { LetterFileDocumentService } from "./letter-file-document.service";
import { LdapService } from "./ldap.service";
import { NotificationService } from "./notification.service";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { HistoryOperationType, HistoryTarget } from "../entities/letter-file-history.entity";
import { UserPreferencesService } from "./user-preferences.service";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";
import { NotificationType } from "../entities/notification.entity";
import { LetterFileEventService } from "./LetterFileEvent.service";

@Injectable()
export class LetterFileService extends AbstractPastaCrudService<LetterFile> {
  constructor(
    @InjectRepository(LetterFile) public readonly repo: Repository<LetterFile>,
    private readonly fluxService: FluxElementService,
    private readonly folderService: LetterFileFolderService,
    private readonly documentService: LetterFileDocumentService,
    private readonly storageService: StorageService,
    private readonly commentService: LetterFileCommentService,
    private readonly ccUserService: LetterFileCCUserService,
    private readonly archiveService: ArchiveService,
    private readonly tagsService: LetterFileTagService,
    private readonly indexationService: LetterFileIndexationService,
    private readonly savedFluxService: LetterFileSavedFuxService,
    private readonly ldapService: LdapService,
    private readonly notificationService: NotificationService,
    private readonly historyService: LetterFileHistoryService,
    private readonly userPreferencesService: UserPreferencesService,
    private readonly letterFileEventService: LetterFileEventService
  ) {
    super();
  }

  async getHome(mail: string) {
    const maxListLength = 100;

    const { relatedMails } = await this.ldapService.lookup(mail);
    const query = this.repo
      .createQueryBuilder("letterFile")
      .select()
      .leftJoinAndSelect("letterFile.upstream", "upstream")
      .leftJoinAndSelect("letterFile.downstream", "downstream")
      .leftJoinAndSelect("letterFile.ccUsers", "ccUsers")
      .leftJoinAndSelect("letterFile.tags", "tags")
      .andWhere(qb => {
        qb.where(
          `letterFile.id in ${qb
            .subQuery()
            .select("innerLf.id")
            .from(LetterFile, "innerLf")
            .leftJoin("innerLf.upstream", "upstream")
            .leftJoin("innerLf.downstream", "downstream")
            .leftJoin("innerLf.ccUsers", "ccUsers")
            .leftJoin("innerLf.tags", "tags")
            .andWhere(
              new Brackets((qb: SelectQueryBuilder<LetterFile>) => {
                return qb
                  .where("upstream.mail IN (:...relatedMails)", { relatedMails })
                  .orWhere("downstream.mail IN (:...relatedMails)", { relatedMails })
                  .orWhere("ccUsers.mail IN (:...relatedMails)", { relatedMails });
              })
            )
            .getQuery()}`
        );
      });

    const userPrefs = await this.userPreferencesService.findOne({
      select: ["hideLetterFilesBefore"],
      where: { login: mail },
    });
    if (userPrefs && userPrefs.hideLetterFilesBefore) {
      query.where("letterFile.creationDate >= :hideLetterFilesBefore", {
        hideLetterFilesBefore: userPrefs.hideLetterFilesBefore,
      });
    }

    const letterFiles = await query.orderBy("letterFile.modificationDate", "DESC").getMany();

    const result = HomeDto.fromPlain(
      letterFiles.reduce(
        (acc, letterFile) => {
          if (this.isCurrent(letterFile, relatedMails)) {
            if (letterFile.draft) acc.current.draft++;
            else acc.current.total++;
            if (acc.current.data.length < maxListLength) {
              acc.current.data.push({
                ...omit(letterFile, ["upstream", "downstream"]),
                from: maxBy(letterFile.upstream, "position"),
                tags: sortBy(letterFile.tags, "id"),
                recalled: letterFile.downstream.find(el => el.position === 0)?.recalled,
              });
            }
          } else if (this.isDownstream(letterFile, relatedMails)) {
            const current = letterFile.downstream.find(lf => lf.position === 0);
            if (!letterFile.draft) {
              acc.todo.total++;
              if (acc.todo.data.length < maxListLength) {
                acc.todo.data.push({
                  ...omit(letterFile, ["upstream", "downstream"]),
                  current,
                  tags: sortBy(letterFile.tags, "id"),
                });
              }
            }
          } else if (this.isUpstream(letterFile, relatedMails) || this.isCCUser(letterFile, relatedMails)) {
            const current = letterFile.downstream.find(lf => lf.position === 0);
            const status = this.getStatus(letterFile, relatedMails);
            if (status === LetterFileStatus.ARCHIVED) acc.done.archived++;
            else acc.done.total++;
            if (acc.done.data.length < maxListLength) {
              acc.done.data.push({
                ...omit(letterFile, ["upstream", "downstream"]),
                current,
                tags: sortBy(letterFile.tags, "id"),
                status,
              });
            }
          }

          return acc;
        },
        {
          todo: { data: [], total: 0 },
          current: { data: [], total: 0, draft: 0 },
          done: { data: [], total: 0, archived: 0 },
        } as HomeDto
      )
    );
    result.current.data = sortBy(result.current.data, "recalled", "modificationDate");
    return result;
  }

  isUpstream(letterFile: LetterFile, relatedMails: string[]): boolean {
    return letterFile.upstream.some(element => relatedMails.includes(element.mail));
  }

  isDownstream(letterFile: LetterFile, relatedMails: string[]): boolean {
    return letterFile.downstream.some(element => relatedMails.includes(element.mail));
  }

  isCCUser(letterFile: LetterFile, relatedMails: string[]): boolean {
    return letterFile.ccUsers.some(element => relatedMails.includes(element.mail));
  }

  isCurrent(letterFile: LetterFile, relatedMails: string[]): boolean {
    const currentElement = letterFile.downstream.find(element => element.position === 0);
    return relatedMails.includes(currentElement?.mail);
  }

  isArchived(letterFile: LetterFile): boolean {
    return letterFile.archived || false;
  }

  getStatus(letterFile: LetterFile, relatedMails: string[]): LetterFileStatus {
    if (this.isArchived(letterFile)) {
      return LetterFileStatus.ARCHIVED;
    }

    if (this.isCurrent(letterFile, relatedMails)) {
      return LetterFileStatus.CURRENT;
    }

    if (this.isDownstream(letterFile, relatedMails)) {
      return LetterFileStatus.DOWNSTREAM;
    }

    if (this.isUpstream(letterFile, relatedMails)) {
      return LetterFileStatus.UPSTREAM;
    }

    if (this.isCCUser(letterFile, relatedMails)) {
      return LetterFileStatus.CC;
    }

    return LetterFileStatus.UNKNOWN;
  }

  async getNextChronoNumber(): Promise<string> {
    const chronoNumberPrefix = this.getChronoNumberPrefix();

    const count = await this.repo
      .createQueryBuilder("letterFile")
      .withDeleted()
      .select("letterFile.chronoNumber")
      .where("letterFile.chronoNumber LIKE :chronoNumberPrefix", { chronoNumberPrefix: `${chronoNumberPrefix}%` })
      .getCount();

    return `${chronoNumberPrefix}${padStart(`${count + 1}`, 7, "0")}`;
  }

  async createOne(query: IPastaCrudQuery, body: DeepPartial<LetterFile>): Promise<LetterFile> {
    const mail = get(body.downstream, "0.mail", null) || get(body.downstream, "0", null);

    if (!mail) {
      throw new InternalServerErrorException("No mail found in downstream");
    }

    // retry mechanism to handle rare case where we try to insert the same chrononumber for multiple letter files
    let letterFile: LetterFile;
    let nbTries = 0;
    const maxTries = 10;
    while (!letterFile && nbTries <= maxTries) {
      nbTries += 1;
      try {
        if (nbTries > 1) {
          body.chronoNumber = await this.getNextChronoNumber();
        }
        letterFile = await super.createOne(query, body);
      } catch (e) {
        if (e.code === "23505") {
          // PSQL throws a QueryFailedError with code '23505' upon a failed unicity check
          if (nbTries >= maxTries) throw new ConflictException("Could not create letter file");
        } else {
          // no retry on other errors
          throw e;
        }
      }
    }
    const flux = await this.fluxService.addOneDownstream(mail, letterFile.id);
    letterFile.downstream = [omit(flux[0], ["letterFile", "id"])] as DownstreamFluxElement[];
    await this.indexationService.indexNewLetterFile(letterFile, pick(flux[0], "mail", "name"));

    await this.historyService.registerEvent({
      letterFile,
      label: "Création",
      target: HistoryTarget["BASE-INFO"],
      type: HistoryOperationType.CREATE,
    });

    return letterFile;
  }

  async updateOne(query: IPastaCrudQuery, id: number, body: DeepPartial<LetterFile>): Promise<LetterFile> {
    const result = await super.updateOne(query, id, body);
    if (body.tags) {
      const tags = await this.tagsService.getMany({ filter: { id: { $in: body.tags.map(tag => tag.id) } } });
      await this.indexationService.putTags(
        id,
        tags.data.map(tag => tag.name),
        result.modificationDate
      );
      await this.historyService.registerEvent({
        letterFile: result,
        label: `Modification des badges: "${tags.data.map(tag => tag.name).join(", ")}"`,
        target: HistoryTarget["BASE-INFO"],
        type: HistoryOperationType.UPDATE,
      });
    }

    if (Object.keys(omit(body, "tags")).length) {
      await this.indexationService.patchBase(id, {
        ...pick(
          body,
          "name",
          "chronoNumber",
          "deadline",
          "arrivalDate",
          "externalMailNumber",
          "draft",
          "archived",
          "senderOrganization",
          "senderName",
          "senderRole",
          "mailType",
          "recipientName",
          "manager"
        ),
        modificationDate: result.modificationDate,
      } as any);

      const baseFieldsMap: Partial<Record<keyof LetterFile, string>> = {
        name: "nom du parapheur",
        deadline: "date limite de traitement",
        manager: "responsable du parapheur",
      };
      const externalFieldsMap: Partial<Record<keyof LetterFile, string>> = {
        senderOrganization: "organisme",
        senderName: "nom, prénom",
        senderRole: "fonction",
        mailType: "type de courrier",
        externalMailNumber: "numéro de courrier extérieur",
        arrivalDate: "date du courrier extérieur",
        recipientName: "destinataire du courrier reçu",
      };
      const modifiedFields = Object.keys(body);
      const modifiedBaseFields = intersection(modifiedFields, Object.keys(baseFieldsMap));
      const modifiedExternalFields = intersection(modifiedFields, Object.keys(externalFieldsMap));
      if (modifiedBaseFields.length) {
        await this.historyService.registerEvent({
          letterFile: result,
          label: this.getModifiedFieldLabel(modifiedBaseFields, baseFieldsMap, body),
          target: HistoryTarget["BASE-INFO"],
          type: HistoryOperationType.UPDATE,
        });
      } else if (modifiedExternalFields.length) {
        await this.historyService.registerEvent({
          letterFile: result,
          label: this.getModifiedFieldLabel(modifiedExternalFields, externalFieldsMap, body),
          target: HistoryTarget["EXTERNAL-INFO"],
          type: HistoryOperationType.UPDATE,
        });
      }
    }

    return result;
  }

  async next(query: IPastaCrudQuery, id: number, user: User) {
    const letterFile = await this.getOne(
      {
        join: ["downstream", "upstream"],
      },
      id
    );

    if (!letterFile) {
      throw new NotFoundInDbError("No letter file found");
    }

    if (letterFile.draft) {
      throw new NextError("Not permitted on a draft letter file");
    }

    const current = letterFile.downstream.find(el => el.position === 0);
    await this.fluxService.moveToUpstream(current.id);
    if (letterFile.downstream.length === 1) {
      await this.updateOne({}, letterFile.id, { archived: true, unArchivedDate: null });
      await this.letterFileEventService.emit("archive", letterFile.id, user);
    } else {
      await super.updateOne({}, letterFile.id, { modificationDate: dayjs() });
      await this.letterFileEventService.emit("next", letterFile.id, user);
    }

    await this.commentService.freeze(letterFile.id);
    const ccUsersToNotify = await this.ccUserService.findBy({ frozen: false, letterFile: { id: letterFile.id } });
    await this.ccUserService.freeze(letterFile.id);

    if (ccUsersToNotify.length) {
      for (const ccUser of ccUsersToNotify) {
        await this.notificationService.add(ccUser.mail, current.name, letterFile, NotificationType.CC);
      }
    }

    const result = await this.getOne({ ...query, join: ["downstream", "upstream"] }, id);
    if (result.downstream.length) {
      await this.notificationService.add(
        result.downstream.find(el => el.position === 0).mail,
        current.name,
        result,
        NotificationType.AtYourLevel
      );
    }

    await this.indexationService.putFlux(id, result);
    return result;
  }

  async take(query: IPastaCrudQuery, id: number, user: User, isSuperadmin: boolean) {
    const lf = await this.getOne({}, id);
    if (lf.draft) {
      throw new TakeDraftError("Should not take a draft letter file");
    }
    if (lf.archived) {
      throw new TakeArchivedError("Should not take an archived letter file");
    }

    await this.commentService.freeze(id);
    await this.ccUserService.freeze(id);
    await this.fluxService.take(user.login, id);

    const result = await this.getOne({ ...query, join: ["downstream"] }, id);
    await this.indexationService.putDownstream(id, result.downstream);
    if (isSuperadmin) {
      // superadmin might not be already related to letter file so we need to reindex that part as well
      const { relatedUsers } = await this.indexationService.getOne(id);
      await this.indexationService.putRelatedUsers(id, [...relatedUsers, { mail: user.login }]);
    }

    // socket event
    await this.letterFileEventService.emit("take", id, user);

    return result;
  }

  async recall(_query: IPastaCrudQuery, id: number) {
    const lf = await this.getOne({ join: ["downstream"] }, id);
    if (lf.draft) {
      throw new RecallDraftError("Cannot recall a draft letter file");
    }
    if (lf.archived) {
      throw new RecallArchivedError("Cannot recall an archived letter file");
    }

    const current = await this.fluxService.getCurrent(id);
    await this.fluxService.setRecalled(current.id);

    await this.notificationService.add(current.mail, current.name, lf, NotificationType.RECALL);
    await this.historyService.registerEvent({
      letterFile: lf,
      label: `Relance ${current.name}`,
      target: HistoryTarget.FLUX,
      type: HistoryOperationType.UPDATE,
    });
    return lf;
  }

  async undraft(query: IPastaCrudQuery, id: number, user: User) {
    const letterFile = await this.getOne(
      {
        join: ["downstream", "upstream", "folders", "tags"],
      },
      id
    );

    if (!letterFile) {
      throw new NotFoundInDbError("No letter file found");
    }

    if (!letterFile.draft) {
      throw new UndraftError("Cannot undraft a letterfile that is not draft");
    }

    await this.repo.save({ ...letterFile, draft: false });
    const result = await this.next(query, id, user);
    await this.indexationService.patchBase(letterFile.id, { draft: false });
    return result;
  }

  async deleteOne(_query: IPastaCrudQuery, id: number): Promise<any> {
    await this.storageService.removeObjectsInDir(`${id}/`, true);
    const result = await this.repo.softDelete(id);
    await this.indexationService.remove(id);
    return result;
  }

  async appendDownstream(query: IPastaCrudQuery, id: number, body: AppendDownstreamDto) {
    const letterFile = await this.getOne(
      {
        join: ["downstream"],
      },
      id
    );

    const addedElement = await this.fluxService.addOneDownstream(body.mail, id);
    await this.indexationService.putDownstream(letterFile.id, [...letterFile.downstream, addedElement[0]]);
    const { relatedUsers } = await this.indexationService.getOne(letterFile.id);
    await this.indexationService.putRelatedUsers(letterFile.id, [...relatedUsers, addedElement[0]]);
    return this.getOne(query, id);
  }

  async emptyDownstream(query: IPastaCrudQuery, id: number) {
    const letterFile = await this.getOne({ join: ["upstream", "ccUsers", "downstream"] }, id); // ensures it exists
    const current = letterFile.downstream.find(el => el.position === 0);
    await this.fluxService.emptyDownstream(id);
    await this.indexationService.putDownstream(id, [current]);
    await this.indexationService.putRelatedUsers(id, [current, ...letterFile.upstream, ...letterFile.ccUsers]);
    return this.getOne(query, id);
  }

  async reorderDownstream(query: IPastaCrudQuery, id: number, body: ReorderDownstreamDto) {
    const letterFile = await this.getOne(
      {
        join: ["downstream", "upstream"],
      },
      id
    );

    if (!letterFile) {
      throw new NotFoundInDbError("No letter file found");
    }

    await this.fluxService.reorder(letterFile.downstream, body.downstream);
    return this.getOne(query, id);
  }

  async reorderFolders(query: IPastaCrudQuery, id: number, body: ReorderLetterFileFoldersDto) {
    const letterFile = await this.getOne(
      {
        join: ["folders"],
      },
      id
    );

    if (!letterFile) {
      throw new NotFoundInDbError("No letter file found");
    }

    await this.folderService.reorder(letterFile.folders, body.folders);
    return this.getOne(query, id);
  }

  async removeDownstream(query: IPastaCrudQuery, id: number, downstreamId: number) {
    await this.fluxService.removeOneDownstream(downstreamId);
    const lf = await this.getOne(
      {
        join: ["downstream", "upstream", "ccUsers"],
      },
      id
    );
    await this.indexationService.putDownstream(lf.id, lf.downstream);
    await this.indexationService.putRelatedUsers(lf.id, [...lf.upstream, ...lf.downstream, ...lf.ccUsers]);
    return this.getOne(query, id);
  }

  async generateArchive(id: number) {
    const letterFile = await this.getOne({ join: ["folders", "folders.documents"] }, id);

    const key = await this.archiveService.generateArchiveForLf(letterFile);

    await this.historyService.registerEvent({
      letterFile,
      label: "Téléchargement de tous les dossiers et documents",
      target: HistoryTarget.DEFAULT,
      type: HistoryOperationType.DOWNLOAD,
    });

    return key;
  }

  async search(
    dto: LetterFileSearchDto,
    pagination: { offset: number; limit: number },
    mail: string
  ): Promise<PastaCrudGetManyResult<LetterFile>> {
    const filters: object[] = [];
    let should: object[] = [];
    const mustNot: object[] = [];

    const userPrefs = await this.userPreferencesService.findOne({
      select: ["hideLetterFilesBefore"],
      where: { login: mail },
    });
    if (userPrefs && userPrefs.hideLetterFilesBefore) {
      filters.push({
        range: {
          creationDate: { gte: userPrefs.hideLetterFilesBefore },
        },
      });
    }

    if (!isNil(dto.archived)) filters.push({ term: { archived: { value: dto.archived } } });
    if (dto.tags) {
      filters.push({
        terms_set: {
          tags: {
            terms: dto.tags,
            minimum_should_match_script: { source: dto.tags.length.toString() },
          },
        },
      });
    }
    if (dto.fileTypes) {
      // Warning, ES documentation states that doing a wildcard search that starts with *
      // may cause performance issues
      // https://www.elastic.co/guide/en/elasticsearch/reference/8.8/query-dsl-wildcard-query.html
      // A potential solution would be to index the list of extensions as keyword and do a "term" search
      should = dto.fileTypes.map(fileType => {
        return {
          wildcard: { "documents.name.keyword": { value: `*${fileType}` } },
        };
      });
    }
    if (dto.startDate) {
      mustNot.push({ range: { archivedDate: { lt: dto.startDate } } });
    }
    if (dto.endDate) {
      mustNot.push({ range: { creationDate: { gt: dto.endDate } } });
    }
    if (dto.createdStartDate) {
      filters.push({ range: { creationDate: { gte: dto.createdStartDate } } });
    }
    if (dto.createdEndDate) {
      filters.push({ range: { creationDate: { lte: dto.createdEndDate } } });
    }
    if (dto.createdByMe) {
      filters.push({
        term: { "creator.mail": { value: mail } },
      });
    }
    if (dto.alreadySeen) {
      filters.push({
        term: { "upstream.mail": { value: mail } },
      });
      filters.push({
        term: { "downstream.mail": { value: mail } },
      });
    }

    const orConditions: object[] = [];
    if (dto.inTodo) {
      orConditions.push({ term: { "downstream.mail": { value: mail } } });
    }
    if (dto.inCurrent) {
      orConditions.push({ term: { "current.mail": { value: mail } } });
    }
    if (dto.inDone) {
      orConditions.push({
        bool: {
          filter: [{ term: { "upstream.mail": { value: mail } } }],
          mustNot: [{ term: { "current.mail": { value: mail } } }, { term: { "downstream.mail": { value: mail } } }],
        },
      });
    }

    const { total, items } = await this.indexationService.search(
      {
        andConditions: { searchPattern: dto.searchPattern || "", filters, should, mustNot },
        orConditions,
        userEmails: (await this.ldapService.lookup(mail)).relatedMails,
        highlight: [
          "name",
          "chronoNumber",
          "current.name",
          "comments",
          "documents.name",
          "documents.name.keyword",
          "externalMailNumber",
          "ccUsers.name",
          "downstream.name",
          "upstream.name",
          "senderOrganization",
          "senderName",
          "senderRole",
          "mailType",
          "recipientName",
          "manager",
          "folders",
        ],
      },
      { from: pagination.offset, size: pagination.limit }
    );

    const result = await this.getMany({
      join: ["tags", "upstream", "downstream", "ccUsers", "folders", "folders.documents"],
      filter: { $and: [{ id: { $in: items.map(item => item.id) } }] },
    });

    const page = (pagination.offset + pagination.limit) / pagination.limit;
    const isLastPage = (page - 1) * pagination.limit + items.length >= total;

    return {
      // preserve ES sorting
      data: items.map(item => {
        return {
          ...result.data.find(letterFile => letterFile.id === item.id),
          highlight: item.highlight,
        };
      }),
      page,
      total,
      isLastPage,
      limit: pagination.limit,
      offset: pagination.offset,
    };
  }

  async removeAllDocs(id: number) {
    const letterFile = await this.getOne({ join: ["folders"] }, id);

    const rootFolder = letterFile.folders.find(folder => folder.name === "$$ROOT$$")!;
    const nonRootFolders = letterFile.folders.filter(folder => folder.name !== "$$ROOT$$");
    await this.documentService.forgetForFolder(rootFolder.id);
    await this.folderService.forget(nonRootFolders.map(folder => folder.id));

    await this.storageService.removeObjectsInDir(`${id}/`, true);
    await this.indexationService.putDocuments(letterFile.id, []);
    await this.indexationService.putFolders(letterFile.id, []);

    await this.historyService.registerEvent({
      target: HistoryTarget.DOC,
      type: HistoryOperationType.DELETE,
      label: "Suppression de tous les classeurs et documents",
      letterFile,
    });
  }

  async applyFlux(lfId: number, fluxId: number) {
    // automatically produces 404 errors if not found
    await this.getOne({}, lfId);
    const savedFlux = await this.savedFluxService.getOne({}, fluxId);

    await this.fluxService.addManyDownstream(savedFlux.flux, lfId, savedFlux.name);
    const result = await this.getOne({ join: ["downstream"] }, lfId);
    await this.indexationService.putDownstream(lfId, result.downstream);
    const { relatedUsers } = await this.indexationService.getOne(lfId);
    await this.indexationService.putRelatedUsers(lfId, [...relatedUsers, ...result.downstream]);
  }

  async clone(id: number, user: User) {
    // no need for upstream, ccUsers or comments because they are empty by design
    const source = await this.getOne({ join: ["tags", "folders", "folders.documents", "downstream"] }, id);
    const clone = await this.createOne(
      { join: ["folders"] },
      {
        name: `[Dupliqué] ${source.name}`,
        chronoNumber: await this.getNextChronoNumber(),
        draft: true,
        archived: false,
        unArchivedDate: null,
        ...pick(source, "tags", "deadline", "arrivalDate", "externalMailNumber"),
        downstream: [{ mail: user.login }],
      }
    );

    // docs & folders
    for (const folder of sortBy(source.folders, "position")) {
      const folderMap = new Map<number, number>();
      if (folder.name === "$$ROOT$$") {
        folderMap.set(folder.id, clone.folders[0].id);
      } else {
        const clonedFolder = await this.folderService.createOne(
          {},
          { ...pick(folder, "name"), letterFile: { id: clone.id } },
          true
        );
        folderMap.set(folder.id, clonedFolder.id);
      }

      for (const doc of folder.documents) {
        let storageKey: string;
        if (doc.type === LetterFileDocumentType.DEFAULT) {
          storageKey = this.getKey(doc.name + doc.extension);
          await this.storageService.copyObject(storageKey, doc.storageKey);
        }
        await this.documentService.createOne(
          {},
          {
            type: doc.type,
            name: doc.name,
            extension: doc.extension,
            position: doc.position,
            storageKey,
            url: doc.url,
            folder: { id: folderMap.get(folder.id) },
          },
          true
        );
      }
    }

    const fullClone = await this.getOne({ join: ["tags", "folders", "folders.documents", "downstream"] }, clone.id);
    await this.indexationService.indexFull(fullClone);
    return fullClone;
  }

  async reject(query: IPastaCrudQuery, id: number, userId: number) {
    const current = await this.fluxService.getCurrent(id);

    // TODO new "reject" here with fluxService

    await this.notificationService.add("todo", current.name, { id }, NotificationType.AtYourLevel);
    await this.historyService.registerEvent({
      letterFile: { id },
      type: HistoryOperationType.UPDATE,
      label: `renvoi à ${"todo"}`,
      target: HistoryTarget.FLUX,
    });
    await this.commentService.createOne(
      {},
      { text: `renvoi à ${"todo"}`, author: { id: userId }, frozen: true, letterFile: id as any },
      true
    );

    await this.commentService.freeze(id);
    await this.ccUserService.freeze(id);

    const letterFile = await this.getOne({ ...query, join: ["upstream", "downstream"] }, id);
    await this.indexationService.putFlux(id, letterFile);
    return letterFile;
  }

  async unarchive(id: number, mail: string) {
    await this.updateOne({}, id, { archived: false, unArchivedDate: new Date() });
    await this.fluxService.addOneDownstream(mail, id);
    await this.historyService.registerEvent({
      letterFile: { id },
      type: HistoryOperationType.UPDATE,
      label: "Parapheur désarchivé",
      target: HistoryTarget.DEFAULT,
    });
  }

  private getChronoNumberPrefix() {
    return dayjs().format("YYYYMM");
  }

  private getKey(docName: string) {
    // truthfully, keyFn only needs originalname to work properly
    return keyFn({}, { originalname: docName } as Express.Multer.File);
  }

  private getModifiedFieldLabel(
    fields: string[],
    fieldsMap: Partial<Record<keyof LetterFile, string>>,
    body: DeepPartial<LetterFile>
  ): string {
    if (fields.length > 1) {
      return `Modification des champs "${fields
        .map(field => fieldsMap[field])
        .join(", ")
        .trimEnd()}"`;
    }
    const label = `Modification du champ "${fieldsMap[fields[0]]}"`;
    const newVal = body[fields[0]] || "";
    if (newVal) {
      const dateVal = dayjs(newVal);
      return `${label} -> ${dateVal.isValid() ? dateVal.format("DD/MM/YYYY") : newVal}`;
    }
    return `${label} -> champ supprimé`;
  }
}
