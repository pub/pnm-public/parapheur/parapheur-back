import { HedwigeClient } from "@pnm3/hedwige-ts-sdk";
import { HedwigeConfig } from "../config/hedwige.config";
import { ParapheurConfig } from "../config/parapheur.config";
import { NotificationService } from "./notification.service";
import { UserPreferencesService } from "./user-preferences.service";
import { MockRepository, mockObject } from "@pasta/back-core";
import { Test, TestingModule } from "@nestjs/testing";
import { get, set } from "lodash";
import { MailingFrequency } from "../entities/user-preferences.entity";
import { getRepositoryToken } from "@nestjs/typeorm";
import { Notification } from "../entities/notification.entity";
import { HEDWIGE_SYMBOL } from "./hedwige.service";
import dayjs from "dayjs";
import { TemplatingService } from "./templating.service";

describe("NotificationService", () => {
  let service: NotificationService;
  const mockRepo = new MockRepository();
  let parapheurConf: ParapheurConfig;
  let hedwigeConf: HedwigeConfig;
  let hedwigeService: HedwigeClient;
  let userPreferencesService: UserPreferencesService;
  let templatingService: TemplatingService;

  beforeEach(async () => {
    parapheurConf = mockObject(ParapheurConfig);
    parapheurConf.startOfDayHour = 8;
    parapheurConf.startOfDayMinute = 0;
    parapheurConf.morningEmailHour = 10;
    parapheurConf.morningEmailMinute = 0;
    parapheurConf.afternoonEmailHour = 15;
    parapheurConf.afternoonEmailMinute = 0;
    parapheurConf.endOfDayHour = 19;
    parapheurConf.endOfDayMinute = 55;
    hedwigeConf = mockObject(HedwigeConfig);
    hedwigeService = mockObject(HedwigeClient);
    userPreferencesService = mockObject(UserPreferencesService);
    templatingService = mockObject(TemplatingService);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NotificationService,
        { provide: getRepositoryToken(Notification), useValue: mockRepo },
        { provide: ParapheurConfig, useValue: parapheurConf },
        { provide: HedwigeConfig, useValue: hedwigeConf },
        { provide: HEDWIGE_SYMBOL, useValue: hedwigeService },
        { provide: UserPreferencesService, useValue: userPreferencesService },
        { provide: TemplatingService, useValue: templatingService },
      ],
    }).compile();

    service = module.get<NotificationService>(NotificationService);
  });

  it("should exist", async () => {
    expect(NotificationService).toBeDefined();
  });

  describe("the getSendDate method", () => {
    it("returns next monday 8AM when in saturday + realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(6);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.REAL_TIME);

      // then
      expect(sendDate).toEqual(dayjs(now).add(2, "days").hour(8).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns next monday 8AM when in sunday + realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(0);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.REAL_TIME);

      // then
      expect(sendDate).toEqual(dayjs(now).add(1, "days").hour(8).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns next monday 10AM when in saturday + non realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(6);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.ONCE_A_DAY);

      // then
      expect(sendDate).toEqual(dayjs(now).add(2, "days").hour(10).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns next monday 10AM when in sunday + non realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(0);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.ONCE_A_DAY);

      // then
      expect(sendDate).toEqual(dayjs(now).add(1, "days").hour(10).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns 8AM tomorrow when after 19:55 and not friday, realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(1).hour(19).minute(56);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.REAL_TIME);

      // then
      expect(sendDate).toEqual(dayjs(now).add(1, "days").hour(8).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns 10AM tomorrow when after 19:55 and not friday, non realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(1).hour(19).minute(56);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.ONCE_A_DAY);

      // then
      expect(sendDate).toEqual(dayjs(now).add(1, "days").hour(10).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns next monday 8AM when after 19:55 and friday, realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(5).hour(19).minute(56);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.REAL_TIME);

      // then
      expect(sendDate).toEqual(dayjs(now).add(3, "days").hour(8).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns next monday 10AM when after 19:55 and friday, non realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(5).hour(19).minute(56);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.ONCE_A_DAY);

      // then
      expect(sendDate).toEqual(dayjs(now).add(3, "days").hour(10).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns today 8AM when before 8AM, realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(1).hour(6).minute(0);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.REAL_TIME);

      // then
      expect(sendDate).toEqual(dayjs(now).hour(8).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns today 10AM when 6AM, non realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(1).hour(6).minute(0);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.ONCE_A_DAY);

      // then
      expect(sendDate).toEqual(dayjs(now).hour(10).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns today 10AM when 9AM, non realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(1).hour(9).minute(0);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.ONCE_A_DAY);

      // then
      expect(sendDate).toEqual(dayjs(now).hour(10).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns now when during working hours, realtime", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(1).hour(11).minute(3);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.REAL_TIME);

      // then
      expect(sendDate).toEqual(dayjs(now).hour(11).minute(3).second(0).millisecond(0).toDate());
    });

    it("returns 3PM when 11AM, twice a day", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(1).hour(11).minute(0);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.TWICE_A_DAY);

      // then
      expect(sendDate).toEqual(dayjs(now).hour(15).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns tomorrow 10AM when 11AM, once a day", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(1).hour(11).minute(0);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.ONCE_A_DAY);

      // then
      expect(sendDate).toEqual(dayjs(now).add(1, "day").hour(10).minute(0).second(0).millisecond(0).toDate());
    });

    it("returns tomorrow 10AM when 4PM, twice a day", async () => {
      // given
      const getSendDate: (mailingFrequency: MailingFrequency) => Date = get(service, "getSendDate");
      const now = dayjs().day(1).hour(16).minute(0);
      set(service, "getNow", jest.fn().mockReturnValue(now));

      // when
      const sendDate = getSendDate.call(service, MailingFrequency.TWICE_A_DAY);

      // then
      expect(sendDate).toEqual(dayjs(now).add(1, "day").hour(10).minute(0).second(0).millisecond(0).toDate());
    });
  });
});
