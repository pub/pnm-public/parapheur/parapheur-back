import { Injectable, OnModuleInit } from "@nestjs/common";
import { PastaWebsocketService } from "../PastaSocket/services/pasta.websocket.service";
import { Socket } from "socket.io";
import { LdapService } from "./ldap.service";
import { User } from "@pasta/back-auth";

@Injectable()
export class LetterFileEventService implements OnModuleInit {
  constructor(
    private readonly pastaWsService: PastaWebsocketService,
    private readonly ldapService: LdapService
  ) {}

  onModuleInit() {
    this.pastaWsService.on("viewLetterFile", async (letterFileId: number | null, client: Socket) => {
      for (const room of client.rooms) {
        if (room.startsWith("letter-file-") && room !== this.getRoomName(letterFileId)) {
          client.leave(room);
        }
      }
      if (letterFileId) {
        await client.join(this.getRoomName(letterFileId));
      }
    });
  }

  async emit(event: string, letterFileId: number, user: User) {
    const result = await this.ldapService.lookup(user.login);
    this.pastaWsService.to(this.getRoomName(letterFileId), {
      type: event,
      data: { letterFileId, userId: user.id, userName: result.name },
    });
  }

  private getRoomName(letterFileId: number) {
    return `letter-file-${letterFileId}`;
  }
}
