import { LetterFile } from "../entities/letter-file.entity";
import { stringify } from "csv-stringify";
import { LetterFileStatus } from "../types/letter-file-status.enum";
import dayjs from "dayjs";
import { flatten, sortBy } from "lodash";
import { ParapheurConfig } from "../config/parapheur.config";
import { Inject, Injectable } from "@nestjs/common";
import { AbstractExporter } from "./abstract.exporter";

export interface LetterFileCSVMap {
  [k: string]: string;
  position: string;
  name: string;
  manager: string;
  tags: string;
  status: string;
  dueDateFor: string;
  remainingDays: string;
  assignedTo: string;
  latestUpstreamDate: string;
  initiatedBy: string;
  initiatedDate: string;
  previousAssignee: string;
  flux: string;
  ccUsers: string;
  chronoNumber: string;
  recipientName: string;
  senderOrganization: string;
  senderName: string;
  senderRole: string;
  mailType: string;
  externalMailNumber: string;
  arrivalDate: string;
  fileNames: string;
  link: string;
  documents?: string;
}

@Injectable()
export class CsvExporter extends AbstractExporter {
  name = "csv";

  @Inject(ParapheurConfig)
  private readonly parapheurConf: ParapheurConfig;

  async process(
    letterFiles: (LetterFile & { status?: LetterFileStatus })[],
    docPathMap?: Record<number, { key: string; path: string }[]>
  ) {
    const key = this.getKey();
    const columns: LetterFileCSVMap = {
      position: "Parapheur positionné",
      name: "Objet",
      manager: "Responsable du parapheur",
      tags: "Badges",
      status: "Statut",
      dueDateFor: "Date limite de traitement",
      remainingDays: "Jours restants avant échéance",
      assignedTo: "Avancement dans le flux",
      latestUpstreamDate: "Date du dernier changement d'état",
      initiatedBy: "Initié par",
      initiatedDate: "Date d'initiation du parapheur",
      previousAssignee: "Provenant",
      flux: "Flux",
      ccUsers: "Utilisateurs en copie",
      chronoNumber: "Numéro Chrono",
      recipientName: "Destinataire du courrier",
      senderOrganization: "Expéditeur Organisme",
      senderName: "Expéditeur Nom prénom",
      senderRole: "Expéditeur Fonction",
      mailType: "Expéditeur Type de courrier",
      externalMailNumber: "Expéditeur Référence courrier extérieur",
      arrivalDate: "Expéditeur Date du courrier extérieur",
      fileNames: "Nom des PJ",
      link: "Lien d'accès direct",
    };
    if (docPathMap) {
      columns.documents = "Archive des documents";
    }

    const data = letterFiles.map(lf => this.mapOneToCsv(lf, docPathMap[lf.id]));
    await this.uploadFile(key, stringify(data, { header: true, columns, bom: true, delimiter: ";" }));
    return key;
  }

  private mapOneToCsv(
    letterFile: LetterFile & { status?: LetterFileStatus },
    docPaths?: { key: string; path: string }[]
  ): LetterFileCSVMap {
    const sortedUpStream = sortBy(letterFile.upstream, "position");
    const sortedDownStream = sortBy(letterFile.downstream, "position");
    const result: LetterFileCSVMap = {
      position: this.getRelativePosition(letterFile),
      name: letterFile.name,
      manager: letterFile.manager || "",
      tags: (letterFile.tags || [])
        .map(tag => tag.name)
        .join(", ")
        .trimEnd(),
      status: letterFile.status ? this.getActiveStatus(letterFile) : null,
      dueDateFor: letterFile.deadline ? dayjs(letterFile.deadline).format("DD/MM/YYYY") : "",
      remainingDays: this.getRemainingDays(letterFile),
      assignedTo: letterFile.downstream.find(el => el.position === 0)?.name || "",
      latestUpstreamDate: sortedUpStream.at(-1) ? dayjs(sortedUpStream.at(-1).creationDate).format("DD/MM/YYYY") : "",
      initiatedBy: sortBy([...letterFile.upstream, ...letterFile.downstream], "creationDate").at(-1).name,
      initiatedDate: letterFile.creationDate ? dayjs(letterFile.creationDate).format("DD/MM/YYYY") : "",
      previousAssignee: sortedUpStream.at(-1)?.name || "",
      flux: [...sortedUpStream, ...sortedDownStream]
        .map(el => el.name)
        .join(", ")
        .trimEnd(),
      ccUsers: letterFile.ccUsers
        .map(el => el.name)
        .join(", ")
        .trimEnd(),
      chronoNumber: letterFile.chronoNumber,
      recipientName: letterFile.recipientName || "",
      senderOrganization: letterFile.senderOrganization || "",
      senderName: letterFile.senderName || "",
      senderRole: letterFile.senderRole || "",
      mailType: letterFile.mailType || "",
      externalMailNumber: letterFile.externalMailNumber || "",
      arrivalDate: letterFile.arrivalDate ? dayjs(letterFile.arrivalDate).format("DD/MM/YYYY") : "",
      fileNames: flatten(letterFile.folders.map(folder => folder.documents))
        .map(doc => doc.name)
        .join(", ")
        .trimEnd(),
      link: this.parapheurConf.frontBaseUrl + "letter-file/" + letterFile.chronoNumber,
    };

    if (docPaths) {
      const docMap = docPaths.reduce((acc, curr) => {
        return { ...acc, [curr.key]: curr.path };
      }, {} as Record<string, string>);
      result.documents = flatten(letterFile.folders.map(folder => folder.documents))
        .map(doc => docMap[doc.storageKey || doc.url])
        .join(", ");
    }

    return result;
  }

  private getRelativePosition(letterFile: LetterFile & { status?: LetterFileStatus }) {
    switch (letterFile.status) {
      case LetterFileStatus.CC:
        return "Copie";
      case LetterFileStatus.CURRENT:
        return "En cours à mon niveau";
      case LetterFileStatus.UPSTREAM:
      case LetterFileStatus.ARCHIVED:
        return "En aval";
      case LetterFileStatus.DOWNSTREAM:
        return "En amont";
      default:
        return "indéterminé";
    }
  }

  private getActiveStatus(letterFile: LetterFile & { status?: LetterFileStatus }) {
    if (letterFile.draft) return "Brouillon";
    if (letterFile.archived) return "Archivé";
    return "Actif";
  }

  private getRemainingDays(letterFile: LetterFile): string {
    if (!letterFile.deadline) return "";
    return dayjs(letterFile.deadline)
      .diff(dayjs(new Date().setHours(0, 0, 0, 0)), "day")
      .toString();
  }
}
