import { Injectable } from "@nestjs/common";
import { createWriteStream } from "fs";
import * as fs from "fs/promises";
import * as path from "path";
import * as os from "os";
import archiver from "archiver";
import { StorageService, base64Encode } from "@pasta/back-files";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { LetterFile } from "../entities/letter-file.entity";
import { User } from "@pasta/back-auth";
import { exportsBasePath } from "./abstract.exporter";
import { v4 } from "uuid";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import dedent from "dedent";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";

export const archiveBasePath = "archives";

@Injectable()
export class ArchiveService {
  @InjectLogger("default", ArchiveService.name)
  logger: PastaLogger;

  constructor(private readonly storageService: StorageService) {}

  async generateArchiveForLf(lf: LetterFile) {
    const tmpFolder = await fs.mkdtemp(path.join(os.tmpdir(), "parapheur-"));
    const archiveName = lf.name || `Parapheur-${lf.chronoNumber}`;
    const bucketPath = lf.id.toString();
    const archivePath = path.join(
      path.dirname(tmpFolder),
      `${archiveName.replaceAll(" ", "_").replaceAll("/", "_").substring(0, 128)}.zip`
    );

    for (const folder of lf.folders) {
      const folderName = folder.name === "$$ROOT$$" ? "dossier racine" : folder.name;
      await this.prepareFolder(tmpFolder, folderName, folder.documents);
    }

    return this.finalizeArchive(tmpFolder, archivePath, bucketPath);
  }

  async generateArchiveForFolder(folder: LetterFileFolder) {
    const tmpFolder = await fs.mkdtemp(path.join(os.tmpdir(), "parapheur-"));
    const folderName = folder.name === "$$ROOT$$" ? "dossier racine" : folder.name;
    const bucketPath = `${folder.letterFile.id}/${folder.id}`;
    const archivePath = path.join(
      path.dirname(tmpFolder),
      `${folderName.replaceAll(" ", "_").replaceAll("/", "_").substring(0, 128)}.zip`
    );

    await this.prepareFolder(tmpFolder, folderName, folder.documents);

    return this.finalizeArchive(tmpFolder, archivePath, bucketPath);
  }

  async generateArchiveForMany(csvKey: string, letterFiles: LetterFile[], author: User) {
    this.logger.info("prepareArchiveWithDocuments");
    const tmpFolder = await fs.mkdtemp(path.join(os.tmpdir(), "export-"));
    this.logger.debug(`use tmp folder ${tmpFolder}`);
    const localCsvPath = path.join(tmpFolder, csvKey.split("/").at(-1));
    this.logger.info(`download CSV from ${csvKey} to ${localCsvPath}`);
    await this.storageService.downloadFile(csvKey, localCsvPath);

    this.logger.info(`begin downloading documents for ${letterFiles.length} letter files`);
    for (const letterFile of letterFiles) {
      const letterFileLocalDir = path.join(tmpFolder, letterFile.name || `Parapheur-${letterFile.chronoNumber}`);
      for (const folder of letterFile.folders) {
        const folderName = folder.name === "$$ROOT$$" ? "dossier racine" : folder.name;
        this.logger.info(`begin downloading ${folder.documents.length} documents for folder ${folder.id}`);
        await this.prepareFolder(letterFileLocalDir, folderName, folder.documents);
        this.logger.info(`finished downloading ${folder.documents.length} documents for folder ${folder.id}`);
      }
    }
    this.logger.info(`finished downloading all documents to ${tmpFolder}`);

    const archiveName = `${v4()}.zip`;
    const archivePath = path.join(await fs.mkdtemp(path.join(os.tmpdir(), "zip-")), archiveName);
    this.logger.info(`begin writting archive to ${archivePath}`);
    await this.archiveDir(tmpFolder, archivePath);

    const uploadPath = path.join(exportsBasePath, author.id + "", archiveName);
    this.logger.info(`upload archive ${uploadPath} to minio`);
    await this.storageService.uploadFile(uploadPath, archivePath, {
      originalname: base64Encode(uploadPath.split("/").at(-1)),
    });

    await this.storageService.removeObject(csvKey);

    return { storageKey: uploadPath, link: await this.storageService.getLink(uploadPath, true) };
  }

  private async archiveDir(dir: string, to: string) {
    this.logger.debug(`archive directory "${dir}" to "${to}"`);
    const output = createWriteStream(to);
    const archive = archiver("zip", { zlib: { level: 9 } });
    archive.directory(dir, false);
    archive.pipe(output);

    return new Promise((resolve, reject) => {
      output.on("close", () => {
        resolve(to);
      });

      archive.on("error", err => {
        reject(err);
      });

      archive.finalize();
    });
  }

  private async prepareFolder(tmpFolder: string, folderName: string, documents: LetterFileDocument[]) {
    await fs.mkdir(path.join(tmpFolder, folderName), { recursive: true });
    for (const doc of documents) {
      const localFilePath = path.join(
        tmpFolder,
        folderName,
        doc.type === LetterFileDocumentType.URL ? `${doc.name}.url` : doc.name + doc.extension
      );
      if (doc.type === LetterFileDocumentType.DEFAULT) {
        await this.storageService.downloadFile(doc.storageKey, localFilePath);
      } else if (doc.type === LetterFileDocumentType.URL) {
        await fs.writeFile(localFilePath, this.getUrlFileContent(doc.url));
      }
    }
  }

  private getUrlFileContent(url: string) {
    return dedent`
    [InternetShortcut]
    URL=${url}
  `;
  }

  private async finalizeArchive(tmpFolder: string, archivePath: string, bucketPath: string) {
    await this.archiveDir(tmpFolder, archivePath);

    const uploadKey = path.join(archiveBasePath, bucketPath, path.basename(archivePath));
    await this.storageService.uploadFile(uploadKey, archivePath);

    return this.storageService.getLink(uploadKey);
  }
}
