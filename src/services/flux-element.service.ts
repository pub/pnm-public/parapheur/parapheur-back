import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { pick, uniq, omit } from "lodash";
import { Repository } from "typeorm";
import { DownstreamFluxElement } from "../entities/downstream-flux-element.entity";
import { UpstreamFluxElement } from "../entities/upstream-flux-element.entity";
import { FluxError } from "../errors/letter-file.error";
import { LdapService } from "./ldap.service";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { HistoryOperationType, HistoryTarget } from "../entities/letter-file-history.entity";
import { LdapInvalidMailError } from "../errors/ldap.error";

@Injectable()
export class FluxElementService {
  constructor(
    private readonly ldapService: LdapService,
    @InjectRepository(DownstreamFluxElement) private readonly downRepo: Repository<DownstreamFluxElement>,
    @InjectRepository(UpstreamFluxElement) private readonly upRepo: Repository<UpstreamFluxElement>,
    private readonly historyService: LetterFileHistoryService
  ) {}

  async lookupLdap(
    mail: string,
    position?: number
  ): Promise<Pick<DownstreamFluxElement, "mail" | "name" | "structure"> & { position?: number }> {
    const ldapEntry = await this.ldapService.lookup(mail);
    return { ...omit(ldapEntry, "service"), position, structure: ldapEntry.service };
  }

  public async addOneUpstream(mail: string, letterFileId: number) {
    const result = await this.addMany([mail], letterFileId, this.upRepo);
    await this.historyService.registerEvent({
      letterFile: { id: letterFileId },
      label: `Ajout de "${result[0].name}" en amont`,
      target: HistoryTarget.FLUX,
      type: HistoryOperationType.CREATE,
    });
    return result;
  }

  public async addOneDownstream(mail: string, letterFileId: number) {
    const result = await this.addMany([mail], letterFileId, this.downRepo);
    await this.historyService.registerEvent({
      letterFile: { id: letterFileId },
      label: `Ajout de "${result[0].name}" en aval`,
      target: HistoryTarget.FLUX,
      type: HistoryOperationType.CREATE,
    });
    return result;
  }

  public async addManyDownstream(
    items: { mail: string; name: string }[],
    letterFileId: number,
    savedFluxName?: string
  ) {
    await this.addMany(
      items.map(item => item.mail),
      letterFileId,
      this.downRepo
    );

    await this.historyService.registerEvent({
      letterFile: { id: letterFileId },
      label: savedFluxName
        ? `Ajout du flux enregistré "${savedFluxName}" en aval (${items.length} éléments)`
        : `Ajout de ${items.length} éléments en aval`,
      target: HistoryTarget.FLUX,
      type: HistoryOperationType.CREATE,
    });
  }

  public async emptyDownstream(letterFileId: number) {
    await this.downRepo
      .createQueryBuilder("fluxElement")
      .delete()
      .where("letterFileId = :letterFileId", { letterFileId })
      .andWhere("position > 0")
      .execute();

    await this.historyService.registerEvent({
      letterFile: { id: letterFileId },
      label: "Suppression de tous les éléments restant du flux aval",
      target: HistoryTarget.FLUX,
      type: HistoryOperationType.DELETE,
    });
  }

  public async setRecalled(id: number) {
    await this.downRepo.update({ id }, { recalled: true });
  }

  private async addMany(
    mails: string[],
    letterFileId: number,
    repo: Repository<UpstreamFluxElement | DownstreamFluxElement>
  ): Promise<Array<UpstreamFluxElement | DownstreamFluxElement>> {
    const lastEl = await repo.findOne({ where: { letterFile: { id: letterFileId } }, order: { position: "DESC" } });
    const startPosition = lastEl ? lastEl.position + 1 : 0;

    const resolvedFluxElements = await Promise.all(
      mails.map((mail, index) => this.lookupLdap(mail, startPosition + index))
    );

    const inserted: Array<UpstreamFluxElement | DownstreamFluxElement> = [];
    for (const el of resolvedFluxElements.values()) {
      const result = await repo.save({ ...el, letterFile: { id: letterFileId } });
      inserted.push(result);
    }

    return inserted;
  }

  public async moveToUpstream(downstreamId: number) {
    const downstream = await this.downRepo.findOne({ where: { id: downstreamId }, relations: ["letterFile"] });
    const upstreamCount = await this.upRepo.count({
      where: {
        letterFile: {
          id: downstream.letterFile.id,
        },
      },
    });

    await this.upRepo.save({
      ...pick(downstream, ["mail", "name", "letterFile", "structure", "constantId"]),
      position: upstreamCount,
    });

    await this.downRepo.update({ letterFile: { id: downstream.letterFile.id } }, { position: () => "position - 1" });
    await this.downRepo.delete(downstreamId);

    const downstreamCount = await this.downRepo.count({
      where: {
        letterFile: {
          id: downstream.letterFile.id,
        },
      },
    });

    if (downstreamCount === 0) {
      await this.historyService.registerEvent({
        letterFile: { id: downstream.letterFile.id },
        label: "Parapheur archivé",
        target: HistoryTarget.FLUX,
        type: HistoryOperationType.UPDATE,
      });
    } else {
      const current = await this.downRepo.findOne({
        where: { letterFile: { id: downstream.letterFile.id }, position: 0 },
      });
      await this.historyService.registerEvent({
        letterFile: { id: downstream.letterFile.id },
        label: `Passage à l'étape suivante (${current.name})`,
        target: HistoryTarget.FLUX,
        type: HistoryOperationType.UPDATE,
      });
    }
  }

  async take(mail: string, letterFileId: number): Promise<void> {
    await this.downRepo
      .createQueryBuilder("fluxElement")
      .update()
      .set({ position: () => "position + 1" })
      .where("letterFileId = :letterFileId", { letterFileId })
      .execute();

    await this.downRepo.save({
      letterFile: { id: letterFileId },
      ...(await this.lookupLdap(mail, 0)),
    });

    await this.historyService.registerEvent({
      letterFile: { id: letterFileId },
      label: "Prend la main",
      target: HistoryTarget.FLUX,
      type: HistoryOperationType.UPDATE,
    });
  }

  async reorder(fluxElements: DownstreamFluxElement[], sortedIds: number[]): Promise<DownstreamFluxElement[]> {
    if (fluxElements.length !== uniq(sortedIds).length) {
      throw new FluxError("Downstream and sorted ids length mismatch");
    }

    const downstream = await this.downRepo.save(
      sortedIds.map((el, index) => {
        const fluxElement = fluxElements.find(fl => fl.id === el);

        if (!fluxElement) {
          throw new FluxError(`Flux element with id ${el} not found`);
        }

        return {
          ...fluxElement,
          position: index,
        };
      })
    );

    return downstream;
  }

  async removeOneDownstream(downstreamId: number): Promise<void> {
    const downstreamEl = await this.downRepo.findOne({ where: { id: downstreamId }, relations: ["letterFile"] });
    await this.downRepo.delete(downstreamId);

    await this.downRepo
      .createQueryBuilder("fluxElement")
      .update()
      .set({ position: () => "position - 1" })
      .where("position >= :position", { position: downstreamEl.position })
      .andWhere("letterFileId = :letterFileId", { letterFileId: downstreamEl.letterFile.id })
      .execute();

    await this.historyService.registerEvent({
      letterFile: { id: downstreamEl.letterFile.id },
      label: `Retire l'élément aval "${downstreamEl.name}"`,
      target: HistoryTarget.FLUX,
      type: HistoryOperationType.DELETE,
    });
  }

  async getCurrent(lfId: number) {
    return this.downRepo.findOneBy({ letterFile: { id: lfId }, position: 0 });
  }

  async getDownstream(lfId: number) {
    return this.downRepo.find({ where: { letterFile: { id: lfId } } });
  }
}
