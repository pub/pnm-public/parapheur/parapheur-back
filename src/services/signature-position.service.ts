import { Injectable } from "@nestjs/common";
import { AbstractPastaCrudService, IPastaCrudQuery } from "@pasta/back-core";
import { SignaturePosition } from "../entities/SignaturePosition.entity";
import { DeepPartial, Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { LetterFileDocumentService } from "./letter-file-document.service";
import { HistoryOperationType, HistoryTarget } from "../entities/letter-file-history.entity";
import { differenceWith } from "lodash";
import { PositionPatch } from "../dtos/signature-position.dto";

@Injectable()
export class SignaturePositionService extends AbstractPastaCrudService<SignaturePosition> {
  constructor(
    @InjectRepository(SignaturePosition) public readonly repo: Repository<SignaturePosition>,
    private readonly historyService: LetterFileHistoryService,
    private readonly docService: LetterFileDocumentService
  ) {
    super();
  }

  async getManySignPos(docId: number) {
    return this.repo.findBy({ document: { id: docId } });
  }

  async createOne(query: IPastaCrudQuery, body: DeepPartial<SignaturePosition>): Promise<SignaturePosition> {
    const doc = await this.docService.getOne({ join: ["folder", "folder.letterFile"] }, body.document.id);
    const { folder } = doc;
    const { letterFile } = folder;
    const result = await super.createOne(query, body);
    await this.historyService.registerEvent({
      letterFile,
      label: `Pré-positionnement de la signature sur la page ${body.page + 1}`,
      target: HistoryTarget.SIGNATURE_POSITION,
      type: HistoryOperationType.CREATE,
    });
    return result;
  }

  async updateOne(
    query: IPastaCrudQuery,
    id: number,
    body: DeepPartial<SignaturePosition>
  ): Promise<SignaturePosition> {
    const signPos = await this.getOne({ join: ["document", "document.folder", "document.folder.letterFile"] }, id);
    const { document } = signPos;
    const { folder } = document;
    const { letterFile } = folder;
    const result = await super.updateOne(query, id, body);
    await this.historyService.registerEvent({
      letterFile,
      label: `Mise à jour du pré-positionnement de la signature sur la page ${signPos.page + 1}`,
      target: HistoryTarget.SIGNATURE_POSITION,
      type: HistoryOperationType.UPDATE,
    });
    return result;
  }

  async deleteOne(query: IPastaCrudQuery, id: number): Promise<any> {
    const signPos = await this.getOne({ join: ["document", "document.folder", "document.folder.letterFile"] }, id);
    const { document } = signPos;
    const { folder } = document;
    const { letterFile } = folder;
    const result = await super.deleteOne(query, id);
    await this.historyService.registerEvent({
      letterFile,
      label: `Suppression du pré-positionnement de la signature sur la page ${signPos.page + 1}`,
      target: HistoryTarget.SIGNATURE_POSITION,
      type: HistoryOperationType.DELETE,
    });
    return result;
  }

  async patchForDoc(docId: number, body: PositionPatch[]) {
    const current = await this.getManySignPos(docId);

    const toDelete = differenceWith(current, body, (a, b) => a.id === b.id);
    const toUpdate = body.filter(item => {
      if (!item.id) return false;
      const curr = current.find(curr => curr.id === item.id);
      if (!curr) return false;
      return curr.x !== item.x || curr.y !== item.y || curr.width !== item.width || curr.height !== item.height;
    });
    const toCreate = differenceWith(body, current, (a, b) => a.id === b.id);

    for (const item of toCreate) {
      await this.createOne({}, { ...item, document: { id: docId } });
    }
    for (const item of toDelete) {
      await this.deleteOne({}, item.id);
    }
    for (const item of toUpdate) {
      await this.updateOne({}, item.id, item);
    }

    return this.getManySignPos(docId);
  }
}
