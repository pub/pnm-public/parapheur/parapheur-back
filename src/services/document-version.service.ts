import { Injectable } from "@nestjs/common";
import { AbstractPastaCrudService } from "@pasta/back-core";
import { DocumentVersion } from "../entities/DocumentVersion.entity";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { StorageService } from "@pasta/back-files";

@Injectable()
export class DocumentVersionService extends AbstractPastaCrudService<DocumentVersion> {
  constructor(
    @InjectRepository(DocumentVersion) public readonly repo: Repository<DocumentVersion>,
    private readonly storageService: StorageService
  ) {
    super();
  }

  async getLink(id: number) {
    const docVersion = await this.getOne({ join: ["document"] }, id);
    return this.storageService.getLink(
      docVersion.storageKey,
      true,
      docVersion.document.name + docVersion.extension || docVersion.document.extension
    );
  }

  async getVersions(documentId: number) {
    const versions = await this.repo.find({
      relations: ["document"],
      where: { document: { id: documentId } },
      order: { creationDate: "DESC" },
    });

    const document = versions[0].document;

    const currentVersion = DocumentVersion.fromPlain({
      id: documentId,
      storageKey: document.storageKey,
      author: versions[0].author,
      document,
    });

    return [currentVersion, ...versions].map((v, index, array) => {
      return {
        ...v,
        author: v.author ?? "Document initial",
        creationDate: array[index + 1]?.creationDate ?? document.creationDate,
      };
    });
  }
}
