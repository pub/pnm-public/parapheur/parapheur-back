import { InjectRepository } from "@nestjs/typeorm";
import { ExportStatus, LetterFileExport } from "../entities/export.entity";
import { In, Not, Repository } from "typeorm";
import { Injectable } from "@nestjs/common";
import { AbstractPastaCrudService } from "@pasta/back-core";
import { Queue } from "bull";
import { InjectQueue } from "@nestjs/bull";
import { LetterFileSearchDto } from "../dtos/letter-file.dto";
import { User } from "@pasta/back-auth";

@Injectable()
export class LetterFileExportService extends AbstractPastaCrudService<LetterFileExport> {
  constructor(
    @InjectRepository(LetterFileExport) public readonly repo: Repository<LetterFileExport>,
    @InjectQueue("exports") private readonly exportsQueue: Queue
  ) {
    super();
  }

  async startExport(searchQuery: LetterFileSearchDto, withDocuments: boolean, author: User) {
    const theExport = await this.createOne({}, { author, searchQuery, withDocuments });
    await this.exportsQueue.add({ exportId: theExport.id });
    return theExport;
  }

  async startArchive(author: User) {
    const theExport = await this.createOne({}, { author, searchQuery: {}, withDocuments: true });
    await this.exportsQueue.add({ exportId: theExport.id });
    return theExport;
  }

  async isReady(userId: number): Promise<boolean> {
    const found = await this.repo.findOne({
      relations: ["author"],
      where: {
        status: Not(In([ExportStatus.ERROR, ExportStatus.COMPLETED])),
        author: { id: userId },
      },
    });

    return !found;
  }
}
