import { StorageService } from "@pasta/back-files";
import { LetterFileDocumentService } from "./letter-file-document.service";
import { Inject, Injectable } from "@nestjs/common";
import { Readable } from "stream";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { CACHE_MANAGER, Cache } from "@nestjs/cache-manager";
import { WopiConfig } from "../config/wopi.config";
import { v4 } from "uuid";
import axios from "axios";
import { XMLParser } from "fast-xml-parser";
import { Dictionary, flatten, get } from "lodash";
import { asDuration } from "@pasta/back-core";
import { getConvertedKey, getPagePrefix } from "../sign-utils";

export interface WopiTokenData {
  documentId: number;
  userId: number;
  timestamp: number;
}

@Injectable()
export class WopiService {
  private readonly tokenPrefix = "wopi_";

  constructor(
    private readonly storageService: StorageService,
    private readonly documentService: LetterFileDocumentService,
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
    private readonly wopiConfig: WopiConfig
  ) {}

  async getDoc(id: number) {
    return this.documentService.getOne({}, id);
  }

  async getInfos(doc: LetterFileDocument) {
    const infos = await this.storageService.statObject(doc.storageKey);
    return {
      BaseFileName: doc.name + doc.extension,
      size: infos.size,
      LastModifiedTime: infos.lastModified,
    };
  }

  async getFile(doc: LetterFileDocument) {
    return this.storageService.streamObject(doc.storageKey);
  }

  async putFile(doc: LetterFileDocument, buffer: Buffer) {
    await this.storageService.uploadFromStream(doc.storageKey, Readable.from(buffer));
    // need to clean sign related tmp files as opening sign panel wouldn't show the updated version
    await this.storageService.removeObject(getConvertedKey(doc.storageKey));
    const fullDoc = await this.documentService.getOne({ join: ["folder", "folder.letterFile"] }, doc.id);
    await this.storageService.removeObjectsInDir(
      getPagePrefix(fullDoc.folder.letterFile.id, fullDoc.folder.id, fullDoc.id)
    );
  }

  async generateToken(documentId: number, userId: number) {
    const token = `${userId}_${documentId}_${v4()}`;
    const data: WopiTokenData = { documentId, userId, timestamp: Date.now() };
    await this.cacheManager.set(this.getKey(token), data, this.wopiConfig.tokenTTL);
    return token;
  }

  async getTokenData(token: string): Promise<WopiTokenData | undefined> {
    return this.cacheManager.get<WopiTokenData>(this.getKey(token));
  }

  async getEditables(): Promise<Dictionary<string>> {
    const discoveryCacheKey = `${this.tokenPrefix}discovery`;
    let result = await this.cacheManager.get<Dictionary<string>>(discoveryCacheKey);
    if (!result) {
      const discoveryRaw = await axios.get("hosting/discovery", { baseURL: this.wopiConfig.clientUrl });
      const parser = new XMLParser({ ignoreAttributes: false, attributesGroupName: "@_" });
      result = flatten(
        get(parser.parse(discoveryRaw.data), "wopi-discovery.net-zone.app").map(item => get(item, "action"))
      )
        .map(item => {
          const val: { "@_ext": string; "@_name": "view" | "edit" | "getinfo" | "view_comment"; "@_urlsrc": string } =
            get(item, "@_");
          return {
            extension: val["@_ext"],
            action: val["@_name"],
            url: val["@_urlsrc"],
          };
        })
        .filter(item => item.extension && item.action === "edit")
        .reduce((acc, curr) => {
          return { ...acc, [curr.extension]: curr.url };
        }, {} as Dictionary<string>);
      this.cacheManager.set(discoveryCacheKey, result, asDuration("1h", "millisecond"));
    }

    return result;
  }

  private getKey(token: string) {
    return `${this.tokenPrefix}${token}`;
  }
}
