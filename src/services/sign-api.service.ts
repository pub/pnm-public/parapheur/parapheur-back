import axios, { AxiosInstance } from "axios";
import { SignApiConf } from "../config/sign-api.config";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { Injectable } from "@nestjs/common";

export interface Signature {
  x: number;
  y: number;
  page: number;
  width: number;
  height: number;
  key: string;
}

@Injectable()
export class SignApi {
  @InjectLogger("default", SignApi.name)
  logger: PastaLogger;

  public readonly axios: AxiosInstance;

  constructor(private readonly conf: SignApiConf) {
    this.axios = axios.create({
      baseURL: this.conf.baseURL,
      headers: {
        "X-Api-Key": this.conf.secretKey,
      },
    });
  }

  async generatePreview(document: string, prefix: string, page: number): Promise<{ urls: string[] }> {
    this.logger.debug(`generate preview for ${document} to ${prefix}`);
    const result = await this.axios.post(`${this.conf.baseURL}/generate_preview`, { document, prefix, page });
    return result.data;
  }

  async sign(document: string, signatures: Signature[], destination: string): Promise<{ url: string }> {
    this.logger.debug("sign", SignApi.name, { document, signatures, destination });
    const result = await this.axios.post(`${this.conf.baseURL}/sign`, { document, signatures, destination });
    return result.data;
  }

  async getNbPages(document: string): Promise<number> {
    const result = await this.axios.post(`${this.conf.baseURL}/count-pages`, { document });
    return result.data.nbPages;
  }

  async generateEnhancedSignature(signatureKey: string, text: string, destination: string): Promise<{ url: string }> {
    this.logger.debug(`generate sign image`, SignApi.name, { signature_key: signatureKey, text, destination });
    const result = await this.axios.post(`${this.conf.baseURL}/enhance_signature`, {
      signature_key: signatureKey,
      text,
      destination,
    });
    return result.data;
  }

  async convertToPdf(document: string, destination: string): Promise<void> {
    this.logger.debug(`convert document ${document} to ${destination}`)
    await this.axios.post(`${this.conf.baseURL}/convert`, { document, destination });
    return
  }
}
