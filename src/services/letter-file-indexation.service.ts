import { Injectable } from "@nestjs/common";
import { Dictionary, flatten, isNil, pick, uniq, uniqBy } from "lodash";
import { ElasticsearchService } from "../elasticsearch/elasticsearch.service";
import { LetterFileComment } from "../entities/letter-file-comment.entity";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import { LetterFileTag } from "../entities/letter-file-tag.entity";
import { LetterFile } from "../entities/letter-file.entity";
import { IndexedLetterFile } from "../search-schema";
import { searchAliasName } from "../utils";
import { UpstreamFluxElement } from "../entities/upstream-flux-element.entity";
import { DownstreamFluxElement } from "../entities/downstream-flux-element.entity";
import { LetterFileCCUser } from "../entities/letter-file-cc-user.entity";
import dayjs from "dayjs";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";

export interface ESQuery {
  searchPattern?: string;
  filters?: object[];
  mustNot?: object[];
  should?: object[];
}

@Injectable()
export class LetterFileIndexationService {
  @InjectLogger("default", LetterFileIndexationService.name)
  logger: PastaLogger;

  constructor(private readonly esService: ElasticsearchService) {}

  async indexFull(letterFile: LetterFile) {
    const body: IndexedLetterFile = {
      id: letterFile.id,
      creationDate: letterFile.creationDate,
      lastIndexedAt: letterFile.modificationDate,
      name: letterFile.name,
      chronoNumber: letterFile.chronoNumber,
      deadline: letterFile.deadline,
      arrivalDate: letterFile.arrivalDate,
      externalMailNumber: letterFile.externalMailNumber,
      draft: letterFile.draft,
      archived: letterFile.archived,
      tags: this.convertTags(letterFile.tags || []),
      creator: this.getCreator(letterFile),
      current: this.getCurrent(letterFile),
      upstream: this.convertFlux(letterFile.upstream || []),
      downstream: this.convertDownstream(letterFile.downstream || []),
      ccUsers: this.convertFlux(letterFile.ccUsers || []),
      relatedUsers: this.convertUsers([
        ...(letterFile.upstream || []),
        ...(letterFile.downstream || []),
        ...(letterFile.ccUsers || []),
      ]),
      comments: this.convertComments(letterFile.comments || []),
      folders: this.convertFolders(letterFile.folders || []),
      documents: this.convertDocuments(flatten(letterFile.folders.map(folder => folder.documents || []))),
      touchedDates: this.computeTouchedDates(letterFile),
      senderOrganization: letterFile.senderOrganization,
      senderName: letterFile.senderName,
      senderRole: letterFile.senderRole,
      mailType: letterFile.mailType,
      recipientName: letterFile.recipientName,
      manager: letterFile.manager,
    };
    if (letterFile.archived) body.archivedDate = letterFile.modificationDate;
    await this.esService.index({ id: letterFile.id + "", index: searchAliasName, body });
  }

  /**
   * search
   * @param andConditions those conditions are additive (AND operator)
   * @param orConditions  should match at least one of those conditions (OR operator + minimum = 1)
   * @returns array of ids matching the search
   */
  async search(
    params: {
      andConditions?: ESQuery;
      orConditions?: object[];
      userEmails: string[];
      highlight: string[];
    },
    pagination: { from: number; size: number }
  ): Promise<{ total: number; ids: number[]; items: { id: number; highlight: Dictionary<string[]> }[] }> {
    const body = {
      sort: ["_score", { lastIndexedAt: "desc" }],
      query: {
        bool: {
          must: [],
          filter: [{ terms: { "relatedUsers.mail": params.userEmails } }],
          mustNot: [
            {
              bool: {
                must: [{ term: { draft: true } }],
                mustNot: [{ terms: { "current.mail": params.userEmails } }],
              },
            },
          ],
        },
      },
      highlight: { fields: params.highlight.map(el => ({ [el]: {} })), pre_tags: "${", post_tags: "}$" },
    };
    if (params.andConditions) {
      const must = [];
      if (params.andConditions.searchPattern) {
        must.push({ query_string: { query: `*${params.andConditions.searchPattern}*` } });
      }
      const query = {
        bool: {
          must,
          filter: params.andConditions.filters || [],
          mustNot: params.andConditions.mustNot || [],
          should: params.andConditions.should || [],
          minimum_should_match: params.andConditions.should?.length ? 1 : 0, // https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html
        },
      };
      body.query.bool.must.push(query);
    }
    if (params.orConditions?.length) {
      const query = {
        bool: {
          should: params.orConditions,
          minimum_should_match: 1,
        },
      };
      body.query.bool.must.push(query);
    }

    const result = await this.esService.search(body, searchAliasName, pagination);
    return {
      total: result.body.hits.total.value,
      ids: result.body.hits.hits.map(doc => parseInt(doc._id)),
      items: result.body.hits.hits.map(doc => {
        return {
          id: parseInt(doc._id),
          highlight: doc.highlight,
        };
      }),
    };
  }

  async indexNewLetterFile(
    letterFile: Pick<LetterFile, "id" | "creationDate" | "modificationDate" | "draft" | "chronoNumber">,
    creator: { name: string; mail: string }
  ) {
    const body: Partial<IndexedLetterFile> = {
      id: letterFile.id,
      creationDate: letterFile.creationDate,
      lastIndexedAt: letterFile.modificationDate,
      chronoNumber: letterFile.chronoNumber,
      draft: letterFile.draft,
      archived: false,
      creator,
      current: creator,
      upstream: [],
      downstream: [],
      ccUsers: [],
      relatedUsers: [{ mail: creator.mail }],
      touchedDates: [this.removeTime(letterFile.creationDate)],
    };
    await this.esService.index({ id: letterFile.id + "", index: searchAliasName, body });
  }

  async patchBase(
    id: number,
    changes: Partial<
      Pick<
        LetterFile,
        | "modificationDate"
        | "name"
        | "chronoNumber"
        | "deadline"
        | "arrivalDate"
        | "externalMailNumber"
        | "draft"
        | "archived"
        | "senderOrganization"
        | "senderName"
        | "senderRole"
        | "mailType"
        | "recipientName"
        | "manager"
      >
    >
  ) {
    const body: Partial<IndexedLetterFile> = {
      lastIndexedAt: changes.modificationDate!,
      touchedDates: await this.getTouchedDates(id),
    };

    if (changes.name) body.name = changes.name;
    if (changes.chronoNumber) body.chronoNumber = changes.chronoNumber;
    if (changes.deadline) body.deadline = changes.deadline;
    if (changes.arrivalDate) body.arrivalDate = changes.arrivalDate;
    if (changes.externalMailNumber) body.externalMailNumber = changes.externalMailNumber;
    if (!isNil(changes.draft)) body.draft = changes.draft;
    if (!isNil(changes.archived)) {
      body.archived = changes.archived;
      body.archivedDate = changes.modificationDate;
    }
    if (changes.senderOrganization) body.senderOrganization = changes.senderOrganization;
    if (changes.senderName) body.senderName = changes.senderName;
    if (changes.senderRole) body.senderRole = changes.senderRole;
    if (changes.mailType) body.mailType = changes.mailType;
    if (changes.recipientName) body.recipientName = changes.recipientName;
    if (changes.manager) body.manager = changes.manager;

    await this.esService.updateDocument({ id: `${id}`, index: searchAliasName, body });
  }

  async putTags(id: number, tags: string[], modificationDate: Date) {
    await this.esService.updateDocument({
      id: id + "",
      index: searchAliasName,
      body: {
        lastIndexedAt: modificationDate,
        touchedDates: await this.getTouchedDates(id),
        tags,
      },
    });
  }

  async putFlux(id: number, lf: Pick<LetterFile, "upstream" | "downstream" | "modificationDate">) {
    await this.esService.updateDocument({
      id: id + "",
      index: searchAliasName,
      body: {
        current: this.getCurrent(lf),
        upstream: this.convertFlux(lf.upstream),
        downstream: this.convertDownstream(lf.downstream),
        lastIndexedAt: lf.modificationDate,
        touchedDates: await this.getTouchedDates(id),
      },
    });
  }

  async putUpstream(id: number, upstream: UpstreamFluxElement[], modificationDate: Date) {
    await this.esService.updateDocument({
      id: id + "",
      index: searchAliasName,
      body: {
        upstream: this.convertFlux(upstream),
        lastIndexedAt: modificationDate,
        touchedDates: await this.getTouchedDates(id),
      },
    });
  }

  async putDownstream(id: number, downstream: DownstreamFluxElement[]) {
    await this.esService.updateDocument({
      id: id + "",
      index: searchAliasName,
      body: {
        current: this.getCurrent({ downstream }),
        downstream: this.convertDownstream(downstream),
        lastIndexedAt: this.getNow(),
        touchedDates: await this.getTouchedDates(id),
      },
    });
  }

  async putCCUsers(id: number, ccUsers: LetterFileCCUser[]) {
    await this.esService.updateDocument({
      id: id + "",
      index: searchAliasName,
      body: {
        ccUsers: this.convertFlux(ccUsers),
        lastIndexedAt: this.getNow(),
        touchedDates: await this.getTouchedDates(id),
      },
    });
  }

  async putRelatedUsers(id: number, users: { mail: string }[]) {
    this.logger.debug(`Put related users`, LetterFileIndexationService.name, { id, users });
    await this.esService.updateDocument({
      id: id + "",
      index: searchAliasName,
      body: { relatedUsers: this.convertUsers(users) },
    });
  }

  async putComments(id: number, comments: LetterFileComment[]) {
    await this.esService.updateDocument({
      id: id + "",
      index: searchAliasName,
      body: {
        comments: this.convertComments(comments),
        lastIndexedAt: this.getNow(),
        touchedDates: await this.getTouchedDates(id),
      },
    });
  }

  async putFolders(id: number, folders: LetterFileFolder[]) {
    await this.esService.updateDocument({
      id: id + "",
      index: searchAliasName,
      body: {
        folders: this.convertFolders(folders),
        lastIndexedAt: this.getNow(),
        touchedDates: await this.getTouchedDates(id),
      },
    });
  }

  async putDocuments(id: number, documents: LetterFileDocument[]) {
    await this.esService.updateDocument({
      id: id + "",
      index: searchAliasName,
      body: {
        documents: this.convertDocuments(documents),
        lastIndexedAt: this.getNow(),
        touchedDates: await this.getTouchedDates(id),
      },
    });
  }

  async remove(id: number) {
    await this.esService.deleteDocument(id + "", searchAliasName);
  }

  async getOne(id: number): Promise<IndexedLetterFile> {
    const result = await this.esService.getDocument(`${id}`, searchAliasName);
    return result.body._source;
  }

  private getNow() {
    return dayjs().toISOString();
  }

  private getCreator(letterFile: LetterFile) {
    return pick(
      letterFile.draft
        ? letterFile.downstream.find(user => user.position === 0)
        : letterFile.upstream.find(user => user.position === 0),
      "mail",
      "name"
    );
  }

  private getCurrent(letterFile: Pick<LetterFile, "downstream">) {
    return letterFile.downstream.length
      ? pick(
          letterFile.downstream.find(user => user.position === 0),
          "mail",
          "name"
        )
      : { mail: "", name: "" };
  }

  private convertTags(tags: LetterFileTag[]) {
    return tags.map(tag => tag.name);
  }

  private convertDownstream(flux: DownstreamFluxElement[]) {
    return flux
      .filter(el => el.position !== 0)
      .map(el => ({
        mail: el.mail,
        name: el.name,
      }));
  }

  private convertFlux(flux: UpstreamFluxElement[] | DownstreamFluxElement[] | LetterFileCCUser[]) {
    return flux.map(el => ({
      mail: el.mail,
      name: el.name,
    }));
  }

  private convertComments(comments: LetterFileComment[]) {
    return comments.map(comment => comment.text);
  }

  private convertFolders(folders: LetterFileFolder[]) {
    return folders.filter(folder => folder.name !== "$$ROOT$$").map(folder => folder.name);
  }

  private convertDocuments(documents: LetterFileDocument[]) {
    return documents.map(doc => ({
      name: doc.type === LetterFileDocumentType.URL ? doc.name : doc.name + doc.extension,
    }));
  }

  private removeTime(date: Date) {
    return dayjs(new Date(date).setHours(0, 0, 0, 0)).toISOString();
  }

  private computeTouchedDates(lf: LetterFile): string[] {
    return uniq(
      [
        lf.creationDate,
        lf.modificationDate,
        ...(lf.comments || []).map(comment => comment.creationDate),
        ...(lf.comments || []).map(comment => comment.modificationDate),
        ...(lf.folders || []).map(folder => folder.creationDate),
        ...(lf.folders || []).map(folder => folder.modificationDate),
        ...flatten((lf.folders || []).map(folder => folder.documents.map(doc => doc.creationDate))),
        ...flatten((lf.folders || []).map(folder => folder.documents.map(doc => doc.modificationDate))),
        ...(lf.upstream || []).map(upstream => upstream.creationDate),
        ...(lf.upstream || []).map(upstream => upstream.modificationDate),
        ...(lf.downstream || []).map(downstream => downstream.creationDate),
        ...(lf.downstream || []).map(downstream => downstream.modificationDate),
        ...(lf.ccUsers || []).map(ccUsers => ccUsers.creationDate),
        ...(lf.ccUsers || []).map(ccUsers => ccUsers.modificationDate),
      ].map(date => this.removeTime(date))
    );
  }

  private async getTouchedDates(id: number) {
    const result = await this.esService.getDocument(id + "", searchAliasName);
    const doc: IndexedLetterFile = result.body._source;
    return uniq([...(doc.touchedDates || []), dayjs(new Date().setHours(0, 0, 0, 0)).toISOString()]);
  }

  private convertUsers(users: { mail: string }[]) {
    return uniqBy(
      users.map(el => ({
        mail: el.mail,
      })),
      "mail"
    );
  }
}
