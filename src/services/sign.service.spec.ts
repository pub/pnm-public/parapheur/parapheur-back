import { Test, TestingModule } from "@nestjs/testing";
import { SignService } from "./sign.service";
import { SignApi } from "./sign-api.service";
import { LetterFileDocumentService } from "./letter-file-document.service";
import { StorageService } from "@pasta/back-files";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { UserPreferencesService } from "./user-preferences.service";
import { DocumentVersionService } from "./document-version.service";
import { MockRepository, asConfig, mockObject } from "@pasta/back-core";
import { User } from "@pasta/back-auth";
import { SignatureDto } from "../dtos/sign.dto";
import { DocumentVersion } from "../entities/DocumentVersion.entity";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { set } from "lodash";
import { HistoryOperationType, HistoryTarget } from "../entities/letter-file-history.entity";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";
import { ParapheurConfig } from "../config/parapheur.config";
import { SignaturePositionService } from "./signature-position.service";

describe("SignService", () => {
  let service: SignService;
  let signApi: SignApi;
  let documentRepo: MockRepository<LetterFileDocument>;
  let documentService: LetterFileDocumentService;
  let storageService: StorageService;
  let historyService: LetterFileHistoryService;
  let userPrefService: UserPreferencesService;
  let documentVersionService: DocumentVersionService;
  let parapheurConfig: ParapheurConfig;
  let signaturePositionService: SignaturePositionService;

  beforeEach(async () => {
    signApi = mockObject(SignApi);
    documentRepo = new MockRepository<LetterFileDocument>();
    documentService = mockObject(LetterFileDocumentService);
    set(documentService, "repo", documentRepo);
    storageService = mockObject(StorageService);
    historyService = mockObject(LetterFileHistoryService);
    userPrefService = mockObject(UserPreferencesService);
    documentVersionService = mockObject(DocumentVersionService);
    parapheurConfig = {} as ParapheurConfig;
    signaturePositionService = mockObject(SignaturePositionService);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: SignService, useClass: SignService },
        { provide: SignApi, useValue: signApi },
        { provide: LetterFileDocumentService, useValue: documentService },
        { provide: StorageService, useValue: storageService },
        { provide: LetterFileHistoryService, useValue: historyService },
        { provide: UserPreferencesService, useValue: userPrefService },
        { provide: DocumentVersionService, useValue: documentVersionService },
        { provide: ParapheurConfig, useValue: parapheurConfig },
        { provide: SignaturePositionService, useValue: signaturePositionService },
      ],
    }).compile();

    service = module.get(SignService);
  });

  it("should be defined", async () => {
    expect(service).toBeDefined();
  });

  describe("the sign method", () => {
    it("works", async () => {
      // given
      const docId = 1;
      const signatures: SignatureDto[] = [{ x: 20, y: 30, width: 100, height: 50, key: 1, page: 2 }];
      const user = { id: 10, login: "bob@gouv.fr", firstName: "bob", lastName: "kelso" } as User;
      (documentService.getOne as jest.Mock).mockResolvedValue({
        id: 1,
        name: "mondoc",
        extension: ".pdf",
        mimeType: "application/pdf",
        nbPages: 5,
        type: LetterFileDocumentType.DEFAULT,
        storageKey: "50/40/doc.pdf",
        folder: { id: 40, letterFile: { id: 50 } },
        signaturePositions: [],
      });
      (userPrefService.findOne as jest.Mock).mockResolvedValue({ signatures: ["key0", "key1"] });
      const docVersionMockRepo = new MockRepository<DocumentVersion>();
      docVersionMockRepo.count.mockResolvedValue(14);
      (documentVersionService.repo as any) = docVersionMockRepo;
      const getNowMock = jest.fn().mockReturnValue("2023-09-11 12:54:21.536324");
      set(service, "getNow", getNowMock);
      parapheurConfig.allowedConversionFormats = ["doc"];

      // when
      const result = await service.sign(docId, signatures, user);

      // then
      expect(result).toBeUndefined();
      expect(documentService.getOne).toHaveBeenCalledWith(
        { join: ["folder", "folder.letterFile", "signaturePositions"] },
        docId
      );
      expect(userPrefService.findOne).toHaveBeenCalledWith({ where: { login: "bob@gouv.fr" } });
      expect(docVersionMockRepo.count).toHaveBeenCalledWith({
        where: { document: { id: 1 } },
        relations: ["document"],
      });
      expect(storageService.copyObject).toHaveBeenCalledWith("versions/50/1/v15.pdf", "50/40/doc.pdf");
      expect(documentVersionService.createOne).toHaveBeenCalledWith(
        {},
        { document: { id: 1 }, author: "bob kelso", extension: ".pdf", storageKey: "versions/50/1/v15.pdf" }
      );
      expect(signApi.sign).toHaveBeenCalledWith(
        "50/40/doc.pdf",
        [{ x: 20, y: 30, width: 100, height: 50, key: "key1", page: 2 }],
        "50/40/doc.pdf"
      );
      expect(documentRepo.save).toHaveBeenCalledWith({
        id: 1,
        signed: true,
        extension: ".pdf",
        mimeType: "application/pdf",
        storageKey: "50/40/doc.pdf",
        modificationDate: "2023-09-11 12:54:21.536324",
        signaturePositionsCount: 0,
      });
      expect(storageService.removeObjectsInDir).toHaveBeenCalledWith("pages/50/40/1/");
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: 50 },
        type: HistoryOperationType.UPDATE,
        target: HistoryTarget.DOC,
        label: "signé: mondoc.pdf",
      });
    });
  });

  describe("the getPage method", () => {
    it("returns page link from storage", async () => {
      // given
      const docId = 1;
      const page = 10;
      (documentService.getOne as jest.Mock).mockResolvedValue({ id: 1, folder: { id: 20, letterFile: { id: 30 } } });
      (storageService.getLink as jest.Mock).mockResolvedValue("thelink");

      // when
      const result = await service.getPage(docId, page);

      // then
      expect(result).toEqual("thelink");
      expect(documentService.getOne).toHaveBeenCalledWith({ join: ["folder", "folder.letterFile"] }, docId);
      expect(storageService.statObject).toHaveBeenCalledWith("pages/30/20/1/10.png");
      expect(storageService.getLink).toHaveBeenCalledWith("pages/30/20/1/10.png");
    });

    it("generates a preview and returns the link", async () => {
      // given
      const docId = 1;
      const page = 10;
      (documentService.getOne as jest.Mock).mockResolvedValue({
        id: 1,
        storageKey: "thekey",
        extension: ".pdf",
        folder: { id: 20, letterFile: { id: 30 } },
      });
      (storageService.statObject as jest.Mock).mockRejectedValue(new Error("boum"));
      (signApi.generatePreview as jest.Mock).mockResolvedValue({ urls: ["theurl"] });
      parapheurConfig.allowedConversionFormats = ["doc"];

      // when
      const result = await service.getPage(docId, page);

      // then
      expect(result).toEqual("theurl");
      expect(documentService.getOne).toHaveBeenCalledWith({ join: ["folder", "folder.letterFile"] }, docId);
      expect(storageService.statObject).toHaveBeenCalledWith("pages/30/20/1/10.png");
      expect(storageService.getLink).not.toHaveBeenCalled();
      expect(signApi.generatePreview).toHaveBeenCalledWith("thekey", "pages/30/20/1/", 10);
    });
  });
});
