import { AbstractPastaCrudService } from "@pasta/back-core";
import { Suggestion } from "../entities/Suggestion.entity";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";

export class SuggestionService extends AbstractPastaCrudService<Suggestion> {
  constructor(@InjectRepository(Suggestion) public readonly repo: Repository<Suggestion>) {
    super();
  }
}
