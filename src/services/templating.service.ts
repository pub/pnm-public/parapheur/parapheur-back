import Mustache from "mustache";
import mjml from "mjml";
import fs from "fs/promises";

export interface LetterFileTemplateData {
  from: string;
  title: string;
  date: string;
  link: string;
  isLast: boolean;
}

export interface TemplateData {
  atYourLevel?: Array<LetterFileTemplateData>;
  addedToCc?: Array<LetterFileTemplateData>;
  recalled?: Array<LetterFileTemplateData>;
}

export class TemplatingService {
  private template: string;

  private async getTemplate() {
    if (!this.template) {
      this.template = (await fs.readFile("./templates/notification.template.mjml")).toString();
    }
    return this.template;
  }

  async render(data: TemplateData): Promise<string> {
    return mjml(Mustache.render(await this.getTemplate(), data)).html;
  }
}
