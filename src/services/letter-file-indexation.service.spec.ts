import { Test, TestingModule } from "@nestjs/testing";
import { LetterFileIndexationService } from "./letter-file-indexation.service";
import { ElasticsearchService } from "../elasticsearch/elasticsearch.service";
import { mockObject } from "@pasta/back-core";
import { LetterFile } from "../entities/letter-file.entity";
import dayjs from "dayjs";
import { LetterFileTag } from "../entities/letter-file-tag.entity";
import { searchAliasName, searchIndexName } from "../utils";
import { UpstreamFluxElement } from "../entities/upstream-flux-element.entity";
import { DownstreamFluxElement } from "../entities/downstream-flux-element.entity";
import { LetterFileCCUser } from "../entities/letter-file-cc-user.entity";
import { LetterFileComment } from "../entities/letter-file-comment.entity";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import { set } from "lodash";
import { LetterFileDocument } from "../entities/letter-file-document.entity";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";

describe("LetterFileIndexationService", () => {
  let service: LetterFileIndexationService;
  let esService: ElasticsearchService;

  beforeEach(async () => {
    esService = mockObject(ElasticsearchService);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: LetterFileIndexationService, useClass: LetterFileIndexationService },
        { provide: ElasticsearchService, useValue: esService },
      ],
    }).compile();

    service = module.get(LetterFileIndexationService);
  });

  it("should be defined", async () => {
    expect(service).toBeDefined();
  });

  describe("the indexFull method", () => {
    it("indexes draft with base infos", async () => {
      // given
      const creationDate = dayjs("2023-09-11 12:46:55.197328").toDate();
      const modificationDate = dayjs("2023-09-11 12:54:21.536324").toDate();
      const arrivalDate = dayjs("2023-09-11 12:46:55.197328").toDate();
      const deadline = dayjs("2023-09-11 12:54:21.224076").toDate();
      const lf: LetterFile = {
        id: 1,
        creationDate,
        modificationDate,
        name: "Mon parapheur",
        chronoNumber: "123",
        deadline,
        arrivalDate,
        externalMailNumber: "9876543210",
        draft: true,
        archived: false,
        tags: [] as LetterFileTag[],
        upstream: [] as UpstreamFluxElement[],
        downstream: [
          {
            id: 11,
            mail: "elise@gouv.fr",
            name: "Elise Humbert",
            position: 0,
            creationDate,
            modificationDate,
          },
        ] as DownstreamFluxElement[],
        ccUsers: [] as LetterFileCCUser[],
        comments: [] as LetterFileComment[],
        folders: [] as LetterFileFolder[],
        senderOrganization: "orga",
        senderName: "sender name",
        senderRole: "sender role",
        mailType: "type",
        recipientName: "recipient",
        manager: "manager",
        history: [],
      };
      const computeTouchedDatesMock = jest.fn().mockReturnValue(["thedate"]);
      set(service, "computeTouchedDates", computeTouchedDatesMock);

      // when
      const result = await service.indexFull(lf);

      // then
      expect(result).toBeUndefined();
      expect(esService.index).toHaveBeenCalledWith({
        id: "1",
        index: searchAliasName,
        body: {
          archived: false,
          arrivalDate,
          ccUsers: [],
          chronoNumber: "123",
          comments: [],
          creationDate,
          creator: { mail: "elise@gouv.fr", name: "Elise Humbert" },
          current: { mail: "elise@gouv.fr", name: "Elise Humbert" },
          deadline,
          documents: [],
          downstream: [],
          draft: true,
          externalMailNumber: "9876543210",
          folders: [],
          id: 1,
          lastIndexedAt: modificationDate,
          mailType: "type",
          manager: "manager",
          name: "Mon parapheur",
          recipientName: "recipient",
          relatedUsers: [{ mail: "elise@gouv.fr" }],
          senderName: "sender name",
          senderOrganization: "orga",
          senderRole: "sender role",
          tags: [],
          touchedDates: ["thedate"],
          upstream: [],
        },
      });
      expect(computeTouchedDatesMock).toHaveBeenCalledWith(lf);
    });

    it("indexes archived with relations", async () => {
      // given
      const creationDate = dayjs("2023-09-11 12:46:55.197328").toDate();
      const modificationDate = dayjs("2023-09-11 12:54:21.536324").toDate();
      const lf: LetterFile = {
        id: 1,
        creationDate,
        modificationDate,
        name: "Mon parapheur",
        chronoNumber: "123",
        draft: false,
        archived: true,
        tags: [{ id: 1, name: "le tag" }] as LetterFileTag[],
        upstream: [
          {
            id: 10,
            mail: "bob@gouv.fr",
            name: "Robert Palourde",
            position: 0,
            creationDate,
            modificationDate,
          },
        ] as UpstreamFluxElement[],
        downstream: [
          {
            id: 11,
            mail: "elise@gouv.fr",
            name: "Elise Humbert",
            position: 0,
            creationDate,
            modificationDate,
          },
          {
            id: 21,
            mail: "charlotte@gouv.fr",
            name: "Charlotte Dupont",
            position: 1,
            creationDate,
            modificationDate,
          },
        ] as DownstreamFluxElement[],
        ccUsers: [
          {
            id: 12,
            mail: "merlin@gouv.fr",
            name: "Merlin Broche",
            creationDate,
            modificationDate,
          },
        ] as LetterFileCCUser[],
        comments: [
          {
            id: 20,
            text: "the comment",
            creationDate,
            modificationDate,
          },
        ] as LetterFileComment[],
        folders: [
          {
            id: 30,
            name: "$$ROOT$$",
            creationDate,
            modificationDate,
            documents: [
              {
                type: LetterFileDocumentType.DEFAULT,
                id: 40,
                name: "mydoc",
                extension: ".pdf",
                creationDate,
                modificationDate,
              },
            ],
          },
        ] as LetterFileFolder[],
        history: [],
      };
      const computeTouchedDatesMock = jest.fn().mockReturnValue(["thedate"]);
      set(service, "computeTouchedDates", computeTouchedDatesMock);

      // when
      const result = await service.indexFull(lf);

      // then
      expect(result).toBeUndefined();
      expect(esService.index).toHaveBeenCalledWith({
        id: "1",
        index: searchAliasName,
        body: {
          archived: true,
          archivedDate: modificationDate,
          arrivalDate: undefined,
          ccUsers: [{ mail: "merlin@gouv.fr", name: "Merlin Broche" }],
          chronoNumber: "123",
          comments: ["the comment"],
          creationDate,
          creator: { mail: "bob@gouv.fr", name: "Robert Palourde" },
          current: { mail: "elise@gouv.fr", name: "Elise Humbert" },
          deadline: undefined,
          documents: [{ name: "mydoc.pdf" }],
          downstream: [
            {
              mail: "charlotte@gouv.fr",
              name: "Charlotte Dupont",
            },
          ],
          draft: false,
          externalMailNumber: undefined,
          folders: [],
          id: 1,
          lastIndexedAt: modificationDate,
          mailType: undefined,
          manager: undefined,
          recipientName: undefined,
          name: "Mon parapheur",
          relatedUsers: [
            { mail: "bob@gouv.fr" },
            { mail: "elise@gouv.fr" },
            { mail: "charlotte@gouv.fr" },
            { mail: "merlin@gouv.fr" },
          ],
          senderName: undefined,
          senderOrganization: undefined,
          senderRole: undefined,
          tags: ["le tag"],
          touchedDates: ["thedate"],
          upstream: [
            {
              mail: "bob@gouv.fr",
              name: "Robert Palourde",
            },
          ],
        },
      });
      expect(computeTouchedDatesMock).toHaveBeenCalledWith(lf);
    });
  });

  describe("the search method", () => {
    it("does a search with OR condition", async () => {
      // given
      const searchParams = { userEmails: ["kevin@gouv.fr"], highlight: [], orConditions: [{ some: "condition" }] };
      const pagination = { from: 0, size: 25 };
      const searchResult = { body: { hits: { total: { value: 23 }, hits: [{ _id: "29", highlight: "toto" }] } } };
      (esService.search as jest.Mock).mockResolvedValue(searchResult);

      // when
      const result = await service.search(searchParams, pagination);

      // then
      expect(result).toEqual({
        ids: [29],
        items: [
          {
            highlight: "toto",
            id: 29,
          },
        ],
        total: 23,
      });
      expect(esService.search).toHaveBeenCalledWith(
        {
          highlight: {
            fields: [],
            post_tags: "}$",
            pre_tags: "${",
          },
          query: {
            bool: {
              filter: [{ terms: { "relatedUsers.mail": ["kevin@gouv.fr"] } }],
              must: [
                {
                  bool: {
                    minimum_should_match: 1,
                    should: [{ some: "condition" }],
                  },
                },
              ],
              mustNot: [
                {
                  bool: {
                    must: [{ term: { draft: true } }],
                    mustNot: [{ terms: { "current.mail": ["kevin@gouv.fr"] } }],
                  },
                },
              ],
            },
          },
          sort: ["_score", { lastIndexedAt: "desc" }],
        },
        searchAliasName,
        { from: 0, size: 25 }
      );
    });

    it("does a search with AND condition", async () => {
      // given
      const searchParams = {
        userEmails: ["kevin@gouv.fr"],
        highlight: [],
        andConditions: {
          searchPattern: "my search",
          filters: [{ some: "filter" }],
          mustNot: [{ some: "must not" }],
          should: [{ some: "should" }],
        },
      };
      const pagination = { from: 0, size: 25 };
      const searchResult = { body: { hits: { total: { value: 23 }, hits: [{ _id: "29", highlight: "toto" }] } } };
      (esService.search as jest.Mock).mockResolvedValue(searchResult);

      // when
      const result = await service.search(searchParams, pagination);

      // then
      expect(result).toEqual({
        ids: [29],
        items: [
          {
            highlight: "toto",
            id: 29,
          },
        ],
        total: 23,
      });
      expect(esService.search).toHaveBeenCalledWith(
        {
          highlight: {
            fields: [],
            post_tags: "}$",
            pre_tags: "${",
          },
          query: {
            bool: {
              filter: [{ terms: { "relatedUsers.mail": ["kevin@gouv.fr"] } }],
              must: [
                {
                  bool: {
                    filter: [{ some: "filter" }],
                    minimum_should_match: 1,
                    must: [{ query_string: { query: "*my search*" } }],
                    mustNot: [{ some: "must not" }],
                    should: [{ some: "should" }],
                  },
                },
              ],
              mustNot: [
                {
                  bool: {
                    must: [{ term: { draft: true } }],
                    mustNot: [{ terms: { "current.mail": ["kevin@gouv.fr"] } }],
                  },
                },
              ],
            },
          },
          sort: ["_score", { lastIndexedAt: "desc" }],
        },
        searchAliasName,
        { from: 0, size: 25 }
      );
    });

    it("does a search with AND condition, no should", async () => {
      // given
      const searchParams = {
        userEmails: ["kevin@gouv.fr"],
        highlight: [],
        andConditions: {
          searchPattern: "my search",
          filters: [{ some: "filter" }],
          mustNot: [{ some: "must not" }],
        },
      };
      const pagination = { from: 0, size: 25 };
      const searchResult = { body: { hits: { total: { value: 23 }, hits: [{ _id: "29", highlight: "toto" }] } } };
      (esService.search as jest.Mock).mockResolvedValue(searchResult);

      // when
      const result = await service.search(searchParams, pagination);

      // then
      expect(result).toEqual({
        ids: [29],
        items: [
          {
            highlight: "toto",
            id: 29,
          },
        ],
        total: 23,
      });
      expect(esService.search).toHaveBeenCalledWith(
        {
          highlight: {
            fields: [],
            post_tags: "}$",
            pre_tags: "${",
          },
          query: {
            bool: {
              filter: [{ terms: { "relatedUsers.mail": ["kevin@gouv.fr"] } }],
              must: [
                {
                  bool: {
                    filter: [{ some: "filter" }],
                    minimum_should_match: 0,
                    must: [{ query_string: { query: "*my search*" } }],
                    mustNot: [{ some: "must not" }],
                    should: [],
                  },
                },
              ],
              mustNot: [
                {
                  bool: {
                    must: [{ term: { draft: true } }],
                    mustNot: [{ terms: { "current.mail": ["kevin@gouv.fr"] } }],
                  },
                },
              ],
            },
          },
          sort: ["_score", { lastIndexedAt: "desc" }],
        },
        searchAliasName,
        { from: 0, size: 25 }
      );
    });

    it("does a search with highlight", async () => {
      // given
      const searchParams = { userEmails: ["kevin@gouv.fr"], highlight: ["tutu"] };
      const pagination = { from: 0, size: 25 };
      const searchResult = { body: { hits: { total: { value: 23 }, hits: [{ _id: "29", highlight: "toto" }] } } };
      (esService.search as jest.Mock).mockResolvedValue(searchResult);

      // when
      const result = await service.search(searchParams, pagination);

      // then
      expect(result).toEqual({
        ids: [29],
        items: [
          {
            highlight: "toto",
            id: 29,
          },
        ],
        total: 23,
      });
      expect(esService.search).toHaveBeenCalledWith(
        {
          highlight: {
            fields: [{ tutu: {} }],
            post_tags: "}$",
            pre_tags: "${",
          },
          query: {
            bool: {
              filter: [{ terms: { "relatedUsers.mail": ["kevin@gouv.fr"] } }],
              must: [],
              mustNot: [
                {
                  bool: {
                    must: [{ term: { draft: true } }],
                    mustNot: [{ terms: { "current.mail": ["kevin@gouv.fr"] } }],
                  },
                },
              ],
            },
          },
          sort: ["_score", { lastIndexedAt: "desc" }],
        },
        searchAliasName,
        { from: 0, size: 25 }
      );
    });
  });

  describe("the indexNewLetterFile method", () => {
    it("works", async () => {
      // given
      const creationDate = dayjs().toDate();
      const modificationDate = dayjs().toDate();
      const letterFile = {
        id: 1,
        creationDate,
        modificationDate,
        draft: true,
        chronoNumber: "123",
      };
      const user = { name: "Robert Palourde", mail: "bob@gouv.fr" };
      const removeTimeMock = jest.fn().mockReturnValue("2023-09-11 13:54:51.685504");
      set(service, "removeTime", removeTimeMock);

      // when
      const result = await service.indexNewLetterFile(letterFile, user);

      // then
      expect(result).toBeUndefined();
      expect(esService.index).toHaveBeenCalledWith({
        id: "1",
        index: searchAliasName,
        body: {
          archived: false,
          ccUsers: [],
          chronoNumber: "123",
          creationDate,
          creator: {
            mail: "bob@gouv.fr",
            name: "Robert Palourde",
          },
          current: {
            mail: "bob@gouv.fr",
            name: "Robert Palourde",
          },
          downstream: [],
          draft: true,
          id: 1,
          lastIndexedAt: modificationDate,
          relatedUsers: [
            {
              mail: "bob@gouv.fr",
            },
          ],
          touchedDates: ["2023-09-11 13:54:51.685504"],
          upstream: [],
        },
      });
    });
  });

  describe("the patchBase method", () => {
    it("works", async () => {
      // given
      const id = 100;
      const modificationDate = dayjs().toDate();
      const deadline = dayjs().toDate();
      const arrivalDate = dayjs().toDate();
      const changes = {
        modificationDate,
        name: "bidule",
        chronoNumber: "456",
        deadline,
        arrivalDate,
        externalMailNumber: "toto",
        draft: false,
        senderOrganization: "orga",
        senderName: "sender name",
        senderRole: "sender role",
        mailType: "mail type",
        recipientName: "recipient name",
        manager: "manager",
      };
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.patchBase(id, changes);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "100",
        index: searchAliasName,
        body: {
          name: "bidule",
          chronoNumber: "456",
          deadline,
          arrivalDate,
          externalMailNumber: "toto",
          draft: false,
          senderOrganization: "orga",
          senderName: "sender name",
          senderRole: "sender role",
          mailType: "mail type",
          recipientName: "recipient name",
          manager: "manager",
          lastIndexedAt: modificationDate,
          touchedDates: ["date1", "date2"],
        },
      });
    });

    it("handles archived true", async () => {
      // given
      const id = 100;
      const modificationDate = dayjs().toDate();
      const changes = {
        modificationDate,
        archived: true,
      };
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.patchBase(id, changes);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "100",
        index: searchAliasName,
        body: {
          archived: true,
          archivedDate: modificationDate,
          lastIndexedAt: modificationDate,
          touchedDates: ["date1", "date2"],
        },
      });
    });
  });

  describe("the putTags method", () => {
    it("works", async () => {
      // given
      const id = 10;
      const tags = ["tag1", "tag2"];
      const modificationDate = dayjs().toDate();
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.putTags(id, tags, modificationDate);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "10",
        index: searchAliasName,
        body: { tags: ["tag1", "tag2"], lastIndexedAt: modificationDate, touchedDates: ["date1", "date2"] },
      });
    });
  });

  describe("the putFlux method", () => {
    it("works", async () => {
      // given
      const id = 10;
      const modificationDate = dayjs().toDate();
      const lf = {
        upstream: [{ mail: "bob@gouv.fr", name: "Robert Palourde", position: 0 }] as UpstreamFluxElement[],
        downstream: [
          { mail: "elise@gouv.fr", name: "Elise Jolivet", position: 0 },
          { mail: "nicolas@gouv.fr", name: "Nicolas Bruno", position: 1 },
        ] as DownstreamFluxElement[],
        modificationDate,
      };
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.putFlux(id, lf);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "10",
        index: searchAliasName,
        body: {
          current: { mail: "elise@gouv.fr", name: "Elise Jolivet" },
          upstream: [{ mail: "bob@gouv.fr", name: "Robert Palourde" }],
          downstream: [{ mail: "nicolas@gouv.fr", name: "Nicolas Bruno" }],
          lastIndexedAt: modificationDate,
          touchedDates: ["date1", "date2"],
        },
      });
    });
  });

  describe("the putUpstream method", () => {
    it("works", async () => {
      // given
      const id = 10;
      const modificationDate = dayjs().toDate();
      const upstream = [{ mail: "bob@gouv.fr", name: "Robert Palourde", position: 0 }] as UpstreamFluxElement[];
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.putUpstream(id, upstream, modificationDate);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "10",
        index: searchAliasName,
        body: {
          upstream: [{ mail: "bob@gouv.fr", name: "Robert Palourde" }],
          lastIndexedAt: modificationDate,
          touchedDates: ["date1", "date2"],
        },
      });
    });
  });

  describe("the putDownstream method", () => {
    it("works", async () => {
      // given
      const id = 10;
      const getNowMock = jest.fn().mockReturnValue("now");
      set(service, "getNow", getNowMock);
      const downstream = [
        { mail: "bob@gouv.fr", name: "Robert Palourde", position: 0 },
        { mail: "benjamin@gouv.fr", name: "Benjamin Lecourbe", position: 1 },
      ] as DownstreamFluxElement[];
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.putDownstream(id, downstream);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "10",
        index: searchAliasName,
        body: {
          current: { mail: "bob@gouv.fr", name: "Robert Palourde" },
          downstream: [{ mail: "benjamin@gouv.fr", name: "Benjamin Lecourbe" }],
          lastIndexedAt: "now",
          touchedDates: ["date1", "date2"],
        },
      });
    });
  });

  describe("the putCCUsers method", () => {
    it("works", async () => {
      // given
      const id = 10;
      const getNowMock = jest.fn().mockReturnValue("now");
      set(service, "getNow", getNowMock);
      const ccUsers = [
        { mail: "bob@gouv.fr", name: "Robert Palourde" },
        { mail: "benjamin@gouv.fr", name: "Benjamin Lecourbe" },
      ] as LetterFileCCUser[];
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.putCCUsers(id, ccUsers);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "10",
        index: searchAliasName,
        body: {
          ccUsers: [
            { mail: "bob@gouv.fr", name: "Robert Palourde" },
            { mail: "benjamin@gouv.fr", name: "Benjamin Lecourbe" },
          ],
          lastIndexedAt: "now",
          touchedDates: ["date1", "date2"],
        },
      });
    });
  });

  describe("the putRelatedUsers method", () => {
    it("works", async () => {
      // given
      const id = 10;
      const relatedUsers = [
        { mail: "bob@gouv.fr" },
        { mail: "benjamin@gouv.fr" },
        { mail: "bob@gouv.fr" },
        { mail: "benjamin@gouv.fr" },
      ] as { mail: string }[];

      // when
      const result = await service.putRelatedUsers(id, relatedUsers);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "10",
        index: searchAliasName,
        body: {
          relatedUsers: [{ mail: "bob@gouv.fr" }, { mail: "benjamin@gouv.fr" }],
        },
      });
    });
  });

  describe("the putComments method", () => {
    it("works", async () => {
      // given
      const id = 10;
      const getNowMock = jest.fn().mockReturnValue("now");
      set(service, "getNow", getNowMock);
      const comments = [{ text: "abc" }, { text: "def" }] as LetterFileComment[];
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.putComments(id, comments);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "10",
        index: searchAliasName,
        body: {
          comments: ["abc", "def"],
          lastIndexedAt: "now",
          touchedDates: ["date1", "date2"],
        },
      });
    });
  });

  describe("the putFolders method", () => {
    it("works", async () => {
      // given
      const id = 10;
      const getNowMock = jest.fn().mockReturnValue("now");
      set(service, "getNow", getNowMock);
      const folders = [{ name: "$$ROOT$$" }, { name: "mon dossier" }] as LetterFileFolder[];
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.putFolders(id, folders);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "10",
        index: searchAliasName,
        body: {
          folders: ["mon dossier"],
          lastIndexedAt: "now",
          touchedDates: ["date1", "date2"],
        },
      });
    });
  });

  describe("the putDocuments method", () => {
    it("works", async () => {
      // given
      const id = 10;
      const getNowMock = jest.fn().mockReturnValue("now");
      set(service, "getNow", getNowMock);
      const documents = [
        { type: LetterFileDocumentType.DEFAULT, name: "image", extension: ".jpg" },
        { type: LetterFileDocumentType.DEFAULT, name: "letter", extension: ".pdf" },
      ] as LetterFileDocument[];
      const getTouchedDatesMock = jest.fn().mockResolvedValue(["date1", "date2"]);
      set(service, "getTouchedDates", getTouchedDatesMock);

      // when
      const result = await service.putDocuments(id, documents);

      // then
      expect(result).toBeUndefined();
      expect(esService.updateDocument).toHaveBeenCalledWith({
        id: "10",
        index: searchAliasName,
        body: {
          documents: [{ name: "image.jpg" }, { name: "letter.pdf" }],
          lastIndexedAt: "now",
          touchedDates: ["date1", "date2"],
        },
      });
    });
  });

  describe("the remove method", () => {
    it("works", async () => {
      // given
      const id = 12;

      // when
      const result = await service.remove(id);

      // then
      expect(result).toBeUndefined();
      expect(esService.deleteDocument).toHaveBeenCalledWith("12", searchAliasName);
    });
  });

  describe("the getOne method", () => {
    it("works", async () => {
      // given
      const id = 27;
      const expected = { body: { _source: {} } };
      (esService.getDocument as jest.Mock).mockResolvedValue(expected);

      // when
      const result = await service.getOne(id);

      // then
      expect(result).toBe(expected.body._source);
      expect(esService.getDocument).toHaveBeenCalledWith("27", searchAliasName);
    });
  });
});
