import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AbstractPastaCrudService } from "@pasta/back-core";
import { Repository } from "typeorm";
import { LetterFileTag } from "../entities/letter-file-tag.entity";

@Injectable()
export class LetterFileTagService extends AbstractPastaCrudService<LetterFileTag> {
  constructor(@InjectRepository(LetterFileTag) public readonly repo: Repository<LetterFileTag>) {
    super();
  }
}
