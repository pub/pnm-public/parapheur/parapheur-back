import { LetterFileCCUserService } from "./letter-file-cc-user.service";
import { LdapService } from "./ldap.service";
import { LetterFileHistoryService } from "./letter-file-history.service";
import { LetterFileIndexationService } from "./letter-file-indexation.service";
import {
  IPastaCrudQuery,
  MockRepository,
  PastaCrudHookService,
  PastaCrudOperatorService,
  mockObject,
} from "@pasta/back-core";
import { Test, TestingModule } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { LetterFileCCUser } from "../entities/letter-file-cc-user.entity";
import { DeepPartial } from "typeorm";
import { get, set } from "lodash";
import { getAbstractClass } from "../tests/utils";
import { HistoryOperationType, HistoryTarget } from "../entities/letter-file-history.entity";
import { LetterFile } from "../entities/letter-file.entity";
import { LetterFileSavedFuxService } from "./saved-flux.service";

describe("LetterFileCCUserService", () => {
  let repo: MockRepository<LetterFileCCUser>;
  let lfRepo: MockRepository<LetterFileCCUser>;
  let service: LetterFileCCUserService;
  let indexationService: LetterFileIndexationService;
  let ldapService: LdapService;
  let historyService: LetterFileHistoryService;
  let hookService: PastaCrudHookService;
  let savedFluxService: LetterFileSavedFuxService;

  beforeEach(async () => {
    repo = new MockRepository<LetterFileCCUser>();
    lfRepo = new MockRepository<LetterFile>();
    indexationService = mockObject(LetterFileIndexationService);
    ldapService = mockObject(LdapService);
    historyService = mockObject(LetterFileHistoryService);
    hookService = mockObject(PastaCrudHookService);
    savedFluxService = mockObject(LetterFileSavedFuxService);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: LetterFileCCUserService, useClass: LetterFileCCUserService },
        { provide: getRepositoryToken(LetterFileCCUser), useValue: repo },
        { provide: getRepositoryToken(LetterFile), useValue: lfRepo },
        { provide: LetterFileIndexationService, useValue: indexationService },
        { provide: LdapService, useValue: ldapService },
        { provide: LetterFileHistoryService, useValue: historyService },
        { provide: PastaCrudOperatorService, useValue: jest.fn() },
        { provide: PastaCrudHookService, useValue: hookService },
        { provide: LetterFileSavedFuxService, useValue: savedFluxService },
      ],
    }).compile();

    service = module.get(LetterFileCCUserService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("the createOne method", () => {
    it("creates the entity", async () => {
      // given
      const query: IPastaCrudQuery = {};
      const body: DeepPartial<LetterFileCCUser> = {
        name: "abc",
        mail: "def@hig.com",
        author: { id: 1 },
        letterFile: 24 as any,
      };
      const ldapLookupResult = {
        mail: "jane.doe@gouv.fr",
        name: "Jane Doe",
        relatedMails: ["jane.doe@abc.com"],
      };
      (ldapService.lookup as jest.Mock).mockResolvedValue(ldapLookupResult);
      const createResult = { name: "some name" };
      const abstractPastaCrudServiceCreateOne = jest.fn();
      set(
        getAbstractClass(service),
        "createOne",
        abstractPastaCrudServiceCreateOne.mockResolvedValueOnce(createResult)
      );
      const updateCCUsersIndexMock = jest.fn();
      set(service, "updateCCUsersIndex", updateCCUsersIndexMock);
      const relatedUsers = ["toto@tutu.fr"];
      (indexationService.getOne as jest.Mock).mockResolvedValue({ relatedUsers });

      // when
      const entity = await service.createOne(query, body);

      // then
      expect(entity).toBe(createResult);
      expect(abstractPastaCrudServiceCreateOne).toHaveBeenCalledTimes(1);
      expect(abstractPastaCrudServiceCreateOne).toHaveBeenCalledWith(query, {
        ...ldapLookupResult,
        author: body.author,
        letterFile: body.letterFile,
      });
      expect(ldapService.lookup).toHaveBeenCalledWith(body.mail);
      expect(updateCCUsersIndexMock).toHaveBeenCalledTimes(1);
      expect(indexationService.getOne).toHaveBeenCalledWith(body.letterFile);
      expect(indexationService.putRelatedUsers).toHaveBeenCalledWith(body.letterFile, [...relatedUsers, createResult]);
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: body.letterFile },
        label: `Ajout en copie "${createResult.name}"`,
        target: HistoryTarget.CCUSER,
        type: HistoryOperationType.CREATE,
      });
    });

    describe("the deleteOne method", () => {
      it("deletes one", async () => {
        // given
        const query = {};
        const id = 1;
        const getOneResult = {
          id: 1,
          mail: "jane.doe@gouv.fr",
          name: "Jane Doe",
          frozen: false,
          letterFile: {
            id: 42,
            upstream: [{ mail: "up" }],
            downstream: [{ mail: "down" }],
            ccUsers: [{ mail: "jane.doe@gouv.fr" }, { mail: "yes@oui.com" }],
          },
          author: { id: 98 },
        };
        const getOneMock = jest.fn().mockResolvedValue(getOneResult);
        set(service, "getOne", getOneMock);
        const updateCCUsersIndexMock = jest.fn();
        set(service, "updateCCUsersIndex", updateCCUsersIndexMock);

        // when
        const result = await service.deleteOne(query, id);

        // then
        expect(result).toBeUndefined();
        expect(indexationService.putRelatedUsers).toHaveBeenCalledWith(getOneResult.letterFile.id, [
          { mail: "up" },
          { mail: "down" },
          { mail: "yes@oui.com" },
        ]);
        expect(historyService.registerEvent).toHaveBeenCalledWith({
          letterFile: { id: getOneResult.letterFile.id },
          label: `Suppression de la copie à "${getOneResult.name}"`,
          target: HistoryTarget.CCUSER,
          type: HistoryOperationType.DELETE,
        });
      });
    });

    describe("the freeze method", () => {
      it("freezes", async () => {
        // given
        const lfId = 32;
        const updateResponse = { id: 1, frozen: true };
        (repo.update as jest.Mock).mockResolvedValue(updateResponse);

        // when
        const result = await service.freeze(lfId);

        // then
        expect(result).toBe(updateResponse);
        expect(repo.update).toHaveBeenCalledWith({ letterFile: { id: lfId } }, { frozen: true });
      });
    });

    describe("the updateCCUsersIndex method", () => {
      it("works", async () => {
        // given
        const lfId = 12;
        const mockGetMany = jest.fn().mockResolvedValue({ data: [{ id: 1 }] });
        set(service, "getMany", mockGetMany);

        // when
        await (get(service, "updateCCUsersIndex") as Function).call(service, lfId);

        // then
        expect(mockGetMany).toHaveBeenCalledWith({ join: ["letterFile"], filter: { "letterFile.id": { $eq: lfId } } });
        expect(indexationService.putCCUsers).toHaveBeenCalledWith(lfId, [{ id: 1 }]);
      });
    });
  });
});
