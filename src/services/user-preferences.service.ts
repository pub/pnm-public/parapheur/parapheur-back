import { Injectable } from "@nestjs/common";
import { AbstractPastaCrudService, IPastaCrudQuery } from "@pasta/back-core";
import { UserPreferences } from "../entities/user-preferences.entity";
import { DeepPartial, Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { StorageService } from "@pasta/back-files";
import { EnhanceSignatureDto } from "../dtos/enhance-signature.dto";
import { md5 } from "../utils";

@Injectable()
export class UserPreferencesService extends AbstractPastaCrudService<UserPreferences> {
  private readonly storagePrefix: string = "signatures";
  constructor(
    @InjectRepository(UserPreferences) public readonly repo: Repository<UserPreferences>,
    private readonly storageService: StorageService
  ) {
    super();
  }

  async updateOne(query: IPastaCrudQuery, id: number, body: DeepPartial<UserPreferences>): Promise<UserPreferences> {
    if (body.signatures) {
      body.signatures = await Promise.all(
        body.signatures.map(async sign => {
          if (sign.split("/").at(0) === this.storagePrefix) {
            return sign;
          }
          const newKey = `${this.storagePrefix}/${id}/${sign}`;
          await this.storageService.moveObject(newKey, sign);
          return newKey;
        })
      );
    }
    return super.updateOne(query, id, body);
  }

  getEnhancedSignKey(userPreferences: UserPreferences, options: EnhanceSignatureDto): string {
    // we have to garantee that hash will be the same no matter the order of entries in options
    const reducedOptions = Object.keys(options)
      .sort()
      .reduce((acc, curr) => {
        return `${acc}/${curr}:${options[curr]}`;
      }, "");
    const hash = md5(reducedOptions);
    const extension = userPreferences.signatures.at(options.signatureIndex).split(".").at(-1);
    const result = `${this.storagePrefix}/enhanced/${userPreferences.id}/${hash}.${extension}`;
    return result;
  }
}
