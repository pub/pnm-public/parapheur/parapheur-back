import { LetterFileHistoryService } from "./letter-file-history.service";
import { LetterFileIndexationService } from "./letter-file-indexation.service";
import { MockRepository, PastaCrudHookService, PastaCrudOperatorService, mockObject } from "@pasta/back-core";
import { Test, TestingModule } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { LetterFileFolderService } from "./letter-file-folder.service";
import { LetterFileFolder } from "../entities/letter-file-folder.entity";
import { StorageService } from "@pasta/back-files";
import { ArchiveService } from "./archive.service";
import { LetterFileDocumentService } from "./letter-file-document.service";
import { get, set } from "lodash";
import { getAbstractClass } from "../tests/utils";
import { HistoryOperationType, HistoryTarget } from "../entities/letter-file-history.entity";
import { LetterFileError, RootFolderError } from "../errors/letter-file.error";
import { NotFoundInDbError } from "../errors/not-found.error";
import { LetterFileDocumentType } from "../types/letter-file-document-type.enum";

describe("LetterFileFolderService", () => {
  let service: LetterFileFolderService;
  let repo: MockRepository<LetterFileFolder>;
  let storageService: StorageService;
  let documentsService: LetterFileDocumentService;
  let archiveService: ArchiveService;
  let indexationService: LetterFileIndexationService;
  let historyService: LetterFileHistoryService;
  let hookService: PastaCrudHookService;

  beforeEach(async () => {
    repo = new MockRepository<LetterFileFolder>();
    storageService = mockObject(StorageService);
    documentsService = mockObject(LetterFileDocumentService);
    archiveService = mockObject(ArchiveService);
    indexationService = mockObject(LetterFileIndexationService);
    historyService = mockObject(LetterFileHistoryService);
    hookService = mockObject(PastaCrudHookService);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: LetterFileFolderService, useClass: LetterFileFolderService },
        { provide: getRepositoryToken(LetterFileFolder), useValue: repo },
        { provide: StorageService, useValue: storageService },
        { provide: LetterFileDocumentService, useValue: documentsService },
        { provide: ArchiveService, useValue: archiveService },
        { provide: LetterFileIndexationService, useValue: indexationService },
        { provide: LetterFileHistoryService, useValue: historyService },
        { provide: PastaCrudOperatorService, useValue: jest.fn() },
        { provide: PastaCrudHookService, useValue: hookService },
      ],
    }).compile();

    service = module.get(LetterFileFolderService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("the getNextPosition method", () => {
    it("returns 1 when max = 0", async () => {
      // given
      const lfId = 1;
      (repo.queryBuilder.getRawOne as jest.Mock).mockResolvedValue({ maxPosition: 0 });

      // when
      const position = await service.getNextPosition(lfId);

      // then
      expect(position).toEqual(1);
    });

    it("returns 20 when max = 19", async () => {
      // given
      const lfId = 1;
      (repo.queryBuilder.getRawOne as jest.Mock).mockResolvedValue({ maxPosition: 19 });

      // when
      const position = await service.getNextPosition(lfId);

      // then
      expect(position).toEqual(20);
    });
  });

  describe("the createOne method", () => {
    it("creates one", async () => {
      // given
      const query = {};
      const body = { letterFile: { id: 1 } };
      const createResult = { name: "some name" };
      const abstractPastaCrudServiceCreateOne = jest.fn();
      const position = 1;
      const getNextPositionMock = jest.fn().mockResolvedValue(position);
      set(service, "getNextPosition", getNextPositionMock);
      set(
        getAbstractClass(service),
        "createOne",
        abstractPastaCrudServiceCreateOne.mockResolvedValueOnce(createResult)
      );
      const updateIndexMock = jest.fn();
      set(service, "updateIndex", updateIndexMock);

      // when
      const result = await service.createOne(query, body);

      // then
      expect(result).toEqual(createResult);
      expect(getNextPositionMock).toHaveBeenCalledWith(body.letterFile.id);
      expect(updateIndexMock).toHaveBeenCalledWith(body.letterFile.id);
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: body.letterFile.id },
        label: "Ajout de dossier",
        target: HistoryTarget.FOLDER,
        type: HistoryOperationType.CREATE,
      });
    });

    it("skips indexation", async () => {
      // given
      const query = {};
      const body = { letterFile: { id: 1 } };
      const createResult = { name: "some name" };
      const abstractPastaCrudServiceCreateOne = jest.fn().mockResolvedValueOnce(createResult);
      set(getAbstractClass(service), "createOne", abstractPastaCrudServiceCreateOne);
      const position = 1;
      const getNextPositionMock = jest.fn().mockResolvedValue(position);
      set(service, "getNextPosition", getNextPositionMock);
      const updateIndexMock = jest.fn();
      set(service, "updateIndex", updateIndexMock);

      // when
      const result = await service.createOne(query, body, true);

      // then
      expect(result).toEqual(createResult);
      expect(getNextPositionMock).toHaveBeenCalledWith(body.letterFile.id);
      expect(updateIndexMock).not.toHaveBeenCalled();
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: body.letterFile.id },
        label: "Ajout de dossier",
        target: HistoryTarget.FOLDER,
        type: HistoryOperationType.CREATE,
      });
    });
  });

  describe("the deleteOne method", () => {
    it("deletes one", async () => {
      // given
      const query = {};
      const folderId = 1;
      const dbFolder = { id: 1, position: 3, name: "some name", letterFile: { id: 2 } };
      repo.findOne.mockResolvedValue(dbFolder);
      const abstractPastaCrudServiceDeleteOne = jest.fn().mockResolvedValue(undefined);
      set(getAbstractClass(service), "deleteOne", abstractPastaCrudServiceDeleteOne);
      const updateIndexMock = jest.fn();
      set(service, "updateIndex", updateIndexMock);
      const getFolderNamePrettyMock = jest.fn().mockReturnValue("toto");
      set(service, "getFolderNamePretty", getFolderNamePrettyMock);

      // when
      const result = await service.deleteOne(query, folderId);

      // then
      expect(result).toBeUndefined();
      expect(repo.findOne).toHaveBeenCalledWith({ where: { id: folderId }, relations: ["letterFile"] });
      expect(abstractPastaCrudServiceDeleteOne).toHaveBeenCalledWith(query, folderId);
      expect(repo.createQueryBuilder).toHaveBeenCalled();
      expect(storageService.removeObjectsInDir).toHaveBeenCalledWith("2/1/");
      expect(updateIndexMock).toHaveBeenCalledWith(2);
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: 2 },
        label: 'Suppression du dossier "toto"',
        target: HistoryTarget.FOLDER,
        type: HistoryOperationType.DELETE,
      });
    });

    it("throw upon deleting root folder", async () => {
      // given
      const query = {};
      const folderId = 1;
      const dbFolder = { id: 1, position: 3, name: "$$ROOT$$", letterFile: { id: 2 } };
      repo.findOne.mockResolvedValue(dbFolder);
      const abstractPastaCrudServiceDeleteOne = jest.fn().mockResolvedValue(undefined);
      set(getAbstractClass(service), "deleteOne", abstractPastaCrudServiceDeleteOne);

      // when
      const promise = service.deleteOne(query, folderId);

      // then
      await expect(promise).rejects.toEqual(new RootFolderError("Cannot delete root folder"));
      expect(abstractPastaCrudServiceDeleteOne).not.toHaveBeenCalled();
    });
  });

  describe("the updateOne method", () => {
    it("updates one", async () => {
      // given
      const query = { a: "b", join: ["letterFile"] };
      const folderId = 1;
      const body = { name: "new" };
      const getOneResult = { id: 1, name: "old" };
      const abstractPastaCrudServiceGetOne = jest.fn().mockResolvedValue(getOneResult);
      set(getAbstractClass(service), "getOne", abstractPastaCrudServiceGetOne);
      const updateResult = { name: "new", id: 1, letterFile: { id: 2 } };
      const abstractPastaCrudServiceUpdateOne = jest.fn().mockResolvedValue(updateResult);
      set(getAbstractClass(service), "updateOne", abstractPastaCrudServiceUpdateOne);
      const updateIndexMock = jest.fn();
      set(service, "updateIndex", updateIndexMock);
      const getFolderNamePrettyMock = jest.fn().mockReturnValueOnce("old pretty").mockReturnValueOnce("new pretty");
      set(service, "getFolderNamePretty", getFolderNamePrettyMock);

      // when
      const response = await service.updateOne(query, folderId, body);

      // then
      expect(response).toEqual(updateResult);
      expect(abstractPastaCrudServiceGetOne).toHaveBeenCalledWith({}, folderId);
      expect(abstractPastaCrudServiceUpdateOne).toHaveBeenCalledWith(
        { a: "b", join: ["letterFile"] },
        folderId,
        body
      );
      expect(updateIndexMock).toHaveBeenCalledWith(2);
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: 2 },
        label: 'Renommage du dossier de "old pretty" vers "new pretty"',
        target: HistoryTarget.FOLDER,
        type: HistoryOperationType.UPDATE,
      });
    });
  });

  describe("the reorder method", () => {
    it("reorders folders", async () => {
      // given
      const folders = [
        { id: 1, position: 0 },
        { id: 2, position: 1 },
        { id: 3, position: 2 },
      ] as LetterFileFolder[];
      const saveResult = [
        { id: 3, position: 0 },
        { id: 1, position: 1 },
        { id: 2, position: 2 },
      ];
      repo.save.mockResolvedValue(saveResult);

      // when
      const result = await service.reorder(folders, [3, 1, 2]);

      // then
      expect(result).toEqual(saveResult);
      expect(repo.save).toHaveBeenCalledWith([
        { id: 3, position: 0 },
        { id: 1, position: 1 },
        { id: 2, position: 2 },
      ]);
    });

    it("throws upon arrays length mismatch", async () => {
      // given
      const folders = [
        { id: 1, position: 0 },
        { id: 2, position: 1 },
      ] as LetterFileFolder[];
      const saveResult = [
        { id: 3, position: 0 },
        { id: 1, position: 1 },
        { id: 2, position: 2 },
      ];
      repo.save.mockResolvedValue(saveResult);

      // when
      const promise = service.reorder(folders, [3, 1, 2]);

      // then
      await expect(promise).rejects.toEqual(new LetterFileError("Folders and sorted ids length mismatch"));
      expect(repo.save).not.toHaveBeenCalled();
    });

    it("throws upon folder not found", async () => {
      // given
      const folders = [
        { id: 1, position: 0 },
        { id: 2, position: 1 },
        { id: 4, position: 2 },
      ] as LetterFileFolder[];
      const saveResult = [
        { id: 3, position: 0 },
        { id: 1, position: 1 },
        { id: 2, position: 2 },
      ];
      repo.save.mockResolvedValue(saveResult);

      // when
      const promise = service.reorder(folders, [3, 1, 2]);

      // then
      await expect(promise).rejects.toEqual(new LetterFileError("Folder with id 3 not found"));
      expect(repo.save).not.toHaveBeenCalled();
    });
  });

  describe("the reorderDocuments method", () => {
    it("reorders documents", async () => {
      // given
      const query = {};
      const folderId = 1;
      const body = { documents: [3, 1, 2] };
      const documents = [
        { id: 1, position: 0 },
        { id: 2, position: 1 },
        { id: 3, position: 2 },
      ];
      const getOneResult1st = { id: 1, name: "thename", documents };
      const getOneResult2nd = {
        id: 1,
        name: "thename",
        documents: [
          { id: 3, position: 0 },
          { id: 1, position: 1 },
          { id: 2, position: 2 },
        ],
      };
      const abstractPastaCrudServiceGetOne = jest
        .fn()
        .mockResolvedValueOnce(getOneResult1st)
        .mockResolvedValueOnce(getOneResult2nd);
      set(getAbstractClass(service), "getOne", abstractPastaCrudServiceGetOne);

      // when
      const result = await service.reorderDocuments(query, folderId, body);

      // then
      expect(result).toEqual(getOneResult2nd);
      expect(documentsService.reorder).toHaveBeenCalledWith(documents, body.documents);
    });

    it("throws when folder not found", async () => {
      // given
      const query = {};
      const folderId = 1;
      const body = { documents: [3, 1, 2] };
      const abstractPastaCrudServiceGetOne = jest.fn().mockResolvedValueOnce(undefined);
      set(getAbstractClass(service), "getOne", abstractPastaCrudServiceGetOne);

      // when
      const promise = service.reorderDocuments(query, folderId, body);

      // then
      expect(promise).rejects.toEqual(new NotFoundInDbError("Folder not found"));
      expect(documentsService.reorder).not.toHaveBeenCalled();
    });
  });

  describe("the generateArchive method", () => {
    it("generates an archive for non root folder", async () => {
      // given
      const folderId = 1;
      const documents = [
        { id: 1, position: 0, storageKey: "a", name: "doc1", type: LetterFileDocumentType.DEFAULT },
        { id: 2, position: 1, storageKey: "b", name: "doc2", type: LetterFileDocumentType.DEFAULT },
      ];
      const getOneResult = { id: 1, name: "thename", letterFile: { id: 2 }, documents };
      const abstractPastaCrudServiceGetOne = jest.fn().mockResolvedValue(getOneResult);
      set(getAbstractClass(service), "getOne", abstractPastaCrudServiceGetOne);
      const returnedKey = "thereturnedkey";
      (archiveService.generateArchiveForFolder as jest.Mock).mockResolvedValue(returnedKey);
      const getFolderNamePrettyMock = jest.fn().mockReturnValue("thename pretty");
      set(service, "getFolderNamePretty", getFolderNamePrettyMock);

      // when
      const result = await service.generateArchive(folderId);

      // then
      expect(result).toEqual(returnedKey);
      expect(abstractPastaCrudServiceGetOne).toHaveBeenCalledWith({ join: ["letterFile", "documents"] }, folderId);
      expect(archiveService.generateArchiveForFolder).toHaveBeenCalledWith(getOneResult);
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: 2 },
        label: 'Téléchargement du dossier "thename pretty"',
        target: HistoryTarget.FOLDER,
        type: HistoryOperationType.DOWNLOAD,
      });
    });

    it("generates an archive for root folder", async () => {
      // given
      const folderId = 1;
      const documents = [
        { id: 1, position: 0, storageKey: "a", name: "doc1", type: LetterFileDocumentType.DEFAULT },
        { id: 2, position: 1, storageKey: "b", name: "doc2", type: LetterFileDocumentType.DEFAULT },
      ];
      const getOneResult = {
        id: 1,
        name: "$$ROOT$$",
        letterFile: { id: 2, chronoNumber: "20230904000027" },
        documents,
      };
      const abstractPastaCrudServiceGetOne = jest.fn().mockResolvedValue(getOneResult);
      const expectedName = "Parapheur-20230904000027-Racine";
      set(getAbstractClass(service), "getOne", abstractPastaCrudServiceGetOne);
      const returnedKey = "thereturnedkey";
      (archiveService.generateArchiveForFolder as jest.Mock).mockResolvedValue(returnedKey);
      const expectedPrettyName = `${expectedName} pretty`;
      const getFolderNamePrettyMock = jest.fn().mockReturnValue(expectedPrettyName);
      set(service, "getFolderNamePretty", getFolderNamePrettyMock);

      // when
      const result = await service.generateArchive(folderId);

      // then
      expect(result).toEqual(returnedKey);
      expect(abstractPastaCrudServiceGetOne).toHaveBeenCalledWith({ join: ["letterFile", "documents"] }, folderId);
      expect(archiveService.generateArchiveForFolder).toHaveBeenCalledWith(getOneResult);
      expect(historyService.registerEvent).toHaveBeenCalledWith({
        letterFile: { id: 2 },
        label: `Téléchargement du dossier "${expectedPrettyName}"`,
        target: HistoryTarget.FOLDER,
        type: HistoryOperationType.DOWNLOAD,
      });
    });
  });

  describe("the updateIndex method", () => {
    it("updates the index", async () => {
      // given
      const lfId = 1;
      const siblings = [{ id: 2 }, { id: 18 }];
      repo.find.mockResolvedValue(siblings);

      // when
      await (get(service, "updateIndex") as Function).call(service, lfId);

      // then
      expect(repo.find).toHaveBeenCalledWith({ relations: ["letterFile"], where: { letterFile: { id: lfId } } });
      expect(indexationService.putFolders).toHaveBeenCalledWith(lfId, siblings);
    });
  });

  describe("the gerFolderNamePretty method", () => {
    it("handles root folder", async () => {
      // given

      // when
      const result = (get(service, "getFolderNamePretty") as Function).call(service, { name: "$$ROOT$$" });

      // then
      expect(result).toEqual("racine");
    });

    it("handles non root folder", async () => {
      // given

      // when
      const result = (get(service, "getFolderNamePretty") as Function).call(service, { name: "le dossier" });

      // then
      expect(result).toEqual("le dossier");
    });
  });
});
