FROM node:20.9.0

WORKDIR /opt/app/

RUN apt-get update && apt-get install -y vim && apt-get clean;

COPY .npmrc .
COPY package-lock.json .
COPY package.json .
COPY .pastarc.json .

RUN npm ci --production

COPY dist/ ./dist/
ADD templates templates

CMD ["node", "dist/main.js"]
